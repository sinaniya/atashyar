import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

GridLayout {

    property var properties: ({
                                  "shape_name": "Circle",
                                  "radius": parseFloat(radiusTextField.text)
                              })
    property var errors: ({})

    // js function
    function pushError(err) {
        errors = Object.assign({}, errors, err)
    }

    function rmItemError(errKey) {
        delete errors[errKey]
    }

    function isIncludeErr(errKey) {
        if (errors.hasOwnProperty(errKey))
            return true
        else
            return false
    }

    columns: 3
    rowSpacing: 16 * dp

    CustomLabel {
        text: "Radius "
        toolTip.text: "شعاع"
    }

    TextField {
        id: radiusTextField
        validator: DoubleValidator {
            bottom: 1
            top: 10
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای شعاع"))
                rmItemError("خطای شعاع")
            if (!acceptableInput && !isIncludeErr("خطای شعاع"))
                pushError({
                              "خطای شعاع": "فیلد شعاع نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["radius"] = parseFloat(
                               radiusTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }
}
