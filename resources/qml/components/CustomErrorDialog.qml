import QtQuick 2.0
import QtQuick.Controls 2.12

Dialog {
    property alias dialogContentBox: rect
    property alias dialogContentCol: col
    visible: false
    title: "Error"
    modal: true
    anchors.centerIn: parent
    standardButtons: Dialog.Ok

    contentItem: Rectangle {
        id: rect
        implicitWidth: 600
        implicitHeight: 300
        color: "#33334c"

        Column {
            id: col
            width: parent.width
            anchors.centerIn: parent
            spacing: (4 * dp)
        }
    }
}
