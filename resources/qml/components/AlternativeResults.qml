import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ScrollView {
    height: parent.height - 128*dp
    width: parent.width / 4
    anchors.fill: parent
    anchors.leftMargin: 32*dp
    anchors.topMargin: 48*dp
    anchors.bottomMargin: 96*dp
    anchors.rightMargin: 8*dp
    clip: true

    ScrollBar.vertical.policy: ScrollBar.AlwaysOn

    GridLayout {
        id: altResultGridLayout
        columns: 3
        anchors.fill: parent

        CustomLabel {
            text: "Cut"
        }

        CustomLabel {
            text: "Diameter (mm)"
        }

        CustomLabel {
            text: "Count"
        }

        CustomLabel {
            text: "Section 1 "
        }
        TextField {

        }

        TextField {

        }

        CustomLabel {
            text: "Section 2 "
        }
        TextField {

        }

        TextField {

        }

        CustomLabel {
            text: "Section 3 "
        }
        TextField {

        }

        TextField {

        }

        CustomLabel {
            text: "Section 4 "
        }
        TextField {

        }

        TextField {

        }

        Rectangle {
            Layout.fillWidth: true
            height: 2*dp
            color: "orange"
            Layout.columnSpan: 3
            Layout.margins: {
                top: 8*dp
                bottom: 8*dp
            }
        }

        GridLayout {
            Layout.columnSpan: 3
            Layout.fillWidth: true
            columns: 3
            rows: 7

            Item {
                Layout.fillWidth: true
            }

            CustomLabel {
                text: "Diameter (mm)"
            }

            CustomLabel {
                text: "Count"
            }

            CustomLabel {
                text: "Floor/Advance"
                Layout.columnSpan: 3
                Layout.fillWidth: true
            }

            CustomLabel {
                text: "Bottom"
            }

            TextField {

            }

            TextField {

            }

            CustomLabel {
                text: "Column"
            }

            TextField {

            }

            TextField {

            }

            CustomLabel {
                text: "Perimeter"
                Layout.columnSpan: 3
                Layout.fillWidth: true
            }

            CustomLabel {
                text: "Bottom"
            }

            TextField {

            }

            TextField {

            }

            CustomLabel {
                text: "Column"
            }

            TextField {

            }

            TextField {

            }

        }

        Rectangle {
            Layout.fillWidth: true
            height: 2*dp
            color: "orange"
            Layout.columnSpan: 3
            Layout.margins: {
                top: 8*dp
                bottom: 8*dp
            }
        }


        CustomLabel {
            text: "Number of holes "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["number_of_total_holes"]: "";
            font.pixelSize: 20*dp
        }
        Item {
            Layout.fillWidth: true
        }

        CustomLabel {
            text: "Total explosive emulsion"
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["total_explosive_emulsion"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            text: " Kg"
        }

        CustomLabel {
            text: "Total explosive anfo"
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["total_explosive_anfo"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            text: " Kg"
        }

        Label {
//            text: "Specific explosive emulsion"
            Text {
                width: 100*dp
                text: "Specific explosive emulsion"
                font.pixelSize: 20*dp
                color: "orange"
                wrapMode: Text.WordWrap
            }
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["specific_explosive_emulsion"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            textFormat: Text.RichText
            text: " Kg/m<sup>3</sup>"
        }

        CustomLabel {
            text: "Specific explosive anfo"
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["specific_explosive_anfo"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            textFormat: Text.RichText
            text: " Kg/m<sup>3</sup>"
        }

        CustomLabel {
            text: "Specific drilling "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["specific_drilling"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            textFormat: Text.RichText
            text: " m/m<sup>3</sup>"
        }


        Item {
            Layout.fillHeight: true
        }
    }
}
