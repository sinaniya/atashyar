import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

GridLayout {

    property var properties: ({
                                  "shape_name": "HorseShoe",
                                  "radius": parseFloat(radiusTextField.text),
                                  "height": parseFloat(heightTextField.text)
                              })
    property var errors: ({})

    // js function
    function pushError(err) {
        errors = Object.assign({}, errors, err)
    }

    function rmItemError(errKey) {
        delete errors[errKey]
    }

    function isIncludeErr(errKey) {
        if (errors.hasOwnProperty(errKey))
            return true
        else
            return false
    }

    anchors.fill: parent
    columns: 3
    rowSpacing: 16 * dp

    CustomLabel {
        text: "rasius "
        toolTip.text: "شعاع"
    }

    TextField {
        id: radiusTextField
        validator: DoubleValidator {
            bottom: 3.5
            top: 100
            notation: DoubleValidator.StandardNotation
            decimals: 3
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای شعاع"))
                rmItemError("خطای شعاع")
            if (!acceptableInput && !isIncludeErr("خطای شعاع"))
                pushError({
                              "خطای شعاع": "فیلد شعاع نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["radius"] = parseFloat(
                               radiusTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }

    CustomLabel {
        text: "Height "
        toolTip.text: "ارتفاع"
    }

    TextField {
        id: heightTextField
        validator: DoubleValidator {
            bottom: 0.5
            top: 100
            notation: DoubleValidator.StandardNotation
            decimals: 3
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای ارتفاع"))
                rmItemError("خطای ارتفاع")
            if (!acceptableInput && !isIncludeErr("خطای ارتفاع"))
                pushError({
                              "خطای ارتفاع": "فیلد ارتفاع نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["height"] = parseFloat(
                               heightTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }
}
