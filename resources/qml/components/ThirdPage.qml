import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtQuick.Window 2.12
import Qt.labs.platform 1.0

import Backend 1.0

Page {
    id: thPage
    property alias sideView: showDepthOfHole
    property alias showingProperties: showProperty
    property alias panBtn: pan
    property alias zoomBtn: zoom
    property alias resultFigureCanvas: resultFigureCanvas
    property var unCheckedButtons: unCheckedButtonsFunc
    property var explosiveType;
    property var result: secondPage.result

    function getResult() {
        var response = drawer.get_result()
        return response
    }

    function unCheckedButtonsFunc() {
        drawer.home()
        if (showProperty.checked) {
            drawer.hide_property()
            showProperty.checked = false
        }
        if (pan.checked) {
            // toggle pan off
            drawer.pan()
            pan.checked = false
        }
        if (zoom.checked) {
            // toggle pan off
            drawer.zoom()
            zoom.checked = false
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#40405f"

        Loader {
            id: resultLoader
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.right: resultFigureCanvasContainer.left
            source: (secondPage.methodName === "Konya" || secondPage.methodName === "Olafsson") && explosiveType === "Emulsion" ? "AlternativeResults.qml": "DefaultResults.qml"
//            source: "Test.qml"
        }

        Connections {
            target: drawer

            function onXys_changed() {
                let properties = {}
                let methodName = secondPage.methodName
                Object.assign(properties, firstPage.shapeInputItem.properties,
                              secondPage.methodLoader.item.inputObject)
                switch (methodName) {
                case "EBM":
                    result = drawer.ebm(thirdPage.resultFigureCanvas,
                                        properties)
                    break
                case "EBMAngular":
                    result = drawer.ebm_angular(thirdPage.resultFigureCanvas,
                                                properties)
                    break
                case "NTNU":
                    result = drawer.ntnu(thirdPage.resultFigureCanvas,
                                         properties)
                    break
                case "Konya":
                    result = drawer.konya(thirdPage.resultFigureCanvas,
                                          properties)
                    break
                case "KonyaAngular":
                    result = drawer.konya_angular(thirdPage.resultFigureCanvas,
                                                  properties)
                    break
                case "Olafsson":
                    result = drawer.olafsson(thirdPage.resultFigureCanvas,
                                             properties)
                    break
                case "OlafssonAngular":
                    result = drawer.olafsson_angular(
                                thirdPage.resultFigureCanvas, properties)
                    break
                case "Gustafsson":
                    result = drawer.gustafsson(thirdPage.resultFigureCanvas,
                                               properties)
                    break
                }
            }
        }

        Rectangle {
            id: resultFigureCanvasContainer
            height: parent.height - 128 * dp
            width: (parent.width * 3 / 4) - 128 * dp
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 32 * dp
            anchors.topMargin: 48 * dp
            anchors.bottomMargin: 96 * dp
            color: "transparent"

            FigureCanvas {
                id: resultFigureCanvas
                dpi_ratio: Screen.devicePixelRatio
                anchors.fill: parent
                anchors.margins: 16 * dp

                Rectangle {
                    ToolBar {
                        RowLayout {
                            Button {
                                text: qsTr("home")
                                Layout.leftMargin: 8 * dp
                                onClicked: {
                                    // drawer.home into below function
                                    unCheckedButtonsFunc()
                                }
                            }
                            Button {
                                id: pan
                                text: qsTr("pan")
                                checkable: true
                                onClicked: {
                                    if (zoom.checked) {
                                        zoom.checked = false
                                        drawer.zoom()
                                    }
                                    if (addHole.checked) {
                                        addHole.checked = false
                                        plot.removeAdder()
                                    }

                                    if (removeHole.checked) {
                                        removeHole.checked = false
                                    }
                                    if (showProperty.checked) {
                                        // toggle showProperty off
                                        showProperty.checked = false
                                        drawer.hide_property()
                                    }

                                    drawer.pan()
                                }
                            }

                            Button {
                                id: zoom
                                text: qsTr("zoom")
                                checkable: true
                                onClicked: {
                                    if (pan.checked) {
                                        // toggle pan off
                                        pan.checked = false
                                    }
                                    if (addHole.checked) {
                                        addHole.checked = false
                                        drawer.removeAdder()
                                    }

                                    if (removeHole.checked) {
                                        removeHole.checked = false
                                    }
                                    if (showProperty.checked) {
                                        // toggle showProperty off
                                        showProperty.checked = false
                                        drawer.hide_property()
                                    }

                                    drawer.zoom()
                                }
                            }

                            Button {
                                id: addHole
                                text: qsTr("Add")
                                checkable: true
                                onClicked: {
                                    if (pan.checked) {
                                        // toggle pan off
                                        pan.checked = false
                                    }
                                    if (zoom.checked) {
                                        zoom.checked = false
                                    }
                                    if (showProperty.checked) {
                                        // toggle showProperty off
                                        showProperty.checked = false
                                        drawer.hide_property()
                                    }

                                    if (addHole.checked) {
                                        drawer.addPoint()
                                    } else {
                                        drawer.removeAdder()
                                    }

                                    if (removeHole.checked) {
                                        removeHole.checked = false
                                    }
                                }
                            }

                            Button {
                                id: removeHole
                                text: qsTr("Remove")
                                checkable: true
                                onClicked: {
                                    if (pan.checked) {
                                        pan.checked = false
                                    }
                                    if (zoom.checked) {
                                        zoom.checked = false
                                    }
                                    if (showProperty.checked) {
                                        // toggle showProperty off
                                        showProperty.checked = false
                                        drawer.hide_property()
                                    }
                                    if (addHole.checked) {
                                        addHole.checked = false
                                        drawer.removeAdder()
                                    }
                                    if (removeHole.checked) {
                                        drawer.remove_button()
                                    } else {
                                        drawer.disable_remove_button()
                                    }
                                }
                            }

                            Button {
                                id: save
                                text: qsTr("save")
                                onClicked: drawer.export()
                                Layout.rightMargin: 8 * dp
                            }
                            Button {
                                id: showProperty
                                text: qsTr("show property")
                                checkable: true
                                onClicked: {
                                    if (zoom.checked) {
                                        zoom.checked = false
                                        drawer.zoom()
                                    }
                                    if (pan.checked) {
                                        pan.checked = false
                                        drawer.pan()
                                    }

                                    if (showProperty.checked)
                                        drawer.show_property()
                                    else
                                        drawer.hide_property()
                                }

                                Layout.rightMargin: 8 * dp
                            }
                            Button {
                                id: showDepthOfHole
                                text: qsTr("side view")
                                checkable: false
                                onClicked: drawer.set_view_to_side()
                                Layout.rightMargin: 8 * dp
                            }
                        }
                    }
                }
            }
            ToolBar {
                anchors.horizontalCenter: parent.horizontalCenter
                width: resultFigureCanvasContainer.width - 32 * dp
                anchors.bottom: parent.bottom
                TextInput {
                    id: location
                    anchors.right: parent.right
                    anchors.rightMargin: 8 * dp
                    readOnly: true
                    text: drawer !== null ? drawer.coordinates : " "
                    font.pixelSize: 16 * dp
                    color: "white"
                }
            }
        }

        ExportButton {
            id: exportButton
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 16 * dp
            MouseArea {
                cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    exportFileLocation.open()
                }
            }
        }

        BackButton {
            anchors.right: exportButton.left
            anchors.bottom: parent.bottom
            anchors.margins: 16 * dp
            MouseArea {
                cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    unCheckedButtonsFunc()
                    stackLayout.currentIndex--
                }
            }
        }

        FolderDialog {
            id: exportFileLocation
            folder: currentFolder
            currentFolder: StandardPaths.standardLocations(
                               StandardPaths.DocumentsLocation)[0]
            onAccepted: {
                drawer.export_excel(folder)
                close()
            }
            onRejected: close()
        }
    }
}
