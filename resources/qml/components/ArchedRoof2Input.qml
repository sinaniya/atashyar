import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

GridLayout {

    property var properties: ({
                                  "shape_name": "Arched Roof 2",
                                  "width": parseFloat(widthTextField.text),
                                  "abutment": parseFloat(
                                                  abutmentTextField.text),
                                  "arch": parseFloat(archTextField.text)
                              })
    property var errors: ({})

    // js function
    function pushError(err) {
        errors = Object.assign({}, errors, err)
    }

    function rmItemError(errKey) {
        delete errors[errKey]
    }

    function isIncludeErr(errKey) {
        if (errors.hasOwnProperty(errKey))
            return true
        else
            return false
    }

    columns: 3
    rowSpacing: 16 * dp

    CustomLabel {
        text: "width "
        toolTip.text: "عرض"
    }

    TextField {
        id: widthTextField
        validator: DoubleValidator {
            bottom: 1
            top: 10
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای عرض"))
                rmItemError("خطای عرض")
            if (!acceptableInput && !isIncludeErr("خطای عرض"))
                pushError({
                              "خطای عرض": "فیلد عرض نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["width"] = parseFloat(widthTextField.text)
    }
    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }

    CustomLabel {
        text: "Abutment (h1) "
        toolTip.text: "ارتفاع پاتاق"
    }

    TextField {
        id: abutmentTextField
        validator: DoubleValidator {
            bottom: 1
            top: 10
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای ارتفاع پاتاق"))
                rmItemError("خطای ارتفاع پاتاق")
            if (!acceptableInput && !isIncludeErr("خطای ارتفاع پاتاق"))
                pushError({
                              "خطای ارتفاع پاتاق": "فیلد ارتفاع پاتاق نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["abutment"] = parseFloat(
                               abutmentTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }

    CustomLabel {
        text: "Arch (h2) "
        toolTip.text: "ارتفاع تاج"
    }

    TextField {
        id: archTextField
        validator: DoubleValidator {
            bottom: 1
            top: 10
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای ارتفاع تاج"))
                rmItemError("خطای ارتفاع تاج")
            if (!acceptableInput && !isIncludeErr("خطای ارتفاع تاج"))
                pushError({
                              "خطای ارتفاع تاج": "فیلد ارتفاع تاج نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["arch"] = parseFloat(archTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }
}
