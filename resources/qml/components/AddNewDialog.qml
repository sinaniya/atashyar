import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Dialog {
    property var rockProperties: ({
                                    "name": addNewRockNameField.text,
                                    "specific_surface_energy": parseFloat(addNewRockSpecificSurfaceEnergyField.text),
                                    "tension_strength": parseFloat(addNewRockTensionStrengthField.text),
                                    "density": parseFloat(addNewRockDensityField.text),
                                    "wave_velocity": parseFloat(addNewRockWaveVelocityField.text)
                                  })
    property var explosiveProperties: ({
                                        "name": addNewExpNameField.text,
                                        "explosive_type": selectExpTypeGB.checkedButton.text,
                                        "density": parseFloat(addNewExpDensityField.text),
                                        "velocity_of_detonation": parseFloat(addNewExpVelocityField.text),
                                        "specific_energy": parseFloat(addNewExpSpecificEnergyField.text),
                                        "explosion_pressure": parseFloat(addNewExpExplosionPressureField.text),
                                        "length": parseFloat(addNewExpLengthField.text) || 0,
                                        "diameter": parseFloat(addNewExpDiameterField.text) || 0
                                       })
    title: "Add New ..."
    visible: false
    modal: true
    width: root.width / 2
    height: root.height * 2 / 3
    anchors.centerIn: parent

    contentItem: Item {

        TabBar {
            id: selectNew
            anchors.horizontalCenter: parent.horizontalCenter
            TabButton {
                width: newRockText.width + 32*dp
                MouseArea {
                    anchors.fill: parent
                    onClicked: selectNew.setCurrentIndex(0)
                    CustomLabel {
                        id: newRockText
                        text: "New Rock"
                        anchors.centerIn: parent
                        m_id.onClicked: selectNew.setCurrentIndex(0)
                        toolTip.text: "سنگ جدید"
                    }
                }
            }
            TabButton {
                width: newExpText.width + 32*dp
                MouseArea {
                    anchors.fill: parent
                    onClicked: selectNew.setCurrentIndex(1)
                    CustomLabel {
                        id: newExpText
                        text: "New Explosive"
                        anchors.centerIn: parent
                        m_id.onClicked: selectNew.setCurrentIndex(1)
                        toolTip.text: "ماده منفجره جدید"
                    }
                }
            }
        }

        Rectangle {
            id: addNewPageContainer
            anchors.top: selectNew.bottom
            anchors.bottom: parent.bottom
            width: parent.width

            StackLayout {
                anchors.fill: parent
                currentIndex: selectNew.currentIndex

                Page {
                    id: addNewRockPage
                    background: Rectangle {
                        color: "#40405f"
                    }

                    GridLayout {
                        columns: 4

                        CustomLabel {text: "Name"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp}
                        TextField {id: addNewRockNameField; width: 100*dp ; Layout.alignment: Qt.AlignLeft}
                        Item {Layout.fillWidth: true; Layout.columnSpan: 2}
                        CustomLabel {text: "Specific Surface Energy"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewRockSpecificSurfaceEnergyField; width: 100*dp ; Layout.alignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "Kj/m<sup>2</sup>"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                        CustomLabel {text: "Tension Strength"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewRockTensionStrengthField; width: 100*dp ; Layout.alignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "Mpa"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                        CustomLabel {text: "Density"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id:addNewRockDensityField; width: 100*dp ; Layout.alignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "Kg/m<sup>3</sup>"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                        CustomLabel {text: "Wave's Velocity"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewRockWaveVelocityField; width: 100*dp ; Layout.alignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "m/s"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                    }
                }

                Page {
                    id: addNewExpPage
                    background: Rectangle {
                        color: "#40405f"
                    }

                    GridLayout {
                        columns: 4

                        CustomLabel {text: "Explosive Type "; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp}
                        RowLayout {
                            Layout.columnSpan: 2
                            ButtonGroup {
                                id: selectExpTypeGB
                            }
                            RadioButton {
                                checked: true
                                text: "Bulk"
                                font.pixelSize: 20*dp
                                ButtonGroup.group: selectExpTypeGB
                            }
                            RadioButton {
                                text: "Cartridge"
                                font.pixelSize: 20*dp
                                ButtonGroup.group: selectExpTypeGB
                            }
                        }
                        Item {Layout.fillWidth: true}

                        CustomLabel {text: "Name "; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewExpNameField; width: 100*dp}
                        Item {Layout.fillWidth: true; Layout.columnSpan: 2}
                        CustomLabel {text: "Density"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewExpDensityField; width: 100*dp ; horizontalAlignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "Kg/m<sup>3</sup>"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                        CustomLabel {text: "Velocity of Detonation"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewExpVelocityField; width: 100*dp ; horizontalAlignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "m/s"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                        CustomLabel {text: "Specific Energy"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewExpSpecificEnergyField; width: 100*dp ; horizontalAlignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "Mj/Kg"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                        CustomLabel {text: "Explosion Pressure"; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp }
                        TextField {id: addNewExpExplosionPressureField; width: 100*dp ; horizontalAlignment: Qt.AlignLeft}
                        CustomLabel {textFormat: Text.RichText ;text: "MPa"; Layout.alignment: Qt.AlignLeft;}
                        Item {Layout.fillWidth: true}
                        CustomLabel {text: "Length "; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp; visible: selectExpTypeGB.checkedButton.text === "Cartridge"}
                        TextField {id: addNewExpLengthField; width: 100*dp ; horizontalAlignment: Qt.AlignLeft; visible: selectExpTypeGB.checkedButton.text === "Cartridge"}
                        CustomLabel {textFormat: Text.RichText ;text: " cm"; Layout.alignment: Qt.AlignLeft; visible: selectExpTypeGB.checkedButton.text === "Cartridge"}
                        Item {Layout.fillWidth: true; visible: selectExpTypeGB.checkedButton.text === "Cartridge"}
                        CustomLabel {text: "Diameter "; Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 16*dp; visible: selectExpTypeGB.checkedButton.text === "Cartridge" }
                        TextField {id: addNewExpDiameterField; width: 100*dp ; horizontalAlignment: Qt.AlignLeft; visible: selectExpTypeGB.checkedButton.text === "Cartridge"}
                        CustomLabel {textFormat: Text.RichText ;text: " mm"; Layout.alignment: Qt.AlignLeft; visible: selectExpTypeGB.checkedButton.text === "Cartridge"}
                        Item {Layout.fillWidth: true; visible: selectExpTypeGB.checkedButton.text === "Cartridge"}
                    }
                }
            }
        }
        RowLayout {
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.rightMargin: 8*dp
            anchors.bottomMargin: 8*dp
            Button {
                onClicked: addNewDialog.close()
                contentItem: Text {
                    text: qsTr("Cancel")
                    color: "#1d1d2b"
                    font.pixelSize: 20 * dp
                    font.bold: true
                    padding: 8 * dp
                }

                background: Rectangle {
                    height: 48 * dp
                    radius: 8 * dp
                    color: "#ffa500"
                }
            }
            Button {
                onClicked:  {
                    selectNew.currentIndex === 0 ? rocksDB.add_rock(rockProperties): explosivesDB.add_explosive(explosiveProperties)
                    addNewDialog.close()
                }
                contentItem: Text {
                    text: qsTr("Save")
                    color: "#1d1d2b"
                    font.pixelSize: 20 * dp
                    font.bold: true
                    padding: 8 * dp
                }

                background: Rectangle {
                    height: 48 * dp
                    radius: 8 * dp
                    color: "#ffa500"
                }
            }
        }
    }
}
