import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../js/script.js" as Script

Page {

    property var result: ({
                              "number_of_total_holes": 0,
                              "total_explosive_emulsion": 0,
                              "total_explosive_anfo": 0,
                              "specific_explosive_emulsion": 0,
                              "specific_explosive_anfo": 0,
                              "specific_drilling": 0,
                              "explosive_diameter_cut": 0,
                              "explosive_diameter_advance": 0,
                              "explosive_perimetral_diameter": 0,
                              "number_of_cartridges_per_hole_cut": 0,
                              "number_of_cartridges_advance_per_hole": 0,
                              "number_of_cartridges_perimeter_per_hole": 0,
                              "q_first_section": 0,
                              "q_second_section": 0,
                              "q_third_section": 0,
                              "q_fourth_section": 0,
                              "cartridge_diameter_perimeter": 0,
                              "cartridge_diameter": 0,
                              "number_of_cartridges_floor_per_hole": 0
                          })

    property var methodNameToDrawerMethod_Map: ({
                                                    "EBM": drawer.ebm,
                                                    "EBMAngular": drawer.ebm_angular,
                                                    "NTNU": drawer.ntnu,
                                                    "Olafsson": drawer.olafsson,
                                                    "OlafssonAngular": drawer.olafsson_angular,
                                                    "Konya": drawer.konya,
                                                    "KonyaAngular": drawer.konya_angular,
                                                    "Gustafsson": drawer.gustafsson
                                                })
    property var props: ({
                             "plot_view": superViewSelector.checkedButton.text
                         })

    property alias methodLoader: methodLoader
    property alias methodSelector: methodSelector

    property var parallelMethods: ["EBM", "NTNU", "Olafsson", "Konya"]
    property var angularMethods: ["EBM", "Olafsson", "Konya", "Gustafsson"]
    property string methodName: "EBM"

    // js function
    function forceToReloadPage() {
        // force to relaod
        methodLoader.active = false
        methodLoader.active = true
    }
    function chkDisNxtBtn() {
        let properties = Object.assign({}, firstPage.shapeInputItem.properties,
                                       methodLoader.item.inputObject)
        secondPageNextBtn.enabled = true
        for (const key in properties) {
            if (properties[key] === undefined || properties[key] === null
                    || properties[key] === 0) {
                secondPageNextBtn.enabled = false
            } else if ((key === "width" || key === "height" || key === "radius"
                        || key === "abutment" || key === "arc") && isNaN(
                           properties[key])) {
                secondPageNextBtn.enabled = false
            }
        }
    }
    function isEmpty(obj) {
        return Object.keys(obj).length === 0
    }

    function clearErrDlg(dlgId) {
        // first destroy in errors from previous
        for (var i = dlgId.dialogContentCol.children.length; i > 0; i--) {
            //                        console.log("destroying: " + i)
            dlgId.dialogContentCol.children[i - 1].destroy()
        }
    }

    function createErrorAndFDrawerMethod(methodName, properties) {
        var inputValidation = Script.secondPage()

        var errors = {}
        var finalCase = ""

        switch (methodName) {
        case "EBM":
            errors = inputValidation.ebm_validation(properties)
            //                        result = drawer.ebm(thirdPage.resultFigureCanvas,
            //                                            properties)
            finalCase = "EBM"
            break
        case "EBMAngular":
            //                        result = drawer.ebm_angular(
            //                                    thirdPage.resultFigureCanvas, properties)
            errors = inputValidation.ebm_angular_validation(properties)
            finalCase = "EBMAngular"
            break
        case "NTNU":
            //                        result = drawer.ntnu(thirdPage.resultFigureCanvas,
            //                                             properties)
            errors = inputValidation.ntnu_validation(properties)
            finalCase = "NTNU"
            break
        case "Olafsson":
            //                        result = drawer.olafsson(thirdPage.resultFigureCanvas,
            //                                                 properties)
            errors = inputValidation.olafsson_validation(properties)
            finalCase = "Olafsson"
            break
        case "OlafssonAngular":
            //                        result = drawer.olafsson_angular(
            //                                    thirdPage.resultFigureCanvas, properties)
            errors = inputValidation.olafsson_angular_validation(properties)
            finalCase = "OlafssonAngular"
            break
        case "Konya":
            //                        result = drawer.konya(thirdPage.resultFigureCanvas,
            //                                              properties)
            errors = inputValidation.konya_validation(properties)
            finalCase = "Konya"
            break
        case "KonyaAngular":
            //                        result = drawer.konya_angular(
            //                                    thirdPage.resultFigureCanvas, properties)
            errors = inputValidation.konya_angular_validation(properties)
            finalCase = "KonyaAngular"
            break
        case "Gustafsson":
            //                        result = drawer.gustafsson(
            //                                    thirdPage.resultFigureCanvas, properties)
            errors = inputValidation.gustafsson_validation(properties)
            finalCase = "Gustafsson"
            break
        }
        return [errors, finalCase]
    }

    function wrap_dir(dir, str) {
        if (dir === 'rtl')
            return '\u202B' + str + '\u202C'
        return '\u202A' + str + '\u202C'
    }

    function createErrObjects(errors, dlgId) {
        var sizeHeight = Object.keys(errors).length * (40 * dp)
        for (const key in errors) {
            //            console.log("second page ....." + key + " : " + errors[key])
            var component
            var newObj
            var keyStr = key
            var valueStr = errors[key]
            var concatStr = " : "

            // persian and english concat
            var finalStr = wrap_dir('rtl', keyStr) + wrap_dir(
                        'rtl', concatStr) + wrap_dir('rtl', valueStr)
            component = Qt.createComponent("CustomErrorText.qml")
            newObj = component.createObject(dlgId.dialogContentCol, {
                                                "text": finalStr
                                            })
        }
        dlgId.dialogContentBox.implicitHeight = sizeHeight
    }

    function createProperties() {
        var properties = {}
        Object.assign(properties, props)
        Object.assign(properties, firstPage.shapeInputItem.properties)
        Object.assign(properties, methodLoader.item.inputObject)
        return properties
    }

    Rectangle {
        anchors.fill: parent
        color: "#40405f"

        Rectangle {
            height: parent.height - 128 * dp
            width: parent.width - 32 * dp
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.topMargin: 48 * dp
            anchors.bottomMargin: 96 * dp
            color: "transparent"

            CustomLabel {
                id: methodLabel
                text: qsTr("Method ")
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 16 * dp
                anchors.topMargin: 32 * dp
            }

            RowLayout {
                id: superMethodSelectorContainer
                anchors.left: methodLabel.right
                anchors.top: parent.top
                anchors.topMargin: 16 * dp

                ButtonGroup {
                    id: superMethodSelector
                    onCheckedButtonChanged: {
                        methodLoader.active = false
                        methodName = methodSelector.currentText
                                + (checkedButton.text === "Angular" ? "Angular" : "")
                        methodLoader.source = "SecondPage" + methodName + ".qml"
                        methodLoader.active = true
                    }
                }
                RadioButton {
                    checked: true
                    text: "Parallel"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: superMethodSelector
                }
                RadioButton {
                    text: "Angular"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: superMethodSelector
                }
            }

            ComboBox {
                id: methodSelector
                model: superMethodSelector.checkedButton.text
                       === "Parallel" ? parallelMethods : angularMethods
                width: 200 * dp
                anchors.top: parent.top
                anchors.left: superMethodSelectorContainer.right
                anchors.margins: 16 * dp

                onCurrentValueChanged: {
                    // force to relaod
                    methodLoader.active = false
                    methodName = currentText
                            + (superMethodSelector.checkedButton.text === "Angular"
                               && currentText !== "Gustafsson" ? "Angular" : "")
                    methodLoader.source = "SecondPage" + methodName + ".qml"
                    methodLoader.active = true
                }
            }

            CustomLabel {
                id: viewLabel
                text: qsTr("View ")
                anchors.top: parent.top
                anchors.left: methodSelector.right
                anchors.margins: 16 * dp
                anchors.topMargin: 32 * dp
            }

            RowLayout {
                id: superViewSelectorContainer
                anchors.left: viewLabel.right
                anchors.top: parent.top
                anchors.topMargin: 16 * dp

                ButtonGroup {
                    id: superViewSelector
                }
                RadioButton {
                    checked: true
                    text: "2D"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: superViewSelector
                }
                RadioButton {
                    text: "3D"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: superViewSelector
                }
            }

            Loader {
                id: methodLoader
                asynchronous: true
                anchors.top: methodSelector.bottom
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.margins: 16 * dp
                onLoaded: chkDisNxtBtn()
            }
            // handle reloading from first Page
            Connections {
                target: firstPage
                function onMessage() {
                    forceToReloadPage()
                }
            }
        }

        CalculateButton {
            id: secondPageNextBtn
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 16 * dp
            MouseArea {
                cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    // first destroy in errors from previous
                    clearErrDlg(csErrDlgSecondPage)
                    var properties = createProperties()
                    thirdPage.explosiveType = properties["explosive_type"]
                    var arrResult = createErrorAndFDrawerMethod(methodName,
                                                                properties)

                    var errors = arrResult[0]
                    var rightMethod = arrResult[1]
                    createErrObjects(errors, csErrDlgSecondPage)
                    if (!isEmpty(errors))
                        csErrDlgSecondPage.visible = true
                    else {
                        result = methodNameToDrawerMethod_Map[rightMethod](
                                    thirdPage.resultFigureCanvas, properties)
                        thirdPage.unCheckedButtons()
                        stackLayout.currentIndex++
                        // Checking buttons in different view mode
                        if (props["plot_view"] === "2D") {
                            thirdPage.sideView.enabled = false
                            thirdPage.showingProperties.enabled = true
                            thirdPage.panBtn.enabled = true
                            thirdPage.zoomBtn.enabled = true
                        } else {
                            thirdPage.sideView.enabled = true
                            thirdPage.showingProperties.enabled = false
                            thirdPage.panBtn.enabled = false
                            thirdPage.zoomBtn.enabled = false
                        }
                    }

                    for (let k in result) {
                        console.log(k, result[k])
                    }
                }
            }
        }

        BackButton {
            anchors.right: secondPageNextBtn.left
            anchors.bottom: parent.bottom
            anchors.margins: 16 * dp

            MouseArea {
                cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                anchors.fill: parent
                hoverEnabled: true
                onClicked: stackLayout.currentIndex--
            }
        }
        CustomErrorDialog {
            id: csErrDlgSecondPage
        }
    }
}
