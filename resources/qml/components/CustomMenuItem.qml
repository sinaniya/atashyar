import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

RoundButton {
    height: 96 * dp
    width: 96 * dp
    anchors.horizontalCenter: parent.horizontalCenter
    checkable: true
    Material.foreground: "white"

    display: AbstractButton.TextUnderIcon
    background: Rectangle {
        id: bgBtn
        color: if (down) {
                   down ? "#ffa500" : "#33334c"
               } else {
                   hovered ? "#40405f" : "#33334c"
               }

        radius: 48 * dp
        anchors.fill: parent
        anchors.margins: 3
    }
}
