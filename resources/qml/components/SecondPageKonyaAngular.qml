import QtQuick 2.12
import QtQuick.Window 2.3
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import "../../js/toolTipMessages.js" as TooTipMsg

Item {
    property variant inputObject: ({
                                       "advance": parseFloat(advance.text),
                                       "hole_diameter": parseFloat(
                                                            holeDiameter.text),
                                       "explosive_type": explosiveComboBox.currentText,
                                       "explosive_density": parseFloat(
                                                                mainExplosiveDensity.text),
                                       "rock_density": parseFloat(
                                                           densityField.text),
                                       "contour_blasting": contourBlastingButtonGroup.checkedButton.text,
                                       "contour_explosive_type": extraFieldsLoader.item.contour_explosive_type,
                                       "perimeter_explosive_diameter": extraFieldsLoader.item.perimeter_explosive_diameter,
                                       "angle": parseFloat(angleField.text),
                                       "cut_rows": parseFloat(
                                                       cutRowsComboBox.currentText),
                                       "x_length": parseFloat(
                                                       xLengthTextField.text),
                                       "cut_type": cutTypeButtonGroup.checkedButton.text
                                   })
    // js function
    function getToolTipMsg(type, tooltip) {
        var msgFuncs = TooTipMsg.message()
        var msgFunc
        var msgs
        var msg = ""
        switch (type) {
        case 'rock':
            msgFunc = msgFuncs.rock
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'explosive':
            msgFunc = msgFuncs.explosive
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'hole':
            msgFunc = msgFuncs.hole
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        }
        return msg
    }

    width: parent.width
    height: parent.height

    ColumnLayout {
        width: parent.width / 3 + 128 * dp
        anchors {
            top: parent.top
            bottom: parent.bottom
        }
        anchors.horizontalCenter: parent.horizontalCenter

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.topMargin: 16 * dp
            Layout.rightMargin: 16 * dp
            Layout.leftMargin: 16 * dp
            Layout.bottomMargin: 64 * dp
            border.color: "orange"
            radius: 24 * dp
            color: Qt.rgba(0, 0, 255, 0.1)

            GridLayout {
                width: parent.width
                height: parent.height
                columns: 3

                CustomLabel {
                    text: "Rock Type"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("rock", "Rock_Type")
                }

                ComboBox {
                    id: rockComboBox
                    Layout.topMargin: 16 * dp
                    Layout.rightMargin: 16 * dp
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    Layout.alignment: Qt.AlignLeft

                    Component.onCompleted: model = rocksDB.get_all_names()

                    onCurrentValueChanged: {
                        var obj = rocksDB.get_properties_by_name(currentValue)
                        densityField.text = obj["density"]
                    }
                }

                CustomLabel {
                    text: "Density "
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("rock", "Density")
                }
                TextField {
                    id: densityField
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: " Kg/m<sup>3</sup>"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "Advance "
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Advance")
                }

                TextField {
                    id: advance
                    Layout.alignment: Qt.AlignLeft
                    Layout.fillWidth: true
                    validator: DoubleValidator {
                        bottom: 0.001
                        top: 100
                        notation: DoubleValidator.StandardNotation
                        decimals: 3
                    }
                    text: "3"
                    Layout.topMargin: 16 * dp
                }

                CustomLabel {
                    text: " m"
                    Layout.alignment: Qt.AlignLeft
                    Layout.topMargin: 16 * dp
                    Layout.rightMargin: 16 * dp
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "Hole Diameter "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Hole_Diameter")
                }

                TextField {
                    id: holeDiameter
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    text: "45"
                    validator: DoubleValidator {
                        bottom: 0.001
                        top: 100
                        notation: DoubleValidator.StandardNotation
                        decimals: 3
                    }
                }

                CustomLabel {
                    text: " mm"
                    Layout.alignment: Qt.AlignLeft
                    Layout.rightMargin: 16 * dp
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "Explosive "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Explosive_Type")
                }

                ComboBox {
                    id: explosiveComboBox
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    Layout.rightMargin: 16 * dp
                    Component.onCompleted: model = explosivesDB.get_all_names()
                    onCurrentValueChanged: {
                        var obj = explosivesDB.get_properties_by_name(
                                    currentValue)
                        mainExplosiveDensity.text = obj["density"]
                    }
                }

                CustomLabel {
                    text: "Explosive Density "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Density")
                }

                TextField {
                    id: mainExplosiveDensity
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    validator: DoubleValidator {
                        bottom: 0.001
                        top: 100
                        notation: DoubleValidator.StandardNotation
                        decimals: 3
                    }
                    text: ""
                }

                CustomLabel {
                    textFormat: Text.RichText
                    text: " Kg/m<sup>3</sup>"
                    Layout.alignment: Qt.AlignLeft
                    Layout.rightMargin: 16 * dp
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "V Cut (Wedge Cut)"
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Wedge_Cut")
                }
                RowLayout {
                    Layout.columnSpan: 2

                    ButtonGroup {
                        id: cutTypeButtonGroup
                        onCheckedButtonChanged: cutTypeImage.source = "../../images/VCut"
                                                + checkedButton.text.replace(
                                                    /\s/g, "")
                    }

                    RadioButton {
                        checked: true
                        text: "Type I"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: cutTypeButtonGroup
                    }
                    RadioButton {
                        text: "Type II"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: cutTypeButtonGroup
                    }
                }
                CustomLabel {
                    text: "Angle"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Angle")
                }
                TextField {
                    id: angleField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                    text: "60"
                }
                CustomLabel {
                    text: "degrees"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "degrees")
                }

                CustomLabel {
                    text: "Cut Rows"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Cut_Rows")
                }
                ComboBox {
                    id: cutRowsComboBox
                    Layout.fillWidth: true
                    model: ["2", "3"]
                    Layout.columnSpan: 2
                }

                Image {
                    id: cutTypeImage
                    Layout.columnSpan: 3
                    source: "../../images/VCut" + cutTypeButtonGroup.checkedButton.text.replace(
                                /\s/g, "")
                    Layout.preferredHeight: 150 * dp
                    Layout.preferredWidth: 150 * dp
                    Layout.topMargin: 16 * dp
                    sourceSize.width: 512
                    sourceSize.height: 512
                    Layout.alignment: Qt.AlignCenter
                    fillMode: Image.PreserveAspectFit
                }
                CustomLabel {
                    text: "Length (X)"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    visible: cutTypeButtonGroup.checkedButton.text === "Type II"
                }
                TextField {
                    id: xLengthTextField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                    Layout.topMargin: 16 * dp
                    text: "0.5"
                    visible: cutTypeButtonGroup.checkedButton.text === "Type II"
                }
                CustomLabel {
                    text: "m"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    visible: cutTypeButtonGroup.checkedButton.text === "Type II"
                }

                CustomLabel {
                    text: "Contour Blasting "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Contour_Blasting")
                }

                RowLayout {

                    ButtonGroup {
                        id: contourBlastingButtonGroup
                    }

                    RadioButton {
                        checked: true
                        text: "Normal"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: contourBlastingButtonGroup
                    }
                    RadioButton {
                        text: "Controlled"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: contourBlastingButtonGroup
                    }
                }
                Item {
                    Layout.fillWidth: true
                }

                Loader {
                    Layout.columnSpan: 3
                    Layout.fillWidth: true
                    id: extraFieldsLoader
                    source: contourBlastingButtonGroup.checkedButton.text
                            === "Normal" ? "SecondPageKonyaNormal.qml" : "SecondPageKonyaControlled.qml"
                }

                Item {
                    Layout.fillHeight: true
                }
            }
        }
    }
}
