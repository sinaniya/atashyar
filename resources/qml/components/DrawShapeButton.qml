import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Button {
    contentItem: Text {
        text: qsTr("Draw Shape")
        color: "#ffa500"
        font.pixelSize: 20*dp
        font.bold: true
        font.underline: true
        padding: 8*dp
    }
    flat: true
}
