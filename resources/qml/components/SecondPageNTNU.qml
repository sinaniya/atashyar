import QtQuick 2.12
import QtQuick.Window 2.3
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import "../../js/toolTipMessages.js" as TooTipMsg

Item {

    property variant inputObject: {
        "blastability": blastabilityButtonGroup.checkedButton.text,
        "hole_diameter": parseFloat(holeDiameterButtonGroup.checkedButton.text),
        "hole_length": parseFloat(holeLength.text),
        "explosive_type": explosiveComboBox.currentText,
        "primeter_explosive": perimetralExplosiveComboBox.currentText,
        "skill_level": skillLevelButtonGroup.checkedButton.text
    }
    // js function
    function getToolTipMsg(type, tooltip) {
        var msgFuncs = TooTipMsg.message()
        var msgFunc
        var msgs
        var msg = ""
        switch (type) {
        case 'rock':
            msgFunc = msgFuncs.rock
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'explosive':
            msgFunc = msgFuncs.explosive
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'hole':
            msgFunc = msgFuncs.hole
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        }
        return msg
    }

    width: parent.width
    height: parent.height

    ColumnLayout {
        width: parent.width / 3 + 32 * dp
        anchors {
            top: parent.top
            bottom: parent.bottom
        }
        anchors.horizontalCenter: parent.horizontalCenter

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.topMargin: 16 * dp
            Layout.rightMargin: 16 * dp
            Layout.leftMargin: 16 * dp
            Layout.bottomMargin: 64 * dp
            border.color: "orange"
            radius: 24 * dp
            color: Qt.rgba(0, 0, 255, 0.1)

            GridLayout {
                width: parent.width
                height: parent.height
                columns: 3

                CustomLabel {
                    text: "Blastability "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Blastability")
                }

                RowLayout {

                    ButtonGroup {
                        id: blastabilityButtonGroup
                    }

                    RadioButton {
                        checked: true
                        text: "Good"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: blastabilityButtonGroup
                    }
                    RadioButton {
                        text: "Poor"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: blastabilityButtonGroup
                    }
                }
                Item {
                    Layout.fillWidth: true
                }

                CustomLabel {
                    text: "Skill level "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Skill_level")
                }
                RowLayout {

                    ButtonGroup {
                        id: skillLevelButtonGroup
                    }

                    RadioButton {
                        checked: true
                        text: "High"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: skillLevelButtonGroup
                    }
                    RadioButton {
                        text: "Low"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: skillLevelButtonGroup
                    }
                }

                Item {
                    Layout.fillWidth: true
                }

                CustomLabel {
                    text: "Hole diameter "
                    Layout.leftMargin: 16
                    toolTip.text: getToolTipMsg("hole", "Hole_Diameter")
                }

                ButtonGroup {
                    id: holeDiameterButtonGroup
                }

                RadioButton {
                    checked: true
                    text: "45mm"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: holeDiameterButtonGroup
                }
                RadioButton {
                    text: "48mm"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: holeDiameterButtonGroup
                }

                Item {
                    Layout.fillWidth: true
                }

                RadioButton {
                    checked: true
                    text: "51mm"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: holeDiameterButtonGroup
                }
                RadioButton {
                    checked: true
                    text: "64mm"
                    font.pixelSize: 20 * dp
                    ButtonGroup.group: holeDiameterButtonGroup
                }

                CustomLabel {
                    text: "Hole length "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Hole_length")
                }

                TextField {
                    id: holeLength
                    width: 200 * dp
                    Layout.alignment: Qt.AlignHCenter
                    text: "3"
                }

                CustomLabel {
                    text: "m"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "Main explosive "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Mainـexplosive")
                }

                ComboBox {
                    id: explosiveComboBox
                    Layout.alignment: Qt.AlignHCenter
                    model: ["Anfo"]

                    delegate: ItemDelegate {
                        text: modelData
                        width: explosiveComboBox.width
                    }

                    popup: Popup {
                        width: explosiveComboBox.width
                        implicitHeight: root.height / 3

                        contentItem: ListView {
                            clip: true
                            implicitHeight: contentHeight
                            model: explosiveComboBox.popup.visible ? explosiveComboBox.delegateModel : null
                            ScrollBar.vertical: ScrollBar {
                                active: true
                                policy: ScrollBar.AlwaysOn
                            }
                        }
                    }
                }

                Item {
                    Layout.fillWidth: true
                }

                CustomLabel {
                    text: "Main explosive density "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive",
                                                "Main_explosive_density")
                }

                TextField {
                    id: mainExplosiveDensity
                    width: 200
                    Layout.alignment: Qt.AlignHCenter
                    validator: DoubleValidator {
                        bottom: 0.001
                        top: 100
                        notation: DoubleValidator.StandardNotation
                        decimals: 3
                    }
                    text: "0.9"
                }

                CustomLabel {
                    textFormat: Text.RichText
                    text: "Kg/m<sup>3</sup>"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "Primetral explosive "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive",
                                                "Perimetral_Diameter")
                }

                ComboBox {
                    id: perimetralExplosiveComboBox
                    Layout.alignment: Qt.AlignHCenter
                    model: ["Anfo", "Emulsion"]

                    delegate: ItemDelegate {
                        text: modelData
                        width: perimetralExplosiveComboBox.width
                    }

                    popup: Popup {
                        width: perimetralExplosiveComboBox.width
                        implicitHeight: root.height / 3

                        contentItem: ListView {
                            clip: true
                            implicitHeight: contentHeight
                            model: perimetralExplosiveComboBox.popup.visible ? perimetralExplosiveComboBox.delegateModel : null
                            ScrollBar.vertical: ScrollBar {
                                active: true
                                policy: ScrollBar.AlwaysOn
                            }
                        }
                    }
                }

                Item {
                    Layout.fillWidth: true
                }

                Item {
                    Layout.fillHeight: true
                }
            }
        }
    }
}
