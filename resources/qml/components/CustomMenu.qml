import QtQuick 2.15
import QtQuick.Controls 2.12

Menu {
    width: parent.width
    height: 600 * dp
    visible: menuButton.isActive

    CustomMenuItem {
        icon.source: "../../icons/new.svg"
        text: qsTr("New")
        MouseArea {
            cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
            anchors.fill: parent
            hoverEnabled: true
        }
    }
    CustomMenuItem {
        icon.source: "../../icons/open.svg"
        text: qsTr("Open")
        MouseArea {
            cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
            anchors.fill: parent
            hoverEnabled: true
        }
    }
    CustomMenuItem {
        icon.source: "../../icons/add.svg"
        text: qsTr("Add")
        MouseArea {
            cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
            anchors.fill: parent
            hoverEnabled: true
            onClicked: addNewDialog.visible = true
        }
    }
    CustomMenuItem {
        icon.source: "../../icons/save.svg"
        text: qsTr("Save")
        MouseArea {
            cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
            anchors.fill: parent
            hoverEnabled: true
        }
    }
    CustomMenuItem {
        icon.source: "../../icons/close.svg"
        text: qsTr("Exit")
        MouseArea {
            cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
            anchors.fill: parent
            hoverEnabled: true
            onClicked: root.close()
        }
    }

    background: Rectangle {
        width: parent.width
        height: 400 * dp
        opacity: 0
    }
}
