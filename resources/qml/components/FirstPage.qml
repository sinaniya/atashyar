import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import "../../js/script.js" as Script

import Backend 1.0

Page {
    property alias shapeInputItem: shapeInputLoader.item
    property alias homeFigureCanvas: homeFigureCanvas
    signal message(string msg)

    // js function
    function isEmpty(obj) {
        return Object.keys(obj).length === 0
    }

    function clearErrDlg(id) {
        for (var i = id.dialogContentCol.children.length; i > 0; i--) {
            //                        console.log("destroying: " + i)
            id.dialogContentCol.children[i - 1].destroy()
        }
    }

    function getErrorsByShapeInputName(shapeInputName, props) {
        var errors = {}
        switch (shapeInputName) {
        case "Arched Roof 1":
            errors = Script.archedRoof1_Validation(props)
            break
        case "Arched Roof 2":
            errors = Script.archedRoof2_Validation(props)
            break
        case "Circle":
            errors = Script.circle_Validation(props)
            break
        case "D-Shape":
            errors = Script.dShape_Validation(props)
            break
        case "HorseShoe":
            errors = Script.horseShoe_Validation(props)
            break
        case "Rectangle":
            errors = Script.rectAngle_Validation(props)
            break
        default:
            errors = Script.dShape_Validation(props)
        }
        return errors
    }

    function wrap_dir(dir, str) {
        if (dir === 'rtl')
            return '\u202B' + str + '\u202C'
        return '\u202A' + str + '\u202C'
    }

    function createErrors(errors, errDlgId) {
        var sizeHeight = Object.keys(errors).length * (30 * dp)
        for (const key in errors) {
            //                        console.log(key + " : " + errors[key])
            var component
            var newObj
            var keyStr = key
            var valueStr = errors[key]
            var concatStr = " : "
            // persian and english concat
            var finalStr = wrap_dir('rtl', keyStr) + wrap_dir(
                        'rtl', concatStr) + wrap_dir('rtl', valueStr)
            component = Qt.createComponent("CustomErrorText.qml")
            newObj = component.createObject(errDlgId.dialogContentCol, {
                                                "text": finalStr
                                            })
        }
        errDlgId.dialogContentBox.implicitHeight = sizeHeight
    }

    Rectangle {
        anchors.fill: parent
        color: "#40405f"

        Rectangle {
            height: parent.height - 128 * dp
            width: parent.width / 4
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.leftMargin: 32 * dp
            anchors.topMargin: 48 * dp
            anchors.bottomMargin: 96 * dp
            color: "transparent"

            Image {
                id: shapeImg
                width: parent.width
                height: width
                sourceSize.width: 512
                sourceSize.height: 512
                anchors.top: parent.top
                anchors.margins: 16 * dp
                fillMode: Image.PreserveAspectFit
            }

            RowLayout {
                id: shapeSelectorRow
                width: parent.width
                anchors.top: shapeImg.bottom
                anchors.topMargin: 16 * dp
                anchors.bottomMargin: 16 * dp
                Layout.alignment: Qt.AlignLeft

                CustomLabel {
                    id: shapeSelectorLabel
                    text: "Shape "
                    MouseArea {
                        id: shapeLabelMA
                        anchors.fill: parent
                        hoverEnabled: true
                    }
                    ToolTip {
                        visible: shapeLabelMA.containsMouse
                        text: "شکل سطح مقطع"
                    }
                }

                ComboBox {
                    id: shapeSelector
                    Layout.fillWidth: true

                    model: ["D-Shape", "HorseShoe", "Circle", "Rectangle", "Arched Roof 1", "Arched Roof 2"]

                    onCurrentValueChanged: {
                        const IMAGES_DIR = "../../images/"
                        shapeImg.source = IMAGES_DIR + currentText.replace(
                                    /-|\s/g, "")
                        shapeInputLoader.source = currentText.replace(
                                    /-|\s/g, "") + "Input.qml"
                    }
                }
            }

            Loader {
                id: shapeInputLoader
                source: "DShapeInput.qml"
                width: parent.width
                anchors.top: shapeSelectorRow.bottom
                anchors.left: parent.left
            }
        }

        Rectangle {
            height: parent.height - 128 * dp
            width: (parent.width * 3 / 4) - 128 * dp
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: 32 * dp
            anchors.topMargin: 48 * dp
            anchors.bottomMargin: 96 * dp
            color: "transparent"

            FigureCanvas {
                id: homeFigureCanvas
                dpi_ratio: Screen.devicePixelRatio
                anchors.fill: parent
                anchors.margins: 16 * dp
            }
        }

        DrawShapeButton {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.margins: 16 * dp

            onClicked: {
                //                console.log("ERRRRRRRRRRRRRRR")
                clearErrDlg(csErrDlg)
                var props = shapeInputItem.properties
                var shapeInputName = props.shape_name
                var errors = getErrorsByShapeInputName(shapeInputName, props)

                createErrors(errors, csErrDlg)
                if (!isEmpty(errors)) {
                    csErrDlg.visible = true
                } else {
                    drawer.draw(homeFigureCanvas,
                                shapeInputLoader.item.properties)
                }
            }
        }

        NextButton {
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 16 * dp
            enabled: !shapeInputLoader.item.error

            MouseArea {
                cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    clearErrDlg(csErrDlg)
                    var props = shapeInputItem.properties
                    var shapeInputName = props.shape_name
                    var errors = getErrorsByShapeInputName(shapeInputName,
                                                           props)

                    createErrors(errors, csErrDlg)
                    firstPage.message("Clicked next button")
                    if (!isEmpty(errors)) {
                        csErrDlg.visible = true
                    } else {
                        drawer.draw(homeFigureCanvas,
                                    shapeInputLoader.item.properties)
                        stackLayout.currentIndex++
                    }
                }
            }
        }
        CustomErrorDialog {
            id: csErrDlg
        }
    }
}
