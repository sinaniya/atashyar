import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

GridLayout {
    id: grdlayout1
    property var properties: ({
                                  "shape_name": "Arched Roof 1",
                                  "width": parseFloat(widthTextField.text),
                                  "radius": parseFloat(radiusTextField.text),
                                  "height": parseFloat(heightTextField.text)
                              })

    property var errors: ({})

    // js function
    function pushError(err) {
        errors = Object.assign({}, errors, err)
    }

    function rmItemError(errKey) {
        delete errors[errKey]
    }

    function isIncludeErr(errKey) {
        if (errors.hasOwnProperty(errKey))
            return true
        else
            return false
    }

    columns: 3
    rowSpacing: 16 * dp

    CustomLabel {
        text: "width "
        toolTip.text: "عرض"
    }
    TextField {
        id: widthTextField
        validator: DoubleValidator {
            bottom: 1
            top: 10
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای عرض"))
                rmItemError("خطای عرض")
            if (!acceptableInput && !isIncludeErr("خطای عرض"))
                pushError({
                              "خطای عرض": "فیلد عرض نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["width"] = parseFloat(widthTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }

    CustomLabel {
        text: "Radius "
        toolTip.text: "شعاع"
    }

    TextField {
        id: radiusTextField
        validator: DoubleValidator {
            bottom: 0.5
            top: 7
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای شعاع"))
                rmItemError("خطای شعاع")
            if (!acceptableInput && !isIncludeErr("خطای شعاع"))
                pushError({
                              "خطای شعاع": "فیلد شعاع نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["radius"] = parseFloat(
                               radiusTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }

    CustomLabel {
        text: "Height "
        toolTip.text: "ارتفاع"
    }

    TextField {
        id: heightTextField
        validator: DoubleValidator {
            bottom: 1
            top: 10
        }
        onTextChanged: {
            if (acceptableInput && isIncludeErr("خطای ارتفاع"))
                rmItemError("خطای ارتفاع")
            if (!acceptableInput && !isIncludeErr("خطای ارتفاع"))
                pushError({
                              "خطای ارتفاع": "فیلد ارتفاع نمی‌تواند منفی باشد"
                          })
        }
        onEditingFinished: properties["height"] = parseFloat(
                               heightTextField.text)
    }

    CustomLabel {
        text: " m"
        toolTip.text: "متر"
    }
}
