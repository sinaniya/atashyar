import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Button {
    contentItem: Text {
        text: qsTr("Calculate")
        color: "#1d1d2b"
        font.pixelSize: 20*dp
        font.bold: true
        padding: 8*dp
    }

    background: Rectangle {
        height: 48*dp
        width: contentItem.width + 16*dp
        radius: 8*dp
        color: parent.enabled ? "#ffa500": "#bfbfbf"
        border.color: "#FFB06E"
        border.width: parent.enabled && parent.hovered ? 5*dp: 0
    }
}
