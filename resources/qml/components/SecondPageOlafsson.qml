import QtQuick 2.12
import QtQuick.Window 2.3
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import "../../js/toolTipMessages.js" as TooTipMsg

Item {
    property variant inputObject: ({
                                       "advance": parseFloat(advance.text),
                                       "explosive_type": explosiveComboBox.currentText,
                                       "explosive_density": parseFloat(
                                                                mainExplosiveDensity.text),
                                       "hole_diameter": parseFloat(
                                                            holeDiameter.text)
                                   })
    // js function
    function getToolTipMsg(type, tooltip) {
        var msgFuncs = TooTipMsg.message()
        var msgFunc
        var msgs
        var msg = ""
        switch (type) {
        case 'rock':
            msgFunc = msgFuncs.rock
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'explosive':
            msgFunc = msgFuncs.explosive
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'hole':
            msgFunc = msgFuncs.hole
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        }
        return msg
    }

    width: parent.width
    height: parent.height

    ColumnLayout {
        width: parent.width / 3 + 32 * dp
        anchors {
            top: parent.top
            bottom: parent.bottom
        }
        anchors.horizontalCenter: parent.horizontalCenter

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.topMargin: 16 * dp
            Layout.rightMargin: 16 * dp
            Layout.leftMargin: 16 * dp
            Layout.bottomMargin: 64 * dp
            border.color: "orange"
            radius: 24 * dp
            color: Qt.rgba(0, 0, 255, 0.1)

            GridLayout {
                width: parent.width
                height: parent.height
                columns: 3

                CustomLabel {
                    text: "Advance "
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Advance")
                }

                TextField {
                    id: advance
                    Layout.alignment: Qt.AlignLeft
                    Layout.fillWidth: true
                    text: "5"
                    Layout.topMargin: 16 * dp
                }

                CustomLabel {
                    text: " m"
                    Layout.alignment: Qt.AlignLeft
                    Layout.topMargin: 16 * dp
                    Layout.rightMargin: 16 * dp
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "Explosive "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Explosive_Type")
                }

                ComboBox {
                    id: explosiveComboBox
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    Layout.rightMargin: 16 * dp
                    Component.onCompleted: model = explosivesDB.get_all_names()
                    onCurrentValueChanged: {
                        var obj = explosivesDB.get_properties_by_name(
                                    currentValue)
                        mainExplosiveDensity.text = obj["density"]
                    }
                }

                CustomLabel {
                    text: "Explosive Density "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Density")
                }

                TextField {
                    id: mainExplosiveDensity
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    validator: DoubleValidator {
                        bottom: 0.001
                        top: 100
                        notation: DoubleValidator.StandardNotation
                        decimals: 3
                    }
                    text: ""
                }

                CustomLabel {
                    textFormat: Text.RichText
                    text: " Kg/m<sup>3</sup>"
                    Layout.alignment: Qt.AlignLeft
                    Layout.rightMargin: 16 * dp
                    toolTip.visible: false
                }

                CustomLabel {
                    text: "Hole Diameter "
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Hole_Diameter")
                }

                TextField {
                    id: holeDiameter
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignLeft
                    text: "45"
                    validator: DoubleValidator {
                        bottom: 0.001
                        top: 100
                        notation: DoubleValidator.StandardNotation
                        decimals: 3
                    }
                }

                CustomLabel {
                    text: " mm"
                    Layout.alignment: Qt.AlignLeft
                    Layout.rightMargin: 16 * dp
                    toolTip.visible: false
                }
                Item {
                    Layout.fillHeight: true
                }
            }
        }
    }
}
