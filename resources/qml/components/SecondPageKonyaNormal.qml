import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../js/toolTipMessages.js" as TooTipMsg

GridLayout {

    property string contour_explosive_type: perimeteralExplosiveComboBox.currentText
    property real perimeter_explosive_diameter: perimeteralExplosiveComboBox.currentText
                                                === "Emulsion" ? parseFloat(
                                                                     perimeteralExplosiveDiameterComboBox.currentText) : 0
    // js function
    function getToolTipMsg(type, tooltip) {
        var msgFuncs = TooTipMsg.message()
        var msgFunc
        var msgs
        var msg = ""
        switch (type) {
        case 'rock':
            msgFunc = msgFuncs.rock
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'explosive':
            msgFunc = msgFuncs.explosive
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'hole':
            msgFunc = msgFuncs.hole
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        }
        return msg
    }

    columns: 3
    CustomLabel {
        text: "Contour Explosive Type "
        Layout.leftMargin: 16 * dp
        toolTip.text: getToolTipMsg("explosive", "Contour_Explosive_Type")
    }
    ComboBox {
        id: perimeteralExplosiveComboBox
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignLeft
        Layout.rightMargin: 16 * dp
        Component.onCompleted: model = explosivesDB.get_all_names()
    }
    Item {
        Layout.fillWidth: true
    }

    CustomLabel {
        text: "Contour Explosive Diameter "
        Layout.leftMargin: 16 * dp
        toolTip.text: getToolTipMsg("explosive", "Contour_Explosive_Diameter")
        visible: perimeteralExplosiveComboBox.currentText === "Emulsion"
    }
    ComboBox {
        id: perimeteralExplosiveDiameterComboBox
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignLeft
        Layout.rightMargin: 16 * dp
        model: ["22", "27", "30", "35", "40"]
        visible: perimeteralExplosiveComboBox.currentText === "Emulsion"
    }
    CustomLabel {
        text: " mm"
        visible: perimeteralExplosiveComboBox.currentText === "Emulsion"
        toolTip.visible: false
    }
}
