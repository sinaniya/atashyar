import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

RoundButton {
    property bool isActive: false

    icon.source: "../../icons/menu_burger.svg"
    height: 96 * dp
    width: 96 * dp
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top
    text: qsTr("Menu")
    Material.foreground: "white"

    display: AbstractButton.TextUnderIcon

    MouseArea {
        cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            isActive = !isActive
        }
    }
    background: Rectangle {
        id: bgBtn
        color: if (down) {
                   down ? "#ffa500" : "#33334c"
               } else {
                   hovered ? "#40405f" : "#33334c"
               }
        radius: 48 * dp
        anchors.fill: parent
        anchors.margins: 3
    }
}
