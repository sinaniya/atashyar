import QtQuick.Layouts 1.12

StackLayout {
    id: stackLayout
    anchors.fill: parent
    anchors.margins: 48 * dp
    currentIndex: 0

    FirstPage {
        id: firstPage
    }

    SecondPage {
        id: secondPage
    }

    ThirdPage {
        id: thirdPage
    }
}
