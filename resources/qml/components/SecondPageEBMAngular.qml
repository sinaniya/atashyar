import QtQuick 2.12
import QtQuick.Window 2.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import "../../js/toolTipMessages.js" as TooTipMsg

Item {
    width: parent.width
    height: parent.height

    property variant inputObject: {
        "rock_impedance": parseFloat(impedanceField.text),
        "rock_specific_surface_energy": parseFloat(
                    specificSurfaceEnergyField.text),
        "rock_tension_strength": parseFloat(tensionStrengthField.text),
        "rock_density": parseFloat(densityField.text),
        "rock_wave_velocity": parseFloat(waveVelocityField.text),
        "explosive_type": expComboBox.currentText,
        "explosive_impedance": parseFloat(eImpedanceField.text),
        "explosive_diameter": parseFloat(eDiameterField.currentText),
        "explosive_perimetral_diameter": parseFloat(
                    ePerimetralDiameterField.text),
        "explosive_specific_energy": parseFloat(eSpecificEnergyField.text),
        "explosive_explosion_pressure": parseFloat(
                    eExplosionPressureField.text),
        "explosive_density": parseFloat(eDensityField.text),
        "explosive_velocity": parseFloat(eVelocityField.text),
        "hole_diameter": parseFloat(hDiameterField.text),
        "advance": parseFloat(advanceField.text),
        "crushing": crushingField.currentText,
        "cut_type": cutTypeButtonGroup.checkedButton.text,
        "angle": parseFloat(angleField.text),
        "cut_rows": parseFloat(cutRowsComboBox.currentText),
        "x_length": parseFloat(xLengthTextField.text)
    }

    // js function
    function getToolTipMsg(type, tooltip) {
        var msgFuncs = TooTipMsg.message()
        var msgFunc
        var msgs
        var msg = ""
        switch (type) {
        case 'rock':
            msgFunc = msgFuncs.rock
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'explosive':
            msgFunc = msgFuncs.explosive
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'hole':
            msgFunc = msgFuncs.hole
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        }
        return msg
    }

    ColumnLayout {
        id: rockProperties
        width: parent.width / 3
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 16 * dp
            Layout.rightMargin: 8 * dp
            Layout.bottomMargin: 64 * dp
            radius: 24 * dp
            color: Qt.rgba(0, 0, 255, 0.1)
            border.color: "orange"

            GridLayout {
                id: rockGrid
                columns: 3

                CustomLabel {
                    text: "Rock Type"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("rock", "Rock_Type")
                }
                ComboBox {
                    id: rockComboBox
                    Layout.topMargin: 16 * dp
                    Layout.fillWidth: true
                    Layout.columnSpan: 2

                    Component.onCompleted: model = rocksDB.get_all_names()

                    onCurrentValueChanged: {
                        var obj = rocksDB.get_properties_by_name(currentValue)
                        impedanceField.text = obj["impedance"]
                        specificSurfaceEnergyField.text = obj["specific_surface_energy"]
                        tensionStrengthField.text = obj["tension_strength"]
                        densityField.text = obj["density"]
                        waveVelocityField.text = obj["wave_velocity"]
                    }
                }

                CustomLabel {
                    text: "Impedance"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("rock", "Impedance")
                }
                TextField {
                    id: impedanceField
                    width: 100 * dp
                    Layout.alignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "10<sup>6</sup>Kg/m.s<sup>2</sup>"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Specific Surface Energy"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("rock",
                                                "Specific_Surface_Energy")
                }
                TextField {
                    id: specificSurfaceEnergyField
                    width: 100 * dp
                    Layout.alignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "Kj/m<sup>2</sup>"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Tension Strength"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("rock", "Tension_Strength")
                }
                TextField {
                    id: tensionStrengthField
                    width: 100 * dp
                    Layout.alignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "Mpa"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Density"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("rock", "Density")
                }
                TextField {
                    id: densityField
                    width: 100 * dp
                    Layout.alignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "Kg/m<sup>3</sup>"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Wave's Velocity"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16
                    toolTip.text: getToolTipMsg("rock", "Wave's_Velocity")
                }
                TextField {
                    id: waveVelocityField
                    width: 100 * dp
                    Layout.alignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "m/s"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                Item {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }
            }
        }
    }
    ColumnLayout {
        id: expColLayout
        width: parent.width / 3
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: rockProperties.right
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.topMargin: 16 * dp
            Layout.rightMargin: 8 * dp
            Layout.leftMargin: 8 * dp
            Layout.bottomMargin: 64 * dp
            radius: 24 * dp
            color: Qt.rgba(0, 0, 255, 0.1)
            border.color: "orange"

            GridLayout {
                columns: 3

                CustomLabel {
                    text: "Explosive Type"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    MouseArea {
                        id: expTypeLabelMA
                        anchors.fill: parent
                        hoverEnabled: true
                    }
                    ToolTip {
                        visible: expTypeLabelMA.containsMouse
                        text: getToolTipMsg("explosive", "Explosive_Type")
                    }
                }

                ComboBox {
                    id: expComboBox
                    Layout.topMargin: 16 * dp
                    Layout.columnSpan: 2
                    Layout.fillWidth: true

                    Component.onCompleted: model = explosivesDB.get_all_names()

                    onCurrentValueChanged: {
                        var obj = explosivesDB.get_properties_by_name(
                                    currentValue)
                        eImpedanceField.text = obj["impedance"]
                        eSpecificEnergyField.text = obj["specific_energy"]
                        eExplosionPressureField.text = obj["explosion_pressure"]
                        eDensityField.text = obj["density"]
                        eVelocityField.text = obj["velocity_of_detonation"]
                    }
                }

                CustomLabel {
                    text: "Impedance"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Impedance")
                }
                TextField {
                    id: eImpedanceField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "10<sup>6</sup>Kg/m<sup>2</sup>.s"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Diameter"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Diameter")
                }
                ComboBox {
                    id: eDiameterField
                    Layout.fillWidth: true
                    model: [27, 30, 35, 40, 45, 50]
                    currentIndex: 3
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "mm"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Perimetral Diameter"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive",
                                                "Perimetral_Diameter")
                }
                TextField {
                    id: ePerimetralDiameterField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                    text: "22"
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "mm"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Specific Energy"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Specific_Energy")
                }
                TextField {
                    id: eSpecificEnergyField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "Mj/Kg"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Explosion Pressure"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive",
                                                "Explosion_Pressure")
                }
                TextField {
                    id: eExplosionPressureField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "MPa"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Density"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive", "Density")
                }
                TextField {
                    id: eDensityField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "Kg/m<sup>3</sup>"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Velocity of Detonation"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("explosive",
                                                "Velocity_of_Detonation")
                }
                TextField {
                    id: eVelocityField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                }
                CustomLabel {
                    textFormat: Text.RichText
                    text: "m/s"
                    Layout.alignment: Qt.AlignLeft
                    toolTip.visible: false
                }
                Item {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }
            }
        }
    }

    ColumnLayout {
        width: parent.width / 3
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: expColLayout.right
        }

        Rectangle {
            id: holePropertiesContainer
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.rightMargin: 16 * dp
            Layout.leftMargin: 8 * dp
            Layout.bottomMargin: 64 * dp
            Layout.topMargin: 16 * dp
            radius: 24 * dp
            color: Qt.rgba(0, 0, 255, 0.1)
            border.color: "orange"

            GridLayout {
                columns: 3

                CustomLabel {
                    text: "Hole Diameter"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Hole_Diameter")
                }
                TextField {
                    id: hDiameterField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                    Layout.topMargin: 16 * dp
                    text: "45"
                }
                CustomLabel {
                    text: "mm"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Advance"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Advance")
                }
                TextField {
                    id: advanceField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                    text: "3"
                }
                CustomLabel {
                    text: "m"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.visible: false
                }
                CustomLabel {
                    text: "Crushing"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Crushing")
                }
                ComboBox {
                    id: crushingField
                    Layout.fillWidth: true
                    model: ["Low Crushing", "Medium Crushing", "High Crushing"]
                    Layout.columnSpan: 2
                }
                Rectangle {
                    height: 1 * dp
                    width: holePropertiesContainer.width
                    Layout.columnSpan: 3
                    color: "orange"
                    Layout.topMargin: 24
                    Layout.bottomMargin: 24
                }
                CustomLabel {
                    text: "V Cut (Wedge Cut)"
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Wedge_Cut")
                }
                RowLayout {
                    Layout.columnSpan: 2

                    ButtonGroup {
                        id: cutTypeButtonGroup
                        onCheckedButtonChanged: cutTypeImage.source = "../../images/VCut"
                                                + checkedButton.text.replace(
                                                    /\s/g, "")
                    }

                    RadioButton {
                        checked: true
                        text: "Type I"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: cutTypeButtonGroup
                    }
                    RadioButton {
                        text: "Type II"
                        font.pixelSize: 20 * dp
                        ButtonGroup.group: cutTypeButtonGroup
                    }
                }
                CustomLabel {
                    text: "Angle"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Angle")
                }
                TextField {
                    id: angleField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                    text: "60"
                }
                CustomLabel {
                    text: "degrees"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "degrees")
                }

                CustomLabel {
                    text: "Cut Rows"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    toolTip.text: getToolTipMsg("hole", "Cut_Rows")
                }

                ComboBox {
                    id: cutRowsComboBox
                    Layout.fillWidth: true
                    model: ["2", "3"]
                    Layout.columnSpan: 2
                }

                Image {
                    id: cutTypeImage
                    Layout.columnSpan: 3
                    source: "../../images/VCut" + cutTypeButtonGroup.checkedButton.text.replace(
                                /\s/g, "")
                    Layout.preferredHeight: 150 * dp
                    Layout.preferredWidth: 150 * dp
                    Layout.topMargin: 16 * dp
                    sourceSize.width: 512
                    sourceSize.height: 512
                    Layout.alignment: Qt.AlignCenter
                    fillMode: Image.PreserveAspectFit
                }
                CustomLabel {
                    text: "Length (X)"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    visible: cutTypeButtonGroup.checkedButton.text === "Type II"
                }
                TextField {
                    id: xLengthTextField
                    width: 100 * dp
                    horizontalAlignment: Qt.AlignLeft
                    Layout.topMargin: 16 * dp
                    text: "0.5"
                    visible: cutTypeButtonGroup.checkedButton.text === "Type II"
                }
                CustomLabel {
                    text: "m"
                    Layout.alignment: Qt.AlignLeft
                    Layout.leftMargin: 16 * dp
                    Layout.topMargin: 16 * dp
                    visible: cutTypeButtonGroup.checkedButton.text === "Type II"
                }
            }
        }
    }
}
