import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../js/toolTipMessages.js" as TooTipMsg

RowLayout {

    property real perimeter_explosive_diameter: parseFloat(
                                                    perimeterHoleDiameterButtonGroup.checkedButton.text)
    // js function
    function getToolTipMsg(type, tooltip) {
        var msgFuncs = TooTipMsg.message()
        var msgFunc
        var msgs
        var msg = ""
        switch (type) {
        case 'rock':
            msgFunc = msgFuncs.rock
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'explosive':
            msgFunc = msgFuncs.explosive
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        case 'hole':
            msgFunc = msgFuncs.hole
            msgs = msgFunc()
            msg = msgs[tooltip]
            break
        }
        return msg
    }

    CustomLabel {
        text: "Perimeter Explosive Diameter "
        Layout.leftMargin: 16 * dp
        toolTip.text: getToolTipMsg("explosive", "Perimetral_Diameter")
    }

    RowLayout {
        ButtonGroup {
            id: perimeterHoleDiameterButtonGroup
        }

        RadioButton {
            checked: true
            text: "22"
            font.pixelSize: 20 * dp
            ButtonGroup.group: perimeterHoleDiameterButtonGroup
        }
        RadioButton {
            text: "27"
            font.pixelSize: 20 * dp
            ButtonGroup.group: perimeterHoleDiameterButtonGroup
        }

        RadioButton {
            text: "30"
            font.pixelSize: 20 * dp
            ButtonGroup.group: perimeterHoleDiameterButtonGroup
        }
    }
}
