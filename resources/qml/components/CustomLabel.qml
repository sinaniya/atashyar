import QtQuick 2.0
import QtQuick.Controls 2.12

Text {
    property alias m_id: msa
    property alias toolTip: tltp
    color: "#ffa500"
    font.pixelSize: 20 * dp
    MouseArea {
        id: msa
        anchors.fill: parent
        hoverEnabled: true
    }
    ToolTip {
        id: tltp
        visible: msa.containsMouse
    }
}
