import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Rectangle {
    height: parent.height - 128*dp
    width: parent.width / 4
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.leftMargin: 32*dp
    anchors.topMargin: 48*dp
    anchors.bottomMargin: 96*dp
    color: "transparent"

    GridLayout {
        columns: 3
        anchors.fill: parent

        CustomLabel {
            text: "Cartridge diameter "
        }

        Item {
            Layout.fillWidth: true
            Layout.columnSpan: 2
        }

        CustomLabel {
            text: "Cut "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["explosive_diameter_cut"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            text: " mm"
        }

        CustomLabel {
            text: "Advance "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["explosive_diameter_advance"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            text: " mm"
        }

        CustomLabel {
            text: "Perimeter "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["explosive_perimetral_diameter"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            text: " mm"
        }

        Rectangle {
            Layout.fillWidth: true
            height: 2*dp
            color: "orange"
            Layout.columnSpan: 3
            Layout.margins: {
                top: 8*dp
                bottom: 8*dp
            }
        }

        CustomLabel {
            text: "Number of cartridges per hole "
        }

        Item {
            Layout.fillWidth: true
            Layout.columnSpan: 2
        }

        CustomLabel {
            text: "Cut "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["number_of_cartridges_per_hole_cut"]: "";
            font.pixelSize: 20*dp
        }
        Item {
            Layout.fillWidth: true
        }

        CustomLabel {
            text: "Advance "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["number_of_cartridges_advance_per_hole"]: "";
            font.pixelSize: 20*dp
        }
        Item {
            Layout.fillWidth: true
        }

        CustomLabel {
            text: "Perimeter "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["number_of_cartridges_perimeter_per_hole"]: "";
            font.pixelSize: 20*dp
        }
        Item {
            Layout.fillWidth: true
        }

        Rectangle {
            Layout.fillWidth: true
            height: 2*dp
            color: "orange"
            Layout.columnSpan: 3
            Layout.margins: {
                top: 8*dp
                bottom: 8*dp
            }
        }


        CustomLabel {
            text: "Number of holes "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["number_of_total_holes"]: "";
            font.pixelSize: 20*dp
        }
        Item {
            Layout.fillWidth: true
        }

        CustomLabel {
            text: "Total explosive emulsion"
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["total_explosive_emulsion"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            text: " Kg"
        }

        CustomLabel {
            text: "Total explosive anfo"
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["total_explosive_anfo"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            text: " Kg"
        }

        CustomLabel {
            text: "Specific explosive emulsion"
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["specific_explosive_emulsion"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            textFormat: Text.RichText
            text: " Kg/m<sup>3</sup>"
        }

        CustomLabel {
            text: "Specific explosive anfo"
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["specific_explosive_anfo"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            textFormat: Text.RichText
            text: " Kg/m<sup>3</sup>"
        }

        CustomLabel {
            text: "Specific drilling "
        }
        TextField {
            text: typeof  result !== 'undefined' ?  result["specific_drilling"].toFixed(2): "";
            font.pixelSize: 20*dp
        }
        CustomLabel {
            textFormat: Text.RichText
            text: " m/m<sup>3</sup>"
        }


        Item {
            Layout.fillHeight: true
        }
    }
}
