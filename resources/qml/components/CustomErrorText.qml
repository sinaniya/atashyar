import QtQuick 2.0
import QtQuick.Controls 1.2

Text {
    color: "Red"
    font.pixelSize: (20 * dp)
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    LayoutMirroring.enabled: true
    width: (parent.width) - (20 * dp)
    wrapMode: Text.Wrap
}
