function message() {
    var rock = function () {
        return {
            "Rock_Type": "نوع سنگ",
            "Impedance": "امپدانس ماده منفجره",
            "Specific_Surface_Energy": "انرژی سطحی مخصوص سنگ",
            "Tension_Strength": "مقاومت کششی سنگ",
            "Density": "چگالی سنگ",
            "Wave's_Velocity": "سرعت موج سنگ"
        }
    }

    var explosive = function () {
        return {
            "Explosive_Type": "نوع ماده منفجره",
            "Diameter": "قطر ماده منفجره",
            "Impedance": "امپدانس ماده منفجره",
            "Perimetral_Diameter": "قطر ماده منفجره محیطی",
            "Specific_Energy": "انرژی مخصوص ماده منفجره",
            "Explosion_Pressure": "فشار انفجار",
            "Density": "چگالی ماده منفجره",
            "Velocity_of_Detonation": "سرعت انفجار",
            "Contour_Blasting": "آتش‌ کاری پیرامون",
            "Contour_Explosive_Type": "نوع انفجار پیرامون",
            "Contour_Explosive_Diameter": "قطر انفجار پیرامون",
            "Blastability": "قابلیت انفجار",
            "Skill_level": "سطح مهارت",
            "Mainـexplosive": "ماده منفجره اصلی",
            "Main_explosive_density": "چگالی ماده منفجره اصلی"
        }
    }

    var hole = function () {
        return {
            "Hole_Diameter": "قطر چال",
            "Airhole_Diameter": "قطر چال خالی",
            "Advance": "میزان پیشروی",
            "DesiredـAmountـofـCrushing": "مقدار مطلوب چال",
            "Crushing": "خردشدگی",
            "Wedge_Cut": "برش گوه ای",
            "Angle": "زاویه",
            "degrees": "درجه",
            "Cut_Rows": "تعداد ردیف (برش گوه ای)",
            "Hole_length": "طول چال"
        }
    }

    return {
        "rock": rock,
        "explosive": explosive,
        "hole": hole
    }
}
