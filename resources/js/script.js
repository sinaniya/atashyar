.pragma library
// Connecting ErrorMessage.js
.import "errorMessage.js" as ErrorMsg

// js function
function isInclude(key, obj) {
    if (obj.hasOwnProperty(key))
        return true
    else
        return false
}

function isEmpty(field) {
    var returnValue
    switch (true) {
    case isNaN(field):
        returnValue = true
        break
    case field === null:
        returnValue = true
        break
    case field === "":
        returnValue = true
        break
    case field === "0":
        returnValue = true
        break
    case typeof (field) === "undefined":
        returnValue = true
        break
    default:
        returnValue = false
    }

    return returnValue
}

function getArea(properties) {
    var area = 0
    var shapeName = properties['shape_name']
    switch (shapeName) {
    case "Arched Roof 1":
        var theta = Math.asin(properties['width'] / (2 * properties['radius']))
        var h = properties['height'] - properties['radius'] * (1 - Math.cos(
                                                                   theta))
        area = h * properties['width']
                + (((Math.PI * Math.pow(
                         properties['radius'], 2) * 2 * theta) / (2 * Math.PI))
                   - (((h + properties['radius']
                        - properties['height']) * properties['width']) / 2))
        break
    case "Arched Roof 2":
        var w = properties['width']
        var h1 = properties['abutment']
        var h2 = properties['arch']
        var r = (h2 / 2) + Math.pow(w, 2) / (8 * h2)
        var theta = Math.asin(w / (2 * r))
        var h = h1 - r * (1 - Math.cos(theta))
        area = h * w + (((Math.PI * Math.pow(
                              r, 2) * 2 * theta) / (2 * Math.PI)) - (((h + r - h1) * w) / 2))
        break
    case "Circle":
        r = properties['radius']
        area = Math.PI * Math.pow(r, 2)
        break
    case "D-Shape":
        r = properties['width'] / 2
        w = properties['width']
        h = properties['height']
        area = (w * h) + Math.PI * Math.pow(r, 2)
        break
    case "HorseShoe":
        r = properties['radius']
        h = properties['height']
        area = 10000
        break
    case "Rectangle":
        w = properties['width']
        h = properties['height']
        area = (w * h)
        break
    }
    //    console.log(area)
    return area
}

function archedRoof1_Validation(properties) {
    var w = properties["width"]
    var r = properties["radius"]
    var h = properties["height"]
    var errors = {}
    var e1, e2, e3, e4, e5
    e1 = ErrorMsg.message().E1
    e2 = ErrorMsg.message().E2
    e3 = ErrorMsg.message().E3
    e4 = ErrorMsg.message().E4
    e5 = ErrorMsg.message().E5
    if (isEmpty(w) || isEmpty(r) || isEmpty(h))
        Object.assign(errors, {
                          "E1": e1
                      })

    if (w <= 0)
        Object.assign(errors, {
                          "E2": e2
                      })
    if (r <= 0)
        Object.assign(errors, {
                          "E3": e3
                      })
    if (h <= 0)
        Object.assign(errors, {
                          "E3": e4
                      })
    if (r <= (w / 2))
        Object.assign(errors, {
                          "E5": e5
                      })

    return errors
}
function archedRoof2_Validation(properties) {
    var w = properties["width"]
    var h1 = properties["abutment"]
    var h2 = properties["arch"]
    var errors = {}
    var e1, e2, e6, e7, e8
    e1 = ErrorMsg.message().E1
    e2 = ErrorMsg.message().E2
    e6 = ErrorMsg.message().E6
    e7 = ErrorMsg.message().E7
    e8 = ErrorMsg.message().E8
    if (isEmpty(w) || isEmpty(h1) || isEmpty(h2))
        Object.assign(errors, {
                          "E1": e1
                      })

    if (w <= 0)
        Object.assign(errors, {
                          "E2": e2
                      })
    if (h1 <= 0)
        Object.assign(errors, {
                          "E6": e6
                      })
    if (h2 <= 0)
        Object.assign(errors, {
                          "E7": e7
                      })
    if (h2 > (w / 2))
        Object.assign(errors, {
                          "E8": e8
                      })
    return errors
}
function circle_Validation(properties) {
    var r = properties["radius"]
    var errors = {}
    var e1, e3, e9
    e1 = ErrorMsg.message().E1
    e3 = ErrorMsg.message().E3
    e9 = ErrorMsg.message().E9
    if (isEmpty(r)) {
        Object.assign(errors, {
                          "E1": e1
                      })
    }

    if (r <= 0)
        Object.assign(errors, {
                          "E4": e3
                      })

    if (r > 7 || r < 0.5)
        Object.assign(errors, {
                          "E9": e9
                      })
    return errors
}
function dShape_Validation(properties) {
    var w = properties["width"]
    var h = properties["height"] + w / 2
    var errors = {}
    var e1, e2, e4, e10
    e1 = ErrorMsg.message().E1
    e2 = ErrorMsg.message().E2
    e4 = ErrorMsg.message().E4
    e10 = ErrorMsg.message().E10
    if (isEmpty(w) || isEmpty(h)) {
        Object.assign(errors, {
                          "E1": e1
                      })
    }

    if (w <= 0)
        Object.assign(errors, {
                          "E2": e2
                      })
    if (h <= 0)
        Object.assign(errors, {
                          "E3": e4
                      })

    if (w < h / 2 || w > 3 * h)
        Object.assign(errors, {
                          "E10": e10
                      })

    //    console.log(errors["E-empty"])
    return errors
}
function horseShoe_Validation(properties) {
    var w = properties["radius"]
    var h = properties["height"]
    var errors = {}
    //    var e1, e2, e4, e10
    //    e1 = ErrorMsg.message().E1
    //    e2 = ErrorMsg.message().E2
    //    e4 = ErrorMsg.message().E4
    //    e10 = ErrorMsg.message().E10
    //    if (isEmpty(w) || isEmpty(h)) {
    //        Object.assign(errors, {
    //                          "E1": e1
    //                      })
    //    }

    //    if (w <= 0)
    //        Object.assign(errors, {
    //                          "E2": e2
    //                      })
    //    if (h <= 0)
    //        Object.assign(errors, {
    //                          "E3": e4
    //                      })

    //    if (w < h / 2 || w > 3 * h)
    //        Object.assign(errors, {
    //                          "E10": e10
    //                      })

    //    console.log(errors["E-empty"])
    return errors
}
function rectAngle_Validation(properties) {
    var w = properties["width"]
    var h = properties["height"]
    var errors = {}
    var e1, e2, e4, e10
    e1 = ErrorMsg.message().E1
    e2 = ErrorMsg.message().E2
    e4 = ErrorMsg.message().E4
    e10 = ErrorMsg.message().E10
    if (isEmpty(w) || isEmpty(h))
        Object.assign(errors, {
                          "E1": e1
                      })

    if (w <= 0)
        Object.assign(errors, {
                          "E2": e2
                      })
    if (h <= 0)
        Object.assign(errors, {
                          "E3": e4
                      })

    if (w < h / 2 || w > 3 * h)
        Object.assign(errors, {
                          "E10": e10
                      })
    return errors
}

function secondPage() {
    var ebm_validation = function (properties) {
        var errors = {}

        // check the isEmpty()
        var e1, e11
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11

        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue
            if (isEmpty(properties[key]))
                Object.assign(errors, {
                                  "E1": e1
                              })

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }

        // after checking isEmpty()
        var ri = properties["rock_impedance"]
        var rsse = properties["rock_specific_surface_energy"]
        var rts = properties["rock_tension_strength"]
        var rd = properties["rock_density"]
        var wv = properties["wave_velocity"]
        var et = properties["explosive_type"]
        var ei = properties["explosive_impedance"]
        var ed = properties["explosive_diameter"]
        var epd = properties["explosive_perimetral_diameter"]
        var ese = properties["explosive_specific_energy"]
        var eep = properties["explosive_explosion_pressure"]
        var eDensity = properties["explosive_density"]
        var ev = properties["explosive_velocity"]
        var hd = properties["hole_diameter"]
        var ahd = properties["air_hole_diameter"]
        var a = properties["advance"]
        var c = properties["crushing"]

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        var width = properties["width"]
        var radius = width / 2
        var height = properties["height"]
        var area = getArea(properties)

        // Get Require Errors Msg
        var e12 = ErrorMsg.message().E12
        var e13 = ErrorMsg.message().E13
        var e14 = ErrorMsg.message().E14
        var e15 = ErrorMsg.message().E15
        var e16 = ErrorMsg.message().E16
        var e17 = ErrorMsg.message().E17
        var e18 = ErrorMsg.message().E18
        var e19 = ErrorMsg.message().E19
        var e20 = ErrorMsg.message().E20
        var e21 = ErrorMsg.message().E21
        var e22 = ErrorMsg.message().E22
        var e23 = ErrorMsg.message().E23
        var e24 = ErrorMsg.message().E24
        var e25 = ErrorMsg.message().E25
        var e26 = ErrorMsg.message().E26

        if (a <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((a >= ((3 / 4) * width)) || (a >= ((3 / 4) * Math.sqrt(area)))
                || (a >= 5))
            Object.assign(errors, {
                              "E13": e13
                          })

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check rock_density
        // 1800 < rock-density < 5000
        if (rd < 1800 || rd > 5000)
            Object.assign(errors, {
                              "E15": e15
                          })

        // check wave velocity
        // 1500 < wave_velocity< 7500 ---> m/s
        if (wv < 1500 || wv > 7500)
            Object.assign(errors, {
                              "E16": e16
                          })
        // check rock_impedanace
        // 2.7 < rock_impdenace< 37.5
        if (ri < 2.7 || ri > 37.5)
            Object.assign(errors, {
                              "E17": e17
                          })

        // check rock_specific_surface_energy
        // 1.10 < rock_specific_surface_energy < ۳
        if (rsse < 1.10 || rsse > 3)
            Object.assign(errors, {
                              "E18": e18
                          })

        // check rock_tension_strength
        // 2 < rock_tension_strength < ۳۵
        if (rts < 2 || rts > 35)
            Object.assign(errors, {
                              "E19": e19
                          })

        // check explosive_density: دانسیته ماده منفجره
        // 600 < explosive_density < 1700 ---> mm
        if (eDensity < 600 || eDensity > 1700)
            Object.assign(errors, {
                              "E20": e20
                          })

        // check explosive_velocity
        // 2000 < explosive_velocity < 20_000
        if (ev < 2000 || ev > 20000)
            Object.assign(errors, {
                              "E21": e21
                          })

        // check explosive_impedance
        // 1.2 < explosive_impedance < 34
        if (ei < 1.2 || ei > 34)
            Object.assign(errors, {
                              "E22": e22
                          })

        // check explosive_specific_energy
        // 2 < explosive_specific_energy < 10
        if (ese <= 2 || ese > 10)
            Object.assign(errors, {
                              "E23": e23
                          })

        // check explosive_explosion_pressure
        // 350 < explosive_explosion_pressure < 25000
        if (eep < 350 || eep > 25000)
            Object.assign(errors, {
                              "E24": e24
                          })

        // check explosive_diameter
        // ۲۷ < explosive_diameter < hole-diameter
        if (ed < 27 || ed > hd)
            Object.assign(errors, {
                              "E25": e25
                          })

        // check explosive_perimetral_diameter
        // 15 < explosive_perimetral_diameter < 40
        if (epd < 15 || epd > 40 || epd > hd)
            Object.assign(errors, {
                              "E26": e26
                          })
        return errors
    }
    var ebm_angular_validation = function (properties) {
        var errors = {}

        var e1, e11
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11

        // check isEmpty()
        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue
            if (isEmpty(properties[key])) {
                Object.assign(errors, {
                                  "E1": e1
                              })
            }

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }
        // after check the isEmpty()
        var ri = properties["rock_impedance"]
        var rsse = properties["rock_specific_surface_energy"]
        var rts = properties["rock_tension_strength"]
        var rd = properties["rock_density"]
        var rwv = properties["rock_wave_velocity"]
        var et = properties["explosive_type"]
        var ei = properties["explosive_impedance"]
        var ed = properties["explosive_diameter"]
        var epd = properties["explosive_perimetral_diameter"]
        var ese = properties["explosive_specific_energy"]
        var eep = properties["explosive_explosion_pressure"]
        var eDensity = properties["explosive_density"]
        var ev = properties["explosive_velocity"]
        var hd = properties["hole_diameter"]
        var a = properties["advance"]
        //        var c = properties["crushing"]
        var ct = properties["cut_type"]
        var angle = properties["angle"]
        //        var cr = properties["cut_rows"]
        var xl = properties["x_length"]

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        var width = properties["width"]
        var height = properties["height"]
        var area = getArea(properties)

        // Get Require Errors Msg
        var e12 = ErrorMsg.message().E12
        var e30 = ErrorMsg.message().E30
        var e14 = ErrorMsg.message().E14
        var e15 = ErrorMsg.message().E15
        var e16 = ErrorMsg.message().E16
        var e17 = ErrorMsg.message().E17
        var e18 = ErrorMsg.message().E18
        var e19 = ErrorMsg.message().E19
        var e20 = ErrorMsg.message().E20
        var e21 = ErrorMsg.message().E21
        var e22 = ErrorMsg.message().E22
        var e23 = ErrorMsg.message().E23
        var e24 = ErrorMsg.message().E24
        var e25 = ErrorMsg.message().E25
        var e26 = ErrorMsg.message().E26
        var e27 = ErrorMsg.message().E27
        var e28 = ErrorMsg.message().E28
        var e29 = ErrorMsg.message().E29

        if (a <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((a >= ((1 / 2) * width)) || (a >= ((1 / 2) * Math.sqrt(area)))
                || (a >= 5))
            Object.assign(errors, {
                              "E30": e30
                          })

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check rock_density
        // 1800 < rock-density < 5000
        if (rd < 1800 || rd > 5000)
            Object.assign(errors, {
                              "E15": e15
                          })

        // check wave velocity
        // 1500 < wave_velocity< 7500 ---> m/s
        if (rwv < 1500 || rwv > 7500)
            Object.assign(errors, {
                              "E16": e16
                          })
        // check rock_impedanace
        // 2.7 < rock_impdenace< 37.5
        if (ri < 2.7 || ri > 37.5)
            Object.assign(errors, {
                              "E17": e17
                          })

        // check rock_specific_surface_energy
        // 1.10 < rock_specific_surface_energy < ۳
        if (rsse < 1.10 || rsse > 3)
            Object.assign(errors, {
                              "E18": e18
                          })

        // check rock_tension_strength
        // 2 < rock_tension_strength < ۳۵
        if (rts < 2 || rts > 35)
            Object.assign(errors, {
                              "E19": e19
                          })

        // check explosive_density: دانسیته ماده منفجره
        // 600 < explosive_density < 1700 ---> mm
        if (eDensity < 600 || eDensity > 1700)
            Object.assign(errors, {
                              "E20": e20
                          })

        // check explosive_velocity
        // 2000 < explosive_velocity < 20_000
        if (ev < 2000 || ev > 20000)
            Object.assign(errors, {
                              "E21": e21
                          })

        // check explosive_impedance
        // 1.2 < explosive_impedance < 34
        if (ei < 1.2 || ei > 34)
            Object.assign(errors, {
                              "E22": e22
                          })

        // check explosive_specific_energy
        // 2 < explosive_specific_energy < 10
        if (ese <= 2 || ese > 10)
            Object.assign(errors, {
                              "E23": e23
                          })

        // check explosive_explosion_pressure
        // 350 < explosive_explosion_pressure < 25000
        if (eep < 350 || eep > 25000)
            Object.assign(errors, {
                              "E24": e24
                          })

        // check explosive_diameter
        // ۲۷ < explosive_diameter < hole-diameter
        if (ed < 27 || ed > hd)
            Object.assign(errors, {
                              "E25": e25
                          })

        // check explosive_perimetral_diameter
        // 15 < explosive_perimetral_diameter < 40
        if (epd < 15 || epd > 40 || epd > hd)
            Object.assign(errors, {
                              "E26": e26
                          })

        if (ct === "Type I") {
            // angle must be in range (40-60)
            if (angle < 40 || angle > 60)
                Object.assign(errors, {
                                  "E27": e27
                              })
        } else {
            // angle must be in range (60-70)
            // x-length = 23 * hd
            var xl_mm = (xl * 1000)
            if (angle < 60 || angle > 70)

                Object.assign(errors, {
                                  "E28": e28
                              })
            if (xl_mm > 23 * hd || xl_mm < 0)
                Object.assign(errors, {
                                  "E29": e29
                              })
        }

        return errors
    }
    var gustafsson_validation = function (properties) {
        var errors = {}

        var e1, e11, e12, e13, e14, e20, e27
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11
        e12 = ErrorMsg.message().E12
        e13 = ErrorMsg.message().E13
        e14 = ErrorMsg.message().E14
        e20 = ErrorMsg.message().E20
        e27 = ErrorMsg.message().E27

        // check isEmpty()
        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue

            if (isEmpty(properties[key]))
                Object.assign(errors, {
                                  "E1": e1
                              })

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }
        // after checking isEmpty()
        var eDensity = properties["explosive_density"]
        var hd = properties["hole_diameter"]
        var a = properties["advance"]
        var angle = properties["angle"]

        var width = properties["width"]
        var height = properties["height"]
        var area = getArea(properties)

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        if (a <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((a >= ((3 / 4) * width)) || (a >= ((3 / 4) * Math.sqrt(area)))
                || (a >= 5))
            Object.assign(errors, {
                              "E13": e13
                          })

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check explosive_density: دانسیته ماده منفجره
        // 600 < explosive_density < 1700 ---> mm
        if (eDensity < 600 || eDensity > 1700)
            Object.assign(errors, {
                              "E20": e20
                          })

        // angle must be in range (40-60)
        if (angle < 40 || angle > 60)
            Object.assign(errors, {
                              "E27": e27
                          })

        return errors
    }
    var konya_validation = function (properties) {
        var errors = {}

        var e1, e11
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11

        // check isEmpty()
        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue

            if (isEmpty(properties[key]))
                Object.assign(errors, {
                                  "E1": e1
                              })

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }

        // after checking isEmpty()
        var eDensity = properties["explosive_density"]
        var hd = properties["hole_diameter"]
        var a = properties["advance"]
        var rd = properties["rock_density"]
        var cb = properties["contour_blasting"]
        var cet = properties["contour_explosive_type"]
        var ped = properties["perimeter_explosive_diameter"]

        var width = properties["width"]
        var height = properties["height"]
        var area = getArea(properties)

        // Get Require Errors Msg
        var e12 = ErrorMsg.message().E12
        var e13 = ErrorMsg.message().E13
        var e14 = ErrorMsg.message().E14
        var e15 = ErrorMsg.message().E15
        var e20 = ErrorMsg.message().E20
        var e26 = ErrorMsg.message().E26

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        if (a <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((a >= ((3 / 4) * width)) || (a >= ((3 / 4) * Math.sqrt(area)))
                || (a >= 5))
            Object.assign(errors, {
                              "E13": e13
                          })

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check rock_density
        // 1800 < rock-density < 5000
        if (rd < 1800 || rd > 5000)
            Object.assign(errors, {
                              "E15": e15
                          })

        // check explosive_density: دانسیته ماده منفجره
        // 600 < explosive_density < 1700 ---> mm
        if (eDensity < 600 || eDensity > 1700)
            Object.assign(errors, {
                              "E20": e20
                          })

        // check explosive_perimetral_diameter
        // 15 < explosive_perimetral_diameter < 40
        if (ped < 15 || ped > 40 || ped > hd)
            Object.assign(errors, {
                              "E26": e26
                          })

        return errors
    }
    var konya_angular_validation = function (properties) {
        var errors = {}

        var e1, e11
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11

        // check isEmpty()
        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue

            if (isEmpty(properties[key]))
                Object.assign(errors, {
                                  "E1": e1
                              })

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }

        // after checking isEmpty()
        var eDensity = properties["explosive_density"]
        var hd = properties["hole_diameter"]
        var a = properties["advance"]
        var rd = properties["rock_density"]
        var ped = properties["perimeter_explosive_diameter"]
        var angle = properties["angle"]

        var width = properties["width"]
        var height = properties["height"]
        var area = getArea(properties)

        // Get Require Errors Msg
        var e12 = ErrorMsg.message().E12
        var e30 = ErrorMsg.message().E30
        var e14 = ErrorMsg.message().E14
        var e15 = ErrorMsg.message().E15
        var e20 = ErrorMsg.message().E20
        var e26 = ErrorMsg.message().E26
        var e27 = ErrorMsg.message().E27

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        if (a <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((a >= ((1 / 2) * width)) || (a >= ((1 / 2) * Math.sqrt(area)))
                || (a >= 5))
            Object.assign(errors, {
                              "E30": e30
                          })

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check rock_density
        // 1800 < rock-density < 5000
        if (rd < 1800 || rd > 5000)
            Object.assign(errors, {
                              "E15": e15
                          })

        // check explosive_density: دانسیته ماده منفجره
        // 600 < explosive_density < 1700 ---> mm
        if (eDensity < 600 || eDensity > 1700)
            Object.assign(errors, {
                              "E20": e20
                          })

        // check explosive_perimetral_diameter
        // 15 < explosive_perimetral_diameter < 40
        if (ped < 15 || ped > 40 || ped > hd)
            Object.assign(errors, {
                              "E26": e26
                          })

        // angle must be in range (40-60)
        if (angle < 40 || angle > 60)
            Object.assign(errors, {
                              "E27": e27
                          })

        return errors
    }
    var ntnu_validation = function (properties) {
        var errors = {}

        var e1, e11
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11

        // check isEmpty()
        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue

            if (isEmpty(properties[key]))
                Object.assign(errors, {
                                  "E1": e1
                              })

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }

        // after checing isEmpty()
        var hd = properties["hole_diameter"]
        var hl = properties["hole_length"]
        var rd = properties["rock_density"]
        var pe = properties["primeter_explosive"]

        var width = properties["width"]
        var height = properties["height"]
        var area = getArea(properties)

        // Get Require Errors Msg
        var e12 = ErrorMsg.message().E12
        var e13 = ErrorMsg.message().E13
        var e14 = ErrorMsg.message().E14
        var e15 = ErrorMsg.message().E15
        var e26 = ErrorMsg.message().E26

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        if (hl <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((hl >= ((3 / 4) * width)) || (hl >= ((3 / 4) * Math.sqrt(area)))
                || (hl >= 5))
            Object.assign(errors, {
                              "E13": e13
                          })

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check rock_density
        // 1800 < rock-density < 5000
        if (rd < 1800 || rd > 5000)
            Object.assign(errors, {
                              "E15": e15
                          })

        // check explosive_perimetral_diameter
        // 15 < explosive_perimetral_diameter < 40
        if (pe < 15 || pe > 40 || pe > hd)
            Object.assign(errors, {
                              "E26": e26
                          })
        return errors
    }
    var olafsson_validation = function (properties) {
        var errors = {}

        var e1, e11
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11

        // check the isEmpty()
        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue

            if (isEmpty(properties[key]))
                Object.assign(errors, {
                                  "E1": e1
                              })

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }

        // after checing isEmpty()
        var a = properties["advance"]
        var eDensity = properties["explosive_density"]
        var hd = properties["hole_diameter"]

        var width = properties["width"]
        var height = properties["height"]
        var area = getArea(properties)

        var e12 = ErrorMsg.message().E12
        var e13 = ErrorMsg.message().E13
        var e14 = ErrorMsg.message().E14
        var e20 = ErrorMsg.message().E20

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        if (a <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((a > ((3 / 4) * width)) || (a > ((3 / 4) * Math.sqrt(area)))
                || (a > 5)) {
            Object.assign(errors, {
                              "E13": e13
                          })
        }

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check explosive_density: دانسیته ماده منفجره
        // 600 < explosive_density < 1700 ---> mm
        if (eDensity < 600 || eDensity > 1700)
            Object.assign(errors, {
                              "E20": e20
                          })

        return errors
    }
    var olafsson_angular_validation = function (properties) {
        var errors = {}

        var e1, e11
        e1 = ErrorMsg.message().E1
        e11 = ErrorMsg.message().E11

        // check isEmpty()
        for (const key in properties) {
            if (typeof properties[key] === 'string'
                    || properties[key] instanceof String)
                continue

            if (isEmpty(properties[key]))
                Object.assign(errors, {
                                  "E1": e1
                              })

            if (properties[key] <= 0)
                Object.assign(errors, {
                                  "E11": e11
                              })
        }
        // after checking isEmpty()
        var a = properties["advance"]
        var eDensity = properties["explosive_density"]
        var hd = properties["hole_diameter"]
        var angle = properties["angle"]

        var width = properties["width"]
        var height = properties["height"]
        var area = getArea(properties)

        var e12 = ErrorMsg.message().E12
        var e30 = ErrorMsg.message().E30
        var e14 = ErrorMsg.message().E14
        var e20 = ErrorMsg.message().E20

        // check advance(پیشروی)
        // minimum: 75 cm
        // maximum: min(3/4w, 3/4sqr(A), 5m)
        if (a <= 0.75)
            Object.assign(errors, {
                              "E12": e12
                          })
        if ((a > ((1 / 2) * width)) || (a > ((1 / 2) * Math.sqrt(area)))
                || (a > 5))
            Object.assign(errors, {
                              "E30": e30
                          })

        // check hole_diameter: قطر چال
        // 30 < hole_diameter < 76 ---> mm
        if (hd < 30 || hd > 76)
            Object.assign(errors, {
                              "E14": e14
                          })

        // check explosive_density: دانسیته ماده منفجره
        // 600 < explosive_density < 1700 ---> mm
        if (eDensity < 600 || eDensity > 1700)
            Object.assign(errors, {
                              "E20": e20
                          })
        // angle must be in range (40-60)
        if (angle < 40 || angle > 60)
            Object.assign(errors, {
                              "E27": e27
                          })

        return errors
    }
    return {
        "olafsson_angular_validation": olafsson_angular_validation,
        "olafsson_validation": olafsson_validation,
        "ntnu_validation": ntnu_validation,
        "konya_angular_validation": konya_angular_validation,
        "konya_validation": konya_validation,
        "gustafsson_validation": gustafsson_validation,
        "ebm_angular_validation": ebm_angular_validation,
        "ebm_validation": ebm_validation
    }
}

function helper() {
    var clearErrDlg = function (id) {
        for (var i = id.dialogContentCol.children.length; i > 0; i--) {
            //                        console.log("destroying: " + i)
            id.dialogContentCol.children[i - 1].destroy()
        }
    }

    var getErrorsByShapeInputName = function (shapeInputName, props) {
        var errors = {}
        switch (shapeInputName) {
        case "Arched Roof 1":
            errors = archedRoof1_Validation(props)
            break
        case "Arched Roof 2":
            errors = archedRoof2_Validation(props)
            break
        case "Circle":
            errors = circle_Validation(props)
            break
        case "D-Shape":
            errors = dShape_Validation(props)
            break
        case "Rectangle":
            errors = rectAngle_Validation(props)
            break
        default:
            errors = dShape_Validation(props)
        }
        return errors
    }

    var wrap_dir = function (dir, str) {
        if (dir === 'rtl')
            return '\u202B' + str + '\u202C'
        return '\u202A' + str + '\u202C'
    }

    var createErrors = function (errors, errDlgId) {
        for (const key in errors) {
            //                        console.log(key + " : " + errors[key])
            var component
            var newObj
            var keyStr = key
            var valueStr = errors[key]
            var concatStr = " : "
            // persian and english concat
            var finalStr = wrap_dir('rtl', keyStr) + wrap_dir(
                        'rtl', concatStr) + wrap_dir('rtl', valueStr)
            component = Qt.createComponent(
                          "../qml/components/CustomErrorText.qml")
            newObj = component.createObject(errDlgId.dialogContentCol, {
                                                "text": finalStr
                                            })
        }
    }

    return {
        "clearErrDlg": clearErrDlg,
        "getErrorsByShapeInputName": getErrorsByShapeInputName,
        "wrap_dir": wrap_dir,
        "createErrors": createErrors
    }
}
