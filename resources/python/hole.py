from typing import Tuple
from enum import Enum


class HoleType(Enum):
    ADVANCE = 1
    PERIMETER = 2
    CUT = 3
    UNCHARGED = 4
    FLOOR = 5
    BUFFER = 6


HOLES_TYPE_TO_STRING_MAP = {
    HoleType.ADVANCE: "Advance",
    HoleType.PERIMETER: "Perimeter",
    HoleType.CUT: "Cut",
    HoleType.UNCHARGED: "Uncharged",
    HoleType.FLOOR: "Floor",
    HoleType.BUFFER: "Buffer",
}

HOLES_TYPE_TO_COLOR_MAP = {
    HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]: "#9C27B0",
    HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]: "#3F51B5",
    HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]: "#F44336",
    HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]: "#9E9E9E",
    HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]: "#795548",
    HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]: "#FF9800",
}


class Hole:
    def __init__(
        self,
        coordinates: Tuple[float, float],
        hole_type: str = "",
        hole_length: float = 0,
        hole_diameter: float = 0,
        explosive_type: str = "",
        hole_end_length: float = 0,
        hole_end_concentration: float = 0,
        hole_middle_length: float = 0,
        hole_middle_concentration: float = 0,
        stemming: float = 0,
        primer_type: str = "",
        angle: float = 0,
    ) -> None:
        self.coordinates = coordinates
        self.hole_type = hole_type
        self.hole_length = hole_length
        self.hole_diameter = hole_diameter
        self.explosive_type = explosive_type
        self.hole_end_length = hole_end_length
        self.hole_end_concentration = hole_end_concentration
        self.hole_middle_length = hole_middle_length
        self.hole_middle_concentration = hole_middle_concentration
        self.stemming = stemming
        self.primer_type = primer_type
        self.angle = angle

    @property
    def coordinates(self) -> Tuple[float, float]:
        return self._coordinates

    @coordinates.setter
    def coordinates(self, value: Tuple[float, float]) -> None:
        self._coordinates = value

    @property
    def hole_type(self) -> str:
        return self._hole_type

    @hole_type.setter
    def hole_type(self, value: str) -> None:
        self._hole_type = value

    @property
    def hole_length(self) -> float:
        return self._hole_length

    @hole_length.setter
    def hole_length(self, value: float) -> None:
        self._hole_length = value

    @property
    def hole_diameter(self) -> float:
        return self._hole_diameter

    @hole_diameter.setter
    def hole_diameter(self, value: float) -> None:
        self._hole_diameter = value

    @property
    def explosive_type(self) -> str:
        return self._explosive_type

    @explosive_type.setter
    def explosive_type(self, value: str) -> None:
        self._explosive_type = value

    @property
    def hole_end_length(self) -> float:
        return self._hole_end_length

    @hole_end_length.setter
    def hole_end_length(self, value: float) -> None:
        self._hole_end_length = value

    @property
    def hole_end_concentration(self) -> float:
        return self._hole_end_concentration

    @hole_end_concentration.setter
    def hole_end_concentration(self, value: float) -> None:
        self._hole_end_concentration = value

    @property
    def hole_middle_length(self) -> float:
        return self._hole_middle_length

    @hole_middle_length.setter
    def hole_middle_length(self, value: float) -> None:
        self._hole_middle_length = value

    @property
    def hole_middle_concentration(self) -> float:
        return self._hole_middle_concentration

    @hole_middle_concentration.setter
    def hole_middle_concentration(self, value: float) -> None:
        self._hole_middle_concentration = value

    @property
    def stemming(self) -> float:
        return self._stemming

    @stemming.setter
    def stemming(self, value: float) -> None:
        self._stemming = value

    @property
    def primer_type(self) -> str:
        return self._primer_type

    @primer_type.setter
    def primer_type(self, value: str) -> None:
        self._primer_type = value

    @property
    def primer_diameter(self) -> float:
        return self._primer_diameter

    @primer_diameter.setter
    def primer_diameter(self, value: float) -> None:
        self._primer_diameter = value

    @property
    def primer_weight(self) -> float:
        return self._primer_weight

    @primer_weight.setter
    def primer_weight(self, value: float) -> None:
        self._primer_weight = value

    @property
    def primer_length(self) -> float:
        return self._primer_length

    @primer_length.setter
    def primer_length(self, value: float) -> None:
        self._primer_length = value

    @property
    def angle(self) -> float:
        return self._angle

    @angle.setter
    def angle(self, value: float) -> None:
        self._angle = value
