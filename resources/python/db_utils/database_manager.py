from PyQt5.QtCore import QFile
from PyQt5.QtSql import QSqlDatabase

from .rocks_db import RocksDB
from .explosives_db import ExplosivesDB


class DatabaseManager:
    def __init__(self):
        self.rocks_db = RocksDB()
        self.explosives_db = ExplosivesDB()

    def create_tables(self):
        self.rocks_db.create_table()
        self.explosives_db.create_table()

    def connect_to_database(self):
        db_file = "db.sqlite"

        database = QSqlDatabase.database()
        if not database.isValid():
            database = QSqlDatabase.addDatabase("QSQLITE")
            if not database.isValid():
                return

        database.setDatabaseName(db_file)

        if not database.open():
            QFile.remove(db_file)

        self.create_tables()
