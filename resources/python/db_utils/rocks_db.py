from PyQt5.QtCore import QObject, pyqtSlot, QJsonValue
from PyQt5.QtSql import QSqlDatabase, QSqlQuery

from .default_rocks import default_rocks


TABLE_NAME = "Rocks"


class RocksDB(QObject):
    def create_table(self):
        if TABLE_NAME in QSqlDatabase.database().tables():
            return

        query = QSqlQuery()
        if not query.exec_(
            """
                CREATE TABLE IF NOT EXISTS 'Rocks' (
                    'id' INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL,
                    'name' VARCHAR(50) NOT NULL,
                    'impedance' NUMERIC NOT NULL,
                    'specific_surface_energy' NUMERIC NOT NULL,
                    'tension_strength' NUMERIC NOT NULL,
                    'density' NUMERIC NOT NULL,
                    'wave_velocity' NUMERIC NOT NULL,
                    'created' VARCHAR(16) NOT NULL
                )
            """
        ):
            return

        self.seed_table()

    def seed_table(self):
        query = QSqlQuery()
        query.exec_("SELECT id FROM Rocks")
        if not query.first():
            query.prepare(
                """
            INSERT INTO Rocks (
                name,
                impedance,
                specific_surface_energy,
                tension_strength,
                density,
                wave_velocity,
                created
            ) VALUES (?, ?, ?, ?, ?, ?, ?)
            """
            )

            for default_rock in default_rocks:
                query.bindValue(0, default_rock["name"])
                query.bindValue(1, default_rock["impedance"])
                query.bindValue(2, default_rock["specific_surface_energy"])
                query.bindValue(3, default_rock["tension_strength"])
                query.bindValue(4, default_rock["density"])
                query.bindValue(5, default_rock["wave_velocity"])
                query.bindValue(6, default_rock["created"])
                query.exec_()

    @pyqtSlot(result="QVariantList")
    def get_all_names(self):
        query = QSqlQuery()
        query.exec_(
            """
                SELECT name FROM Rocks
            """
        )

        rocks = []
        while query.next():
            rocks.append(query.value("name"))

        return rocks

    @pyqtSlot(str, result=QJsonValue)
    def get_properties_by_name(self, name):
        query = QSqlQuery()
        query.exec_(
            "SELECT impedance, specific_surface_energy, tension_strength, "
            'density, wave_velocity, created FROM rocks WHERE name="'
            + name
            + '"'
        )

        if query.first():
            rock_props = dict(
                impedance=query.value("impedance"),
                specific_surface_energy=query.value("specific_surface_energy"),
                tension_strength=query.value("tension_strength"),
                density=query.value("density"),
                wave_velocity=query.value("wave_velocity"),
                created=query.value("created"),
            )

        else:
            print("query error")

        return rock_props

    @pyqtSlot(QJsonValue)
    def add_rock(self, properties):
        properties_dict = properties.toVariant()
        impedance = (
            properties_dict["density"] * properties_dict["wave_velocity"]
        )
        query = QSqlQuery()
        query.prepare(
            """
            INSERT INTO Rocks (
                name,
                impedance,
                specific_surface_energy,
                tension_strength,
                density,
                wave_velocity,
                created
            ) VALUES (?, ?, ?, ?, ?, ?, ?)
            """
        )
        query.bindValue(0, properties_dict["name"])
        query.bindValue(1, impedance)
        query.bindValue(2, properties_dict["specific_surface_energy"])
        query.bindValue(3, properties_dict["tension_strength"])
        query.bindValue(4, properties_dict["density"])
        query.bindValue(5, properties_dict["wave_velocity"])
        query.bindValue(6, "User")
        query.exec_()
