from PyQt5.QtCore import QObject, pyqtSlot, QJsonValue
from PyQt5.QtSql import QSqlDatabase, QSqlQuery

from .default_explosives import default_explosives


TABLE_NAME = "Explosives"


class ExplosivesDB(QObject):
    def create_table(self):
        if TABLE_NAME in QSqlDatabase.database().tables():
            return

        query = QSqlQuery()
        if not query.exec_(
            """
                CREATE TABLE IF NOT EXISTS 'Explosives' (
                'id' INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL,
                'name' VARCHAR(64) NOT NULL,
                'explosive_type' VARCHAR(16) NOT NULL,
                'impedance' NUMERIC NOT NULL,
                'length' NUMERIC NOT NULL,
                'diameter' NUMERIC NOT NULL,
                'specific_energy' NUMERIC NOT NULL,
                'explosion_pressure' NUMERIC NOT NULL,
                'density' NUMERIC NOT NULL,
                'velocity_of_detonation' NUMERIC NOT NULL,
                'created' VARCHAR(16) NOT NULL
                )
            """
        ):
            return

        self.seed_table()

    def seed_table(self):
        query = QSqlQuery()
        query.exec_("SELECT id FROM Explosives")
        if not query.first():
            query.prepare(
                "INSERT INTO Explosives ("
                "name,"
                "explosive_type,"
                "impedance,"
                "length,"
                "diameter,"
                "specific_energy,"
                "explosion_pressure,"
                "density,"
                "velocity_of_detonation,"
                "created"
                ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
            )

            for default_explosive in default_explosives:
                query.bindValue(0, default_explosive["name"])
                query.bindValue(1, default_explosive["explosive_type"])
                query.bindValue(2, default_explosive["impedance"])
                query.bindValue(3, default_explosive["length"])
                query.bindValue(4, default_explosive["diameter"])
                query.bindValue(5, default_explosive["specific_energy"])
                query.bindValue(6, default_explosive["explosion_pressure"])
                query.bindValue(7, default_explosive["density"])
                query.bindValue(8, default_explosive["velocity_of_detonation"])
                query.bindValue(9, default_explosive["created"])
                query.exec_()

    @pyqtSlot(result="QVariantList")
    def get_all_names(self):
        query = QSqlQuery()
        query.exec_("SELECT name FROM Explosives")

        explosives = []
        while query.next():
            explosives.append(query.value("name"))
        return explosives

    @pyqtSlot(str, result=QJsonValue)
    def get_properties_by_name(self, name):
        query = QSqlQuery()
        query.exec_(
            "SELECT explosive_type, impedance, length, diameter,"
            " specific_energy, explosion_pressure, density,"
            ' velocity_of_detonation, created FROM explosives WHERE name="'
            + name
            + '"'
        )

        if query.first():
            exp_props = dict(
                explosive_type=query.value("explosive_type"),
                impedance=query.value("impedance"),
                length=query.value("length"),
                diameter=query.value("diameter"),
                specific_energy=query.value("specific_energy"),
                explosion_pressure=query.value("explosion_pressure"),
                density=query.value("density"),
                velocity_of_detonation=query.value("velocity_of_detonation"),
                created=query.value("created"),
            )

        else:
            print("query error")

        return exp_props

    @pyqtSlot(QJsonValue)
    def add_explosive(self, properties):
        properties_dict = properties.toVariant()
        impedance = (
            properties_dict["density"]
            * properties_dict["velocity_of_detonation"]
        )
        query = QSqlQuery()
        query.prepare(
            "INSERT INTO Explosives ("
            "name,"
            "explosive_type,"
            "impedance,"
            "length,"
            "diameter,"
            "specific_energy,"
            "explosion_pressure,"
            "density,"
            "velocity_of_detonation,"
            "created"
            ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
        )

        query.bindValue(0, properties_dict["name"])
        query.bindValue(1, properties_dict["explosive_type"])
        query.bindValue(2, impedance)
        query.bindValue(3, properties_dict["length"])
        query.bindValue(4, properties_dict["diameter"])
        query.bindValue(5, properties_dict["specific_energy"])
        query.bindValue(6, properties_dict["explosion_pressure"])
        query.bindValue(7, properties_dict["density"])
        query.bindValue(8, properties_dict["velocity_of_detonation"])
        query.bindValue(9, "User")
        query.exec_()
