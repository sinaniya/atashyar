import math

from matplotlib import patches
import numpy as np
from mpl_toolkits.mplot3d import art3d
from sympy import symbols, solveset

from .shape import Shape


class Circle(Shape):
    def __init__(self, parent=None, properties=None):
        super().__init__(parent, properties)
        self.last_x_index_side = None
        self.has_only_path_in_draw = True
        self.number_of_remaining_holes = None
        self.x_coordinates_property = None

    def is_point_near_edges(self, point):
        x, y = point
        return (
            (self.radius - self.ERROR_TOLERANCE) ** 2
            < x ** 2 + y ** 2
            < (self.radius + self.ERROR_TOLERANCE) ** 2
        )

    def snap_point_near_edges(self, point):
        x, y = point
        theta = math.atan2(y, x)
        return (self.radius * math.cos(theta), self.radius * math.sin(theta))

    def is_point_in_shape(self, point):
        x, y = point
        return x ** 2 + y ** 2 < (self.radius - self.ERROR_TOLERANCE) ** 2

    def is_point_inside_cut(self, point):
        return True

    def get_area(self):
        return np.pi * self.radius ** 2

    def get_perimeter(self):
        return 2 * np.pi * self.radius

    def get_perimeter_average_length(self):
        return np.pi * (2 * self.radius - self.perimeter_burden)

    def check_space(self):
        return self.radius < self.perimeter_burden + 0.4

    def draw(self):
        self.draw_patch()

    def draw_patch(self):
        patch = patches.Circle(
            (0, 0), self.radius, fill=False, edgecolor="lime"
        )
        self.sending_patch = patches.Circle(
            (0, 0), self.radius, fill=False, edgecolor="lime"
        )
        self.axes.add_patch(patch)
        if not self.is_2d:
            art3d.pathpatch_2d_to_3d(patch, z=0, zdir="z")

    # def draw_plot_ebm(self):

    #     self.draw()

    #     self.draw_cut_ebm()

    #     # Curve holes
    #     advance_hole_properties = dict(
    #         hole_type="Advance",
    #         hole_length=self.hole_length,
    #         hole_diameter=self.hole_diameter,
    #         explosive_type=self.explosive_type,
    #     )
    #     burden_curve_radius = self.radius - self.perimeter_burden
    #     number_of_circles = round(
    #         (burden_curve_radius - self.cut_length) / self.advance_burden
    #     )
    #     operational_burden_advance = (
    #         burden_curve_radius - self.cut_length
    #     ) / number_of_circles

    #     number_of_curves = 0
    #     smallest_radius = 0
    #     if number_of_circles >= 2:
    #         for curve_number in range(1, number_of_circles):
    #             curve_radius = (
    #                 self.radius
    #                 - self.perimeter_burden
    #                 - curve_number * operational_burden_advance
    #             )
    #             number_of_holes_curve = round(
    #                 2 * np.pi * curve_radius / self.advance_spacing
    #             )
    #             theta = np.linspace(0, 2 * np.pi, number_of_holes_curve + 1)
    #             for t in theta:
    #                 x_hole = curve_radius * np.cos(t)
    #                 y_hole = curve_radius * np.sin(t)
    #                 if (
    #                     x_hole > -self.cut_length and x_hole < self.cut_length
    #                 ) and (
    #                     y_hole > -self.cut_length and y_hole < self.cut_length
    #                 ):
    #                     continue
    #                 self.add_hole(x_hole, y_hole, advance_hole_properties)

    #             number_of_curves += 1
    #             smallest_radius = curve_radius

    #     if (
    #         math.floor(
    #             (smallest_radius - self.cut_length / 2) / self.advance_burden
    #         )
    #         == 1
    #     ):
    #         last_circle_radius = (smallest_radius + self.cut_length / 2) / 2
    #         theta = np.linspace(0, 2 * np.pi, 5)
    #         for t in theta:
    #             self.add_hole(
    #                 last_circle_radius * np.cos(t),
    #                 last_circle_radius * np.sin(t),
    #                 advance_hole_properties,
    #             )

    #     # Perimeter holes
    #     perimeter_hole_properties = dict(
    #         hole_type="Perimeter",
    #         hole_length=self.hole_length,
    #         hole_diameter=self.hole_diameter,
    #         explosive_type=self.explosive_type,
    #         stemming=self.perimeter_stemming,
    #     )

    #     number_of_perimetral_holes = round(
    #         2 * np.pi * self.radius / self.perimeter_spacing
    #     )
    #     perimetral_theta = np.linspace(
    #         0, 2 * np.pi, number_of_perimetral_holes
    #     )
    #     for t in perimetral_theta:
    #         self.add_hole(
    #             self.radius * np.cos(t),
    #             self.radius * np.sin(t),
    #             perimeter_hole_properties,
    #         )

    #     number_of_holes_burden_curve = round(
    #         2 * np.pi * burden_curve_radius / self.advance_spacing
    #     )
    #     burden_curve_theta = np.linspace(
    #         0, 2 * np.pi, number_of_holes_burden_curve
    #     )
    #     for t in burden_curve_theta:
    #         x_hole = burden_curve_radius * np.cos(t)
    #         y_hole = burden_curve_radius * np.sin(t)
    #         if (x_hole > -self.cut_length and x_hole < self.cut_length) and (
    #             y_hole < self.cut_length and y_hole > -self.cut_length
    #         ):
    #             continue
    #         self.add_hole(x_hole, y_hole, advance_hole_properties)

    #     self.final_draw()

    def draw_cut_ebm(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        self.add_hole(0, 0, air_hole_properties)

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )

        self.add_hole(0.15, 0, cut_hole_properties)
        self.add_hole(0, 0.18, cut_hole_properties)
        self.add_hole(-0.22, 0, cut_hole_properties)
        self.add_hole(0, -0.25, cut_hole_properties)
        self.add_hole(
            -self.cut_length / 2, self.cut_length / 2, cut_hole_properties
        )
        self.add_hole(
            self.cut_length / 2, self.cut_length / 2, cut_hole_properties
        )
        self.add_hole(
            -self.cut_length / 2, -self.cut_length / 2, cut_hole_properties
        )
        self.add_hole(
            self.cut_length / 2, -self.cut_length / 2, cut_hole_properties
        )

        patch = patches.Rectangle(
            (-self.cut_length / 2, -self.cut_length / 2),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.sending_patch_cut = patches.Rectangle(
            (-self.cut_length / 2, -self.cut_length / 2),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.axes.add_patch(patch)
        if not self.is_2d:
            art3d.pathpatch_2d_to_3d(patch, z=0, zdir="z")

    def draw_plot_ebm_angular(self):

        self.draw()

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        number_of_perimeter_holes = round(
            2 * np.pi * self.radius / self.perimeter_spacing
        )
        thetas = np.linspace(0, 2 * np.pi, number_of_perimeter_holes)
        for t in thetas:
            self.add_hole(
                self.radius * np.cos(t),
                self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        # Cut
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )

        self.outer_cut_hole_positive_x = self.triangle_half_length

        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # Cut rows equal two
        self.x_coordinates_property = {}
        if self.cut_rows == 2:
            # reset counter for each row (i)
            self.x_coordinates_property[0] = []
            self.x_coordinates_property[1] = []
            counter = 0

            # hole num 1
            self.add_hole(
                -self.outer_cut_hole_positive_x,
                -self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x
            y = -self.cut_rows_vertical_distance / 2
            point_in_cut_negative = {"x": -x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_negative)

            # hole num 2
            self.add_hole(
                -self.outer_cut_hole_positive_x,
                self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            point_in_cut_negative = {"x": -x, "index": counter, "y": -y}
            self.x_coordinates_property[1].append(point_in_cut_negative)

            # hole num 3
            self.add_hole(
                self.outer_cut_hole_positive_x,
                -self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x
            y = -self.cut_rows_vertical_distance / 2
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_positive)

            # hole num 4
            self.add_hole(
                self.outer_cut_hole_positive_x,
                self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            point_in_cut_positive = {"x": x, "index": counter, "y": -y}
            self.x_coordinates_property[1].append(point_in_cut_positive)
            counter += 1

            # hole num 5
            self.add_hole(
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length,
                -self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            x = -self.outer_cut_hole_positive_x - self.cut_develop_front_length
            y = -self.cut_rows_vertical_distance / 2
            point_in_cut_negative = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_negative)

            # hole num 6
            self.add_hole(
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length,
                self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            point_in_cut_negative = {"x": x, "index": counter, "y": -y}
            self.x_coordinates_property[1].append(point_in_cut_negative)

            # hole num 7
            self.add_hole(
                self.outer_cut_hole_positive_x + self.cut_develop_front_length,
                -self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x + self.cut_develop_front_length
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_positive)

            # hole num 8
            self.add_hole(
                self.outer_cut_hole_positive_x + self.cut_develop_front_length,
                self.cut_rows_vertical_distance / 2,
                cut_hole_properties,
            )
            point_in_cut_positive = {"x": x, "index": counter, "y": -y}
            self.x_coordinates_property[1].append(point_in_cut_positive)

            counter += 1
            self.last_x_index_side = counter

            # Check if a smaller triangle needed
            if self.hole_length / self.cut_develop_length > 1:
                small_triangle_length = (
                    2
                    * self.triangle_half_length
                    * (self.hole_length - self.cut_develop_length)
                    / self.hole_length
                )
                x = small_triangle_length / 2
                y = -self.cut_rows_vertical_distance / 2
                length = math.fabs(x)

                self.add_hole(
                    -small_triangle_length / 2,
                    -self.cut_rows_vertical_distance / 2,
                    cut_hole_properties,
                )
                point_in_cut_negative_1 = {
                    "x": -x,
                    "index": 0,
                    "type": "small-triangle",
                    "length": length,
                    "y": y,
                }
                self.x_coordinates_property[0].append(point_in_cut_negative_1)

                self.add_hole(
                    -small_triangle_length / 2,
                    self.cut_rows_vertical_distance / 2,
                    cut_hole_properties,
                )
                point_in_cut_negative_2 = {
                    "x": -x,
                    "index": 0,
                    "type": "small-triangle",
                    "length": length,
                    "y": y,
                }
                self.x_coordinates_property[1].append(point_in_cut_negative_2)

                self.add_hole(
                    small_triangle_length / 2,
                    -self.cut_rows_vertical_distance / 2,
                    cut_hole_properties,
                )
                point_in_cut_positive_1 = {
                    "x": x,
                    "index": 0,
                    "type": "small-triangle",
                    "length": length,
                    "y": y,
                }
                self.x_coordinates_property[0].append(point_in_cut_positive_1)

                self.add_hole(
                    small_triangle_length / 2,
                    self.cut_rows_vertical_distance / 2,
                    cut_hole_properties,
                )
                point_in_cut_positive_1 = {
                    "x": x,
                    "index": 0,
                    "type": "small-triangle",
                    "length": length,
                    "y": -y,
                }
                self.x_coordinates_property[1].append(point_in_cut_positive_1)

            # Fill the remaining width
            x = symbols("x")
            x1, _ = sorted(
                list(
                    solveset(
                        x ** 2
                        + (self.cut_rows_vertical_distance / 2) ** 2
                        - self.radius ** 2,
                        x,
                    )
                )
            )
            remaining_width = -self.cut_develop_length - self.x_length / 2 - x1
            self.number_of_remaining_holes = number_of_remaining_holes = round(
                remaining_width / self.cut_wall_filler_length
            )
            operational_burden = (
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length
                - x1
            ) / number_of_remaining_holes

            # add remaining in correspond
            minus_index_1 = 1
            for i in range(1, number_of_remaining_holes):
                counter = i
                x = (
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden
                )
                y = self.cut_rows_vertical_distance / 2
                self.add_hole(
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden,
                    self.cut_rows_vertical_distance / 2,
                    advance_hole_properties,
                )
                point_in_cut_negative = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": y,
                }
                self.x_coordinates_property[1].append(point_in_cut_negative)

                self.add_hole(
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden,
                    -self.cut_rows_vertical_distance / 2,
                    advance_hole_properties,
                )
                point_in_cut_negative = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": -y,
                }
                self.x_coordinates_property[0].append(point_in_cut_negative)

                x = (
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden
                )
                y = self.cut_rows_vertical_distance / 2
                self.add_hole(
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden,
                    self.cut_rows_vertical_distance / 2,
                    advance_hole_properties,
                )
                point_in_cut_positive = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": y,
                }
                self.x_coordinates_property[1].append(point_in_cut_positive)

                self.add_hole(
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden,
                    -self.cut_rows_vertical_distance / 2,
                    advance_hole_properties,
                )
                point_in_cut_positive = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": -y,
                }
                self.x_coordinates_property[0].append(point_in_cut_positive)

                counter += 1

            # First ring
            x = symbols("x")
            y = self.cut_rows_vertical_distance / 2
            r = self.radius - self.perimeter_burden
            if y <= r:
                phi = np.arcsin(y / r)
                number_of_first_ring_holes = (
                    round(r * (np.pi - 2 * phi) / self.advance_spacing) + 1
                )
                thetas = np.linspace(
                    phi, np.pi - phi, number_of_first_ring_holes
                )[1:-1]
                for t in thetas:
                    self.add_hole(
                        r * np.cos(t), -r * np.sin(t), advance_hole_properties
                    )
                    self.add_hole(
                        r * np.cos(t), r * np.sin(t), advance_hole_properties
                    )

            # Fill white space
            last_x1 = -self.radius + self.perimeter_burden
            last_x2 = self.radius - self.perimeter_burden
            number_of_remaining_crowns = round(
                (
                    self.radius
                    - self.cut_rows_vertical_distance / 2
                    - self.perimeter_burden
                )
                / self.advance_burden
            )
            if number_of_remaining_crowns > 1:
                operational_burden_advance = (
                    self.radius
                    - self.cut_rows_vertical_distance / 2
                    - self.perimeter_burden
                ) / number_of_remaining_crowns
                y = (
                    self.cut_rows_vertical_distance / 2
                    + operational_burden_advance
                )
                for i in range(1, number_of_remaining_crowns):
                    new_radius = (
                        self.radius
                        - self.perimeter_burden
                        - i * operational_burden_advance
                    )
                    if new_radius > y:
                        x = symbols("x")
                        x1, x2 = sorted(
                            list(
                                solveset(x ** 2 + y ** 2 - new_radius ** 2, x)
                            )
                        )

                        last_x1 = x1
                        last_x2 = x2
                        phi = np.arcsin(y / new_radius)
                        number_of_this_crown_holes = round(
                            new_radius
                            * (np.pi - 2 * phi)
                            / self.advance_spacing
                        )
                        thetas = np.linspace(
                            phi, np.pi - phi, number_of_this_crown_holes
                        )
                        for t in thetas:
                            self.add_hole(
                                new_radius * np.cos(t),
                                -new_radius * np.sin(t),
                                advance_hole_properties,
                            )
                            self.add_hole(
                                new_radius * np.cos(t),
                                new_radius * np.sin(t),
                                advance_hole_properties,
                            )

                number_of_holes = (
                    round((last_x2 - last_x1) / self.advance_burden) + 1
                )
                operational_burden = (last_x2 - last_x1) / number_of_holes
                if number_of_holes > 1:
                    for j in range(1, number_of_holes):
                        self.add_hole(
                            last_x1 + j * operational_burden,
                            -y,
                            cut_hole_properties,
                        )
                        self.add_hole(
                            last_x1 + j * operational_burden,
                            y,
                            cut_hole_properties,
                        )

                else:
                    self.add_hole(0, -y, advance_hole_properties)
                    self.add_hole(0, y, advance_hole_properties)

        # Cut rows equal three
        else:
            # reset counter for each row (i)
            self.x_coordinates_property[0] = []
            self.x_coordinates_property[1] = []
            self.x_coordinates_property[2] = []
            counter = 0

            # hole num 1
            self.add_hole(
                -self.outer_cut_hole_positive_x,
                -self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x
            y = -self.cut_rows_vertical_distance
            point_in_cut_negative = {"x": -x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_negative)

            # hole num 2
            self.add_hole(
                -self.outer_cut_hole_positive_x, 0, cut_hole_properties
            )
            y = 0
            point_in_cut_negative = {"x": -x, "index": counter, "y": y}
            self.x_coordinates_property[1].append(point_in_cut_negative)

            # hole num 3
            self.add_hole(
                -self.outer_cut_hole_positive_x,
                self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            y = self.cut_rows_vertical_distance
            point_in_cut_negative = {"x": -x, "index": counter, "y": y}
            self.x_coordinates_property[2].append(point_in_cut_negative)

            # hole num 4
            self.add_hole(
                self.outer_cut_hole_positive_x,
                -self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x
            y = -self.cut_rows_vertical_distance
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_positive)

            # hole num 5
            self.add_hole(
                self.outer_cut_hole_positive_x, 0, cut_hole_properties
            )
            x = self.outer_cut_hole_positive_x
            y = 0
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[1].append(point_in_cut_positive)

            # hole num 6
            self.add_hole(
                self.outer_cut_hole_positive_x,
                self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x
            y = self.cut_rows_vertical_distance
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[2].append(point_in_cut_positive)

            counter += 1

            # hole num 7
            self.add_hole(
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length,
                -self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = -self.outer_cut_hole_positive_x - self.cut_develop_front_length
            y = -self.cut_rows_vertical_distance
            point_in_cut_negative = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_negative)

            # hole num 8
            self.add_hole(
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length,
                0,
                cut_hole_properties,
            )
            x = -self.outer_cut_hole_positive_x - self.cut_develop_front_length
            y = 0
            point_in_cut_negative = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[1].append(point_in_cut_negative)

            # hole num 9
            self.add_hole(
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length,
                self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = -self.outer_cut_hole_positive_x - self.cut_develop_front_length
            y = self.cut_rows_vertical_distance
            point_in_cut_negative = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[2].append(point_in_cut_negative)

            # hole num 10
            self.add_hole(
                self.outer_cut_hole_positive_x + self.cut_develop_front_length,
                -self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x + self.cut_develop_front_length
            y = -self.cut_rows_vertical_distance
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[0].append(point_in_cut_positive)

            # hole num 11
            self.add_hole(
                self.outer_cut_hole_positive_x + self.cut_develop_front_length,
                0,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x + self.cut_develop_front_length
            y = 0
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[1].append(point_in_cut_positive)

            # hole num 12
            self.add_hole(
                self.outer_cut_hole_positive_x + self.cut_develop_front_length,
                self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x + self.cut_develop_front_length
            y = self.cut_rows_vertical_distance
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[2].append(point_in_cut_positive)

            counter += 1
            self.last_x_index_side = counter

            # Check if a smaller triangle needed
            if self.hole_length / self.cut_develop_length > 1:
                if self.method == "EBM Angular":
                    small_triangle_length = (
                        2
                        * self.triangle_half_length
                        * (self.hole_length - self.cut_develop_length)
                        / self.hole_length
                    )

                    x = small_triangle_length / 2
                    y = -self.cut_rows_vertical_distance
                    length = math.fabs(x)
                    self.add_hole(
                        -small_triangle_length / 2,
                        -self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    point_in_cut_negative_1 = {
                        "x": -x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.x_coordinates_property[0].append(
                        point_in_cut_negative_1
                    )

                    self.add_hole(
                        -small_triangle_length / 2, 0, cut_hole_properties
                    )
                    y = 0
                    point_in_cut_negative_1 = {
                        "x": -x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.x_coordinates_property[1].append(
                        point_in_cut_negative_1
                    )

                    self.add_hole(
                        -small_triangle_length / 2,
                        self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    y = self.cut_rows_vertical_distance
                    point_in_cut_negative_1 = {
                        "x": -x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.x_coordinates_property[2].append(
                        point_in_cut_negative_1
                    )

                    self.add_hole(
                        small_triangle_length / 2,
                        -self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    y = -self.cut_rows_vertical_distance
                    point_in_cut_positive_1 = {
                        "x": x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.x_coordinates_property[0].append(
                        point_in_cut_positive_1
                    )

                    self.add_hole(
                        small_triangle_length / 2, 0, cut_hole_properties
                    )
                    y = 0
                    point_in_cut_positive_1 = {
                        "x": x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.x_coordinates_property[1].append(
                        point_in_cut_positive_1
                    )

                    y = self.cut_rows_vertical_distance
                    point_in_cut_positive_1 = {
                        "x": x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.add_hole(
                        small_triangle_length / 2,
                        self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    self.x_coordinates_property[2].append(
                        point_in_cut_positive_1
                    )

                elif (
                    self.method == "Gustafsson"
                    or self.method == "Olafsson Angular"
                    or self.method == "Konya Angular"
                ):
                    number_of_smaller_triangles = (
                        round(self.hole_length / self.cut_develop_length) - 1
                    )
                    step = (
                        self.triangle_half_length / number_of_smaller_triangles
                    )
                    for i in range(1, number_of_smaller_triangles):
                        x = i * step
                        y = -self.cut_rows_vertical_distance
                        length = math.fabs(x)

                        self.add_hole(
                            -i * step,
                            -self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        point_in_cut_negative = {
                            "x": -x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[0].append(
                            point_in_cut_negative
                        )

                        self.add_hole(-i * step, 0, cut_hole_properties)

                        y = 0
                        point_in_cut_negative = {
                            "x": -x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[1].append(
                            point_in_cut_negative
                        )

                        self.add_hole(
                            -i * step,
                            self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        y = self.cut_rows_vertical_distance
                        point_in_cut_negative = {
                            "x": -x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[2].append(
                            point_in_cut_negative
                        )

                        self.add_hole(
                            i * step,
                            -self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        y = -self.cut_rows_vertical_distance
                        point_in_cut_positive = {
                            "x": x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[0].append(
                            point_in_cut_positive
                        )

                        self.add_hole(i * step, 0, cut_hole_properties)
                        y = 0
                        point_in_cut_positive = {
                            "x": x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[1].append(
                            point_in_cut_positive
                        )

                        self.add_hole(
                            i * step,
                            self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        y = self.cut_rows_vertical_distance
                        point_in_cut_positive = {
                            "x": x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[2].append(
                            point_in_cut_positive
                        )

            # Fill the remaining width
            # Central
            remaining_width = (
                self.radius - self.x_length / 2 - self.cut_develop_length
            )
            self.number_of_remaining_holes = number_of_remaining_holes = round(
                remaining_width / self.cut_wall_filler_length
            )
            operational_burden = (
                self.radius
                - self.outer_cut_hole_positive_x
                - self.cut_develop_front_length
            ) / number_of_remaining_holes

            # add remaining in correspond
            minus_index_1 = 1
            for i in range(1, number_of_remaining_holes):
                counter = i

                x = (
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden
                )
                y = 0
                self.add_hole(
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden,
                    0,
                    advance_hole_properties,
                )
                point_in_cut_negative = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": y,
                }
                self.x_coordinates_property[1].append(point_in_cut_negative)

                self.add_hole(
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden,
                    0,
                    advance_hole_properties,
                )
                x = (
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden
                )
                y = 0
                point_in_cut_positive = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": y,
                }
                self.x_coordinates_property[1].append(point_in_cut_positive)

                counter += 1
            # The other two
            x = symbols("x")
            x1, _ = sorted(
                list(
                    solveset(
                        x ** 2
                        + self.cut_rows_vertical_distance ** 2
                        - self.radius ** 2,
                        x,
                    )
                )
            )
            remaining_width = -self.cut_develop_length - self.x_length / 2 - x1
            self.number_of_remaining_holes = number_of_remaining_holes = round(
                remaining_width / self.cut_wall_filler_length
            )
            operational_burden = (
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length
                - x1
            ) / number_of_remaining_holes

            # add remaining in correspond hole map(in angular mode)
            minus_index_1 = 1
            for i in range(1, number_of_remaining_holes):
                counter = i

                x = (
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden
                )
                y = -self.cut_rows_vertical_distance
                self.add_hole(
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden,
                    -self.cut_rows_vertical_distance,
                    advance_hole_properties,
                )
                point_in_cut_negative = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": y,
                }
                self.x_coordinates_property[0].append(point_in_cut_negative)

                self.add_hole(
                    -self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden,
                    self.cut_rows_vertical_distance,
                    advance_hole_properties,
                )
                point_in_cut_negative = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": -y,
                }
                self.x_coordinates_property[2].append(point_in_cut_negative)

                self.add_hole(
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden,
                    -self.cut_rows_vertical_distance,
                    advance_hole_properties,
                )
                x = (
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden
                )
                point_in_cut_positive = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": y,
                }
                self.x_coordinates_property[0].append(point_in_cut_positive)

                self.add_hole(
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden,
                    self.cut_rows_vertical_distance,
                    advance_hole_properties,
                )
                x = (
                    self.outer_cut_hole_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden
                )
                point_in_cut_positive = {
                    "x": x,
                    "index": (self.last_x_index_side + counter)
                    - minus_index_1,
                    "y": -y,
                }
                self.x_coordinates_property[2].append(point_in_cut_positive)

                counter += 1
            # print(self.x_coordinates_property)
            # First ring
            x = symbols("x")
            y = self.cut_rows_vertical_distance / 2
            r = self.radius - self.perimeter_burden
            if y <= r:
                phi = np.arcsin(y / r)
                number_of_first_ring_holes = (
                    round(r * (np.pi - 2 * phi) / self.advance_spacing) + 1
                )
                thetas = np.linspace(
                    phi, np.pi - phi, number_of_first_ring_holes
                )[1:-1]
                for t in thetas:
                    self.add_hole(
                        r * np.cos(t), -r * np.sin(t), advance_hole_properties
                    )
                    self.add_hole(
                        r * np.cos(t), r * np.sin(t), advance_hole_properties
                    )

            # Fill white space
            last_x1 = -self.radius + self.perimeter_burden
            last_x2 = self.radius - self.perimeter_burden
            number_of_remaining_crowns = round(
                (
                    self.radius
                    - self.cut_rows_vertical_distance
                    - self.perimeter_burden
                )
                / self.advance_burden
            )
            if number_of_remaining_crowns > 1:
                operational_burden_advance = (
                    self.radius
                    - self.cut_rows_vertical_distance
                    - self.perimeter_burden
                ) / number_of_remaining_crowns
                y = (
                    self.cut_rows_vertical_distance / 2
                    + 1.2 * operational_burden_advance
                )
                for i in range(1, number_of_remaining_crowns):
                    new_radius = (
                        self.radius
                        - self.perimeter_burden
                        - i * operational_burden_advance
                    )
                    if new_radius > y:
                        x = symbols("x")
                        x1, x2 = sorted(
                            list(
                                solveset(x ** 2 + y ** 2 - new_radius ** 2, x)
                            )
                        )

                        last_x1 = x1
                        last_x2 = x2
                        phi = np.arcsin(y / new_radius)
                        number_of_this_crown_holes = round(
                            new_radius
                            * (np.pi - 2 * phi)
                            / self.advance_spacing
                        )
                        thetas = np.linspace(
                            phi, np.pi - phi, number_of_this_crown_holes
                        )
                        for t in thetas:
                            self.add_hole(
                                new_radius * np.cos(t),
                                -new_radius * np.sin(t),
                                advance_hole_properties,
                            )
                            self.add_hole(
                                new_radius * np.cos(t),
                                new_radius * np.sin(t),
                                advance_hole_properties,
                            )

                number_of_holes = round(
                    (last_x2 - last_x1) / self.advance_burden
                )
                operational_burden = (last_x2 - last_x1) / number_of_holes
                if number_of_holes > 1:
                    for j in range(number_of_holes):
                        self.add_hole(
                            last_x1 + j * operational_burden,
                            -y,
                            advance_hole_properties,
                        )
                        self.add_hole(
                            last_x1 + j * operational_burden,
                            y,
                            advance_hole_properties,
                        )
                else:
                    self.add_hole(0, -y, advance_hole_properties)
                    self.add_hole(0, y, advance_hole_properties)

        self.final_draw()

    def draw_plot_ntnu(self):

        self.draw()

        parameters = self.get_parameters_ntnu()
        burden_perimeter = parameters.get("burden_perimeter")
        spacing_perimeter = parameters.get("spacing_perimeter")
        # burden_first_ring = parameters.get("burden_first_ring")
        # spacing_first_ring = parameters.get("spacing_first_ring")
        # burden_floor = parameters.get("burden_floor")
        # spacing_floor = parameters.get("spacing_floor")
        burden_advance = parameters.get("burden_advance")
        spacing_advance = parameters.get("spacing_advance")

        self.draw_cut_ntnu()

        # Curve holes
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        number_of_circles = round(
            (self.radius - burden_perimeter - self.w4) / burden_advance
        )
        operational_burden_advance = (
            self.radius - burden_perimeter - self.w4
        ) / number_of_circles

        number_of_curves = 0
        if number_of_circles >= 2:
            for curve_number in range(1, number_of_circles):
                curve_radius = (
                    self.radius
                    - burden_perimeter
                    - curve_number * operational_burden_advance
                )
                number_of_holes_curve = round(
                    2 * np.pi * curve_radius / spacing_advance
                )
                theta = np.linspace(0, 2 * np.pi, number_of_holes_curve)
                for t in theta:
                    x_hole = curve_radius * np.cos(t)
                    y_hole = curve_radius * np.sin(t)
                    if (x_hole > -self.w4 and x_hole < self.w4) and (
                        y_hole > -self.w4 and y_hole < self.w4
                    ):
                        continue
                    self.add_hole(x_hole, y_hole, advance_hole_properties)

                number_of_curves += 1

        elif number_of_circles == 1:
            curve_radius = (self.radius + self.w4 / np.sqrt(2)) / 2
            number_of_holes_curve = round(
                2 * np.pi * curve_radius / spacing_advance
            )
            theta = np.linspace(0, 2 * np.pi, number_of_holes_curve)
            for t in theta:
                x_hole = curve_radius * np.cos(t)
                y_hole = curve_radius * np.sin(t)
                self.add_hole(x_hole, y_hole, advance_hole_properties)

        # Perimeter holes
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_perimeter,
        )
        number_of_perimetral_holes = round(
            2 * np.pi * self.radius / spacing_perimeter
        )
        perimetral_theta = np.linspace(
            0, 2 * np.pi, number_of_perimetral_holes
        )
        for t in perimetral_theta:
            self.add_hole(
                self.radius * np.cos(t),
                self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        self.final_draw()

    def draw_cut_ntnu(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        x_air_hole = 0
        y_air_hole = 0
        self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_cut,
        )
        self.add_hole(x_air_hole - self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b1, cut_hole_properties)
        self.add_hole(x_air_hole + self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b1, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(x_air_hole - self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b3, cut_hole_properties)
        self.add_hole(x_air_hole + self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b3, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )

    def draw_plot_olafsson(self):

        self.draw()

        self.draw_cut_olafsson()

        # Curve holes
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        number_of_circles = (
            round(
                (self.radius - self.wall_burden - self.last_x)
                / self.advance_burden
            )
            + 1
        )
        operational_burden_advance = (
            self.radius - self.wall_burden - self.last_x
        ) / number_of_circles

        number_of_curves = 0
        smallest_radius = 0
        if number_of_circles >= 2:
            for curve_number in range(1, number_of_circles):
                curve_radius = (
                    self.radius
                    - self.wall_burden
                    - curve_number * operational_burden_advance
                )
                number_of_holes_curve = round(
                    2 * np.pi * curve_radius / self.advance_spacing
                )
                theta = np.linspace(0, 2 * np.pi, number_of_holes_curve + 1)
                for t in theta:
                    x_hole = curve_radius * np.cos(t)
                    y_hole = curve_radius * np.sin(t)
                    if (x_hole > -self.last_x and x_hole < self.last_x) and (
                        y_hole > -self.last_x and y_hole < self.last_x
                    ):
                        continue
                    self.add_hole(x_hole, y_hole, advance_hole_properties)

                number_of_curves += 1
                smallest_radius = curve_radius

        if (
            math.floor(
                (smallest_radius - self.last_x / 2) / self.advance_burden
            )
            == 1
        ):
            last_circle_radius = (smallest_radius + self.last_x / 2) / 2
            theta = np.linspace(0, 2 * np.pi, 5)
            for t in theta:
                self.add_hole(
                    last_circle_radius * np.cos(t),
                    last_circle_radius * np.sin(t),
                    advance_hole_properties,
                )

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        number_of_perimetral_holes = round(
            2 * np.pi * self.radius / self.wall_spacing
        )
        perimetral_theta = np.linspace(
            0, 2 * np.pi, number_of_perimetral_holes
        )
        for t in perimetral_theta:
            self.add_hole(
                self.radius * np.cos(t),
                self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        self.final_draw()

    def draw_cut_olafsson(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        x_air_hole = 0
        y_air_hole = 0

        self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        required_parameters_length = len(self.required_parameters)
        if required_parameters_length == 4:
            (
                first_section,
                second_section,
                third_section,
                fourth_section,
            ) = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section
            b3, _ = third_section
            _, x4 = fourth_section

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(x_air_hole - b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b3, cut_hole_properties)
            self.add_hole(x_air_hole + b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b3, cut_hole_properties)
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
        elif required_parameters_length == 3:
            (
                first_section,
                second_section,
                third_section,
            ) = self.required_parameters
            _, x1 = first_section
            b2, _ = second_section
            _, x3 = third_section

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )

            self.add_hole(
                x_air_hole,
                y_air_hole - b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole,
                y_air_hole + b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )

        elif required_parameters_length == 2:
            first_section, second_section = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )

        elif required_parameters_length == 1:
            first_section = self.required_parameters
            _, x1 = first_section

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )

    def draw_plot_ebm(self):

        self.draw()

        self.draw_cut_ebm()

        # Curve holes
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        burden_curve_radius = self.radius - self.perimeter_burden
        number_of_circles = round(
            (burden_curve_radius - self.cut_length) / self.advance_burden
        )
        operational_burden_advance = (
            burden_curve_radius - self.cut_length
        ) / number_of_circles

        number_of_curves = 0
        smallest_radius = 0
        if number_of_circles >= 2:
            for curve_number in range(1, number_of_circles):
                curve_radius = (
                    self.radius
                    - self.perimeter_burden
                    - curve_number * operational_burden_advance
                )
                number_of_holes_curve = round(
                    2 * np.pi * curve_radius / self.advance_spacing
                )
                theta = np.linspace(0, 2 * np.pi, number_of_holes_curve + 1)
                for t in theta:
                    x_hole = curve_radius * np.cos(t)
                    y_hole = curve_radius * np.sin(t)
                    if (
                        x_hole > -self.cut_length and x_hole < self.cut_length
                    ) and (
                        y_hole > -self.cut_length and y_hole < self.cut_length
                    ):
                        continue
                    self.add_hole(x_hole, y_hole, advance_hole_properties)

                number_of_curves += 1
                smallest_radius = curve_radius

        if (
            math.floor(
                (smallest_radius - self.cut_length / 2) / self.advance_burden
            )
            == 1
        ):
            last_circle_radius = (smallest_radius + self.cut_length / 2) / 2
            theta = np.linspace(0, 2 * np.pi, 5)
            for t in theta:
                self.add_hole(
                    last_circle_radius * np.cos(t),
                    last_circle_radius * np.sin(t),
                    advance_hole_properties,
                )

        # Perimeter holes
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_stemming,
        )

        number_of_perimetral_holes = round(
            2 * np.pi * self.radius / self.perimeter_spacing
        )
        perimetral_theta = np.linspace(
            0, 2 * np.pi, number_of_perimetral_holes
        )
        for t in perimetral_theta:
            self.add_hole(
                self.radius * np.cos(t),
                self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        number_of_holes_burden_curve = round(
            2 * np.pi * burden_curve_radius / self.advance_spacing
        )
        burden_curve_theta = np.linspace(
            0, 2 * np.pi, number_of_holes_burden_curve
        )
        for t in burden_curve_theta:
            x_hole = burden_curve_radius * np.cos(t)
            y_hole = burden_curve_radius * np.sin(t)
            if (x_hole > -self.cut_length and x_hole < self.cut_length) and (
                y_hole < self.cut_length and y_hole > -self.cut_length
            ):
                continue
            self.add_hole(x_hole, y_hole, advance_hole_properties)

        self.final_draw()
