import math

from matplotlib import patches
import numpy as np
from mpl_toolkits.mplot3d import art3d

from .shape import Shape


class Rectangle(Shape):
    def __init__(self, parent=None, properties=None):
        super().__init__(parent, properties)
        self.has_only_path_in_draw = True
        self.number_of_remaining_holes = None
        self.last_x_index_side = None
        self.x_coordinates_property = None

    def is_point_near_edges(self, point):
        return (
            self.is_point_near_floor(point)
            or self.is_point_near_right_wall(point)
            or self.is_point_near_left_wall(point)
            or self.is_point_near_roof(point)
        )

    def snap_point_near_edges(self, point):
        x, y = point
        if self.is_point_near_floor(point):
            return (x, 0)
        elif self.is_point_near_right_wall(point):
            return (self.width / 2, y)
        elif self.is_point_near_left_wall(point):
            return (-self.width / 2, y)
        elif self.is_point_near_roof(point):
            return (x, self.height)

    def is_point_near_floor(self, point):
        x, y = point
        return (-self.width / 2 < x < self.width / 2) and (
            abs(y) < self.ERROR_TOLERANCE
        )

    def is_point_near_right_wall(self, point):
        x, y = point
        return (0 < y < self.height) and (
            abs(x - self.width / 2) < self.ERROR_TOLERANCE
        )

    def is_point_near_left_wall(self, point):
        x, y = point
        return (0 < y < self.height) and (
            abs(x + self.width / 2) < self.ERROR_TOLERANCE
        )

    def is_point_near_roof(self, point):
        x, y = point
        return (-self.width / 2 < x < self.width / 2) and (
            abs(y - self.height) < self.ERROR_TOLERANCE
        )

    def is_point_in_shape(self, point):
        x, y = point
        return (
            -self.width + self.ERROR_TOLERANCE
            < x
            < self.width - self.ERROR_TOLERANCE
        ) and (self.ERROR_TOLERANCE < y < self.height - self.ERROR_TOLERANCE)

    def is_point_inside_cut(self, point):
        return True

    def get_area(self):
        return self.width * self.height

    def get_perimeter(self):
        return 2 * (self.width + self.height)

    def get_perimeter_average_length(self):
        return (self.width - self.perimeter_burden) + 2 * (
            self.height - self.perimeter_burden / 2
        )

    def check_space(self):
        min_empty_space = min(
            self.width / 2 - 0.4, self.height - self.advance_burden - 0.8
        )
        return min_empty_space < self.perimeter_burden + 0.4

    def draw(self):
        self.draw_patch()

    def draw_patch(self):
        self.patch = patches.Rectangle(
            (-self.width / 2, 0),
            self.width,
            self.height,
            fill=False,
            edgecolor="lime",
            linewidth=2,
        )
        self.sending_patch = patches.Rectangle(
            (-self.width / 2, 0),
            self.width,
            self.height,
            fill=False,
            edgecolor="lime",
            linewidth=2,
        )
        self.axes.add_patch(self.patch)
        if not self.is_2d:
            art3d.pathpatch_2d_to_3d(self.patch, z=0, zdir="z")

    def draw_plot_ebm(self):

        self.draw()

        self.draw_cut_ebm()

        # Walls
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_stemming,
        )

        number_of_holes_perimeter_height = (
            round(self.height / self.perimeter_spacing) + 1
        )
        y_holes = np.linspace(
            0, self.height, number_of_holes_perimeter_height
        )[1:-1]
        for y in y_holes:
            self.add_hole(self.width / 2, y, perimeter_hole_properties)
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)

        # Ceil
        number_of_holes_perimeter_width = (
            round(self.width / self.perimeter_spacing) + 1
        )
        x_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_perimeter_width
        )
        for x in x_holes:
            self.add_hole(x, self.height, perimeter_hole_properties)

        # Floor
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        number_of_holes_floor = round(self.width / self.advance_spacing) + 1
        x_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_floor
        )
        for x in x_holes:
            self.add_hole(x, 0, advance_hole_properties)

        # Burden rectangle
        number_of_holes_column = (
            round(
                (self.height - self.advance_burden - self.perimeter_burden)
                / self.advance_spacing
            )
            + 1
        )
        y_holes = np.linspace(
            self.advance_burden,
            self.height - self.perimeter_burden,
            number_of_holes_column,
        )[:-1]
        for y in y_holes:
            self.add_hole(
                self.width / 2 - self.perimeter_burden,
                y,
                advance_hole_properties,
            )
            self.add_hole(
                -self.width / 2 + self.perimeter_burden,
                y,
                advance_hole_properties,
            )

        number_of_holes_row = (
            round(
                (self.width - 2 * self.perimeter_burden) / self.advance_spacing
            )
            + 1
        )
        x_holes = np.linspace(
            -self.width / 2 + self.perimeter_burden,
            self.width / 2 - self.perimeter_burden,
            number_of_holes_row,
        )
        for x in x_holes:
            self.add_hole(
                x, self.height - self.perimeter_burden, advance_hole_properties
            )

        # Inner rectangles
        width = round(
            (self.width / 2 - self.cut_length / 2 - self.perimeter_burden)
            / self.advance_burden
        )
        height = round(
            (
                self.height
                - self.advance_burden
                - self.perimeter_burden
                - self.cut_length
            )
            / self.advance_burden
        )
        minimum_of_rectangles = min(width, height)
        y = self.height - self.perimeter_burden
        right_x = self.width / 2 - self.perimeter_burden
        left_x = -self.width / 2 + self.perimeter_burden
        operational_burden_advance_h = (
            self.width / 2 - self.perimeter_burden - self.cut_length / 2
        ) / width
        operational_burden_advance_v = (
            self.height
            - self.advance_burden
            - self.cut_length
            - self.perimeter_burden
        ) / height
        for _ in range(1, minimum_of_rectangles):
            y -= operational_burden_advance_v
            number_of_holes_inner_columns = round(y / self.advance_spacing)
            y_holes = np.linspace(
                self.advance_burden, y, number_of_holes_inner_columns
            )
            right_x -= operational_burden_advance_h
            left_x += operational_burden_advance_h
            for y in y_holes:
                self.add_hole(right_x, y, advance_hole_properties)
                self.add_hole(left_x, y, advance_hole_properties)

            number_of_holes_inner_rows = (
                round((right_x - left_x) / self.advance_spacing) + 1
            )
            x_holes = np.linspace(left_x, right_x, number_of_holes_inner_rows)
            for x in x_holes:
                self.add_hole(x, y, advance_hole_properties)

        if minimum_of_rectangles == height:
            number_of_white_space_columns = round(
                (right_x - self.cut_length / 2) / self.advance_burden
            )
            for _ in range(1, number_of_white_space_columns):
                number_of_holes_whitespace_column = (
                    round((y - self.advance_burden) / self.advance_spacing) + 1
                )
                y_holes = np.linspace(
                    self.advance_burden, y, number_of_holes_whitespace_column
                )[:-1]
                right_x -= operational_burden_advance_h
                left_x += operational_burden_advance_h
                for y in y_holes:
                    self.add_hole(right_x, y, advance_hole_properties)
                    self.add_hole(left_x, y, advance_hole_properties)
        else:
            number_of_white_space_rows = round(
                (y - self.advance_burden - self.cut_length)
                / self.advance_spacing
            )
            for _ in range(1, number_of_white_space_rows):
                number_of_holes_whitespace_row = (
                    round((right_x - left_x) / self.advance_spacing) + 1
                )
                x_holes = np.linspace(
                    left_x, right_x, number_of_holes_whitespace_row
                )[1:-1]
                y -= operational_burden_advance_v
                for x in x_holes:
                    self.add_hole(x, y, advance_hole_properties)

        self.final_draw()

    def draw_cut_ebm(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        self.add_hole(
            0, self.advance_burden + self.cut_length / 2, air_hole_properties
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )

        self.add_hole(
            -self.cut_length / 2, self.advance_burden, cut_hole_properties
        )
        self.add_hole(
            -self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            self.cut_length / 2, self.advance_burden, cut_hole_properties
        )
        self.add_hole(
            self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            0, self.advance_burden + self.cut_length / 2, cut_hole_properties
        )
        self.add_hole(
            0.15,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(0, self.advance_burden + 0.58, cut_hole_properties)
        self.add_hole(
            -0.22,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(0, self.advance_burden + 0.15, cut_hole_properties)

        patch = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.sending_patch_cut = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.axes.add_patch(patch)
        if not self.is_2d:
            art3d.pathpatch_2d_to_3d(patch, z=0, zdir="z")

    def draw_plot_ebm_angular(self):

        self.draw()

        # Cut
        self.outer_cut_positive_x = self.triangle_half_length

        self.draw_cut_ebm_angular()

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_stemming,
        )
        number_of_wall_holes = round(self.height / self.perimeter_spacing)
        ys = np.linspace(0, self.height, number_of_wall_holes)[1:-1]
        for y in ys:
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)
            self.add_hole(self.width / 2, y, perimeter_hole_properties)

        number_of_roof_holes = round(self.width / self.perimeter_spacing)
        xs = np.linspace(-self.width / 2, self.width / 2, number_of_roof_holes)
        for x in xs:
            self.add_hole(x, self.height, perimeter_hole_properties)

        # Floor
        advance_hole_properties = dict(
            hole_type="Advance" if self.method == "EBM Angular" else "Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        number_of_floor_holes = round(self.width / self.floor_spacing) + 1
        xs = np.linspace(
            -self.width / 2, self.width / 2, number_of_floor_holes
        )
        for x in xs:
            self.add_hole(x, 0, advance_hole_properties)

        # Fill the remaining width
        remaining_width = (
            self.width / 2 - self.x_length / 2 - self.cut_develop_length
        )
        self.number_of_remaining_holes = number_of_holes_width = round(
            remaining_width / self.cut_wall_filler_length
        )
        operational_burden = (
            self.width / 2
            - self.outer_cut_positive_x
            - self.cut_develop_front_length
        ) / number_of_holes_width

        hole_counter = 0
        for i in range(1, number_of_holes_width):
            for j in range(self.cut_rows):
                counter = 0
                x_negative = (
                    -self.outer_cut_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden
                )
                y = (
                    self.operational_burden_advance
                    + j * self.cut_rows_vertical_distance
                )
                self.add_hole(
                    -self.outer_cut_positive_x
                    - self.cut_develop_front_length
                    - i * operational_burden,
                    self.operational_burden_advance
                    + j * self.cut_rows_vertical_distance,
                    advance_hole_properties,
                )
                point_in_cut_negative = {
                    "x": x_negative,
                    "index": self.last_x_index_side + counter + hole_counter,
                    "y": y,
                }
                self.x_coordinates_property[j].append(point_in_cut_negative)

                self.add_hole(
                    self.outer_cut_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden,
                    self.operational_burden_advance
                    + j * self.cut_rows_vertical_distance,
                    advance_hole_properties,
                )
                x_positive = (
                    self.outer_cut_positive_x
                    + self.cut_develop_front_length
                    + i * operational_burden
                )
                point_in_cut_positive = {
                    "x": x_positive,
                    "index": self.last_x_index_side + counter + hole_counter,
                    "y": y,
                }
                self.x_coordinates_property[j].append(point_in_cut_positive)
                counter += 1
            hole_counter += 1

        # Fill the remaining height
        number_of_holes_row = (
            round(
                (self.width - 2 * self.perimeter_burden) / self.advance_spacing
            )
            + 1
        )
        xs = np.linspace(
            -self.width / 2 + self.perimeter_burden,
            self.width / 2 - self.perimeter_burden,
            number_of_holes_row,
        )
        if (
            self.height
            - self.operational_burden_advance
            - (self.cut_rows - 1) * self.cut_rows_vertical_distance
        ) / self.perimeter_burden >= 2:
            y = self.height - self.perimeter_burden
            for x in xs:
                self.add_hole(x, y, advance_hole_properties)

            if (
                y
                - self.operational_burden_advance
                - (self.cut_rows - 1) * self.cut_rows_vertical_distance
            ) / self.advance_burden > 1:
                remaining_rows = (
                    round(
                        (
                            y
                            - self.operational_burden_advance
                            - (self.cut_rows - 1)
                            * self.cut_rows_vertical_distance
                        )
                        / self.advance_burden
                    )
                    + 1
                )
                operational_burden_advance_this = (
                    y
                    - self.operational_burden_advance
                    - (self.cut_rows - 1) * self.cut_rows_vertical_distance
                ) / remaining_rows
                for i in range(1, remaining_rows):
                    for x in xs:
                        self.add_hole(
                            x,
                            y - i * operational_burden_advance_this,
                            advance_hole_properties,
                        )

        self.final_draw()

    def draw_cut_ebm_angular(self):
        self.x_coordinates_property = {}
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )
        if self.method == "EBM Angular":
            if (
                self.height
                < self.advance_burden
                + (self.cut_rows - 1) * self.cut_rows_vertical_distance
                + 1
            ):
                self.operational_burden_advance = (
                    self.height
                    - (self.cut_rows - 1) * self.cut_rows_vertical_distance
                ) / 2
            else:
                self.operational_burden_advance = self.advance_burden

            for i in range(self.cut_rows):
                # reset counter for each row (i)
                self.x_coordinates_property[i] = []
                counter = 0
                self.add_hole(
                    -self.outer_cut_positive_x,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                x = self.outer_cut_positive_x
                y = (
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance
                )
                point_in_cut_negative = {"x": -x, "index": counter, "y": y}
                self.x_coordinates_property[i].append(point_in_cut_negative)

                self.add_hole(
                    self.outer_cut_positive_x,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                point_in_cut_positive = {"x": x, "index": counter, "y": y}
                self.x_coordinates_property[i].append(point_in_cut_positive)

                counter += 1

                self.add_hole(
                    -self.outer_cut_positive_x - self.cut_develop_front_length,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                x_dev_front_negative = (
                    -self.outer_cut_positive_x - self.cut_develop_front_length
                )
                point_in_cut_negative_dev_front = {
                    "x": x_dev_front_negative,
                    "index": counter,
                    "y": y,
                }
                self.x_coordinates_property[i].append(
                    point_in_cut_negative_dev_front
                )

                self.add_hole(
                    self.outer_cut_positive_x + self.cut_develop_front_length,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                x_dev_front_positive = (
                    self.outer_cut_positive_x + self.cut_develop_front_length
                )
                point_in_cut_positive_dev_front = {
                    "x": x_dev_front_positive,
                    "index": counter,
                    "y": y,
                }
                self.x_coordinates_property[i].append(
                    point_in_cut_positive_dev_front
                )

                counter += 1
                self.last_x_index_side = counter

        elif (
            self.method == "Gustafsson"
            or self.method == "Olafsson Angular"
            or self.method == "Konya Angular"
        ):
            if (
                self.height
                < self.floor_burden
                + (self.cut_rows - 1) * self.cut_rows_vertical_distance
                + 1
            ):
                self.operational_burden_advance = (
                    self.height
                    - (self.cut_rows - 1) * self.cut_rows_vertical_distance
                ) / 2
            else:
                self.operational_burden_advance = self.floor_burden

            for i in range(self.cut_rows):
                # reset counter for each row (i)
                self.x_coordinates_property[i] = []
                counter = 0
                self.add_hole(
                    -self.outer_cut_positive_x,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                x = self.outer_cut_positive_x
                y = (
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance
                )
                point_in_cut_negative = {"x": -x, "index": counter, "y": y}
                self.x_coordinates_property[i].append(point_in_cut_negative)

                self.add_hole(
                    self.outer_cut_positive_x,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                point_in_cut_positive = {"x": x, "index": counter, "y": y}
                self.x_coordinates_property[i].append(point_in_cut_positive)
                counter += 1

                self.add_hole(
                    -self.outer_cut_positive_x - self.cut_develop_front_length,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                x_dev_front_negative = (
                    -self.outer_cut_positive_x - self.cut_develop_front_length
                )
                point_in_cut_negative_dev_front = {
                    "x": x_dev_front_negative,
                    "index": counter,
                    "y": y,
                }
                self.x_coordinates_property[i].append(
                    point_in_cut_negative_dev_front
                )

                self.add_hole(
                    self.outer_cut_positive_x + self.cut_develop_front_length,
                    self.operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                x_dev_front_positive = (
                    self.outer_cut_positive_x + self.cut_develop_front_length
                )
                point_in_cut_positive_dev_front = {
                    "x": x_dev_front_positive,
                    "index": counter,
                    "y": y,
                }
                self.x_coordinates_property[i].append(
                    point_in_cut_positive_dev_front
                )

                counter += 1
                self.last_x_index_side = counter

        # Check if a smaller triangle needed
        if self.hole_length / self.cut_develop_length > 1:
            if self.method == "EBM Angular":
                small_triangle_length = (
                    2
                    * self.triangle_half_length
                    * (self.hole_length - self.cut_develop_length)
                    / self.hole_length
                )
                for i in range(self.cut_rows):
                    x = small_triangle_length / 2
                    y = (
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance
                    )
                    self.add_hole(
                        -small_triangle_length / 2,
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    point_in_cut_negative = {
                        "x": -x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": math.fabs(x),
                        "y": y,
                    }
                    self.x_coordinates_property[i].append(
                        point_in_cut_negative
                    )

                    self.add_hole(
                        small_triangle_length / 2,
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    point_in_cut_positive = {
                        "x": x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": math.fabs(x),
                        "y": y,
                    }
                    self.x_coordinates_property[i].append(
                        point_in_cut_positive
                    )

            elif (
                self.method == "Gustafsson"
                or self.method == "Olafsson Angular"
            ):
                number_of_smaller_triangles = (
                    round(self.hole_length / self.cut_develop_length) - 1
                )
                step = self.triangle_half_length / number_of_smaller_triangles
                for i in range(1, number_of_smaller_triangles):
                    for j in range(self.cut_rows):
                        x = i * step
                        y = (
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance
                        )
                        self.add_hole(
                            -i * step,
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        point_in_cut_negative = {
                            "x": -x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": math.fabs(x),
                            "y": y,
                        }
                        self.x_coordinates_property[j].append(
                            point_in_cut_negative
                        )

                        self.add_hole(
                            i * step,
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        point_in_cut_positive = {
                            "x": x,
                            "index": 0,
                            "type": "small-triangle",
                            "length": math.fabs(x),
                            "y": y,
                        }
                        self.x_coordinates_property[j].append(
                            point_in_cut_positive
                        )

    def draw_plot_ntnu(self):

        self.draw()

        parameters = self.get_parameters_ntnu()
        burden_perimeter = parameters.get("burden_perimeter")
        spacing_perimeter = parameters.get("spacing_perimeter")
        burden_first_ring = parameters.get("burden_first_ring")
        spacing_first_ring = parameters.get("spacing_first_ring")
        burden_floor = parameters.get("burden_floor")
        # spacing_floor = parameters.get("spacing_floor")
        burden_advance = parameters.get("burden_advance")
        spacing_advance = parameters.get("spacing_advance")

        self.draw_cut_ntnu()

        # Perimeter holes
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_perimeter,
        )

        # Walls
        number_of_holes_perimeter_height = (
            round(self.height / spacing_perimeter) + 1
        )
        y_holes = np.linspace(
            0, self.height, number_of_holes_perimeter_height
        )[1:-1]
        for y in y_holes:
            self.add_hole(self.width / 2, y, perimeter_hole_properties)
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)

        # Ceil
        number_of_holes_perimeter_width = (
            round(self.width / spacing_perimeter) + 1
        )
        x_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_perimeter_width
        )
        for x in x_holes:
            self.add_hole(x, self.height, perimeter_hole_properties)

        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        floor_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # Floor
        number_of_holes_floor = round(self.width / spacing_advance) + 1
        x_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_floor
        )
        for x in x_holes:
            self.add_hole(x, 0, floor_hole_properties)

        # Burden rectangle
        number_of_holes_column = (
            round(
                (self.height - burden_floor - burden_perimeter)
                / spacing_first_ring
            )
            + 1
        )
        y_holes = np.linspace(
            burden_floor,
            self.height - burden_perimeter,
            number_of_holes_column,
        )[:-1]
        for y in y_holes:
            self.add_hole(
                self.width / 2 - burden_perimeter, y, advance_hole_properties
            )
            self.add_hole(
                -self.width / 2 + burden_perimeter, y, advance_hole_properties
            )

        number_of_holes_row = (
            round((self.width - 2 * burden_perimeter) / spacing_first_ring) + 1
        )
        x_holes = np.linspace(
            -self.width / 2 + burden_perimeter,
            self.width / 2 - burden_perimeter,
            number_of_holes_row,
        )

        for x in x_holes:
            self.add_hole(
                x, self.height - burden_perimeter, advance_hole_properties
            )

        # ring near first ring
        number_of_column_holes = (
            round(
                (
                    self.height
                    - burden_perimeter
                    - burden_first_ring
                    - burden_floor
                )
                / spacing_first_ring
            )
            + 1
        )
        ys = np.linspace(
            burden_floor,
            self.height - burden_perimeter - burden_first_ring,
            number_of_column_holes,
        )
        xs = [
            -self.width / 2 + burden_perimeter + burden_first_ring,
            self.width / 2 - burden_perimeter - burden_first_ring,
        ]
        for x in xs:
            for y in ys:
                self.add_hole(x, y, advance_hole_properties)
                self.add_hole(-x, y, advance_hole_properties)

        number_of_row_holes = round((xs[1] - xs[0]) / spacing_advance) + 1
        x_primes = np.linspace(xs[0], xs[1], number_of_row_holes)[1:-1]
        y = self.height - burden_perimeter - burden_first_ring
        for x in x_primes:
            self.add_hole(x, y, advance_hole_properties)

        # remainings
        remaining_width = (
            self.width / 2 - self.w4 / 2 - burden_perimeter - burden_first_ring
        )
        number_of_remaining_columns = (
            round(remaining_width / spacing_advance) + 1
        )
        xs = np.linspace(
            self.w4 / 2,
            self.width / 2 - burden_perimeter - burden_first_ring,
            number_of_remaining_columns,
        )[1:-1]
        for x in xs:
            for y in ys[:-1]:
                self.add_hole(x, y, advance_hole_properties)
                self.add_hole(-x, y, advance_hole_properties)

        remaining_height = (
            self.height
            - burden_perimeter
            - burden_first_ring
            - burden_floor
            - self.w4
        )
        number_of_rows = round(remaining_height / burden_advance) + 1
        ys = np.linspace(
            burden_floor + self.w4,
            self.height - burden_perimeter - burden_first_ring,
            number_of_rows,
        )[1:-1]
        if len(xs) >= 1:
            number_of_row_holes = round(2 * xs[0] / spacing_advance) + 1
            row_holes = np.linspace(-xs[0], xs[0], number_of_row_holes)[1:-1]
            for y in ys:
                for hole in row_holes:
                    self.add_hole(hole, y, advance_hole_properties)

        self.final_draw()

    def draw_cut_ntnu(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        x_air_hole = 0
        y_air_hole = self.burden_floor + self.w4 / 2

        self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_cut,
        )

        self.add_hole(x_air_hole - self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b1, cut_hole_properties)
        self.add_hole(x_air_hole + self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b1, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(x_air_hole - self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b3, cut_hole_properties)
        self.add_hole(x_air_hole + self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b3, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )

    def draw_plot_olafsson(self):

        self.draw()

        self.draw_cut_olafsson()

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # Walls
        number_of_holes_perimeter_height = (
            round(self.height / self.wall_spacing) + 1
        )
        y_holes = np.linspace(
            0, self.height, number_of_holes_perimeter_height
        )[1:-1]
        for y in y_holes:
            self.add_hole(self.width / 2, y, perimeter_hole_properties)
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)

        # Ceil
        number_of_holes_perimeter_width = (
            round(self.width / self.roof_spacing) + 1
        )
        x_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_perimeter_width
        )
        for x in x_holes:
            self.add_hole(x, self.height, perimeter_hole_properties)

        # Floor
        floor_hole_properties = dict(
            hole_type="Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        number_of_holes_floor = round(self.width / self.floor_spacing) + 1
        x_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_floor
        )
        for x in x_holes:
            self.add_hole(x, 0, floor_hole_properties)

        # Burden rectangle
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        number_of_holes_column = (
            round(
                (self.height - self.floor_burden - self.wall_burden)
                / self.advance_spacing
            )
            + 1
        )
        y_holes = np.linspace(
            self.floor_burden,
            self.height - self.roof_burden,
            number_of_holes_column,
        )[:-1]
        for y in y_holes:
            self.add_hole(
                self.width / 2 - self.wall_burden, y, advance_hole_properties
            )
            self.add_hole(
                -self.width / 2 + self.wall_burden, y, advance_hole_properties
            )

        number_of_holes_row = (
            round((self.width - 2 * self.wall_burden) / self.advance_spacing)
            + 1
        )
        x_holes = np.linspace(
            -self.width / 2 + self.wall_burden,
            self.width / 2 - self.wall_burden,
            number_of_holes_row,
        )
        for x in x_holes:
            self.add_hole(
                x, self.height - self.roof_burden, advance_hole_properties
            )

        # Inner rectangles
        width = round(
            (self.width / 2 - self.last_x / 2 - self.wall_burden)
            / self.advance_burden
        )
        height = round(
            (self.height - self.floor_burden - self.roof_burden - self.last_x)
            / self.advance_burden
        )
        minimum_of_rectangles = min(width, height)
        y = self.height - self.roof_burden
        right_x = self.width / 2 - self.wall_burden
        left_x = -self.width / 2 + self.wall_burden
        operational_burden_advance_h = (
            self.width / 2 - self.wall_burden - self.last_x / 2
        ) / width
        if height != 0:
            operational_burden_advance_v = (
                self.height
                - self.floor_burden
                - self.last_x
                - self.roof_burden
            ) / height
            for _ in range(1, minimum_of_rectangles):
                y -= operational_burden_advance_v
                number_of_holes_inner_columns = round(y / self.advance_spacing)
                y_holes = np.linspace(
                    self.floor_burden, y, number_of_holes_inner_columns
                )[:-1]
                right_x -= operational_burden_advance_h
                left_x += operational_burden_advance_h
                for y in y_holes:
                    self.add_hole(right_x, y, advance_hole_properties)
                    self.add_hole(left_x, y, advance_hole_properties)

                number_of_holes_inner_rows = (
                    round((right_x - left_x) / self.advance_spacing) + 1
                )
                x_holes = np.linspace(
                    left_x, right_x, number_of_holes_inner_rows
                )
                for x in x_holes:
                    self.add_hole(x, y, advance_hole_properties)

        if minimum_of_rectangles == height:
            number_of_white_space_columns = round(
                (right_x - self.last_x / 2) / self.advance_burden
            )
            for _ in range(1, number_of_white_space_columns):
                number_of_holes_whitespace_column = (
                    round((y - self.floor_burden) / self.advance_spacing) + 1
                )
                y_holes = np.linspace(
                    self.floor_burden, y, number_of_holes_whitespace_column
                )[:-1]
                right_x -= operational_burden_advance_h
                left_x += operational_burden_advance_h
                for y in y_holes:
                    self.add_hole(right_x, y, advance_hole_properties)
                    self.add_hole(left_x, y, advance_hole_properties)
        else:
            number_of_white_space_rows = round(
                (y - self.floor_burden - self.last_x) / self.advance_spacing
            )
            for _ in range(1, number_of_white_space_rows):
                number_of_holes_whitespace_row = (
                    round((right_x - left_x) / self.advance_spacing) + 1
                )
                x_holes = np.linspace(
                    left_x, right_x, number_of_holes_whitespace_row
                )[1:-1]
                y -= operational_burden_advance_v
                for x in x_holes:
                    self.add_hole(x, y, advance_hole_properties)

        self.final_draw()

    def draw_cut_olafsson(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        required_parameters_length = len(self.required_parameters)
        if required_parameters_length == 4:
            (
                first_section,
                second_section,
                third_section,
                fourth_section,
            ) = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section
            b3, _ = third_section
            _, x4 = fourth_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x4 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(x_air_hole - b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b3, cut_hole_properties)
            self.add_hole(x_air_hole + b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b3, cut_hole_properties)
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
        elif required_parameters_length == 3:
            (
                first_section,
                second_section,
                third_section,
            ) = self.required_parameters
            _, x1 = first_section
            b2, _ = second_section
            _, x3 = third_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x3 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )

            self.add_hole(
                x_air_hole,
                y_air_hole - b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole,
                y_air_hole + b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )

        elif required_parameters_length == 2:
            first_section, second_section = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x2 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )

        elif required_parameters_length == 1:
            first_section = self.required_parameters
            _, x1 = first_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x1 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
