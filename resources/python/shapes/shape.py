from __future__ import division
from abc import ABC, abstractmethod
from typing import List, Tuple
import math

from PyQt5.QtCore import QObject
from matplotlib.lines import Line2D
from mpl_toolkits.mplot3d import art3d
from mpl_toolkits.mplot3d.art3d import Line3D
import numpy as np
from sympy import symbols, solveset

from ..hole import (
    Hole,
    HOLES_TYPE_TO_STRING_MAP,
    HoleType,
    HOLES_TYPE_TO_COLOR_MAP,
)
from ..utils.Helper import Helper


class AbstractShape(ABC):
    ERROR_TOLERANCE = 1e-2

    @abstractmethod
    def draw(self):
        pass

    @abstractmethod
    def draw_patch(self):
        pass

    @abstractmethod
    def snap_point_near_edges(
        self, point: Tuple[float, float]
    ) -> Tuple[float, float]:
        pass

    @abstractmethod
    def is_point_near_edges(self, point: Tuple[float, float]) -> bool:
        pass

    @abstractmethod
    def is_point_in_shape(self, point: Tuple[float, float]) -> bool:
        pass

    @abstractmethod
    def is_point_inside_cut(self, point: Tuple[float, float]) -> bool:
        pass

    @abstractmethod
    def get_area(self):
        pass

    @abstractmethod
    def get_perimeter(self):
        pass

    @abstractmethod
    def get_perimeter_average_length(self):
        pass

    @abstractmethod
    def check_space(self):
        pass


class ShapeMetaClass(type(QObject), type(ABC)):
    pass


class Shape(QObject, AbstractShape, metaclass=ShapeMetaClass):
    """This is shape class"""

    def __init__(self, parent=None, properties=None):
        super().__init__(parent)

        self.sc_angular_2d_front = None
        """This property is used for creating scatter angular shape in 2d mode"""
        self.sc_angular_2d_back = None
        """This property is used for creating scatter angular shape in 2d mode"""
        self.is_2d = (
            True
            if ("plot_view" in properties and properties["plot_view"] == "2D")
            or ("plot_view" not in properties)
            else False
        )
        self.is_angular = (
            True
            if ("angle" in properties and properties["angle"] != 0)
            else False
        )
        """This property is indicated 2d selection or not"""
        self.rounded_hole_x = []
        """This property is for creating background of shape in 3d"""
        self.rounded_hole_y = []
        """This property is for creating background of shape in 3d"""
        self.has_only_patch_in_draw = False
        """This property is a boolean to show path in draw"""
        self.patch = None
        """This property is used by children class"""
        self.sending_patch = None
        """This property is used by children class"""
        self.sending_patch_cut = None
        """This property is used by children class and getting patch for helping creating background"""
        self.sc_back = []
        """This property is holding second scatter"""
        self.holes = []
        """This property is holding list of holes"""
        self.method = ""
        """This property is indicated method name from selected input of user"""
        self.result = None
        """This property is holding of results"""
        self.sc = []
        """This property is holding scatter of front"""

        for key, value in properties.items():
            setattr(self, key, value)

    def add_hole(self, x: float, y: float, properties):
        is_duplicate = False
        for hole in self.holes:
            if hole.coordinates == (x, y):
                print("Error: holes in in cover", x, y, hole.hole_type)
                is_duplicate = True
                break
        if not is_duplicate:
            self.holes.append(Hole((x, y), **properties))

    def get_holes_by_type(self, hole_type: str) -> List[Hole]:
        return list(
            filter(lambda hole: hole.hole_type == hole_type, self.holes)
        )

    def draw_line(self, xs: List[float], ys: List[float], color: str) -> None:
        if self.is_2d:
            self.axes.add_line(
                Line2D(xs, ys, linestyle="-", marker="", color=color)
            )
        else:
            self.axes.add_line(
                Line3D(
                    xs,
                    ys,
                    [float(0) for i in xs],
                    linestyle="-",
                    marker="",
                    color=color,
                )
            )

    def draw_rounded_line(
        self, xs: List[float], ys: List[float], zs: List[float], color: str
    ):
        """This method is helper for using creating background in 3d
        Args:
            xs(List[float]): xs array of holes coordinates
            ys(List[float]): ys array of holes coordinates
            zs(List[float]): zs array of holes coordinates
            color(str): color of drawing line
        """
        self.axes.add_line(
            Line3D(xs, ys, zs, linestyle="-", marker="", color=color)
        )

    def append_rounded_point(self, xs: List[float], ys: List[float]):
        """This method is helper for using creating background in 3d, it is used by childes of shape class
        Args:
           xs(List[float]): xs array of holes coordinates
           ys(List[float]): ys array of holes coordinates"""
        self.rounded_hole_x.append(xs)
        self.rounded_hole_y.append(ys)

    def draw_second_rounded_patch(self, z):
        """This method is helper for using creating background in 3d, It is necessary in 3d to drawing a patch,
        If not using it would crash
        Args:
            z(float): z of hole coordinates"""
        art3d.pathpatch_2d_to_3d(self.sending_patch, z=z, zdir="z")

    def draw_second_rounded_patch_cut(self, z):
        """This method is helper for using creating background in 3d, It is necessary in 3d to drawing a patch,
        If not using it would crash
        It is for creating rectangle in cut holes
        Args:
            z(float): z of hole coordinates"""
        art3d.pathpatch_2d_to_3d(self.sending_patch_cut, z=z, zdir="z")

    def remove_hole_by_xy(self, xy: Tuple[float, float]) -> Hole:
        for hole in self.holes:
            if (
                abs(hole.coordinates[0] - xy[0]) < self.ERROR_TOLERANCE
                and abs(hole.coordinates[1] - xy[1]) < self.ERROR_TOLERANCE
            ):
                self.holes.remove(hole)
                return

    def remove_holes(self) -> None:
        for xy in self.black_list:
            self.remove_hole_by_xy(xy)

    def draw_background_shape(self):
        """Draw background of shape in 3d mode"""
        if self.has_only_patch_in_draw:
            if self.sending_patch is not None:
                self.axes.add_patch(self.sending_patch)
                self.draw_second_rounded_patch(float(-self.hole_length))
            if self.sending_patch_cut is not None:
                self.axes.add_patch(self.sending_patch_cut)
                self.draw_second_rounded_patch_cut(float(-self.hole_length))
        else:
            for k in range(len(self.rounded_hole_x)):
                # print(self.rounded_hole_x[k])
                self.draw_rounded_line(
                    self.rounded_hole_x[k],
                    self.rounded_hole_y[k],
                    [float(-self.hole_length) for z in self.rounded_hole_x[k]],
                    color="lime",
                )
            if self.sending_patch is not None:
                self.axes.add_patch(self.sending_patch)
                self.draw_second_rounded_patch(float(-self.hole_length))
            if self.sending_patch_cut is not None:
                self.axes.add_patch(self.sending_patch_cut)
                self.draw_second_rounded_patch_cut(float(-self.hole_length))

    def get_correspond_xz_map(self):
        """Create correspond xz map from x_coordinates_property which is defined in any shape, fineTheta is angle of
        Isosceles triangle, theta_offset_zero is equal to degree from enclosing by remaining holes to 90,
        theta_offset is angular which is used to plural by fineTheta, index is indicator which is used to find
        theta_offset, it means index is crossed to theta_offset_zero, index 0 means x=0, so x_offset = x, in type II
        x_offset is equal to half of x, x_offset depends on tan(theta)
        """
        helper = Helper()
        fineTheta = (180 - self.angle) / 2
        # find angular x1,z1 and correspond's x2,z2 in cut {x1: x2}, {x1: z2}
        coordinates_correspond_map = {}
        half_of_x = self.x_length / 2
        # print(self.x_coordinates_property)
        for row in self.x_coordinates_property:
            number_of_holes_in_row = len(self.x_coordinates_property[row]) / 2
            theta_offset_zero = (90 - fineTheta) / float(
                number_of_holes_in_row
            )
            last_index_point = helper.get_maximum_index_in_dict_into_array(
                self.x_coordinates_property[row], "index"
            )

            row_property = []
            coordinates_correspond_map[row] = row_property
            for point in self.x_coordinates_property[row]:
                for hole in self.holes:
                    x_hole = hole.coordinates[0]
                    y_hole = hole.coordinates[1]
                    if point["x"] == x_hole and point["y"] == y_hole:
                        # find x2 correspond
                        theta_offset = theta_offset_zero * (point["index"] + 1)
                        newTheta = fineTheta + theta_offset
                        if point["index"] == 0:
                            # Check Type
                            if self.cut_type == "Type I":
                                x_offset = math.fabs(x_hole)
                            else:
                                if (
                                    "type" in point
                                    and point["type"] == "small-triangle"
                                ):
                                    x_offset = math.fabs(x_hole)
                                else:
                                    x_offset = math.fabs(x_hole) - half_of_x
                            # print(x_offset)
                        else:
                            # It depends on angle
                            if point["index"] == last_index_point:
                                #     newTheta = 90
                                x_offset = 0
                            else:
                                x_offset = self.advance / math.tan(
                                    math.radians(newTheta)
                                )
                        if point["x"] > 0:
                            x2_temp = point["x"] - x_offset
                        else:
                            x2_temp = point["x"] + x_offset
                        # small triangle z: because x is always zero
                        if (
                            "type" in point
                            and point["type"] == "small-triangle"
                        ):
                            z2_temp = (
                                point["length"]
                                * math.tan(math.radians(fineTheta))
                            ) * -1
                        else:
                            z2_temp = (self.advance / 0.95) * -1
                        coordinates_correspond_map[row].append(
                            [x_hole, x2_temp, y_hole, z2_temp]
                        )
                    else:
                        continue
        return coordinates_correspond_map

    def get_correspond_3d_coordinates_For_Angular(self):
        """This method is return correspond coordinates in 3d mode(xs2, ys2, xs2)
        Returns:
            HOLE_TYPE_TO_COORDINATES_2_MAP(dict): A dictionary which keys are hole_type and values are coordinates"""
        # coordinates_correspond_map is like {0: [ [x1, x2, y1, z2], [x1, x2, y1, z2], ... ],
        # 1: [ [x1, x2, y1, z2], [x1, x2, y1, z2], ... ] }
        # key indicates row number and value is a list which contains all holes's property in that row
        coordinates_correspond_map = self.get_correspond_xz_map()
        #   append to xs1_angular if hole.coordinates is satisfying condition
        HOLE_TYPE_TO_COORDINATES_2_MAP = {
            HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]: [[], [], []],
            HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]: [[], [], []],
            HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]: [[], [], []],
            HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]: [[], [], []],
            HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]: [[], [], []],
            HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]: [[], [], []],
        }

        for hole in self.holes:
            x_hole = hole.coordinates[0]
            y_hole = hole.coordinates[1]
            xs2_temp = None
            zs2_temp = None
            for row in coordinates_correspond_map:
                holes_in_correspond = coordinates_correspond_map[row]
                for hole_props in holes_in_correspond:
                    if x_hole == hole_props[0] and y_hole == hole_props[2]:
                        xs2_temp = hole_props[1]
                        zs2_temp = hole_props[3]
                    else:
                        continue
            if xs2_temp is None and zs2_temp is None:
                # filling like parallel
                xs2_temp = x_hole
                zs2_temp = -self.hole_length
            HOLE_TYPE_TO_COORDINATES_2_MAP[hole.hole_type][0].append(xs2_temp)
            HOLE_TYPE_TO_COORDINATES_2_MAP[hole.hole_type][1].append(y_hole)
            HOLE_TYPE_TO_COORDINATES_2_MAP[hole.hole_type][2].append(zs2_temp)
        return HOLE_TYPE_TO_COORDINATES_2_MAP

    def get_correspond_points_for_angular_2d(self):
        """This method is return correspond coordinates in 2d mode(xs2, ys2, xs2) and angular for creating second
        axes
        Returns:
            xs2(List): List of points in axes order
            ys2(List): List of points in axes order
            zs2(List): List of points in axes order
            xs1(List): List of points in axes order
            zs1(List): List of points in axes order
        """
        coordinates_correspond_map = self.get_correspond_xz_map()
        # print(coordinates_correspond_map)
        #   append to xs1_angular if hole.coordinates is satisfying condition
        xs2 = []
        ys2 = []
        zs2 = []
        xs1 = []
        zs1 = []
        for hole in self.holes:
            x_hole = hole.coordinates[0]
            y_hole = hole.coordinates[1]
            xs2_temp = None
            zs2_temp = None
            # Always return first row for creating second axes in angular mode
            holes_in_correspond = coordinates_correspond_map[0]
            for hole_props in holes_in_correspond:
                if x_hole == hole_props[0] and y_hole == hole_props[2]:
                    xs2_temp = hole_props[1]
                    zs2_temp = hole_props[3]
                else:
                    continue
            if xs2_temp is not None and zs2_temp is not None:
                xs2.append(xs2_temp)
                zs2.append(zs2_temp)
                ys2.append(y_hole)
                xs1.append(x_hole)
                zs1.append(float(0))
        return xs2, ys2, zs2, xs1, zs1

    def connect_holes_in_3d(self, front_map, back_map):
        """Draw a line between holes in 3d mode
        Args:
            front_map(dict): keys are hole_type and values are coordinates for example: Advance: [[],[],[]]
            back_map(dict): keys are hole_type and values are coordinates for example: Advance: [[],[],[]]
        """
        for key in front_map:
            # check existence property
            if (
                len(front_map[key][0]) == 0
                or len(front_map[key][1]) == 0
                or len(front_map[key][2]) == 0
            ):
                continue
            if (
                len(back_map[key][0]) == 0
                or len(back_map[key][1]) == 0
                or len(back_map[key][2]) == 0
            ):
                continue

            xs1 = front_map[key][0]
            ys1 = front_map[key][1]
            zs1 = front_map[key][2]
            xs2 = back_map[key][0]
            ys2 = back_map[key][1]
            zs2 = back_map[key][2]
            for i_1, i_2, j_1, j_2, k_1, k_2 in zip(
                xs1, xs2, ys1, ys2, zs1, zs2
            ):
                self.axes.plot(
                    [i_1, i_2],
                    [j_1, j_2],
                    [k_1, k_2],
                    color=HOLES_TYPE_TO_COLOR_MAP[key],
                )

    def create_second_axes__for_angular(self):
        """Creating second axes in angular and 2d mode"""
        xs2, _, zs2, xs1, zs1 = self.get_correspond_points_for_angular_2d()

        self.sc_angular_2d_back = self.axes_angular_cut_page.scatter(
            xs2, zs2, picker=True, color="red"
        )
        self.sc_angular_2d_front = self.axes_angular_cut_page.scatter(
            xs1, zs1, picker=True, color="green"
        )
        for i in range(len(xs1)):
            self.axes_angular_cut_page.plot(
                [xs2[i], xs1[i]], [zs2[i], zs1[i]], "black"
            )

        self.axes_angular_cut_page.invert_xaxis()
        self.axes_angular_cut_page.invert_yaxis()

    def get_holes_xy_with_z_in_order_to_type(self):
        """This method is a helper for getting all xs and ys in order to each hole type
        Returns:
           dic: includes all xs,ys based on hole_type"""
        # Get all holes by their types
        holes_Advance = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]
        )
        holes_Perimeter = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]
        )
        holes_Cut = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]
        )
        holes_Uncharged = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]
        )
        holes_Floor = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]
        )
        holes_Buffer = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]
        )

        # Get all xy from each holes
        xs_advance = [hole.coordinates[0] for hole in holes_Advance]
        ys_advance = [hole.coordinates[1] for hole in holes_Advance]

        xs_perimeter = [hole.coordinates[0] for hole in holes_Perimeter]
        ys_perimeter = [hole.coordinates[1] for hole in holes_Perimeter]

        xs_cut = [hole.coordinates[0] for hole in holes_Cut]
        ys_cut = [hole.coordinates[1] for hole in holes_Cut]

        xs_uncharged = [hole.coordinates[0] for hole in holes_Uncharged]
        ys_uncharged = [hole.coordinates[1] for hole in holes_Uncharged]

        xs_floor = [hole.coordinates[0] for hole in holes_Floor]
        ys_floor = [hole.coordinates[1] for hole in holes_Floor]

        xs_buffer = [hole.coordinates[0] for hole in holes_Buffer]
        ys_buffer = [hole.coordinates[1] for hole in holes_Buffer]

        # resolve into map ---> map: from hole_type to coordinates
<<<<<<< HEAD
        HOLE_TYPE_TO_COORDINATES_MAP = {
            HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]: [
                xs_advance,
                ys_advance,
                [float(0) for x in xs_advance],
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]: [
                xs_perimeter,
                ys_perimeter,
                [float(0) for x in xs_perimeter],
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]: [
                xs_cut,
                ys_cut,
                [float(0) for x in xs_cut],
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]: [
                xs_uncharged,
                ys_uncharged,
                [float(0) for x in xs_uncharged],
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]: [
                xs_floor,
                ys_floor,
                [float(0) for x in xs_floor],
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]: [
                xs_buffer,
                ys_buffer,
                [float(0) for x in xs_buffer],
            ],
        }
=======
        HOLE_TYPE_TO_COORDINATES_MAP = {HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]: [xs_advance, ys_advance,
                                                                                     [float(0) for x in xs_advance]],
                                        HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]: [xs_perimeter, ys_perimeter,
                                                                                       [float(0) for x in
                                                                                        xs_perimeter]],
                                        HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]: [xs_cut, ys_cut,
                                                                                 [float(0) for x in xs_cut]],
                                        HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]: [xs_uncharged, ys_uncharged,
                                                                                       [float(0) for x in
                                                                                        xs_uncharged]],
                                        HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]: [xs_floor, ys_floor,
                                                                                   [float(0) for x in xs_floor]],
                                        HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]: [xs_buffer, ys_buffer,
                                                                                    [float(0) for x in xs_buffer]]}
>>>>>>> ehsan-dev
        return HOLE_TYPE_TO_COORDINATES_MAP

    def get_holes_xyz_2_in_order_to_type(self):
        """This method is using for background of whole shape that z correspond is hole_length"""
        holes_Advance = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]
        )
        holes_Perimeter = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]
        )
        holes_Cut = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]
        )
        holes_Uncharged = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]
        )
        holes_Floor = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]
        )
        holes_Buffer = self.get_holes_by_type(
            HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]
        )

        # Get all xy from each holes
        xs_2_advance = [hole.coordinates[0] for hole in holes_Advance]
        ys_2_advance = [hole.coordinates[1] for hole in holes_Advance]
        zs_2_advance = [-self.hole_length for x in xs_2_advance]

        xs_2_perimeter = [hole.coordinates[0] for hole in holes_Perimeter]
        ys_2_perimeter = [hole.coordinates[1] for hole in holes_Perimeter]
        zs_2_perimeter = [-self.hole_length for x in xs_2_perimeter]

        xs_2_cut = [hole.coordinates[0] for hole in holes_Cut]
        ys_2_cut = [hole.coordinates[1] for hole in holes_Cut]
        zs_2_cut = [-self.hole_length for x in xs_2_cut]

        xs_2_uncharged = [hole.coordinates[0] for hole in holes_Uncharged]
        ys_2_uncharged = [hole.coordinates[1] for hole in holes_Uncharged]
        zs_2_uncharged = [-self.hole_length for x in xs_2_uncharged]

        xs_2_floor = [hole.coordinates[0] for hole in holes_Floor]
        ys_2_floor = [hole.coordinates[1] for hole in holes_Floor]
        zs_2_floor = [-self.hole_length for x in xs_2_floor]

        xs_2_buffer = [hole.coordinates[0] for hole in holes_Buffer]
        ys_2_buffer = [hole.coordinates[1] for hole in holes_Buffer]
        zs_2_buffer = [-self.hole_length for x in xs_2_buffer]

        # resolve into map ---> map: from hole_type to coordinates
<<<<<<< HEAD
        HOLE_TYPE_TO_COORDINATES_2_MAP = {
            HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]: [
                xs_2_advance,
                ys_2_advance,
                zs_2_advance,
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]: [
                xs_2_perimeter,
                ys_2_perimeter,
                zs_2_perimeter,
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]: [
                xs_2_cut,
                ys_2_cut,
                zs_2_cut,
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]: [
                xs_2_uncharged,
                ys_2_uncharged,
                zs_2_uncharged,
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]: [
                xs_2_floor,
                ys_2_floor,
                zs_2_floor,
            ],
            HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]: [
                xs_2_buffer,
                ys_2_buffer,
                zs_2_buffer,
            ],
        }
=======
        HOLE_TYPE_TO_COORDINATES_2_MAP = {HOLES_TYPE_TO_STRING_MAP[HoleType.ADVANCE]: [xs_2_advance, ys_2_advance,
                                                                                       zs_2_advance],
                                          HOLES_TYPE_TO_STRING_MAP[HoleType.PERIMETER]: [xs_2_perimeter, ys_2_perimeter,
                                                                                         zs_2_perimeter],
                                          HOLES_TYPE_TO_STRING_MAP[HoleType.CUT]: [xs_2_cut, ys_2_cut, zs_2_cut],
                                          HOLES_TYPE_TO_STRING_MAP[HoleType.UNCHARGED]: [xs_2_uncharged, ys_2_uncharged,
                                                                                         zs_2_uncharged],
                                          HOLES_TYPE_TO_STRING_MAP[HoleType.FLOOR]: [xs_2_floor, ys_2_floor,
                                                                                     zs_2_floor],
                                          HOLES_TYPE_TO_STRING_MAP[HoleType.BUFFER]: [xs_2_buffer, ys_2_buffer,
                                                                                      zs_2_buffer]}
>>>>>>> ehsan-dev

        return HOLE_TYPE_TO_COORDINATES_2_MAP

    def final_draw(self) -> None:
        """
        This method is for creating final shape
        """
        self.remove_holes()

        # Get all xy from each holes
        HOLE_TYPE_TO_COORDINATES_MAP = (
            self.get_holes_xy_with_z_in_order_to_type()
        )

        # 2D
        if self.is_2d:
            if self.is_angular:
                self.create_second_axes__for_angular()
            self.set_sc(True, HOLE_TYPE_TO_COORDINATES_MAP)

        # 3D
        else:
            # draw foreground
            self.set_sc(False, HOLE_TYPE_TO_COORDINATES_MAP)
            # draw background line of shape in 3d
            self.draw_background_shape()

            # draw background scatter
            if self.is_angular:
                # angular
                HOLE_TYPE_TO_COORDINATES_2_MAP = (
                    self.get_correspond_3d_coordinates_For_Angular()
                )
                self.set_sc_back(HOLE_TYPE_TO_COORDINATES_2_MAP)
                self.connect_holes_in_3d(
                    HOLE_TYPE_TO_COORDINATES_MAP,
                    HOLE_TYPE_TO_COORDINATES_2_MAP,
                )
            else:
                # parallel
                # create xyz_2_map, parallel xyz_2_map
                HOLE_TYPE_TO_COORDINATES_2_MAP = (
                    self.get_holes_xyz_2_in_order_to_type()
                )
                self.set_sc_back(HOLE_TYPE_TO_COORDINATES_2_MAP)
<<<<<<< HEAD
                self.connect_holes_in_3d(
                    HOLE_TYPE_TO_COORDINATES_MAP,
                    HOLE_TYPE_TO_COORDINATES_2_MAP,
                )

=======
                self.connect_holes_in_3d(HOLE_TYPE_TO_COORDINATES_MAP, HOLE_TYPE_TO_COORDINATES_2_MAP)
>>>>>>> ehsan-dev
        self.canvas.draw_idle()

    def set_sc(self, is_2d, coordinates_map):
        """
        This method is a setter for sc attribute
        Args:
            is_2d(bool): indicates 2d mode or 3d
            coordinates_map(dict): containing map of hole_type to coordinates
        """
        if is_2d:
            for key in coordinates_map:
                self.sc.append(
                    self.axes.scatter(
                        coordinates_map[key][0],
                        coordinates_map[key][1],
                        picker=True,
                        color=HOLES_TYPE_TO_COLOR_MAP[key],
                    )
                )
        else:
            for key in coordinates_map:
                self.sc.append(
                    self.axes.scatter3D(
                        coordinates_map[key][0],
                        coordinates_map[key][1],
                        coordinates_map[key][2],
                        picker=True,
                        color=HOLES_TYPE_TO_COLOR_MAP[key],
                    )
                )

    def set_sc_back(self, coordinates_2_map):
        """
        This method is a setter for sc_back attribute
        Args:
            coordinates_2_map(dict): containing map of hole_type to coordinates
        """
        for key in coordinates_2_map:
            self.sc_back.append(
                self.axes.scatter3D(
                    coordinates_2_map[key][0],
                    coordinates_2_map[key][1],
                    coordinates_2_map[key][2],
                    picker=True,
                    color=HOLES_TYPE_TO_COLOR_MAP[key],
                )
            )

    def ebm(self):

        self.ebm_primary_calculations()
        self.draw_plot_ebm()
        self.ebm_final_calculations()
        return self.result

    def ebm_primary_calculations(self):

        self.method = "EBM"
        number_of_cut_holes = 8

        # Constants
        s_cut = 6400
        if self.crushing == "Low Crushing":
            s_advance = 320
        elif self.crushing == "Medium Crushing":
            s_advance = 425
        elif self.crushing == "High Crushing":
            s_advance = 640
        # s_primeter = 160

        self.hole_length = self.advance / 0.95

        # Cut calculations

        self.cut_length = 0.8
        self.volume_cut = self.advance * self.cut_length ** 2

        if self.explosive_type == "Emulsion":
            self.set_emulsion_diameter()

            eta1_cut = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )

            eta2_cut = 1 / (
                math.e ** (self.hole_diameter / self.explosive_diameter_cut)
                - math.e
                + 1
            )

            eta3_cut = 0.15

            specific_explosive_cut = (
                s_cut * self.rock_specific_surface_energy
            ) / (
                eta1_cut
                * eta2_cut
                * eta3_cut
                * self.explosive_specific_energy
                * 1000
            )

            self.total_explosive_cut = specific_explosive_cut * self.volume_cut

            maximum_total_explosive_cut = (
                (self.hole_length - 5 * self.hole_diameter / 1000)
                * number_of_cut_holes
                * self.explosive_density
                * ((np.pi * self.explosive_diameter_cut ** 2) / 4e6)
            )

            if self.total_explosive_cut > maximum_total_explosive_cut:
                self.total_explosive_cut = maximum_total_explosive_cut
                self.volume_cut = (
                    self.total_explosive_cut / specific_explosive_cut
                )
                self.cut_length = math.sqrt(self.volume_cut / self.advance)

            self.mass_of_one_explosive = (
                self.explosive_density
                * math.pi
                * self.explosive_diameter_cut ** 2
                * 0.25
                * 0.3
                / 1e6
            )
            number_of_explosives_cut = (
                self.total_explosive_cut / self.mass_of_one_explosive
            )

            self.number_of_cartridges_per_hole_cut = round(
                number_of_explosives_cut / number_of_cut_holes
            )
            length_of_explosive_cut = (
                self.number_of_cartridges_per_hole_cut * 0.3
            )

            self.cut_stemming = self.hole_length - length_of_explosive_cut

        elif self.explosive_type in [
            "Anfo Pneumatically Charged",
            "Anfo Bagged",
        ]:
            if self.explosive_type == "Anfo Bagged":
                anfo_diameter = 0.95 * self.hole_diameter
            elif self.explosive_type == "Anfo Pneumatically Charged":
                anfo_diameter = self.hole_diameter

            eta1_cut = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )

            eta2_cut = 1 / (
                math.e ** (self.hole_diameter / anfo_diameter) - math.e + 1
            )

            eta3_cut = 0.15

            specific_explosive_cut = (
                s_cut * self.rock_specific_surface_energy
            ) / (
                eta1_cut
                * eta2_cut
                * eta3_cut
                * self.explosive_specific_energy
                * 1000
            )

            self.total_explosive_cut = specific_explosive_cut * self.volume_cut

            maximum_total_explosive_cut = (
                self.hole_length
                * number_of_cut_holes
                * self.explosive_density
                * ((np.pi * anfo_diameter ** 2) / 4e6)
            )

            if self.total_explosive_cut > maximum_total_explosive_cut:
                self.total_explosive_cut = maximum_total_explosive_cut
                self.volume_cut = (
                    self.total_explosive_cut / specific_explosive_cut
                )
                self.cut_length = math.sqrt(self.volume_cut / self.advance)

            self.total_anfo_cut = (
                self.total_explosive_cut - number_of_cut_holes * 0.4
            )

            self.anfo_per_meter_cut = (
                math.pi
                * (self.hole_diameter / 1000) ** 2
                * self.explosive_density
                / 4
            )

            length_of_anfo_cut = self.total_anfo_cut / (
                self.anfo_per_meter_cut * number_of_cut_holes
            )

            self.cut_stemming = self.hole_length - length_of_anfo_cut

            self.explosive_diameter_cut = 35

            self.number_of_cartridges_per_hole_cut = 1

        # Perimeter calculations
        self.perimeter_spacing = 15 * self.hole_diameter / 1000
        self.perimeter_burden = 1.25 * self.perimeter_spacing
        specific_explosive_perimeter = 90 * (self.hole_diameter / 1000) ** 2

        number_of_holes_perimeter = (
            round((self.get_perimeter() / self.perimeter_spacing)) - 2
        )

        mass_of_one_explosive_perimeter = (
            self.explosive_density
            * math.pi
            * self.explosive_perimetral_diameter ** 2
            * 0.25
            * 0.3
            / 1e6
        )

        self.number_of_cartridges_perimeter_per_hole = math.ceil(
            (specific_explosive_perimeter * self.hole_length)
            / mass_of_one_explosive_perimeter
        )

        self.total_explosive_perimeter = (
            number_of_holes_perimeter
            * self.number_of_cartridges_perimeter_per_hole
            * mass_of_one_explosive_perimeter
        )

        self.volume_perimeter = (
            self.hole_length
            * self.perimeter_burden
            * self.get_perimeter_average_length()
        )

        length_of_explosive_perimeter = (
            self.number_of_cartridges_perimeter_per_hole * 0.3
        )
        self.perimeter_stemming = (
            self.hole_length - length_of_explosive_perimeter
        )

        # Advance calculations
        if self.explosive_type == "Emulsion":
            eta1_advance = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )
            eta2_advance = 1 / (
                math.e
                ** (self.hole_diameter / self.explosive_diameter_advance)
                - math.e
                + 1
            )
            eta3_advance = 0.15

            self.specific_explosive_advance = (
                s_advance
                * self.rock_specific_surface_energy
                / (
                    eta1_advance
                    * eta2_advance
                    * eta3_advance
                    * self.explosive_specific_energy
                    * 1000
                )
            )

            self.advance_burden = (
                self.explosive_diameter_advance
                * 1e-3
                * math.sqrt(
                    (math.pi * self.explosive_density)
                    / (4 * self.specific_explosive_advance)
                )
            )

        elif self.explosive_type in [
            "Anfo Pneumatically Charged",
            "Anfo Bagged",
        ]:
            if self.explosive_type == "Anfo Bagged":
                anfo_diameter = 0.95 * self.hole_diameter
            elif self.explosive_type == "Anfo Pneumatically Charged":
                anfo_diameter = self.hole_diameter

            eta1_advance = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )
            eta2_advance = 1 / (
                math.e ** (self.hole_diameter / anfo_diameter) - math.e + 1
            )
            eta3_advance = 0.15

            self.specific_explosive_advance = (
                s_advance
                * self.rock_specific_surface_energy
                / (
                    eta1_advance
                    * eta2_advance
                    * eta3_advance
                    * self.explosive_specific_energy
                    * 1000
                )
            )

            self.advance_burden = (
                anfo_diameter
                * 1e-3
                * math.sqrt(
                    (math.pi * self.explosive_density)
                    / (4 * self.specific_explosive_advance)
                )
            )

        if self.check_space():
            self.perimeter_burden = self.advance_burden

        self.advance_spacing = self.advance_burden

    def set_emulsion_diameter(self):

        EXPLOSIVE_DIAMETERS = [27, 30, 35, 40, 45, 50]
        if self.hole_diameter >= 28 and self.hole_diameter < 32:
            self.explosive_diameter = (
                self.explosive_diameter_cut
            ) = self.explosive_diameter_advance = EXPLOSIVE_DIAMETERS[0]
        elif self.hole_diameter >= 32 and self.hole_diameter < 37:
            self.explosive_diameter = (
                self.explosive_diameter_cut
            ) = self.explosive_diameter_advance = EXPLOSIVE_DIAMETERS[1]
        elif self.hole_diameter >= 37 and self.hole_diameter < 43:
            self.explosive_diameter = (
                self.explosive_diameter_cut
            ) = self.explosive_diameter_advance = EXPLOSIVE_DIAMETERS[2]
        elif self.hole_diameter >= 43 and self.hole_diameter < 48:
            self.explosive_diameter = (
                self.explosive_diameter_cut
            ) = self.explosive_diameter_advance = EXPLOSIVE_DIAMETERS[3]
        elif self.hole_diameter >= 48 and self.hole_diameter < 52:
            self.explosive_diameter = (
                self.explosive_diameter_cut
            ) = self.explosive_diameter_advance = EXPLOSIVE_DIAMETERS[4]
        elif self.hole_diameter >= 52:
            self.explosive_diameter = (
                self.explosive_diameter_cut
            ) = self.explosive_diameter_advance = EXPLOSIVE_DIAMETERS[5]

    def ebm_final_calculations(self):

        number_of_holes_advance = len(self.get_holes_by_type("Advance"))

        volume_advance = (self.hole_length * (self.get_area())) - (
            self.volume_cut + self.volume_perimeter
        )

        total_explosive_advance = (
            volume_advance * self.specific_explosive_advance
        )
        total_anfo = 0

        if self.explosive_type == "Emulsion":
            number_of_cartridges_advance = (
                total_explosive_advance / self.mass_of_one_explosive
            )
            number_of_cartridges_advance_per_hole = round(
                number_of_cartridges_advance / number_of_holes_advance
            )

            length_of_explosive_advance = (
                0.3 * number_of_cartridges_advance_per_hole
            )

            self.advance_stemming = (
                self.hole_length - length_of_explosive_advance
            )

            total_explosive_emulsion = (
                self.total_explosive_cut
                + total_explosive_advance
                + self.total_explosive_perimeter
            )

        elif self.explosive_type in [
            "Anfo Pneumatically Charged",
            "Anfo Bagged",
        ]:
            number_of_cartridges_advance_per_hole = 1
            self.explosive_diameter_advance = 35
            number_of_cartridges_advance = (
                number_of_holes_advance * number_of_cartridges_advance_per_hole
            )
            length_of_anfo_advance = total_explosive_advance / (
                number_of_holes_advance * self.anfo_per_meter_cut
            )
            self.advance_stemming = self.hole_length - length_of_anfo_advance
            total_anfo_advance = (
                total_explosive_advance - number_of_holes_advance * 0.4
            )
            total_anfo = self.total_anfo_cut + total_anfo_advance
            total_explosive_emulsion = (len(self.holes) - 8) * 0.4

        # Total calculation
        advance_holes = self.get_holes_by_type("Advance")
        for hole in advance_holes:
            hole.stemming = self.advance_stemming

        total_volume = self.volume_cut + volume_advance + self.volume_perimeter
        specific_explosive_emulsion = total_explosive_emulsion / total_volume
        specific_explosive_anfo = total_anfo / total_volume
        specific_drilling = len(self.holes) * self.hole_length / total_volume

        self.result = dict(
            total_explosive_emulsion=total_explosive_emulsion,
            total_explosive_anfo=total_anfo,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=specific_explosive_emulsion,
            specific_explosive_anfo=specific_explosive_anfo,
            specific_drilling=specific_drilling,
            explosive_diameter_cut=self.explosive_diameter_cut,
            explosive_diameter_advance=self.explosive_diameter_advance,
            explosive_perimetral_diameter=self.explosive_perimetral_diameter,
            number_of_cartridges_per_hole_cut=(
                self.number_of_cartridges_per_hole_cut
            ),
            number_of_cartridges_advance_per_hole=(
                number_of_cartridges_advance_per_hole
            ),
            number_of_cartridges_perimeter_per_hole=(
                self.number_of_cartridges_perimeter_per_hole
            ),
        )

    def ebm_angular(self):

        self.ebm_angular_primary_calculations()
        self.draw_plot_ebm_angular()
        self.ebm_angular_final_calculations()
        return self.result

    def ebm_angular_primary_calculations(self):

        self.method = "EBM Angular"

        # Constants
        s_cut = 1280
        if self.crushing == "Low Crushing":
            s_advance = 320
        elif self.crushing == "Medium Crushing":
            s_advance = 425
        elif self.crushing == "High Crushing":
            s_advance = 640
        # s_primeter = 160

        if self.cut_type == "Type I":
            self.x_length = 0
            self.angle = 90 - self.angle / 2

        self.cut_rows_vertical_distance = 20 * self.hole_diameter * 1e-3

        self.hole_length = self.advance / 0.95

        first_hole_length_cut = self.hole_length / math.sin(
            self.angle * np.pi / 180
        )

        self.triangle_half_length = (
            self.x_length
            + first_hole_length_cut * np.cos(self.angle * np.pi / 180)
        )

        self.cut_burden = 34 * self.hole_diameter / 1000

        # Cut calculations
        if self.explosive_type == "Emulsion":
            self.set_emulsion_diameter()

            eta1_cut = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )

            eta2_cut = 1 / (
                math.e ** (self.hole_diameter / self.explosive_diameter_cut)
                - math.e
                + 1
            )

            eta3_cut = 0.15

            specific_explosive_cut = (
                s_cut * self.rock_specific_surface_energy
            ) / (
                eta1_cut
                * eta2_cut
                * eta3_cut
                * self.explosive_specific_energy
                * 1000
            )

            self.cut_develop_length = (
                self.explosive_diameter_advance
                * 1e-3
                * math.sqrt(
                    self.explosive_density
                    * math.pi
                    / (3.2 * specific_explosive_cut)
                )
            )

            self.cut_develop_front_length = (
                (
                    self.cut_develop_length
                    * (self.width - 2 * self.triangle_half_length)
                    / self.width
                )
                if self.shape_name != "Circle"
                else (
                    self.cut_develop_length
                    * (self.radius - self.triangle_half_length)
                    / self.radius
                )
            )

            if self.cut_rows == 2:
                if self.hole_length / self.cut_burden > 1:
                    number_of_cut_holes = 12
                else:
                    number_of_cut_holes = 8
            elif self.cut_rows == 3:
                if self.hole_length / self.cut_burden > 1:
                    number_of_cut_holes = 18
                else:
                    number_of_cut_holes = 12

            self.volume_cut = (
                (
                    self.x_length
                    + 2 * self.cut_develop_length
                    + 2 * self.triangle_half_length
                    + 2 * self.cut_develop_front_length
                )
                * self.hole_length
                * (self.cut_rows - 1)
                * self.cut_rows_vertical_distance
            ) / 2

            self.total_explosive_cut = specific_explosive_cut * self.volume_cut

            self.mass_of_one_explosive = (
                self.explosive_density
                * math.pi
                * self.explosive_diameter_cut ** 2
                * 0.25
                * 0.3
                / 1e6
            )
            number_of_explosives_cut = (
                self.total_explosive_cut / self.mass_of_one_explosive
            )

            self.number_of_cartridges_per_hole_cut = round(
                number_of_explosives_cut / number_of_cut_holes
            )
            length_of_explosive_cut = (
                self.number_of_cartridges_per_hole_cut * 0.3
            )

            self.cut_stemming = self.hole_length - length_of_explosive_cut

        elif self.explosive_type in [
            "Anfo Pneumatically Charged",
            "Anfo Bagged",
        ]:
            if self.explosive_type == "Anfo Bagged":
                anfo_diameter = 0.95 * self.hole_diameter
            elif self.explosive_type == "Anfo Pneumatically Charged":
                anfo_diameter = self.hole_diameter

            eta1_cut = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )

            eta2_cut = 1 / (
                math.e ** (self.hole_diameter / anfo_diameter) - math.e + 1
            )

            eta3_cut = 0.15

            specific_explosive_cut = (
                s_cut * self.rock_specific_surface_energy
            ) / (
                eta1_cut
                * eta2_cut
                * eta3_cut
                * self.explosive_specific_energy
                * 1000
            )

            self.cut_develop_length = (
                self.hole_diameter
                * 1e-3
                * math.sqrt(
                    self.explosive_density
                    * math.pi
                    / (3.2 * specific_explosive_cut)
                )
            )

            self.cut_develop_front_length = (
                (
                    self.cut_develop_length
                    * (self.width - 2 * self.triangle_half_length)
                    / self.width
                )
                if self.shape_name != "Circle"
                else (
                    self.cut_develop_length
                    * (self.radius - self.triangle_half_length)
                    / self.radius
                )
            )

            if self.cut_rows == 2:
                if self.hole_length / self.cut_burden > 1:
                    number_of_cut_holes = 12
                else:
                    number_of_cut_holes = 8
            elif self.cut_rows == 3:
                if self.hole_length / self.cut_burden > 1:
                    number_of_cut_holes = 18
                else:
                    number_of_cut_holes = 12

            self.volume_cut = (
                (
                    self.x_length
                    + 2 * self.cut_develop_length
                    + 2 * self.triangle_half_length
                    + 2 * self.cut_develop_front_length
                )
                * self.hole_length
                * (self.cut_rows - 1)
                * self.cut_rows_vertical_distance
            ) / 2

            self.total_explosive_cut = specific_explosive_cut * self.volume_cut

            self.total_anfo_cut = (
                self.total_explosive_cut - number_of_cut_holes * 0.4
            )

            self.anfo_per_meter_cut = (
                math.pi
                * (self.hole_diameter / 1000) ** 2
                * self.explosive_density
                / 4
            )

            length_of_anfo_cut = self.total_anfo_cut / (
                self.anfo_per_meter_cut * number_of_cut_holes
            )

            self.cut_stemming = self.hole_length - length_of_anfo_cut

            self.explosive_diameter_cut = 35

            self.number_of_cartridges_per_hole_cut = 1

        # Perimeter calculations
        self.perimeter_spacing = 15 * self.hole_diameter / 1000
        self.perimeter_burden = 1.25 * self.perimeter_spacing
        specific_explosive_perimeter = 90 * (self.hole_diameter / 1000) ** 2

        self.number_of_holes_perimeter = (
            round(((self.get_perimeter()) / self.perimeter_spacing)) - 2
        )

        mass_of_one_explosive_perimeter = (
            self.explosive_density
            * math.pi
            * self.explosive_perimetral_diameter ** 2
            * 0.25
            * 0.3
            / 1e6
        )

        self.number_of_cartridges_perimeter_per_hole = math.ceil(
            (specific_explosive_perimeter * self.hole_length)
            / mass_of_one_explosive_perimeter
        )

        self.total_explosive_perimeter = (
            self.number_of_holes_perimeter
            * self.number_of_cartridges_perimeter_per_hole
            * mass_of_one_explosive_perimeter
        )

        self.volume_perimeter = (
            self.hole_length
            * self.perimeter_burden
            * (self.get_perimeter_average_length())
        )

        length_of_explosive_perimeter = (
            self.number_of_cartridges_perimeter_per_hole * 0.3
        )
        self.perimeter_stemming = (
            self.hole_length - length_of_explosive_perimeter
        )

        # Advance calculations
        if self.explosive_type == "Emulsion":
            eta1_advance = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )
            eta2_advance = 1 / (
                math.e
                ** (self.hole_diameter / self.explosive_diameter_advance)
                - math.e
                + 1
            )
            eta3_advance = 0.15

            self.specific_explosive_advance = (
                s_advance
                * self.rock_specific_surface_energy
                / (
                    eta1_advance
                    * eta2_advance
                    * eta3_advance
                    * self.explosive_specific_energy
                    * 1000
                )
            )

            self.advance_burden = (
                self.explosive_diameter_advance
                * 1e-3
                * math.sqrt(
                    (math.pi * self.explosive_density)
                    / (4 * self.specific_explosive_advance)
                )
            )

            self.cut_wall_filler_length = (
                self.explosive_diameter_advance
                * 1e-3
                * math.sqrt(
                    self.explosive_density
                    * math.pi
                    / (4 * self.specific_explosive_advance)
                )
            )

        elif self.explosive_type in [
            "Anfo Pneumatically Charged",
            "Anfo Bagged",
        ]:
            if self.explosive_type == "Anfo Bagged":
                anfo_diameter = 0.95 * self.hole_diameter
            elif self.explosive_type == "Anfo Pneumatically Charged":
                anfo_diameter = self.hole_diameter

            self.explosive_diameter_advance = anfo_diameter

            eta1_advance = 1 - (
                (self.explosive_impedance - self.rock_impedance) ** 2
                / (self.explosive_impedance + self.rock_impedance) ** 2
            )
            eta2_advance = 1 / (
                math.e ** (self.hole_diameter / anfo_diameter) - math.e + 1
            )
            eta3_advance = 0.15

            self.specific_explosive_advance = (
                s_advance
                * self.rock_specific_surface_energy
                / (
                    eta1_advance
                    * eta2_advance
                    * eta3_advance
                    * self.explosive_specific_energy
                    * 1000
                )
            )

            self.advance_burden = (
                anfo_diameter
                * 1e-3
                * math.sqrt(
                    (math.pi * self.explosive_density)
                    / (4 * self.specific_explosive_advance)
                )
            )

            self.cut_wall_filler_length = (
                self.explosive_diameter_advance
                * 1e-3
                * math.sqrt(
                    self.explosive_density
                    * math.pi
                    / (4 * self.specific_explosive_advance)
                )
            )

        if self.check_space():
            self.perimeter_burden = self.advance_burden

        self.advance_spacing = self.advance_burden
        self.floor_spacing = self.advance_spacing
        self.floor_burden = self.advance_burden
        self.wall_spacing = self.roof_spacing = self.perimeter_spacing
        self.wall_burden = self.roof_burden = self.perimeter_burden

    def ebm_angular_final_calculations(self):

        number_of_holes_advance = len(self.get_holes_by_type("Advance"))

        volume_advance = (self.hole_length * (self.get_area())) - (
            self.volume_cut + self.volume_perimeter
        )

        total_explosive_advance = (
            volume_advance * self.specific_explosive_advance
        )
        total_anfo = 0

        if self.explosive_type == "Emulsion":
            number_of_cartridges_advance = (
                total_explosive_advance / self.mass_of_one_explosive
            )
            number_of_cartridges_advance_per_hole = round(
                number_of_cartridges_advance / number_of_holes_advance
            )

            length_of_explosive_advance = (
                0.3 * number_of_cartridges_advance_per_hole
            )

            self.advance_stemming = (
                self.hole_length - length_of_explosive_advance
            )

            total_explosive_emulsion = (
                self.total_explosive_cut
                + total_explosive_advance
                + self.total_explosive_perimeter
            )

        elif self.explosive_type in [
            "Anfo Pneumatically Charged",
            "Anfo Bagged",
        ]:
            number_of_cartridges_advance_per_hole = 1
            # explosive_diameter_advance = 35
            number_of_cartridges_advance = (
                number_of_holes_advance * number_of_cartridges_advance_per_hole
            )
            length_of_anfo_advance = total_explosive_advance / (
                number_of_holes_advance * self.anfo_per_meter_cut
            )
            self.advance_stemming = self.hole_length - length_of_anfo_advance
            total_anfo_advance = (
                total_explosive_advance - number_of_holes_advance * 0.4
            )
            total_anfo = self.total_anfo_cut + total_anfo_advance
            total_explosive_emulsion = (
                len(self.holes) - self.number_of_holes_perimeter
            ) * 0.4

        # Total calculation
        advance_holes = self.get_holes_by_type("Advance")
        for hole in advance_holes:
            hole.stemming = self.advance_stemming

        total_volume = self.volume_cut + volume_advance + self.volume_perimeter
        specific_explosive_emulsion = total_explosive_emulsion / total_volume
        specific_explosive_anfo = total_anfo / total_volume
        specific_drilling = len(self.holes) * self.hole_length / total_volume

        self.result = dict(
            total_explosive_emulsion=total_explosive_emulsion,
            total_explosive_anfo=total_anfo,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=specific_explosive_emulsion,
            specific_explosive_anfo=specific_explosive_anfo,
            specific_drilling=specific_drilling,
            explosive_diameter_cut=self.explosive_diameter_cut,
            explosive_diameter_advance=self.explosive_diameter_advance,
            explosive_perimetral_diameter=self.explosive_perimetral_diameter,
            number_of_cartridges_per_hole_cut=(
                self.number_of_cartridges_per_hole_cut
            ),
            number_of_cartridges_advance_per_hole=(
                number_of_cartridges_advance_per_hole
            ),
            number_of_cartridges_perimeter_per_hole=(
                self.number_of_cartridges_perimeter_per_hole
            ),
        )

    def calculate_burden_ntnu(self, w):
        return (
            0.521 * w + 0.13
            if self.blastability == "Good"
            else 0.604 * w + 0.145
        )

    def get_parameters_ntnu(self):
        if self.blastability == "Good":
            if self.hole_diameter in [45, 48, 51]:
                kb1 = (
                    0.0321 * self.hole_length ** 2
                    - 0.2169 * self.hole_length
                    + 1.2904
                    if self.skill_level == "High"
                    else 0.0393 * self.hole_length ** 2
                    - 0.2554 * self.hole_length
                    + 1.3579
                )
                burden_advance = math.sqrt(1.8 / (1.2 * kb1))
                parameters = dict(
                    burden_perimeter=0.9,
                    spacing_perimeter=0.85,
                    burden_first_ring=1,
                    spacing_first_ring=1.1,
                    burden_floor=1,
                    spacing_floor=1,
                    burden_advance=burden_advance,
                    spacing_advance=1.2 * burden_advance,
                )
            elif self.hole_diameter == 64:
                kb1 = (
                    0.001 * self.hole_length ** 2
                    + 0.0069 * self.hole_length
                    + 0.9388
                    if self.skill_level == "High"
                    else 0.01068 * self.hole_length ** 2
                    - 0.081 * self.hole_length
                    + 1.1585
                )
                burden_advance = math.sqrt(2.6 / (1.2 * kb1))
                parameters = dict(
                    burden_perimeter=1.1,
                    spacing_perimeter=1.05,
                    burden_first_ring=1.3,
                    spacing_first_ring=1.4,
                    burden_floor=1.3,
                    spacing_floor=1.3,
                    burden_advance=burden_advance,
                    spacing_advance=1.2 * burden_advance,
                )
        elif self.blastability == "Poor":
            if self.hole_diameter in [45, 48, 51]:
                kb1 = (
                    0.321 * self.hole_length ** 2
                    - 0.2169 * self.hole_length
                    + 1.2904
                    if self.skill_level == "High"
                    else 0.207 * self.hole_length ** 2
                    - 0.1135 * self.hole_length
                    + 1.0973
                )
                burden_advance = math.sqrt(1.3 / (1.2 * kb1))
                parameters = dict(
                    burden_perimeter=0.85,
                    spacing_perimeter=0.75,
                    burden_first_ring=0.9,
                    spacing_first_ring=1,
                    burden_floor=0.8,
                    spacing_floor=0.8,
                    burden_advance=burden_advance,
                    spacing_advance=1.2 * burden_advance,
                )
            elif self.hole_diameter == 64:
                kb1 = (
                    0.001 * self.hole_length ** 2
                    + 0.0069 * self.hole_length
                    + 0.9388
                    if self.skill_level == "High"
                    else 0.01068 * self.hole_length ** 2
                    - 0.081 * self.hole_length
                    + 1.1585
                )
                burden_advance = math.sqrt(1.8 / (1.2 * kb1))
                parameters = dict(
                    burden_perimeter=1,
                    spacing_perimeter=0.9,
                    burden_first_ring=1.1,
                    spacing_first_ring=1.2,
                    burden_floor=1.1,
                    spacing_floor=1.1,
                    burden_advance=burden_advance,
                    spacing_advance=1.2 * burden_advance,
                )
        return parameters

    def ntnu(self):

        self.ntnu_primary_calculations()
        self.draw_plot_ntnu()
        self.ntnu_final_calculations()
        return self.result

    def ntnu_primary_calculations(self):

        self.method = "NTNU"

        if self.primeter_explosive == "Emulsion":
            if self.hole_diameter in [48, 51]:
                self.cartridge_diameter_perimeter = 27
                self.weight_of_primer = 0.2
            elif self.hole_diameter == 45:
                self.cartridge_diameter_perimeter = 22
                self.weight_of_primer = 0.137
            elif self.hole_diameter == 64:
                self.cartridge_diameter_perimeter = 30
                self.weight_of_primer = 0.24
        else:
            self.cartridge_diameter_perimeter = 35
            self.weight_of_primer = 0.4

        if self.blastability == "Good":
            if self.hole_diameter == 64:
                area_of_air_hole = 38.64 * self.hole_length - 26.8
                self.burden_floor = 1.3
            elif self.hole_diameter == 51:
                area_of_air_hole = 49.32 * self.hole_length - 53.4
                self.burden_floor = 1
            elif self.hole_diameter == 48:
                area_of_air_hole = 60 * self.hole_length - 80
                self.burden_floor = 1
            elif self.hole_diameter == 45:
                area_of_air_hole = 27.96 * self.hole_length - 0.2
                self.burden_floor = 0.9
        elif self.blastability == "Poor":
            if self.hole_diameter == 64:
                area_of_air_hole = 51.67 * self.hole_length - 56.67
                self.burden_floor = 1.1
            elif self.hole_diameter == 51:
                area_of_air_hole = 62.09 * self.hole_length - 33.46
                self.burden_floor = 0.9
            elif self.hole_diameter == 48:
                area_of_air_hole = 72.5 * self.hole_length - 10.25
                self.burden_floor = 0.8
            elif self.hole_diameter == 45:
                area_of_air_hole = 41.25 * self.hole_length - 79.88
                self.burden_floor = 0.7

        self.air_hole_diameter = (
            math.sqrt(4 * area_of_air_hole / math.pi) / 100
        )

        self.b1 = (
            1.5 * self.air_hole_diameter
            if self.hole_diameter == 48
            else 2 * self.air_hole_diameter
        )
        w1 = self.b1 * math.sqrt(2)
        b2 = self.calculate_burden_ntnu(w1)
        self.w2 = 1.5 * b2 * math.sqrt(2)
        self.b3 = self.calculate_burden_ntnu(self.w2)
        w3 = 1.5 * self.b3 * math.sqrt(2)
        b4 = self.calculate_burden_ntnu(w3)
        self.w4 = 1.5 * b4 * math.sqrt(2)

        self.t_cut = 0.1 * self.hole_length
        self.t_floor = 0.1 * self.hole_length
        self.t_perimeter = 0.3 * self.hole_length
        self.t_advance = 0.3 * self.hole_length

    def ntnu_final_calculations(self):
        advance_holes = self.get_holes_by_type("Advance")
        number_of_holes_advance = len(advance_holes)

        # Total calculation
        for hole in advance_holes:
            hole.stemming = self.t_advance

        total_volume = (self.get_area()) * self.hole_length * 0.95
        anfo_weight_per_meter = (
            math.pi * self.hole_diameter ** 2 * 0.25 * 950 / 1000000
        )
        cut_holes = self.get_holes_by_type("Cut")
        anfo_cut = (
            len(cut_holes) * anfo_weight_per_meter * 0.9 * self.hole_length
        )
        q_first_section = (
            q_second_section
        ) = q_third_section = q_fourth_section = (anfo_cut / 4)
        floor_holes = self.get_holes_by_type("Floor")
        anfo_floor = (
            len(floor_holes) * anfo_weight_per_meter * 0.9 * self.hole_length
        )
        buffer_holes = self.get_holes_by_type("Buffer")
        anfo_buffer = (
            len(buffer_holes)
            * anfo_weight_per_meter
            * 0.7
            * self.hole_length
            * 0.6
        )

        perimeter_holes = self.get_holes_by_type("Perimeter")
        anfo_perimeter = (
            len(perimeter_holes)
            * anfo_weight_per_meter
            * 0.7
            * self.hole_length
            * 0.25
        )
        anfo_advance = (
            number_of_holes_advance
            * anfo_weight_per_meter
            * 0.7
            * self.hole_length
        )
        total_anfo = (
            anfo_cut + anfo_floor + anfo_perimeter + anfo_advance + anfo_buffer
        )
        specific_explosive_anfo = total_anfo / total_volume
        specific_drilling = len(self.holes) * self.hole_length / total_volume

        number_of_cartridges_other_per_hole = 1
        if self.primeter_explosive == "Emulsion":
            number_of_cartridges_perimeter_per_hole = round(
                0.7 * self.hole_length / 0.3
            )
        else:
            number_of_cartridges_perimeter_per_hole = 1

        total_explosive_emulsion = (
            number_of_cartridges_perimeter_per_hole
            * self.weight_of_primer
            * len(perimeter_holes)
            + (len(self.holes) - len(perimeter_holes)) * 0.4
        )
        specific_explosive_emulsion = total_explosive_emulsion / total_volume

        self.result = dict(
            total_explosive_emulsion=total_explosive_emulsion,
            total_explosive_anfo=total_anfo,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=specific_explosive_emulsion,
            specific_explosive_anfo=specific_explosive_anfo,
            specific_drilling=specific_drilling,
            explosive_diameter_cut=35,
            explosive_diameter_advance=35,
            explosive_perimetral_diameter=self.cartridge_diameter_perimeter,
            number_of_cartridges_advance_per_hole=(
                number_of_cartridges_other_per_hole
            ),
            number_of_cartridges_floor_per_hole=(
                number_of_cartridges_other_per_hole
            ),
            number_of_cartridges_perimeter_per_hole=(
                number_of_cartridges_perimeter_per_hole
            ),
            number_of_cartridges_per_hole_cut=1,
        )

        return self.result

    def olafsson(self):

        self.olafsson_primary_calculations()
        self.draw_plot_olafsson()
        self.olafsson_final_calculations()
        return self.result

    def olafsson_primary_calculations(self):

        self.method = "Olafsson"

        self.hole_length = self.advance / 0.95
        d = symbols("d")
        solutions = solveset(
            39.4 * d ** 2 - 34.1 * d - 0.15 + self.hole_length, d
        )
        self.air_hole_diameter = max(
            float(min(solutions)), self.hole_diameter / 1000
        )

        hole_end_concentration = (
            7.85e-4 * (self.hole_diameter ** 2) * self.explosive_density * 1e-3
        )
        burden = 0.88 * hole_end_concentration ** 0.35

        self.required_parameters = []
        advance_root = math.sqrt(self.advance)

        b1 = 1.5 * self.air_hole_diameter
        x1 = math.sqrt(2) * b1
        if x1 < advance_root:
            self.required_parameters.append((b1, x1))
            self.last_x = x1
        b2 = x1
        x2 = math.sqrt(2) * 1.5 * b2
        if x2 < advance_root:
            self.required_parameters.append((b2, x2))
            self.last_x = x2
        b3 = x2
        x3 = math.sqrt(2) * 1.5 * b3
        if x3 < advance_root:
            self.required_parameters.append((b3, x3))
            self.last_x = x3
        b4 = x3
        x4 = min(math.sqrt(2) * 1.5 * b4, 2 * burden)
        if x4 < advance_root:
            self.required_parameters.append((b4, x4))
            self.last_x = x4

        required_parameters_length = len(self.required_parameters)
        if required_parameters_length >= 1:
            self.first_cut_holes_minimum_concentration = 1.8621 * b1 + 0.1216
            self.first_cut_holes_end_length = 1.5 * b1
            self.first_cut_holes_end_concentration = (
                self.first_cut_holes_minimum_concentration
            )
            self.first_cut_holes_stemming = 0.5 * b1

        if required_parameters_length >= 2:
            self.second_cut_holes_minimum_concentration = 1.1399 * b2 + 0.01
            self.second_cut_holes_end_length = 1.5 * b2
            self.second_cut_holes_end_concentration = (
                self.second_cut_holes_minimum_concentration
            )
            self.second_cut_holes_stemming = 0.5 * b2

        if required_parameters_length >= 3:
            self.third_cut_holes_minimum_concentration = 1.1399 * b3 + 0.01
            self.third_cut_holes_end_length = 1.5 * b3
            self.third_cut_holes_end_concentration = (
                self.third_cut_holes_minimum_concentration
            )
            self.third_cut_holes_stemming = 0.5 * b3

        if required_parameters_length == 4:
            self.fourth_cut_holes_minimum_concentration = 1.1399 * b4 + 0.01
            self.fourth_cut_holes_end_length = 1.5 * b4
            self.fourth_cut_holes_end_concentration = (
                2 * self.fourth_cut_holes_minimum_concentration
            )
            self.fourth_cut_holes_stemming = 0.5 * b4

        self.floor_burden = burden
        self.floor_spacing = 1.1 * burden
        self.floor_holes_end_length = self.hole_length / 3
        self.floor_holes_end_concentration = hole_end_concentration
        self.floor_holes_middle_concentration = hole_end_concentration
        self.floor_holes_stemming = 0.2 * burden

        self.wall_burden = 0.9 * burden
        self.wall_spacing = 1.1 * burden
        self.wall_holes_end_length = self.hole_length / 6
        self.wall_holes_end_concentration = hole_end_concentration
        self.wall_holes_middle_concentration = 0.4 * hole_end_concentration
        self.wall_holes_stemming = 0.5 * burden

        self.roof_burden = 0.9 * burden
        self.roof_spacing = 1.1 * burden
        self.roof_holes_end_length = self.hole_length / 6
        self.roof_holes_end_concentration = hole_end_concentration
        self.roof_holes_middle_concentration = 0.4 * hole_end_concentration
        self.roof_holes_stemming = 0.5 * burden

        self.advance_burden = burden
        self.advance_spacing = 1.1 * burden
        self.advance_holes_end_length = self.hole_length / 3
        self.advance_holes_end_concentration = hole_end_concentration
        self.advance_holes_middle_concentration = 0.5 * hole_end_concentration
        self.advance_holes_stemming = 0.5 * burden

    def olafsson_final_calculations(self):
        required_parameters_length = len(self.required_parameters)
        q_cut = (
            q_first_section
        ) = q_second_section = q_third_section = q_fourth_section = 0

        if required_parameters_length >= 1:
            self.cut_middle_length_first_section = self.hole_length - (
                self.first_cut_holes_end_length + self.first_cut_holes_stemming
            )
            q_first_section = 4 * (
                self.first_cut_holes_end_length
                * self.first_cut_holes_end_concentration
                + self.cut_middle_length_first_section
                * self.first_cut_holes_minimum_concentration
            )
            q_cut += q_first_section

        if required_parameters_length >= 2:
            self.cut_middle_length_second_section = self.hole_length - (
                self.second_cut_holes_end_length
                + self.second_cut_holes_stemming
            )
            q_second_section = 4 * (
                self.second_cut_holes_end_length
                * self.second_cut_holes_end_concentration
                + self.cut_middle_length_second_section
                * self.second_cut_holes_minimum_concentration
            )
            q_cut += q_second_section

        if required_parameters_length >= 3:
            self.cut_middle_length_third_section = self.hole_length - (
                self.third_cut_holes_end_length + self.third_cut_holes_stemming
            )
            q_third_section = 4 * (
                self.third_cut_holes_end_length
                * self.third_cut_holes_end_concentration
                + self.cut_middle_length_third_section
                * self.third_cut_holes_minimum_concentration
            )
            q_cut += q_third_section
        if required_parameters_length == 4:
            self.cut_middle_length_fourth_section = self.hole_length - (
                self.fourth_cut_holes_end_length
                + self.fourth_cut_holes_stemming
            )
            q_fourth_section = 4 * (
                self.fourth_cut_holes_end_length
                * self.fourth_cut_holes_end_concentration
                + self.cut_middle_length_fourth_section
                * self.fourth_cut_holes_minimum_concentration
            )
            q_cut += q_fourth_section

        self.floor_middle_length = self.hole_length - (
            self.floor_holes_end_length + self.floor_holes_stemming
        )
        floor_holes = self.get_holes_by_type("Floor")
        q_floor = len(floor_holes) * (
            self.floor_holes_end_length * self.floor_holes_end_concentration
            + self.floor_middle_length * self.floor_holes_middle_concentration
        )

        self.perimeter_middle_length = self.hole_length - (
            self.wall_holes_end_length + self.wall_holes_stemming
        )
        perimeter_holes = self.get_holes_by_type("Perimeter")
        q_perimeter = len(perimeter_holes) * (
            self.wall_holes_end_length * self.wall_holes_end_concentration
            + self.perimeter_middle_length
            * self.wall_holes_middle_concentration
        )
        self.advance_middle_length = self.hole_length - (
            self.advance_holes_end_length + self.advance_holes_stemming
        )
        advance_holes = self.get_holes_by_type("Advance")
        q_advance = len(advance_holes) * (
            self.advance_holes_end_length
            * self.advance_holes_end_concentration
            + self.advance_middle_length
            * self.advance_holes_middle_concentration
        )
        q_total = q_cut + q_floor + q_perimeter + q_advance

        specific_q = q_total / (self.hole_length * self.get_area())

        specific_drilling = len(self.holes) / self.get_area()

        self.result = dict(
            total_explosive_emulsion=q_total,
            total_explosive_anfo=0,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=specific_q,
            specific_explosive_anfo=0,
            specific_drilling=specific_drilling,
            explosive_diameter_cut=0,
            explosive_diameter_advance=0,
            explosive_diameter_perimetral=0,
            number_of_cartridges_per_hole_cut=0,
            number_of_cartridges_advance_per_hole=0,
            number_of_cartridges_perimeter_per_hole=0,
        )

    def olafsson_angular(self):

        self.olafsson_angular_primary_calculations()
        self.draw_plot_ebm_angular()
        self.olafsson_angular_final_calculations()
        return self.result

    def olafsson_angular_primary_calculations(self):

        self.method = "Olafsson Angular"

        self.hole_length = self.advance / 0.95

        hole_end_concentration = (
            7.85 * self.hole_diameter ** 2 * self.explosive_density / 1e7
        )

        burden = 0.88 * hole_end_concentration ** 0.35

        self.cut_rows = 3

        # Floor
        self.floor_burden = burden
        self.floor_spacing = 1.1 * burden
        self.floor_hole_end_length = self.hole_length / 3
        self.floor_hole_end_concentration = hole_end_concentration
        self.floor_hole_middle_concentration = hole_end_concentration
        self.floor_stemming = 0.2 * burden

        # Wall
        self.wall_burden = 0.9 * burden
        self.wall_spacing = 1.1 * burden
        self.wall_hole_end_length = self.hole_length / 6
        self.wall_hole_end_concentration = hole_end_concentration
        self.wall_hole_middle_concentration = 0.4 * hole_end_concentration
        self.wall_stemming = 0.5 * burden

        # Roof
        self.roof_burden = 0.9 * burden
        self.roof_spacing = 1.1 * burden
        self.roof_hole_end_length = self.hole_length / 6
        self.roof_hole_end_concentration = hole_end_concentration
        self.roof_hole_middle_concentration = 0.4 * hole_end_concentration
        self.roof_stemming = 0.5 * burden

        # Advance
        self.advance_burden = burden
        self.advance_spacing = 1.1 * burden
        self.advance_hole_end_length = self.hole_length / 3
        self.advance_hole_end_concentration = hole_end_concentration
        self.advance_hole_middle_concentration = 0.5 * hole_end_concentration
        self.advance_stemming = 0.5 * burden

        # Cut
        self.cut_burden = (
            0.2092 * hole_end_concentration ** 2
            - 0.1998 * hole_end_concentration
            + 0.9684
        )
        self.cut_hole_end_concentration = hole_end_concentration
        self.cut_hole_middle_concentration = 0.5 * hole_end_concentration
        self.cut_hole_end_length = self.hole_length / 3
        self.cut_stemming = 0.3 * self.cut_burden
        self.cut_height = (
            0.0737 * hole_end_concentration ** 2
            + 0.0022 * hole_end_concentration
            + 1.3798
        )

        # Cut Develop
        self.cut_develop_length = (
            0.0812 * hole_end_concentration ** 2
            - 0.0556 * hole_end_concentration
            + 0.7358
        )
        self.cut_develop_hole_end_concentration = hole_end_concentration
        self.cut_develop_hole_middle_concentration = (
            0.5 * hole_end_concentration
        )
        self.cut_develop_hole_end_length = self.hole_length / 3
        self.cut_develop_stemming = 0.5 * self.cut_develop_length

        first_hole_length_cut = self.hole_length / math.cos(
            (self.angle / 2) * np.pi / 180
        )

        self.triangle_half_length = first_hole_length_cut * np.sin(
            (self.angle / 2) * np.pi / 180
        )

        self.cut_rows_vertical_distance = self.cut_height / 2

        if self.shape_name == "Circle":
            self.cut_develop_front_length = (
                self.cut_develop_length
                * (self.radius - self.triangle_half_length)
                / self.radius
            )
        else:
            self.cut_develop_front_length = (
                self.cut_develop_length
                * (self.width - 2 * self.triangle_half_length)
                / self.width
            )

        self.cut_wall_filler_length = self.advance_burden
        self.perimeter_stemming = self.wall_stemming
        self.perimeter_spacing = self.wall_spacing
        self.perimeter_burden = self.wall_burden

    def olafsson_angular_final_calculations(self):
        self.result = dict(
            total_explosive_emulsion=0,
            total_explosive_anfo=0,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=0,
            specific_explosive_anfo=0,
            specific_drilling=0,
            explosive_diameter_advance=0,
            explosive_diameter_perimetral=0,
            number_of_cartridges_advance_per_hole=0,
            number_of_cartridges_perimeter_per_hole=0,
            q_first_section=0,
            q_second_section=0,
            q_third_section=0,
            q_fourth_section=0,
        )

    def konya_angular(self):

        self.konya_angular_primary_calculations()
        self.draw_plot_ebm_angular()
        self.konya_angular_final_calculations()
        return self.result

    def konya_angular_primary_calculations(self):

        self.method = "Konya Angular"

        self.hole_length = self.advance / 0.95

        if self.explosive_type == "Emulsion":
            self.set_emulsion_diameter()
        else:
            self.explosive_diameter = self.hole_diameter

        burden = (
            0.012
            * ((2 * self.explosive_density / self.rock_density) + 1.5)
            * self.explosive_diameter
        )

        self.advance_burden = self.floor_burden = burden

        self.advance_spacing = self.floor_spacing = 1.1 * burden

        self.advance_stemming = 0.5 * burden
        self.floor_stemming = 0.2 * burden
        self.perimeter_stemming = burden

        if self.contour_blasting == "Normal":
            if self.contour_explosive_type == "Emulsion":
                perimeter_explosive_density = 1200
                perimeter_explosive_diameter = (
                    self.perimeter_explosive_diameter
                )
            elif self.contour_explosive_type == "Anfo Bagged":
                perimeter_explosive_density = 850
                perimeter_explosive_diameter = self.hole_diameter
            elif self.contour_explosive_type == "Anfo Pneumatically Charged":
                perimeter_explosive_density = 950
                perimeter_explosive_diameter = self.hole_diameter
            self.perimeter_burden = (
                0.012
                * ((2 * perimeter_explosive_density / self.rock_density) + 1.5)
                * perimeter_explosive_diameter
            )
            self.perimeter_spacing = 1.1 * self.perimeter_burden
        elif self.contour_blasting == "Controlled":
            perimeter_explosive_diameter = self.perimeter_explosive_diameter
            if perimeter_explosive_diameter == 22:
                self.weight_of_one_cartridge = 0.14
            elif perimeter_explosive_diameter == 27:
                self.weight_of_one_cartridge = 0.2
            elif perimeter_explosive_diameter == 30:
                self.weight_of_one_cartridge = 0.24
            self.perimeter_spacing = 15 * self.hole_diameter / 1000
            self.perimeter_burden = 1.3 * self.perimeter_spacing
            self.perimeter_explosive_concentration = (
                10 * (self.perimeter_spacing * 1000 / 177) ** 2
            )

        self.wall_burden = self.roof_burden = self.perimeter_burden
        self.wall_spacing = self.roof_spacing = self.perimeter_spacing
        self.wall_stemming = self.roof_stemming = self.perimeter_burden

        self.cut_burden = 2 * burden
        self.cut_stemming = 0.4 * burden

        self.cut_rows_vertical_distance = 1.2 * burden

        first_hole_length_cut = self.hole_length / math.cos(
            (self.angle / 2) * np.pi / 180
        )

        self.triangle_half_length = first_hole_length_cut * np.sin(
            (self.angle / 2) * np.pi / 180
        )

        self.cut_wall_filler_length = self.advance_burden
        self.cut_develop_length = burden

        if self.shape_name == "Circle":
            self.cut_develop_front_length = (
                self.cut_develop_length
                * (self.radius - self.triangle_half_length)
                / self.radius
            )
        else:
            self.cut_develop_front_length = (
                self.cut_develop_length
                * (self.width - 2 * self.triangle_half_length)
                / self.width
            )

    def konya_angular_final_calculations(self):

        self.result = dict(
            total_explosive_emulsion=0,
            total_explosive_anfo=0,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=0,
            specific_explosive_anfo=0,
            specific_drilling=0,
            explosive_diameter_advance=0,
            explosive_diameter_perimetral=0,
            number_of_cartridges_advance_per_hole=0,
            number_of_cartridges_perimeter_per_hole=0,
            q_first_section=0,
            q_second_section=0,
            q_third_section=0,
            q_fourth_section=0,
        )

    def gustafsson(self):

        self.gustafsson_primary_calculations()
        self.draw_plot_ebm_angular()
        self.gustafsson_final_calculations()
        return self.result

    def gustafsson_primary_calculations(self):
        self.method = "Gustafsson"

        self.hole_length = self.advance / 0.95

        hole_end_concentration = (
            self.hole_diameter ** 2
            * math.pi
            * self.explosive_density
            * 1e-6
            / 4
        )

        self.cut_rows = 3

        if self.hole_diameter == 32:
            # cut
            self.cut_height = 1.5
            self.cut_burden = 1
            self.cut_hole_end_length = self.hole_length / 3
            self.cut_hole_middle_concentration = 0.5 * hole_end_concentration
            self.cut_stemming = 0.3 * self.cut_burden

            # cut develop
            self.cut_develop_length = 0.8
            self.cut_develop_hole_end_length = self.hole_length / 3
            self.cut_develop_middle_concentration = (
                0.4 * hole_end_concentration
            )
            self.cut_develop_hole_stemming = 0.45

            # core
            burden = 0.9
            self.spacing = 1

        elif self.hole_diameter == 38:
            # cut
            self.cut_height = 1.6
            self.cut_burden = 1.2
            self.cut_hole_end_length = self.hole_length / 3
            self.cut_hole_middle_concentration = 0.5 * hole_end_concentration
            self.cut_stemming = 0.3 * self.cut_burden

            # cut develop
            self.cut_develop_length = 0.9
            self.cut_develop_hole_end_length = self.hole_length / 3
            self.cut_develop_middle_concentration = (
                0.4 * hole_end_concentration
            )
            self.cut_develop_hole_stemming = 0.45

            # core
            burden = 1
            self.spacing = 1.15

        elif self.hole_diameter == 45:
            # cut
            self.cut_height = 1.8
            self.cut_burden = 1.5
            self.cut_hole_end_length = self.hole_length / 3
            self.cut_hole_middle_concentration = 0.5 * hole_end_concentration
            self.cut_stemming = 0.3 * self.cut_burden

            # cut develop
            self.cut_develop_length = 1
            self.cut_develop_hole_end_length = self.hole_length / 3
            self.cut_develop_middle_concentration = (
                0.4 * hole_end_concentration
            )
            self.cut_develop_hole_stemming = 0.5

            # core
            burden = 1.15
            self.spacing = 1.25

        elif self.hole_diameter == 48:
            # cut
            self.cut_height = 1.9
            self.cut_burden = 1.75
            self.cut_hole_end_length = self.hole_length / 3
            self.cut_hole_middle_concentration = 0.5 * hole_end_concentration
            self.cut_stemming = 0.3 * self.cut_burden

            # cut develop
            self.cut_develop_length = 1.1
            self.cut_develop_hole_end_length = self.hole_length / 3
            self.cut_develop_middle_concentration = (
                0.4 * hole_end_concentration
            )
            self.cut_develop_hole_stemming = 0.55

            # core
            burden = 1.2
            self.spacing = 1.30

        elif self.hole_diameter == 51:
            # cut
            self.cut_height = 2
            self.cut_burden = 2
            self.cut_hole_end_length = self.hole_length / 3
            self.cut_hole_middle_concentration = 0.5 * hole_end_concentration
            self.cut_stemming = 0.3 * self.cut_burden

            # cut develop
            self.cut_develop_length = 1.2
            self.cut_develop_hole_end_length = self.hole_length / 3
            self.cut_develop_middle_concentration = (
                0.4 * hole_end_concentration
            )
            self.cut_develop_hole_stemming = 0.6

            # core
            burden = 1.25
            self.spacing = 1.35

        # floor parameters
        self.floor_burden = burden
        self.floor_spacing = 1.1 * burden
        self.floor_hole_end_length = self.hole_length / 3
        self.floor_hole_end_concentration = hole_end_concentration
        self.floor_hole_middle_concentration = hole_end_concentration
        self.floor_hole_stemming = 0.2 * burden

        # wall parameters
        self.wall_burden = 0.9 * burden
        self.wall_spacing = 1.1 * burden
        self.wall_hole_end_length = self.hole_length / 6
        self.wall_hole_end_concentration = hole_end_concentration
        self.wall_hole_middle_concentration = 0.4 * hole_end_concentration
        self.wall_hole_stemming = 0.5 * burden

        # roof parameters
        self.roof_burden = 0.9 * burden
        self.roof_spacing = 1.1 * burden
        self.roof_hole_end_length = self.hole_length / 6
        self.roof_hole_end_concentration = hole_end_concentration
        self.roof_hole_middle_concentration = 0.4 * hole_end_concentration
        self.roof_hole_stemming = 0.5 * burden

        # advance parameters
        self.advance_burden = burden
        self.advance_spacing = 1.1 * burden
        self.advance_hole_end_length = self.hole_length / 3
        self.advance_hole_end_concentration = hole_end_concentration
        self.advance_hole_middle_concentration = 0.5 * hole_end_concentration
        self.advance_hole_stemming = 0.5 * burden

        self.perimeter_spacing = self.wall_spacing
        self.perimeter_burden = self.wall_burden
        self.cut_wall_filler_length = self.advance_burden
        self.cut_rows_vertical_distance = self.cut_height / 2

        first_hole_length_cut = self.hole_length / math.cos(
            (self.angle / 2) * np.pi / 180
        )

        self.triangle_half_length = first_hole_length_cut * np.sin(
            (self.angle / 2) * np.pi / 180
        )

        self.cut_develop_front_length = (
            (
                self.cut_develop_length
                * (self.width - 2 * self.triangle_half_length)
                / self.width
            )
            if self.shape_name != "Circle"
            else (
                self.cut_develop_length
                * (self.radius - self.triangle_half_length)
                / self.radius
            )
        )

        self.perimeter_stemming = self.wall_hole_stemming

    def gustafsson_final_calculations(self):

        self.result = dict(
            total_explosive_emulsion=0,
            total_explosive_anfo=0,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=0,
            specific_explosive_anfo=0,
            specific_drilling=0,
            explosive_diameter_advance=0,
            explosive_diameter_perimetral=0,
            number_of_cartridges_advance_per_hole=0,
            number_of_cartridges_perimeter_per_hole=0,
            q_first_section=0,
            q_second_section=0,
            q_third_section=0,
            q_fourth_section=0,
        )

    def konya(self):

        self.konya_primary_calculations()
        self.draw_plot_olafsson()
        self.konya_final_calculations()
        return self.result

    def konya_primary_calculations(self):

        self.method = "Konya"

        self.hole_length = self.advance / 0.95

        self.air_hole_diameter = max(
            (41.67 * self.hole_length - 16.51) / 1000,
            self.hole_diameter / 1000,
        )

        if self.explosive_type == "Emulsion":
            self.set_emulsion_diameter()
        else:
            self.explosive_diameter = self.hole_diameter

        self.burden = (
            0.012
            * ((2 * self.explosive_density / self.rock_density) + 1.5)
            * self.explosive_diameter
        )

        self.advance_spacing = (
            self.roof_spacing
        ) = self.wall_spacing = self.floor_spacing = (1.1 * self.burden)
        self.advance_burden = (
            self.floor_burden
        ) = self.wall_burden = self.roof_burden = self.burden

        self.required_parameters = []
        advance_root = math.sqrt(self.advance)

        b1 = 1.5 * self.air_hole_diameter
        x1 = math.sqrt(2) * b1
        if x1 < advance_root:
            self.required_parameters.append((b1, x1))
            self.last_x = x1
        b2 = x1
        x2 = math.sqrt(2) * 1.5 * b2
        if x2 < advance_root:
            self.required_parameters.append((b2, x2))
            self.last_x = x2
        b3 = min(x2, self.burden)
        x3 = math.sqrt(2) * 1.5 * b3
        if x3 < advance_root:
            self.required_parameters.append((b3, x3))
            self.last_x = x3
        b4 = min(x3, self.burden)
        x4 = math.sqrt(2) * 1.5 * b4
        if x4 < advance_root:
            self.required_parameters.append((b4, x4))
            self.last_x = x4

        required_parameters_length = len(self.required_parameters)
        if required_parameters_length >= 1:
            self.first_cut_holes_end_concentration = 1.8621 * b1 + 0.1216
            self.first_cut_holes_end_length = 1.5 * b1
            self.second_cut_holes_middle_concentration = (
                self.first_cut_holes_end_concentration
            )
            self.first_cut_holes_stemming = 0.5 * b1
        if required_parameters_length >= 2:
            self.second_cut_holes_end_concentration = 1.1399 * b2 + 0.01
            self.second_cut_holes_end_length = 1.5 * b2
            self.second_cut_holes_middle_concentration = (
                self.second_cut_holes_end_concentration
            )
            self.second_cut_holes_stemming = 0.5 * b2
        if required_parameters_length >= 3:
            self.third_cut_holes_end_concentration = 1.1399 * b3 + 0.01
            self.third_cut_holes_end_length = 1.5 * b3
            self.third_cut_holes_middle_concentration = (
                self.third_cut_holes_end_concentration / 2
            )

            self.third_cut_holes_stemming = 0.5 * b3
        if required_parameters_length == 4:
            self.fourth_cut_holes_end_concentration = 1.1399 * b4 + 0.01
            self.fourth_cut_holes_end_length = 1.5 * b4
            self.fourth_cut_holes_middle_concentration = (
                self.fourth_cut_holes_end_concentration / 2
            )
            self.fourth_cut_holes_stemming = 0.5 * b4

        if self.contour_blasting == "Normal":
            if self.contour_explosive_type == "Emulsion":
                perimeter_explosive_density = 1200
                self.perimeter_explosive_diameter = (
                    self.perimeter_explosive_diameter
                )
            elif self.contour_explosive_type == "Anfo Bagged":
                perimeter_explosive_density = 850
                self.perimeter_explosive_diameter = self.explosive_diameter
            elif self.contour_explosive_type == "Anfo Pneumatically Charged":
                perimeter_explosive_density = 950
                self.perimeter_explosive_diameter = self.explosive_diameter
            self.perimeter_burden = (
                0.012
                * ((2 * perimeter_explosive_density / self.rock_density) + 1.5)
                * self.perimeter_explosive_diameter
            )
            self.perimeter_spacing = 1.1 * self.perimeter_burden
        elif self.contour_blasting == "Controlled":
            if self.perimeter_explosive_diameter == 22:
                self.weight_of_one_cartridge = 0.14
            elif self.perimeter_explosive_diameter == 27:
                self.weight_of_one_cartridge = 0.2
            elif self.perimeter_explosive_diameter == 30:
                self.weight_of_one_cartridge = 0.24
            self.perimeter_spacing = 15 * self.explosive_diameter / 1000
            self.perimeter_burden = 1.3 * self.perimeter_spacing
            self.perimeter_explosive_concentration = (
                10 * (self.perimeter_spacing * 1000 / 177) ** 2
            )

    def konya_final_calculations(self):
        required_parameters_length = len(self.required_parameters)
        q_cut = (
            q_first_section
        ) = q_second_section = q_third_section = q_fourth_section = 0
        if required_parameters_length >= 1:
            self.cut_middle_length_first_section = self.hole_length - (
                self.first_cut_holes_end_length + self.first_cut_holes_stemming
            )
            q_first_section = 4 * (
                self.first_cut_holes_end_length
                * self.first_cut_holes_end_concentration
                + self.cut_middle_length_first_section
                * self.first_cut_holes_end_concentration
            )
            q_cut += q_first_section
        if required_parameters_length >= 2:
            self.cut_middle_length_second_section = self.hole_length - (
                self.second_cut_holes_end_length
                + self.second_cut_holes_stemming
            )
            q_second_section = 4 * (
                self.second_cut_holes_end_length
                * self.second_cut_holes_end_concentration
                + self.cut_middle_length_second_section
                * self.second_cut_holes_middle_concentration
            )
            q_cut += q_second_section
        if required_parameters_length >= 3:
            self.cut_middle_length_third_section = self.hole_length - (
                self.third_cut_holes_end_length + self.third_cut_holes_stemming
            )
            q_third_section = 4 * (
                self.third_cut_holes_end_length
                * self.third_cut_holes_end_concentration
                + self.cut_middle_length_third_section
                * self.third_cut_holes_middle_concentration
            )
            q_cut += q_third_section
        if required_parameters_length == 4:
            self.cut_middle_length_fourth_section = self.hole_length - (
                self.fourth_cut_holes_end_length
                + self.fourth_cut_holes_stemming
            )
            q_fourth_section = 4 * (
                self.fourth_cut_holes_end_length
                * self.fourth_cut_holes_end_concentration
                + self.cut_middle_length_fourth_section
                * self.fourth_cut_holes_middle_concentration
            )
            q_cut += q_fourth_section

        self.advance_stemming = 0.5 * self.burden
        self.floor_stemming = 0.2 * self.burden
        self.perimeter_stemming = self.perimeter_burden

        advance_holes_explosive_length = (
            self.hole_length - self.advance_stemming
        )
        advance_holes_explosive_volume = (
            ((np.pi * self.explosive_diameter ** 2) / 4)
            * advance_holes_explosive_length
            / 1e6
        )
        advance_holes = self.get_holes_by_type("Advance")
        q_advance = (
            len(advance_holes)
            * self.explosive_density
            * advance_holes_explosive_volume
        )

        floor_holes_explosive_length = self.hole_length - self.floor_stemming
        floor_holes_explosive_volume = (
            ((np.pi * self.explosive_diameter ** 2) / 4)
            * floor_holes_explosive_length
            / 1e6
        )
        floor_holes = self.get_holes_by_type("Floor")
        q_floor = (
            len(floor_holes)
            * self.explosive_density
            * floor_holes_explosive_volume
        )

        perimeter_holes_explosive_length = (
            self.hole_length - self.perimeter_stemming
        )
        perimeter_holes = self.get_holes_by_type("Perimeter")
        if self.explosive_type == "Emulsion":
            if self.contour_blasting == "Normal":
                if self.contour_explosive_type == "Emulsion":
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.perimeter_explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 1200
                        * perimeter_holes_explosive_volume
                    )

                    q_total_emulsion = (
                        q_cut + q_advance + q_floor + q_perimeter
                    )
                    q_total_anfo = 0

                elif self.contour_explosive_type == "Anfo Bagged":
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 950
                        * perimeter_holes_explosive_volume
                    )
                    q_primer_emulsion = len(perimeter_holes) * 0.4
                    q_total_anfo = q_perimeter
                    q_total_emulsion = (
                        q_cut + q_advance + q_floor + q_primer_emulsion
                    )
                else:
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 850
                        * perimeter_holes_explosive_volume
                    )
                    q_primer_emulsion = len(perimeter_holes) * 0.4
                    q_total_anfo = q_perimeter
                    q_total_emulsion = (
                        q_cut + q_advance + q_floor + q_primer_emulsion
                    )

            elif self.contour_blasting == "Controlled":
                perimeter_holes_explosive_volume = (
                    (np.pi * self.perimeter_explosive_diameter ** 2 / 4)
                    * perimeter_holes_explosive_length
                    / 1e6
                )
                q_perimeter = (
                    len(perimeter_holes)
                    * 1200
                    * perimeter_holes_explosive_volume
                )
                q_total_emulsion = q_cut + q_advance + q_floor + q_perimeter
                q_total_anfo = 0

        elif self.explosive_type == "Anfo Bagged":
            if self.contour_blasting == "Normal":
                if self.contour_explosive_type == "Emulsion":
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.perimeter_explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 1200
                        * perimeter_holes_explosive_volume
                    )

                    q_total_emulsion = q_perimeter
                    q_total_anfo = q_cut + q_advance + q_floor

                elif self.contour_explosive_type == "Anfo Bagged":
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 950
                        * perimeter_holes_explosive_volume
                    )
                    q_primer_emulsion = len(self.perimeter_xys) * 0.4
                    q_total_anfo = q_cut + q_advance + q_floor + q_perimeter
                    q_total_emulsion = q_primer_emulsion
                else:
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 850
                        * perimeter_holes_explosive_volume
                    )
                    q_primer_emulsion = len(self.perimeter_xys) * 0.4
                    q_total_anfo = q_cut + q_advance + q_floor + q_perimeter
                    q_total_emulsion = q_primer_emulsion

            elif self.contour_blasting == "Controlled":
                perimeter_holes_explosive_volume = (
                    (np.pi * self.perimeter_explosive_diameter ** 2 / 4)
                    * perimeter_holes_explosive_length
                    / 1e6
                )
                q_perimeter = (
                    len(perimeter_holes)
                    * 1200
                    * perimeter_holes_explosive_volume
                )
                q_total_emulsion = q_perimeter
                q_total_anfo = q_cut + q_advance + q_floor

        elif self.explosive_type == "Anfo Pneumatically Charged":
            if self.contour_blasting == "Normal":
                if self.contour_explosive_type == "Emulsion":
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.perimeter_explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 1200
                        * perimeter_holes_explosive_volume
                    )

                    q_total_emulsion = q_perimeter
                    q_total_anfo = q_cut + q_advance + q_floor

                elif self.contour_explosive_type == "Anfo Bagged":
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 950
                        * perimeter_holes_explosive_volume
                    )
                    q_primer_emulsion = len(perimeter_holes) * 0.4
                    q_total_anfo = q_cut + q_advance + q_floor + q_perimeter
                    q_total_emulsion = q_primer_emulsion
                else:
                    perimeter_holes_explosive_volume = (
                        (np.pi * self.explosive_diameter ** 2 / 4)
                        * perimeter_holes_explosive_length
                        / 1e6
                    )
                    q_perimeter = (
                        len(perimeter_holes)
                        * 850
                        * perimeter_holes_explosive_volume
                    )
                    q_primer_emulsion = len(perimeter_holes) * 0.4
                    q_total_anfo = q_cut + q_advance + q_floor + q_perimeter
                    q_total_emulsion = q_primer_emulsion

            elif self.contour_blasting == "Controlled":
                perimeter_holes_explosive_volume = (
                    (np.pi * self.perimeter_explosive_diameter ** 2 / 4)
                    * perimeter_holes_explosive_length
                    / 1e6
                )
                q_perimeter = (
                    len(perimeter_holes)
                    * 1200
                    * perimeter_holes_explosive_volume
                )
                q_total_emulsion = q_perimeter
                q_total_anfo = q_cut + q_advance + q_floor

        total_volume = self.get_area() * self.hole_length

        specific_explosive_emulsion = q_total_emulsion / total_volume
        specific_explosive_anfo = q_total_anfo / total_volume

        total_drilling = len(self.holes) * self.hole_length
        specific_drilling = total_drilling / total_volume

        self.result = dict(
            total_explosive_emulsion=q_total_emulsion,
            total_explosive_anfo=q_total_anfo,
            number_of_total_holes=len(self.holes),
            specific_explosive_emulsion=specific_explosive_emulsion,
            specific_explosive_anfo=specific_explosive_anfo,
            specific_drilling=specific_drilling,
            cartridge_diameter=self.explosive_diameter,
            cartridge_diameter_perimeter=self.perimeter_explosive_diameter,
            number_of_cartridges_floor_per_hole=0,
            number_of_cartridges_advance_per_hole=0,
            number_of_cartridges_perimeter_per_hole=0,
            q_first_section=q_first_section,
            q_second_section=q_second_section,
            q_third_section=q_third_section,
            q_fourth_section=q_fourth_section,
        )
