import math

from matplotlib import patches
import mpl_toolkits.mplot3d.art3d as art3d

import numpy as np
from sympy import symbols, solveset

from .shape import Shape


class DShape(Shape):
    def __init__(self, parent=None, properties=None):
        super().__init__(parent, properties)
        self.last_x_index_side = None
        self.has_only_path_in_draw = False
        self.x_coordinates_property = None
        self.number_of_remaining_holes = None

    def is_point_near_edges(self, point):
        return (
            self.is_point_near_floor(point)
            or self.is_point_near_right_wall(point)
            or self.is_point_near_left_wall(point)
            or self.is_point_near_curve(point)
        )

    def is_point_near_floor(self, point):
        x, y = point
        return (0 < x < self.width) and (abs(y) < self.ERROR_TOLERANCE)

    def is_point_near_right_wall(self, point):
        x, y = point
        return (0 < y < self.height) and (
            abs(x - self.width) < self.ERROR_TOLERANCE
        )

    def is_point_near_left_wall(self, point):
        x, y = point
        return (0 < y < self.height) and (abs(x) < self.ERROR_TOLERANCE)

    def is_point_near_curve(self, point):
        x, y = point
        return (
            (self.width / 2 - self.ERROR_TOLERANCE) ** 2
            < (x - self.width / 2) ** 2 + (y - self.height) ** 2
            < (self.width / 2 + self.ERROR_TOLERANCE) ** 2
        )

    def snap_point_near_edges(self, point):
        x, y = point
        if self.is_point_near_floor(point):
            return (x, 0)
        elif self.is_point_near_right_wall(point):
            return (self.width, y)
        elif self.is_point_near_left_wall(point):
            return (0, y)
        elif self.is_point_near_curve(point):
            this_radius = math.sqrt(
                (x - self.width / 2) ** 2 + (y - self.height) ** 2
            )
            d = self.width / 2 - this_radius
            theta = math.atan2(y, x)
            return (x + d * math.cos(theta), y + d * math.sin(theta))

    def is_point_in_shape(self, point):
        return self.is_point_inside_rectangle_part(
            point
        ) or self.is_point_inside_crown_part(point)

    def is_point_inside_rectangle_part(self, point):
        x, y = point
        return (
            -self.width + self.ERROR_TOLERANCE
            < x
            < self.width - self.ERROR_TOLERANCE
        ) and (self.ERROR_TOLERANCE < y <= self.height)

    def is_point_inside_crown_part(self, point):
        x, y = point
        return (y > self.height) and (
            (x - self.width / 2) ** 2 + (y - self.height) ** 2
            < (self.width / 2 - self.ERROR_TOLERANCE) ** 2
        )

    def is_point_inside_cut(self, point):
        x, y = point
        if self.method == "EBM":
            if (
                self.width / 2 - 0.4 < x < self.width / 2 + 0.4
                and self.burden_advance < y < self.burden_advance + 0.8
            ):
                return True
        return False

    def draw(self):
        self.draw_line([-self.width / 2, self.width / 2], [0, 0], color="lime")
        self.append_rounded_point([-self.width / 2, self.width / 2], [0, 0])
        self.draw_line(
            [self.width / 2, self.width / 2], [0, self.height], color="lime"
        )
        self.append_rounded_point(
            [self.width / 2, self.width / 2], [0, self.height]
        )
        self.draw_line(
            [-self.width / 2, -self.width / 2],
            [0, self.height],
            color="lime",
        )
        self.append_rounded_point(
            [-self.width / 2, -self.width / 2], [0, self.height]
        )

        self.draw_patch()

    def draw_patch(self):
        if self.is_2d:
            patch = patches.Arc(
                [0, self.height],
                self.width,
                self.width,
                theta1=0,
                theta2=180,
                color="lime",
            )
            self.axes.add_patch(patch)
        else:
            # create half of circle
            r = self.width / 2
            y0 = self.height  # To have the tangent at y=0
            x0 = 0.0

            # Theta varies only between pi/2 and 3pi/2. to have a half-circle
            theta = np.linspace(-np.pi / 2.0, np.pi / 2.0, 201)

            z = np.zeros_like(theta)  # z=0
            y = r * np.cos(theta) + y0  # y - y0 = r*cos(theta)
            x = r * np.sin(theta) + x0  # x - x0 = r*sin(theta)

            self.append_rounded_point(x, y)
            self.axes.plot(x, y, z, color="lime")

    def get_area(self):
        return self.width * self.height + math.pi * self.width ** 2 / 8

    def get_perimeter(self):
        return 2 * self.height + (math.pi * self.width) / 2

    def get_perimeter_average_length(self):
        return 2 * self.height + math.pi * (
            self.width / 2 - self.perimeter_burden / 2
        )

    def check_space(self):
        return self.width / 2 < self.perimeter_burden + 0.4

    def draw_plot_ebm(self):

        self.draw()

        self.draw_cut_ebm()

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_spacing,
        )

        # Crown holes
        number_of_holes_crown = round(
            (np.pi * self.width) / (2 * self.perimeter_spacing)
        )
        crown_theta = np.linspace(0, np.pi, number_of_holes_crown)
        for t in crown_theta:
            self.add_hole(
                (self.width / 2) * np.cos(t),
                self.height + (self.width / 2) * np.sin(t),
                perimeter_hole_properties,
            )

        # Wall holes
        number_of_holes_wall = math.ceil(self.height / self.perimeter_spacing)
        y_hole_wall = np.linspace(0, self.height, number_of_holes_wall)[1:-1]

        for y in y_hole_wall:
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)
            self.add_hole(self.width / 2, y, perimeter_hole_properties)

        # Advance
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        # Floor holes
        number_of_holes_floor = round(self.width / self.advance_spacing)
        x_hole_floor = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_floor
        )

        for x in x_hole_floor:
            self.add_hole(x, 0, advance_hole_properties)

        # start of advance
        # draw first crown next to perimeter
        burden_curve_radius = (self.width - 2 * self.perimeter_burden) / 2
        number_of_holes_burden_curve = round(
            np.pi * burden_curve_radius / self.advance_spacing
        )
        burden_curve_theta = np.linspace(
            0, np.pi, number_of_holes_burden_curve
        )
        for t in burden_curve_theta:
            x_hole = burden_curve_radius * np.cos(t)
            y_hole = self.height + burden_curve_radius * np.sin(t)
            if (abs(x_hole) < self.cut_length / 2) and (
                y_hole < self.advance_burden + self.cut_length
                and y_hole > self.advance_burden
            ):
                continue
            self.add_hole(x_hole, y_hole, advance_hole_properties)

        cut_wall_distance = (
            self.width / 2 - self.cut_length / 2 - self.perimeter_burden
        )

        # other crown after first crown next to perimeter
        # distance between cut and crown of perimeter
        cut_crown_distance = (
            self.height
            + self.width / 2
            - self.perimeter_burden
            - self.cut_length
        )
        # number of tunnel which we can draw
        minimum_curves = round(
            min(cut_wall_distance, cut_crown_distance) / self.advance_burden
        )

        # burden of inner crown
        operational_burden_advance = (
            self.width / 2 - self.perimeter_burden - self.cut_length / 2
        ) / minimum_curves

        # Curve holes
        number_of_curves = 0
        smallest_radius = 0

        # crown of other
        for curve_number in range(1, minimum_curves):
            curve_radius = (
                self.width
                - 2 * self.perimeter_burden
                - 2 * curve_number * operational_burden_advance
            ) / 2
            number_of_holes_curve = round(
                np.pi * curve_radius / self.advance_spacing
            )
            theta = np.linspace(0, np.pi, number_of_holes_curve)
            for t in theta:
                x_hole = curve_radius * np.cos(t)
                y_hole = self.height + curve_radius * np.sin(t)
                if (abs(x_hole) < self.cut_length / 2) and (
                    y_hole < (self.advance_burden + self.cut_length)
                    and y_hole > (self.advance_burden)
                ):
                    continue
                self.add_hole(x_hole, y_hole, advance_hole_properties)

            number_of_curves += 1
            smallest_radius = curve_radius

        # wall of yedone monde be mohit
        number_of_holes_interior_wall = (
            round(self.height / self.advance_spacing) + 1
        )
        y_hole_interior_wall = np.linspace(
            0, self.height, number_of_holes_interior_wall
        )[1:-1]

        for y in y_hole_interior_wall:
            self.add_hole(
                -self.width / 2 + self.perimeter_burden,
                y,
                advance_hole_properties,
            )
            self.add_hole(
                self.width / 2 - self.perimeter_burden,
                y,
                advance_hole_properties,
            )
        # other crown
        for curve_number in range(1, minimum_curves):
            for y in y_hole_interior_wall:
                self.add_hole(
                    -self.width / 2
                    + self.perimeter_burden
                    + curve_number * operational_burden_advance,
                    y,
                    advance_hole_properties,
                )
                self.add_hole(
                    self.width / 2
                    - self.perimeter_burden
                    - curve_number * operational_burden_advance,
                    y,
                    advance_hole_properties,
                )

        # White space holes
        x_1 = (
            -self.width / 2
            + self.perimeter_burden
            + number_of_curves * operational_burden_advance
        )
        x_2 = (
            self.width / 2
            - self.perimeter_burden
            - number_of_curves * operational_burden_advance
        )
        delta_x = x_2 - x_1
        number_of_white_space_curves = math.floor(
            delta_x / self.advance_burden
        )
        if number_of_white_space_curves == 3:
            x_white_space_1 = x_1 + delta_x / 3
            x_white_space_2 = x_2 - delta_x / 3
            y_white_space_1 = self.advance_burden + self.cut_length
            y_white_space_2 = self.height + smallest_radius / math.sqrt(2)
            number_of_white_space_holes = round(
                (y_white_space_2 - y_white_space_1) / self.advance_burden
            )
            for i in range(1, number_of_white_space_holes):
                self.add_hole(
                    x_white_space_1,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )
                self.add_hole(
                    x_white_space_2,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )

        if number_of_white_space_curves == 2:
            x_white_space = x_1 + delta_x / 2
            y_white_space_1 = self.advance_burden + self.cut_length
            y_white_space_2 = self.height + smallest_radius
            number_of_white_space_holes = round(
                (y_white_space_2 - y_white_space_1) / self.advance_burden
            )
            for i in range(1, number_of_white_space_holes):
                self.add_hole(
                    x_white_space,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )

        self.final_draw()

    def draw_cut_ebm(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        self.add_hole(
            0, self.advance_burden + self.cut_length / 2, air_hole_properties
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )

        self.add_hole(
            0.15,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(
            0,
            self.advance_burden + self.cut_length / 2 + 0.18,
            cut_hole_properties,
        )
        self.add_hole(
            -0.22,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(
            0,
            self.advance_burden + self.cut_length / 2 - 0.25,
            cut_hole_properties,
        )
        self.add_hole(
            -self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            -self.cut_length / 2, self.advance_burden, cut_hole_properties
        )
        self.add_hole(
            self.cut_length / 2, self.advance_burden, cut_hole_properties
        )

        patch = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.sending_patch_cut = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.axes.add_patch(patch)
        if not self.is_2d:
            art3d.pathpatch_2d_to_3d(patch)

    def draw_plot_ebm_angular(self):

        self.draw()

        self.draw_cut_ebm_angular()

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_stemming,
        )

        # Wall
        # Wall ---> left: x = -(self.width/2) and right: x = self.width/2
        # Wall ---> range(0-self.height) ---> using linspace from numpy--> step is spacing
        number_of_wall_holes = round(self.height / self.perimeter_spacing) + 1
        ys = np.linspace(0, self.height, number_of_wall_holes)[1:-1]
        for y in ys:
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)
            self.add_hole(self.width / 2, y, perimeter_hole_properties)

        number_of_crown_holes = round(
            ((self.width / 2) * np.pi) / self.perimeter_spacing
        )
        thetas = np.linspace(0, np.pi, number_of_crown_holes)
        # Arc
        # x = r * cos(theta)                ---> r = self.width/2
        # y = self.height + r * sin(theta)  ---> r = self.width/2
        for t in thetas:
            self.add_hole(
                self.width * np.cos(t) / 2,
                self.height + (self.width * np.sin(t) / 2),
                perimeter_hole_properties,
            )

        # Advance
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # Floor
        # y = 0
        # x : range(-self.width/2, +self.width/2) --> using linspace and step is: spacing
        number_of_floor_holes = round(self.width / self.advance_spacing) + 1
        xs = np.linspace(
            -self.width / 2, self.width / 2, number_of_floor_holes
        )
        for x in xs:
            self.add_hole(x, 0, advance_hole_properties)

        for i in range(self.cut_rows):
            if (
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance
                < self.height
            ):
                delta_x = (
                    self.width / 2
                    - self.x_length / 2
                    - self.cut_develop_length
                    - self.perimeter_burden
                )
                self.number_of_remaining_holes = (
                    number_of_remaining_holes
                ) = round(delta_x / self.cut_wall_filler_length)
                if (
                    self.width / 2
                    - self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    - self.perimeter_burden
                    > 0
                ):
                    xs = np.linspace(
                        self.outer_cut_hole_positive_x
                        + self.cut_develop_front_length,
                        self.width / 2 - self.perimeter_burden,
                        number_of_remaining_holes,
                    )[1:]
                    # reset counter for each row (i)
                    counter = 0
                    points_in_cut = []

                    for x in xs:
                        self.add_hole(
                            x,
                            self.operational_burden_advance
                            + i * self.cut_rows_vertical_distance,
                            advance_hole_properties,
                        )
                        y = (
                            self.operational_burden_advance
                            + i * self.cut_rows_vertical_distance
                        )
                        point_in_cut_positive = {
                            "x": x,
                            "index": self.last_x_index_side + counter,
                            "y": y,
                        }
                        points_in_cut.append(point_in_cut_positive)

                        self.add_hole(
                            -x,
                            self.operational_burden_advance
                            + i * self.cut_rows_vertical_distance,
                            advance_hole_properties,
                        )
                        point_in_cut_negative = {
                            "x": -x,
                            "index": self.last_x_index_side + counter,
                            "y": y,
                        }
                        points_in_cut.append(point_in_cut_negative)
                        counter += 1
                    self.x_coordinates_property[i].extend(points_in_cut)
            else:
                x = symbols("x")
                x1, x2 = sorted(
                    list(
                        solveset(
                            x ** 2
                            + (
                                self.operational_burden_advance
                                + i * self.cut_rows_vertical_distance
                                - self.height
                            )
                            ** 2
                            - self.width ** 2 / 4,
                            x,
                        )
                    )
                )
                delta_x = (
                    x2
                    - self.x_length / 2
                    - self.cut_develop_length
                    - self.perimeter_burden
                )
                self.number_of_remaining_holes = (
                    number_of_remaining_holes
                ) = round(delta_x / self.cut_wall_filler_length)
                if (
                    float(x2)
                    - self.perimeter_burden
                    - self.outer_cut_hole_positive_x
                    - self.cut_develop_front_length
                    > 0
                ):
                    xs = np.linspace(
                        self.outer_cut_hole_positive_x
                        + self.cut_develop_front_length,
                        float(x2) - self.perimeter_burden,
                        number_of_remaining_holes,
                    )[1:]
                    # reset counter for each row (i)
                    counter = 0
                    points_in_cut = []
                    for x in xs:
                        self.add_hole(
                            x,
                            self.operational_burden_advance
                            + i * self.cut_rows_vertical_distance,
                            advance_hole_properties,
                        )
                        y = (
                            self.operational_burden_advance
                            + i * self.cut_rows_vertical_distance
                        )
                        point_in_cut_positive = {
                            "x": x,
                            "index": self.last_x_index_side + counter,
                            "y": y,
                        }
                        points_in_cut.append(point_in_cut_positive)

                        self.add_hole(
                            -x,
                            self.operational_burden_advance
                            + i * self.cut_rows_vertical_distance,
                            advance_hole_properties,
                        )
                        point_in_cut_negative = {
                            "x": -x,
                            "index": self.last_x_index_side + counter,
                            "y": y,
                        }
                        points_in_cut.append(point_in_cut_negative)
                    self.x_coordinates_property[i].extend(points_in_cut)

        if (
            self.operational_burden_advance
            + (self.cut_rows - 1) * self.cut_rows_vertical_distance
            > self.height
        ):
            remaining_height = (
                self.height
                + self.width / 2
                - self.operational_burden_advance
                - self.perimeter_burden
                - (self.cut_rows - 1) * self.cut_rows_vertical_distance
            )
            number_of_crowns = (
                round(remaining_height / self.advance_burden) + 1
            )
            operational_burden_advance_this = (
                remaining_height / number_of_crowns
            )
            for i in range(1, number_of_crowns):
                radius = self.width / 2 - i * operational_burden_advance_this
                phi = np.arcsin(
                    (
                        self.operational_burden_advance
                        + (self.cut_rows - 1) * self.cut_rows_vertical_distance
                        - self.height
                    )
                    / radius
                )
                number_of_crown_holes = (
                    round(radius * (np.pi - 2 * phi) / self.advance_spacing)
                    + 1
                )
                thetas = np.linspace(phi, np.pi - phi, number_of_crown_holes)[
                    1:-1
                ]
                for t in thetas:
                    y = self.height + radius * np.sin(t)
                    self.add_hole(
                        radius * np.cos(t),
                        y,
                        advance_hole_properties,
                    )

        else:
            number_of_crowns = round(
                (self.width - 2 * self.perimeter_burden)
                / (2 * self.advance_burden)
            )
            operational_burden_advance_this = (
                self.width - 2 * self.perimeter_burden
            ) / (2 * number_of_crowns)
            delta_y = (
                self.height
                - self.operational_burden_advance
                - (self.cut_rows - 1) * self.cut_rows_vertical_distance
                - self.perimeter_burden
            )
            number_of_column_holes = round(delta_y / self.advance_burden) + 1
            ys = np.linspace(
                self.operational_burden_advance
                + (self.cut_rows - 1) * self.cut_rows_vertical_distance,
                self.height,
                number_of_column_holes,
            )[1:-1]
            for i in range(1, number_of_crowns):
                number_of_crown_holes = (
                    round(
                        (
                            (
                                self.width / 2
                                - i * operational_burden_advance_this
                            )
                            * np.pi
                        )
                        / self.advance_spacing
                    )
                    + 1
                )
                thetas = np.linspace(0, np.pi, number_of_crown_holes)
                for t in thetas:
                    self.add_hole(
                        (self.width / 2 - i * operational_burden_advance_this)
                        * np.cos(t),
                        self.height
                        + (
                            self.width / 2
                            - i * operational_burden_advance_this
                        )
                        * np.sin(t),
                        advance_hole_properties,
                    )
                for y in ys:
                    self.add_hole(
                        self.width / 2 - i * operational_burden_advance_this,
                        y,
                        advance_hole_properties,
                    )
                    self.add_hole(
                        -self.width / 2 + i * operational_burden_advance_this,
                        y,
                        advance_hole_properties,
                    )
            for y in ys:
                self.add_hole(0, y, advance_hole_properties)

            self.add_hole(0, self.height, advance_hole_properties)

        self.final_draw()

    def draw_cut_ebm_angular(self):
        self.x_coordinates_property = {}
        if (
            0
            < self.height
            - self.advance_burden
            - (self.cut_rows - 1) * self.cut_rows_vertical_distance
            < self.advance_burden
        ):
            self.operational_burden_advance = (
                self.height
                - (self.cut_rows - 1) * self.cut_rows_vertical_distance
            ) / 2
        else:
            self.operational_burden_advance = self.advance_burden

        self.outer_cut_hole_positive_x = self.triangle_half_length

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )

        # points_in_cut = []
        for i in range(self.cut_rows):
            # reset counter for each row (i)
            self.x_coordinates_property[i] = []
            counter = 0
            self.add_hole(
                -self.outer_cut_hole_positive_x,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x
            y = (
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance
            )
            point_in_cut_negative = {"x": -x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_negative)

            self.add_hole(
                self.outer_cut_hole_positive_x,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_positive)

            counter += 1

            self.add_hole(
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x_dev_front_negative = (
                -self.outer_cut_hole_positive_x - self.cut_develop_front_length
            )
            point_in_cut_negative_dev_front = {
                "x": x_dev_front_negative,
                "index": counter,
                "y": y,
            }
            self.x_coordinates_property[i].append(
                point_in_cut_negative_dev_front
            )

            self.add_hole(
                self.outer_cut_hole_positive_x + self.cut_develop_front_length,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x_dev_front_positive = (
                self.outer_cut_hole_positive_x + self.cut_develop_front_length
            )
            point_in_cut_positive_dev_front = {
                "x": x_dev_front_positive,
                "index": counter,
                "y": y,
            }
            self.x_coordinates_property[i].append(
                point_in_cut_positive_dev_front
            )

            counter += 1
            self.last_x_index_side = counter

        # small triangle
        if self.hole_length / self.cut_burden > 1:
            if self.method == "EBM Angular":
                small_triangle_length = (
                    2
                    * self.triangle_half_length
                    * (self.hole_length - self.cut_burden)
                    / self.hole_length
                )
                for i in range(self.cut_rows):
                    self.add_hole(
                        small_triangle_length / 2,
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    x = small_triangle_length / 2
                    y = (
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance
                    )
                    length = math.fabs(x)
                    point_in_cut_positive = {
                        "x": x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.x_coordinates_property[i].append(
                        point_in_cut_positive
                    )

                    self.add_hole(
                        -small_triangle_length / 2,
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    point_in_cut_negative = {
                        "x": -x,
                        "index": 0,
                        "type": "small-triangle",
                        "length": length,
                        "y": y,
                    }
                    self.x_coordinates_property[i].append(
                        point_in_cut_negative
                    )
            elif (
                self.method == "Gustafsson"
                or self.method == "Olafsson Angular"
                or self.method == "Konya Angular"
            ):
                number_of_smaller_triangles = round(
                    self.hole_length / self.cut_burden
                )
                step = self.triangle_half_length / number_of_smaller_triangles
                for i in range(1, number_of_smaller_triangles):
                    for j in range(self.cut_rows):
                        x_positive = 0 - i * step
                        length = math.fabs(x_positive)
                        y = (
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance
                        )
                        self.add_hole(
                            x_positive,
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        point_in_cut_positive = {
                            "x": x_positive,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[j].append(
                            point_in_cut_positive
                        )

                        x_negative = 0 + i * step
                        length = math.fabs(x_negative)
                        self.add_hole(
                            x_negative,
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        point_in_cut_negative = {
                            "x": x_negative,
                            "index": 0,
                            "type": "small-triangle",
                            "length": length,
                            "y": y,
                        }
                        self.x_coordinates_property[j].append(
                            point_in_cut_negative
                        )

    def draw_plot_ntnu(self):

        self.draw()

        parameters = self.get_parameters_ntnu()
        burden_perimeter = parameters.get("burden_perimeter")
        spacing_perimeter = parameters.get("spacing_perimeter")
        burden_first_ring = parameters.get("burden_first_ring")
        spacing_first_ring = parameters.get("spacing_first_ring")
        burden_floor = parameters.get("burden_floor")
        spacing_floor = parameters.get("spacing_floor")
        burden_advance = parameters.get("burden_advance")
        spacing_advance = parameters.get("spacing_advance")

        self.draw_cut_ntnu()

        # Crown holes
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_perimeter,
        )

        number_of_holes_crown = round(
            (np.pi * self.width) / (2 * spacing_perimeter)
        )
        crown_theta = np.linspace(0, np.pi, number_of_holes_crown)
        for t in crown_theta:
            self.add_hole(
                (self.width / 2) * np.cos(t),
                self.height + (self.width / 2) * np.sin(t),
                perimeter_hole_properties,
            )

        # Floor holes
        floor_hole_properties = dict(
            hole_type="Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        number_of_holes_floor = round(self.width / spacing_floor)
        x_hole_floor = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_floor
        )
        for x in x_hole_floor:
            self.add_hole(x, 0, floor_hole_properties)

        # Wall holes
        number_of_holes_wall = math.ceil(self.height / spacing_perimeter)
        y_hole_wall = np.linspace(0, self.height, number_of_holes_wall)[1:-1]
        for y in y_hole_wall:
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)
            self.add_hole(self.width / 2, y, perimeter_hole_properties)

        # First ring
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        buffer_hole_properties = dict(
            hole_type="Buffer",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        ring_radius = self.width / 2 - burden_perimeter
        ring_length = ring_radius * np.pi
        number_of_ring_holes = round(ring_length / spacing_first_ring)
        thetas = np.linspace(0, np.pi, number_of_ring_holes)
        for t in thetas:
            self.add_hole(
                ring_radius * np.cos(t),
                self.height + ring_radius * np.sin(t),
                buffer_hole_properties,
            )

        number_of_holes_interior_wall = (
            round((self.height - burden_floor) / spacing_first_ring) + 1
        )
        y_hole_interior_wall = np.linspace(
            burden_floor, self.height, number_of_holes_interior_wall
        )[:-1]

        for y in y_hole_interior_wall:
            self.add_hole(
                -self.width / 2 + burden_perimeter, y, buffer_hole_properties
            )
            self.add_hole(
                self.width / 2 - burden_perimeter, y, buffer_hole_properties
            )

        # Inner rings
        ring_radius -= burden_first_ring
        ring_length = ring_radius * np.pi
        number_of_ring_holes = round(ring_length / spacing_advance) + 1
        thetas = np.linspace(0, np.pi, number_of_ring_holes)
        for t in thetas:
            self.add_hole(
                ring_radius * np.cos(t),
                self.height + ring_radius * np.sin(t),
                advance_hole_properties,
            )

        number_of_holes_interior_wall = (
            round((self.height - burden_floor) / spacing_advance) + 1
        )
        y_hole_interior_wall = np.linspace(
            burden_floor, self.height, number_of_holes_interior_wall
        )[:-1]

        for y in y_hole_interior_wall:
            self.add_hole(
                -self.width / 2 + burden_perimeter + burden_first_ring,
                y,
                advance_hole_properties,
            )
            self.add_hole(
                self.width / 2 - burden_perimeter - burden_first_ring,
                y,
                advance_hole_properties,
            )

        delta_y = self.width / 2 - burden_perimeter - burden_first_ring
        number_of_rings = round(delta_y / burden_advance) + 1
        operational_burden_advance = delta_y / number_of_rings
        for _ in range(1, number_of_rings):
            ring_radius -= operational_burden_advance
            ring_length = np.pi * ring_radius
            number_of_ring_holes = round(ring_length / spacing_advance) + 1
            thetas = np.linspace(0, np.pi, number_of_ring_holes)
            if len(thetas) == 1:
                self.add_hole(0, self.height + ring_radius)
            else:
                for t in thetas:
                    self.add_hole(
                        ring_radius * np.cos(t),
                        self.height + ring_radius * np.sin(t),
                        advance_hole_properties,
                    )
            if ring_radius > self.w4 / 2 + 0.5 * burden_advance:
                for y in y_hole_interior_wall:
                    self.add_hole(ring_radius, y, advance_hole_properties)
                    self.add_hole(-ring_radius, y, advance_hole_properties)
            else:
                delta_y = self.height - burden_floor - self.w4
                number_of_holes_interior_wall = (
                    round(delta_y / spacing_advance) + 1
                )
                ys = np.linspace(
                    burden_floor + self.w4,
                    self.height,
                    number_of_holes_interior_wall,
                )[1:-1]
                for y in ys:
                    self.add_hole(ring_radius, y, advance_hole_properties)
                    self.add_hole(-ring_radius, y, advance_hole_properties)

        self.final_draw()

    def draw_cut_ntnu(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        x_air_hole = 0
        y_air_hole = self.burden_floor + self.w4 / 2

        self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_cut,
        )

        self.add_hole(x_air_hole - self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b1, cut_hole_properties)
        self.add_hole(x_air_hole + self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b1, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(x_air_hole - self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b3, cut_hole_properties)
        self.add_hole(x_air_hole + self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b3, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )

    def draw_plot_olafsson(self):

        self.draw()

        self.draw_cut_olafsson()

        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        burden_curve_radius = (self.width - 2 * self.wall_burden) / 2
        number_of_holes_burden_curve = round(
            np.pi * burden_curve_radius / self.advance_spacing
        )
        burden_curve_theta = np.linspace(
            0, np.pi, number_of_holes_burden_curve
        )
        for t in burden_curve_theta:
            x_hole = burden_curve_radius * np.cos(t)
            y_hole = self.height + burden_curve_radius * np.sin(t)
            if (x_hole < self.last_x / 2 and x_hole > -self.last_x / 2) and (
                y_hole < self.floor_burden + self.last_x
                and y_hole > self.floor_burden
            ):
                continue
            self.add_hole(x_hole, y_hole, advance_hole_properties)

        cut_wall_distance = self.width / 2 - self.last_x / 2 - self.wall_burden
        cut_crown_distance = (
            self.height + self.width / 2 - self.roof_burden - self.last_x
        )

        minimum_curves = round(
            min(cut_wall_distance, cut_crown_distance) / self.advance_burden
        )

        operational_burden_advance = (
            self.width / 2 - self.wall_burden - self.last_x / 2
        ) / minimum_curves

        # Curve holes
        number_of_curves = 0
        smallest_radius = self.width / 2 - self.roof_burden

        for curve_number in range(1, minimum_curves):
            curve_radius = (
                self.width
                - 2 * self.roof_burden
                - 2 * curve_number * operational_burden_advance
            ) / 2
            number_of_holes_curve = round(
                np.pi * curve_radius / self.advance_spacing
            )
            theta = np.linspace(0, np.pi, number_of_holes_curve)
            for t in theta:
                x_hole = curve_radius * np.cos(t)
                y_hole = self.height + curve_radius * np.sin(t)
                if (
                    x_hole < (self.last_x / 2) and x_hole > (-self.last_x / 2)
                ) and (
                    y_hole < (self.floor_burden + self.last_x)
                    and y_hole > (self.floor_burden)
                ):
                    continue
                self.add_hole(x_hole, y_hole, advance_hole_properties)

            number_of_curves += 1
            smallest_radius = curve_radius

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # Crown holes
        number_of_holes_crown = round(
            (np.pi * self.width) / (2 * self.roof_spacing)
        )
        crown_theta = np.linspace(0, np.pi, number_of_holes_crown)
        for t in crown_theta:
            self.add_hole(
                (self.width / 2) * np.cos(t),
                self.height + (self.width / 2) * np.sin(t),
                perimeter_hole_properties,
            )

        # Floor holes
        floor_hole_properties = dict(
            hole_type="Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        number_of_holes_floor = round(self.width / self.floor_spacing)
        x_hole_floor = np.linspace(
            -self.width / 2, self.width / 2, number_of_holes_floor
        )
        for x in x_hole_floor:
            self.add_hole(x, 0, floor_hole_properties)

        # Wall holes
        number_of_holes_wall = math.ceil(self.height / self.wall_spacing)
        y_hole_wall = np.linspace(0, self.height, number_of_holes_wall)[1:-1]

        for y in y_hole_wall:
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)
            self.add_hole(self.width / 2, y, perimeter_hole_properties)

        number_of_holes_interior_wall = (
            round((self.height - self.floor_burden) / self.advance_spacing) + 1
        )
        y_hole_interior_wall = np.linspace(
            self.floor_burden, self.height, number_of_holes_interior_wall
        )[:-1]
        for y in y_hole_interior_wall:
            self.add_hole(
                self.width / 2 - self.wall_burden, y, advance_hole_properties
            )
            self.add_hole(
                -self.width / 2 + self.wall_burden, y, advance_hole_properties
            )

        for curve_number in range(1, minimum_curves):
            for y in y_hole_interior_wall:
                self.add_hole(
                    -self.width / 2
                    + self.wall_burden
                    + curve_number * operational_burden_advance,
                    y,
                    advance_hole_properties,
                )
                self.add_hole(
                    self.width / 2
                    - self.wall_burden
                    - curve_number * operational_burden_advance,
                    y,
                    advance_hole_properties,
                )

        # White space holes
        x_1 = (
            -self.width / 2
            + self.wall_burden
            + number_of_curves * operational_burden_advance
        )
        x_2 = (
            self.width / 2
            - self.wall_burden
            - number_of_curves * operational_burden_advance
        )
        delta_x = x_2 - x_1
        number_of_white_space_curves = math.floor(
            delta_x / self.advance_burden
        )
        if number_of_white_space_curves == 4:
            x_white_space_1 = x_1 + delta_x / 4
            x_white_space_2 = x_2 - delta_x / 4
            y_white_space_1 = self.floor_burden + self.last_x
            y_white_space_2 = self.height + smallest_radius / math.sqrt(2)
            number_of_white_space_holes = round(
                (y_white_space_2 - y_white_space_1) / self.advance_burden
            )
            for i in range(1, number_of_white_space_holes):
                self.add_hole(
                    x_white_space_1,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )

                self.add_hole(
                    x_white_space_2,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )

                self.add_hole(
                    (x_white_space_1 + x_white_space_2) / 2,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )

        if number_of_white_space_curves == 3:
            x_white_space_1 = x_1 + delta_x / 3
            x_white_space_2 = x_2 - delta_x / 3
            y_white_space_1 = self.floor_burden + self.last_x
            y_white_space_2 = self.height + smallest_radius / math.sqrt(2)
            number_of_white_space_holes = round(
                (y_white_space_2 - y_white_space_1) / self.advance_burden
            )
            for i in range(1, number_of_white_space_holes):
                self.add_hole(
                    x_white_space_1,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )
                self.add_hole(
                    x_white_space_2,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )

        if number_of_white_space_curves == 2:
            x_white_space = x_1 + delta_x / 2
            y_white_space_1 = self.floor_burden + self.last_x
            y_white_space_2 = self.height + smallest_radius
            number_of_white_space_holes = round(
                (y_white_space_2 - y_white_space_1) / self.advance_burden
            )
            for i in range(1, number_of_white_space_holes):
                self.add_hole(
                    x_white_space,
                    y_white_space_1 + i * self.advance_burden,
                    advance_hole_properties,
                )
        self.final_draw()

    def draw_cut_olafsson(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        required_parameters_length = len(self.required_parameters)
        if required_parameters_length == 4:
            (
                first_section,
                second_section,
                third_section,
                fourth_section,
            ) = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section
            b3, _ = third_section
            _, x4 = fourth_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x4 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(x_air_hole - b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b3, cut_hole_properties)
            self.add_hole(x_air_hole + b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b3, cut_hole_properties)
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
        elif required_parameters_length == 3:
            (
                first_section,
                second_section,
                third_section,
            ) = self.required_parameters
            _, x1 = first_section
            b2, _ = second_section
            _, x3 = third_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x3 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )

            self.add_hole(
                x_air_hole,
                y_air_hole - b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole,
                y_air_hole + b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )

        elif required_parameters_length == 2:
            first_section, second_section = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x2 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )

        elif required_parameters_length == 1:
            first_section = self.required_parameters
            _, x1 = first_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x1 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
