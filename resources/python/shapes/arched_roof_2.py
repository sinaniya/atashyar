import math

from sympy import asin, cos, solve, symbols

from .arched_roof_1 import ArchedRoof1

# from .shape import Shape


class ArchedRoof2(ArchedRoof1):
    def __init__(self, parent=None, properties=None):
        self.width = properties["width"]
        self.arch = properties["arch"]
        self.abutment = properties["abutment"]
        self.radius = self.calculate_radius()
        self.height = self.abutment + self.arch
        properties["radius"] = self.radius
        properties["height"] = self.height
        super().__init__(parent, properties)

    def is_point_near_edges(self, point):
        return (
            self.is_point_near_floor(point)
            or self.is_point_near_right_wall(point)
            or self.is_point_near_left_wall(point)
            or self.is_point_near_curve(point)
        )

    def is_point_near_floor(self, point):
        x, y = point
        return (-self.width / 2 < x < self.width / 2) and (
            abs(y) < self.ERROR_TOLERANCE
        )

    def is_point_near_right_wall(self, point):
        x, y = point
        return (abs(x - self.width / 2) < self.ERROR_TOLERANCE) and (
            0 < y < self.abutment
        )

    def is_point_near_left_wall(self, point):
        x, y = point
        return (abs(x + self.width / 2) < self.ERROR_TOLERANCE) and (
            0 < y < self.abutment
        )

    def is_point_near_curve(self, point):
        x, y = point
        return (
            (self.radius - self.ERROR_TOLERANCE) ** 2
            < x ** 2 + (y - (self.height - self.radius)) ** 2
            < (self.radius + self.ERROR_TOLERANCE) ** 2
        )

    def snap_point_near_edges(self, point):
        x, y = point
        if self.is_point_near_floor(point):
            return (x, 0)
        elif self.is_point_near_right_wall(point):
            return (self.width / 2, y)
        elif self.is_point_near_left_wall(point):
            return (-self.width / 2, y)
        elif self.is_point_near_curve(point):
            this_radius = math.sqrt(
                x ** 2 + (y - (self.height - self.radius)) ** 2
            )
            d = self.radius - this_radius
            theta = math.atan2(y, x)
            return (x + d * math.cos(theta), y + d * math.sin(theta))

    def is_point_in_shape(self, point):
        return self.is_point_inside_rectangle_part(
            point
        ) or self.is_point_inside_crown_part(point)

    def is_point_inside_rectangle_part(self, point):
        x, y = point
        return (
            -self.width / 2 + self.ERROR_TOLERANCE
            < x
            < self.width / 2 - self.ERROR_TOLERANCE
        ) and (self.ERROR_TOLERANCE < y <= self.abutment)

    def is_point_inside_crown_part(self, point):
        x, y = point
        return (y > self.abutment) and (
            x ** 2 + (y - self.height + self.radius) ** 2
            < (self.radius - self.ERROR_TOLERANCE) ** 2
        )

    def is_point_inside_cut(self, point):
        return True

    def calculate_radius(self):
        r = symbols("r")
        answers = solve(r * cos(asin(self.width / (2 * r))) - r + self.arch, r)
        return float(answers[0])
