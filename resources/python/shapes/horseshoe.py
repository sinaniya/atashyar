import math
from typing import Tuple

from matplotlib import patches
import numpy as np

from .shape import Shape


class HorseShoe(Shape):
    def __init__(self, parent=None, properties=None):
        super().__init__(parent, properties)
        self.width = (self.main_coordinates['p2'][0] + self.main_coordinates['p3'][0]) / 2
        self.x_coordinates_property = None

    def snap_point_near_edges(self, point: Tuple[float, float]) -> Tuple[float, float]:
        pass

    def is_point_near_edges(self, point: Tuple[float, float]) -> bool:
        pass

    def is_point_in_shape(self, point: Tuple[float, float]) -> bool:
        pass

    def is_point_inside_cut(self, point: Tuple[float, float]) -> bool:
        pass

    def get_perimeter_average_length(self):
        p1_total = self.get_perimeter()
        p1 = p1_total - (2 * abs(self.main_parameters['x1']))
        new_radius = self.radius - (self.perimeter_burden / 2)
        new_upper_crown = new_radius * math.pi
        new_small_crowns = 2 * ((2 * new_radius) * self.main_parameters['theta'])
        p2 = new_upper_crown + new_small_crowns
        return (p1 + p2) / 2

    def check_space(self):
        pass

    @property
    def main_parameters(self):
        # equal of 2 degree
        delta = (16 * (self.radius ** 2)) - (4 * (self.height ** 2))
        x1 = ((2 * self.radius) - math.sqrt(delta)) / 2
        y2 = (self.height * self.radius) / (self.radius - x1)
        theta = math.asin(self.height / (2 * self.radius))
        return {'x1': x1, 'y2': y2, 'theta': theta}

    @property
    def main_coordinates(self):
        p1 = [-self.radius, self.height]
        p2 = [self.radius, self.height]
        p3 = [math.fabs(self.main_parameters['x1']), 0]
        p4 = [self.main_parameters['x1'], 0]
        return {'p1': p1, 'p2': p2, 'p3': p3, 'p4': p4}

    def get_area(self):
        s1 = self.radius * self.main_parameters['y2']
        s2 = (math.fabs(self.main_parameters['x1']) * (self.height - self.main_parameters['y2']))
        s3 = (2 * self.main_parameters['theta']) * (self.radius ** 2)
        s4 = (math.pi * (self.radius ** 2)) / 2
        s_total = s4 + (2 * s3) + s2 - s1
        return s_total

    def get_perimeter(self):
        crown_perimeter = math.pi * self.radius
        arc_perimeter = self.main_parameters['theta'] * (2 * self.radius)
        floor_perimeter = 2 * math.fabs(self.main_parameters['x1'])
        total_perimeter = crown_perimeter + (2 * arc_perimeter) + floor_perimeter
        return total_perimeter

    def draw(self):
        self.draw_line([self.main_parameters['x1'], abs(self.main_parameters['x1'])], [0, 0], color="lime")
        self.append_rounded_point([self.main_parameters['x1'], abs(self.main_parameters['x1'])], [0, 0])
        self.draw_patch()

    def draw_patch(self):
        if self.is_2d:
            patch1 = patches.Arc(
                (0, self.height),
                float(2 * self.radius),
                float(2 * self.radius),
                theta1=0,
                theta2=180,
                color="lime"
            )
            self.axes.add_patch(patch1)
            patch2 = patches.Arc(
                self.main_coordinates['p2'],
                float(4 * self.radius),
                float(4 * self.radius),
                theta1=180,
                theta2=180 + math.degrees(self.main_parameters['theta']),
                color="lime"
            )
            self.axes.add_patch(patch2)
            patch3 = patches.Arc(
                self.main_coordinates['p1'],
                float(4 * self.radius),
                float(4 * self.radius),
                theta1=-(math.degrees(self.main_parameters['theta'])),
                theta2=0,
                color="lime"
            )
            self.axes.add_patch(patch3)
        else:
            raise Exception("3d is not supported yet")

    def is_holes_into_cut(self, method_name: str, x_hole, y_hole, x_air_hole, y_air_hole):
        pass
        if method_name.upper() == 'EBM':
            if math.sqrt((math.pow((x_hole - x_air_hole), 2) + math.pow((y_hole - y_air_hole),
                                                                        2))) < 1.5 * self.cut_length:
                return True
        elif method_name.upper() == 'NTNU':
            if math.sqrt((math.pow((x_hole - x_air_hole), 2) + math.pow((y_hole - y_air_hole),
                                                                        2))) < 1.5 * self.w4:
                return True
        elif method_name.upper() == 'OLAFSSON' or method_name.upper() == 'KONYA':
            if math.sqrt(
                    (math.pow((x_hole - x_air_hole), 2) + math.pow((y_hole - y_air_hole),
                                                                   2))) < 1.1 * self.last_x:
                return True
        else:
            raise Exception('Not method name match')

    def first_check_remaining_area(self, parameters):
        # parameters: horizontal_relative, vertical_relative
        # cut_length, advance_burden
        # x_air_hole, y_air_hole
        # advance_hole_properties
        # tolerant_length_of_cut = None
        # if "Olafsson" not in self.method:
        #     tolerant_length_of_cut = 1.5
        # else:
        #     tolerant_length_of_cut = 1.1
        if parameters['horizontal_relative'] < 2 and parameters['vertical_relative'] < 2:
            return False
        elif parameters['horizontal_relative'] >= 2 and parameters['vertical_relative'] < 2:
            # fill the white spaces
            y0 = 0
            x0 = self.radius
            delta_x = self.radius - (parameters['cut_length'] / 2)
            number_of_col_could_have_in_white_space = round(delta_x / parameters['advance_burden']) - 1
            offset_in_col = delta_x / number_of_col_could_have_in_white_space
            if number_of_col_could_have_in_white_space == 1:
                offset_in_col = delta_x / 2
            for col in range(1, (number_of_col_could_have_in_white_space + 1)):
                x_temp = x0 - (col * offset_in_col)
                if abs(x_temp) >= self.radius:
                    break
                y_in_circle_minus_center = math.sqrt(math.pow(self.radius, 2) - math.pow(x_temp, 2))
                y_in_circle = y_in_circle_minus_center + self.height
                delta_y_temp = y_in_circle - y0
                number_of_row_could_have_in_this_col = round(delta_y_temp / parameters['advance_burden']) - 1
                if number_of_row_could_have_in_this_col == 0:
                    continue
                offset_row_in_this_col = delta_y_temp / number_of_row_could_have_in_this_col
                if number_of_row_could_have_in_this_col == 1:
                    offset_row_in_this_col = delta_y_temp / 2
                for row in range(1, (number_of_row_could_have_in_this_col + 1)):
                    y_temp = y0 + (row * offset_row_in_this_col)
                    if y_temp >= y_in_circle:
                        continue
                    if self.is_holes_into_cut(self.method, x_temp, y_temp, parameters['x_air_hole'],
                                              parameters['y_air_hole']):
                        continue
                    self.add_hole(
                        x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
                    self.add_hole(
                        -x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
            return False
        elif parameters['vertical_relative'] >= 2 and parameters['horizontal_relative'] < 2:
            # fill the white spaces
            y0 = parameters['advance_burden']
            x0 = self.radius
            delta_x = 2 * self.radius
            number_of_col_could_have_in_white_space = round(delta_x / parameters['advance_burden']) - 1
            offset_in_col = delta_x / number_of_col_could_have_in_white_space
            if number_of_col_could_have_in_white_space == 1:
                offset_in_col = delta_x / 2
            for col in range(1, (number_of_col_could_have_in_white_space + 1)):
                x_temp = x0 - (col * offset_in_col)
                if abs(x_temp) >= self.radius:
                    break
                y_in_circle_minus_center = math.sqrt(math.pow(self.radius, 2) - math.pow(x_temp, 2))
                y_in_circle = y_in_circle_minus_center + self.height
                delta_y_temp = y_in_circle - y0

                number_of_row_could_have_in_this_col = round(delta_y_temp / parameters['advance_burden']) - 1
                if number_of_row_could_have_in_this_col == 0:
                    continue
                offset_in_this_col = delta_y_temp / number_of_row_could_have_in_this_col
                if number_of_row_could_have_in_this_col == 1:
                    offset_in_this_col = delta_y_temp / 2
                for row in range(1, (number_of_row_could_have_in_this_col + 1)):
                    y_temp = y0 + (row * offset_in_this_col)
                    if y_temp >= y_in_circle:
                        continue
                    if self.is_holes_into_cut(self.method, x_temp, y_temp, parameters['x_air_hole'],
                                              parameters['y_air_hole']):
                        continue
                    self.add_hole(
                        x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
            return False
        elif parameters['horizontal_relative'] == 2 and parameters['vertical_relative'] == 2:
            r = self.radius / 2
            length_of_crown = r * np.pi
            number_of_holes_in_this_crown = round(length_of_crown / parameters['advance_burden'])
            thetas_in_this_crown = np.linspace(0, np.pi, number_of_holes_in_this_crown)
            for theta in thetas_in_this_crown:
                x_hole = r * np.cos(theta)
                y_hole = r * np.sin(theta) + self.height
                if self.is_holes_into_cut(self.method, x_hole, y_hole, parameters['x_air_hole'],
                                          parameters['y_air_hole']):
                    continue
                self.add_hole(x_hole, y_hole, parameters['advance_hole_properties'])
            return False
        elif parameters['horizontal_relative'] == 2 and parameters['vertical_relative'] > 2:
            # fill the white spaces
            latest_x = self.radius
            y0 = 0
            x0 = self.radius
            delta_x = self.radius - (parameters['cut_length'] / 2)
            number_of_col_could_have_in_white_space = round(delta_x / parameters['advance_burden']) - 1
            offset_in_col = delta_x / number_of_col_could_have_in_white_space
            if number_of_col_could_have_in_white_space == 1:
                offset_in_col = delta_x / 2
            for col in range(1, (number_of_col_could_have_in_white_space + 1)):
                x_temp = x0 - (col * offset_in_col)
                if abs(x_temp) >= self.radius:
                    break
                y_in_circle_minus_center = math.sqrt(math.pow(self.radius, 2) - math.pow(x_temp, 2))
                y_in_circle = y_in_circle_minus_center + self.height
                delta_y_temp = y_in_circle - y0
                number_of_row_could_have_in_this_col = round(delta_y_temp / parameters['advance_burden']) - 1
                if number_of_row_could_have_in_this_col == 0:
                    continue
                offset_row_in_this_col = delta_y_temp / number_of_row_could_have_in_this_col
                if number_of_row_could_have_in_this_col == 1:
                    offset_row_in_this_col = delta_y_temp / 2
                for row in range(1, (number_of_row_could_have_in_this_col + 1)):
                    y_temp = y0 + (row * offset_row_in_this_col)
                    if y_temp >= y_in_circle:
                        continue
                    if self.is_holes_into_cut(self.method, x_temp, y_temp, parameters['x_air_hole'],
                                              parameters['y_air_hole']):
                        continue
                    self.add_hole(
                        x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
                    self.add_hole(
                        -x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
                    latest_x = x_temp

            # Fill the vertical
            y0 = parameters['advance_burden']
            x0 = latest_x
            delta_x = 2 * latest_x
            number_of_col_could_have_in_white_space = round(delta_x / parameters['advance_burden']) - 1
            offset_in_col = delta_x / number_of_col_could_have_in_white_space
            if number_of_col_could_have_in_white_space == 1:
                offset_in_col = delta_x / 2
            for col in range(1, (number_of_col_could_have_in_white_space + 1)):
                x_temp = x0 - (col * offset_in_col)
                if abs(x_temp) >= self.radius or abs(x_temp) >= latest_x:
                    break
                y_in_circle_minus_center = math.sqrt(math.pow(self.radius, 2) - math.pow(x_temp, 2))
                y_in_circle = y_in_circle_minus_center + self.height
                delta_y_temp = y_in_circle - y0

                number_of_row_could_have_in_this_col = round(delta_y_temp / parameters['advance_burden']) - 1
                if number_of_row_could_have_in_this_col == 0:
                    continue
                offset_in_this_col = delta_y_temp / number_of_row_could_have_in_this_col
                if number_of_row_could_have_in_this_col == 1:
                    offset_in_this_col = delta_y_temp / 2
                for row in range(1, (number_of_row_could_have_in_this_col + 1)):
                    y_temp = y0 + (row * offset_in_this_col)
                    if y_temp >= y_in_circle:
                        continue
                    if self.is_holes_into_cut(self.method, x_temp, y_temp, parameters['x_air_hole'],
                                              parameters['y_air_hole']):
                        continue
                    self.add_hole(
                        x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
            return False
        elif parameters['vertical_relative'] == 2 and parameters['horizontal_relative'] > 2:
            # fill the white spaces
            y0 = parameters['advance_burden']
            x0 = self.radius
            delta_x = 2 * self.radius
            number_of_col_could_have_in_white_space = round(delta_x / parameters['advance_burden']) - 1
            offset_in_col = delta_x / number_of_col_could_have_in_white_space
            if number_of_col_could_have_in_white_space == 1:
                offset_in_col = delta_x / 2
            for col in range(1, (number_of_col_could_have_in_white_space + 1)):
                x_temp = x0 - (col * offset_in_col)
                if abs(x_temp) >= self.radius:
                    break
                y_in_circle_minus_center = math.sqrt(math.pow(self.radius, 2) - math.pow(x_temp, 2))
                y_in_circle = y_in_circle_minus_center + self.height
                delta_y_temp = y_in_circle - y0

                number_of_row_could_have_in_this_col = round(delta_y_temp / parameters['advance_burden']) - 1
                if number_of_row_could_have_in_this_col == 0:
                    continue
                offset_in_this_col = delta_y_temp / number_of_row_could_have_in_this_col
                if number_of_row_could_have_in_this_col == 1:
                    offset_in_this_col = delta_y_temp / 2
                for row in range(1, (number_of_row_could_have_in_this_col + 1)):
                    y_temp = y0 + (row * offset_in_this_col)
                    if y_temp >= y_in_circle:
                        continue
                    if self.is_holes_into_cut(self.method, x_temp, y_temp, parameters['x_air_hole'],
                                              parameters['y_air_hole']):
                        continue
                    self.add_hole(
                        x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
            # Fill the horizontal
            y0 = 0
            x0 = self.radius
            delta_x = self.radius - (parameters['cut_length'] / 2)
            number_of_col_could_have_in_white_space = round(delta_x / parameters['advance_burden']) - 1
            offset_in_col = delta_x / number_of_col_could_have_in_white_space
            if number_of_col_could_have_in_white_space == 1:
                offset_in_col = delta_x / 2
            for col in range(1, (number_of_col_could_have_in_white_space + 1)):
                x_temp = x0 - (col * offset_in_col)
                if abs(x_temp) >= self.radius:
                    break
                # y_in_circle_minus_center = math.sqrt(math.pow(self.radius, 2) - math.pow(x_temp, 2))
                # y_in_circle = y_in_circle_minus_center + self.height
                delta_y_temp = self.height - y0
                number_of_row_could_have_in_this_col = round(delta_y_temp / parameters['advance_burden']) - 1
                if number_of_row_could_have_in_this_col == 0:
                    continue
                offset_row_in_this_col = delta_y_temp / number_of_row_could_have_in_this_col
                if number_of_row_could_have_in_this_col == 1:
                    offset_row_in_this_col = delta_y_temp / 2
                for row in range(1, (number_of_row_could_have_in_this_col + 1)):
                    y_temp = y0 + (row * offset_row_in_this_col)
                    if y_temp >= self.height:
                        continue
                    if self.is_holes_into_cut(self.method, x_temp, y_temp, parameters['x_air_hole'],
                                              parameters['y_air_hole']):
                        continue
                    self.add_hole(
                        x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
                    self.add_hole(
                        -x_temp,
                        y_temp,
                        parameters['advance_hole_properties'],
                    )
            return False
        else:
            return True

    def draw_primitive_of_shape(self, options):
        # Draw roof
        # roof_spacing, wall_spacing, floor_spacing
        # perimeter_hole_properties, floor_hole_properties
        number_of_holes_in_main_crown = round(
            (np.pi * self.radius) / options['roof_spacing']
        ) + 1
        thetas_in_main_crown = np.linspace(0, np.pi, number_of_holes_in_main_crown)
        for theta in thetas_in_main_crown:
            if (self.height / self.advance_burden) < 0.5:
                if theta == 0. or theta == np.pi:
                    continue
            x_hole = self.radius * np.cos(theta)
            y_hole = self.radius * np.sin(theta) + self.height
            self.add_hole(x_hole, y_hole, options['perimeter_hole_properties'])

        # Floor holes
        number_of_holes_in_floor = round((2 * abs(self.main_parameters['x1'])) / options['floor_spacing']) + 1
        x_holes_in_floor = np.linspace(
            self.main_parameters['x1'], abs(self.main_parameters['x1']), number_of_holes_in_floor
        )
        for x in x_holes_in_floor:
            y_hole = 0
            self.add_hole(x, y_hole, options['floor_hole_properties'])

        # Wall holes
        length_of_main_arcs = (2 * self.radius) * self.main_parameters['theta']
        number_of_holes_in_wall_crowns = round(length_of_main_arcs / options['wall_spacing']) + 1
        thetas_in_main_arcs = np.linspace(np.pi, (np.pi + self.main_parameters['theta']),
                                          number_of_holes_in_wall_crowns)[1:-1]
        for theta in thetas_in_main_arcs:
            x_left = (2 * self.radius) * np.cos(theta) + self.main_coordinates['p2'][0]
            y_left = (2 * self.radius) * np.sin(theta) + self.main_coordinates['p2'][1]
            x_right = -1 * x_left
            y_right = y_left
            self.add_hole(x_left, y_left, options['perimeter_hole_properties'])
            self.add_hole(x_right, y_right, options['perimeter_hole_properties'])

    def draw_secondary_of_shape(self, method_name: str, options):
        # perimeter_burden, advance_burden, floor_burden, advance_spacing
        # advance_hole_properties,
        # spacing_first_ring ,buffer_hole_properties -- > NTNU
        # center of cut: x_air_hole, y_air_hole
        advance_spacing = None
        hole_properties = None
        if method_name.upper() == 'NTNU':
            advance_spacing = options['spacing_first_ring']
            hole_properties = options['buffer_hole_properties']
        else:
            advance_spacing = options['advance_spacing']
            hole_properties = options['advance_hole_properties']

        first_curve_next_to_main_crown_radius = (self.radius - options['perimeter_burden'])
        number_of_holes_in_first_curve_next_to_main_crown = round(
            np.pi * first_curve_next_to_main_crown_radius / advance_spacing
        )
        first_curve_next_to_main_crown_theta = np.linspace(
            0, np.pi, number_of_holes_in_first_curve_next_to_main_crown
        )
        for theta in first_curve_next_to_main_crown_theta:
            if (self.height / options['advance_burden']) < 0.5:
                if theta == 0. or theta == np.pi:
                    continue
            x_hole = first_curve_next_to_main_crown_radius * np.cos(theta)
            y_hole = first_curve_next_to_main_crown_radius * np.sin(theta) + self.height
            if self.is_holes_into_cut(method_name, x_hole, y_hole, options['x_air_hole'],
                                      options['y_air_hole']):
                continue
            self.add_hole(x_hole, y_hole, hole_properties)

        # draw left and right arcs
        R_arc = (2 * self.radius) - options['perimeter_burden']
        ring_height = self.height - options['floor_burden']
        first_ring_theta = math.asin(ring_height / R_arc)
        length_of_arc_first_ring = first_ring_theta * R_arc

        number_of_holes_wall_crown = (
                round(length_of_arc_first_ring / advance_spacing) + 1
        )
        thetas_in_first_ring_arc = np.linspace(np.pi, (np.pi + first_ring_theta), number_of_holes_wall_crown)[1:]
        for theta in thetas_in_first_ring_arc:
            x_left_temp = R_arc * np.cos(theta) + self.main_coordinates['p2'][0]
            y_left_temp = R_arc * np.sin(theta) + self.main_coordinates['p2'][1]
            # symmetry to the x-axis
            x_right_temp = -1 * x_left_temp
            y_right_temp = y_left_temp
            if not self.is_holes_into_cut(method_name, x_left_temp, y_left_temp, options['x_air_hole'],
                                          options['y_air_hole']):
                self.add_hole(x_left_temp, y_left_temp, hole_properties)
            if not self.is_holes_into_cut(method_name, x_right_temp, y_right_temp, options['x_air_hole'],
                                          options['y_air_hole']):
                self.add_hole(x_right_temp, y_right_temp, hole_properties)

    def draw_rest_of_shape(self, options):
        # options:
        # distance_between_cut_and_first_curve_in_horizontal,
        # distance_between_cut_and_first_curve_in_vertical
        # advance_burden, perimeter_burden, advance_spacing,
        # x_air_hole, y_air_hole, cut_length, tolerant_radius
        # difference_height
        # advance_hole_properties

        minimum_distance = min(options['distance_between_cut_and_first_curve_in_horizontal'],
                               options['distance_between_cut_and_first_curve_in_vertical'])

        number_of_curve_could_have = round(minimum_distance / options['advance_burden']) - 1
        # print("num of curves: ", round(minimum_distance / options['advance_burden']))
        if number_of_curve_could_have == 0:
            number_of_curve_could_have += 1
        burden_between_curves = minimum_distance / number_of_curve_could_have

        # Curve holes
        number_of_curves = 0
        smallest_radius = 0

        # crown of other
        for curve_number in range(1, (number_of_curve_could_have + 1)):
            curve_radius = (
                    self.radius
                    - options['perimeter_burden']
                    - (curve_number * burden_between_curves)
            )
            number_of_holes_curve = round(
                np.pi * curve_radius / options['advance_spacing']
            ) + 1
            thetas = np.linspace(0, np.pi, number_of_holes_curve)
            for theta in thetas:
                if (self.height / options['advance_burden']) < 0.5:
                    if theta == 0. or theta == np.pi:
                        continue
                x_hole = curve_radius * np.cos(theta)
                y_hole = self.height + curve_radius * np.sin(theta)
                if self.is_holes_into_cut(self.method, x_hole, y_hole, options['x_air_hole'],
                                          options['y_air_hole']):
                    continue
                self.add_hole(x_hole, y_hole, options['advance_hole_properties'])
            number_of_curves += 1
            smallest_radius = curve_radius

        # fill the side crown
        # calculating curve of side
        R_last_arc = 0
        if options['difference_height'] > 0:
            # draw first arc
            r1 = (2 * self.radius) - options['perimeter_burden'] - burden_between_curves
            theta_1 = math.asin(options['difference_height'] / r1)
            length_of_curve_1_in_horizontal = r1 * theta_1
            number_of_holes_in_curve_1_in_horizontal = round(
                length_of_curve_1_in_horizontal / options['advance_spacing']) + 1
            thetas_in_curve_1 = np.linspace(-theta_1, 0, number_of_holes_in_curve_1_in_horizontal)
            for t in thetas_in_curve_1:
                if t == 0.:
                    continue
                x_hole = r1 * np.cos(t) - self.radius
                y_hole = r1 * np.sin(t) + self.height
                if self.is_holes_into_cut(self.method, x_hole, y_hole, options['x_air_hole'],
                                          options['y_air_hole']):
                    continue
                self.add_hole(x_hole, y_hole, options['advance_hole_properties'])
                self.add_hole(-x_hole, y_hole, options['advance_hole_properties'])
                R_last_arc = r1
            for i in range(1, number_of_curve_could_have):
                r_i = r1 - (i * burden_between_curves)
                theta_i = math.asin(options['difference_height'] / r_i)
                length_of_curve_i_in_horizontal = r_i * theta_i
                number_of_holes_in_curve_i_in_horizontal = round(length_of_curve_i_in_horizontal
                                                                 / options['advance_spacing']) + 1
                thetas_in_curve_i = np.linspace(-theta_i, 0, number_of_holes_in_curve_i_in_horizontal)
                for t in thetas_in_curve_i:
                    if t == 0.:
                        continue
                    x_hole = r_i * np.cos(t) - self.radius
                    y_hole = r_i * np.sin(t) + self.height
                    if self.is_holes_into_cut(self.method, x_hole, y_hole, options['x_air_hole'],
                                              options['y_air_hole']):
                        continue
                    self.add_hole(x_hole, y_hole, options['advance_hole_properties'])
                    self.add_hole(-x_hole, y_hole, options['advance_hole_properties'])
                    R_last_arc = r_i
        return smallest_radius, R_last_arc

    def fill_the_white_spaces(self, options):
        # options:
        # smallest_radius, R_last_arc
        # advance_burden, perimeter_burden, advance_spacing,
        # x_air_hole, y_air_hole, cut_length, tolerant_radius
        # difference_height
        # advance_hole_properties
        y0_lower_bound = options['advance_burden'] + options['cut_length']
        #         # print(y0)
        x0 = options['smallest_radius']
        #         # print(x0)
        delta_x = 2 * options['smallest_radius']
        number_of_col_could_have_in_white_space = round(delta_x / options['advance_burden']) - 1
        if number_of_col_could_have_in_white_space != 0:
            offset_in_col = None
            offset_in_row = None
            if number_of_col_could_have_in_white_space == 1:
                offset_in_col = delta_x / 2
            else:
                offset_in_col = delta_x / number_of_col_could_have_in_white_space
            for col in range(1, (number_of_col_could_have_in_white_space + 1)):
                delta_y_temp = (self.height + options['smallest_radius']) - y0_lower_bound
                number_of_row_could_have_in_white_space_temp = round(delta_y_temp / options['advance_burden']) - 1
                if number_of_row_could_have_in_white_space_temp == 0:
                    continue

                if number_of_row_could_have_in_white_space_temp == 1:
                    offset_in_row = delta_y_temp / 2
                else:
                    offset_in_row = delta_y_temp / number_of_row_could_have_in_white_space_temp

                x_temp = x0 - (col * offset_in_col)
                if x_temp <= (-options['smallest_radius']):
                    break
                y_circle_top = math.sqrt(math.pow(options['smallest_radius'], 2) - math.pow(x_temp, 2)) + self.height
                x_center_of_arc = None
                y_center_of_arc = self.height
                if x_temp > 0:
                    x_center_of_arc = -self.radius
                else:
                    x_center_of_arc = abs(self.radius)
                y0_minus_center = math.sqrt(
                    math.pow(options['R_last_arc'], 2) - math.pow((x_temp - x_center_of_arc), 2))
                y0 = y_center_of_arc - y0_minus_center
                if y0 <= y0_lower_bound:
                    y0 = y0_lower_bound
                delta_y = y_circle_top - y0

                number_of_row_could_have_in_white_space = round(delta_y / options['advance_burden']) - 1
                offset_in_row = delta_y / number_of_row_could_have_in_white_space
                for row in range(1, (number_of_row_could_have_in_white_space + 1)):
                    y_temp = y0 + (row * offset_in_row)
                    if y_temp >= y_circle_top:
                        continue
                    self.add_hole(
                        x_temp,
                        y_temp,
                        options['advance_hole_properties'],
                    )

    def draw_plot_ebm(self):
        self.draw()
        self.draw_cut_ebm()
        # center of cut
        x_air_hole = 0
        y_air_hole = self.advance_burden + (self.cut_length / 2)

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_spacing,
        )
        # Advance
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        # primitive shape holes
        options = dict(roof_spacing=self.perimeter_spacing,
                       floor_spacing=self.advance_spacing,
                       wall_spacing=self.perimeter_spacing,
                       perimeter_hole_properties=perimeter_hole_properties,
                       floor_hole_properties=advance_hole_properties,
                       )
        self.draw_primitive_of_shape(options)

        # At first, we need to check w/b and h/b
        # w = distance_from_cut_to_perimeter
        # h = distance from cut to main crown
        # (w/b, h/b) == 0 or 1 return and does not continue
        # (w/b, h/b) == 2: create middle of cut and wall
        # (w/b, h/b) > 2: continue
        x_avg_point_in_wall_crown = (self.main_coordinates['p2'][0] + self.main_coordinates['p3'][0]) / 2
        _w = x_avg_point_in_wall_crown - (self.cut_length / 2)
        _h = (self.height + self.radius) - (self.cut_length + self.advance_burden)
        horizontal_relative = round(_w / self.advance_burden)
        # print("w/b: ", horizontal_relative)
        vertical_relative = round(_h / self.advance_burden)
        # print("h/b: ", vertical_relative)
        parameters = {
            'horizontal_relative': horizontal_relative,
            'vertical_relative': vertical_relative,
            'x_air_hole': x_air_hole,
            'y_air_hole': y_air_hole,
            'advance_burden': self.advance_burden,
            'cut_length': self.cut_length,
            'advance_hole_properties': advance_hole_properties
        }
        if self.first_check_remaining_area(parameters) is False:
            self.final_draw()
            return
        else:
            # start of advance
            options = dict(
                perimeter_burden=self.perimeter_burden,
                floor_burden=self.advance_burden,
                advance_burden=self.advance_burden,
                advance_spacing=self.advance_spacing,
                advance_hole_properties=advance_hole_properties,
                x_air_hole=x_air_hole,
                y_air_hole=y_air_hole
            )
            self.draw_secondary_of_shape('ebm', options)
            # other crown after first crown next to perimeter
            # x_avg_point_in_wall_crown = (self.main_coordinates['p2'][0] + self.main_coordinates['p3'][0]) / 2
            x_minus_x_center = math.sqrt(
                math.pow(((2 * self.radius) - self.perimeter_burden), 2) - math.pow((y_air_hole - self.height), 2))
            x_avg_point_in_wall_crown = x_minus_x_center + (-1 * self.radius)

            distance_between_cut_and_first_curve_in_horizontal = x_avg_point_in_wall_crown - \
                                                                 (self.cut_length / 2) - self.perimeter_burden

            distance_between_cut_and_first_curve_in_vertical = (self.height + self.radius) - \
                                                               (self.advance_burden + self.cut_length
                                                                + self.perimeter_burden)
            options = {
                'distance_between_cut_and_first_curve_in_horizontal':
                    distance_between_cut_and_first_curve_in_horizontal,
                'distance_between_cut_and_first_curve_in_vertical':
                    distance_between_cut_and_first_curve_in_vertical,
                'advance_burden': self.advance_burden,
                'perimeter_burden': self.perimeter_burden,
                'x_air_hole': x_air_hole,
                'y_air_hole': y_air_hole,
                'cut_length': self.cut_length,
                'advance_spacing': self.advance_spacing,
                'difference_height': (self.height - self.advance_burden),
                'advance_hole_properties': advance_hole_properties
            }
            smallest_radius, R_last_arc = self.draw_rest_of_shape(options)
            # print("**********: ", R_last_arc)
            # White space holes
            options = {
                'advance_burden': self.advance_burden,
                'perimeter_burden': self.perimeter_burden,
                'cut_length': self.cut_length,
                'advance_spacing': self.advance_spacing,
                'difference_height': (self.height - self.advance_burden),
                'smallest_radius': smallest_radius,
                'R_last_arc': R_last_arc,
                'advance_hole_properties': advance_hole_properties
            }
            self.fill_the_white_spaces(options)

            self.final_draw()

    def draw_cut_ebm(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        self.add_hole(
            0, self.advance_burden + self.cut_length / 2, air_hole_properties
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )

        self.add_hole(
            0.15,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(
            0,
            self.advance_burden + self.cut_length / 2 + 0.18,
            cut_hole_properties,
        )
        self.add_hole(
            -0.22,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(
            0,
            self.advance_burden + self.cut_length / 2 - 0.25,
            cut_hole_properties,
        )
        self.add_hole(
            -self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            -self.cut_length / 2, self.advance_burden, cut_hole_properties
        )
        self.add_hole(
            self.cut_length / 2, self.advance_burden, cut_hole_properties
        )

        patch = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.sending_patch_cut = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.axes.add_patch(
            patch
        )
        if not self.is_2d:
            raise Exception("3d is not supported")

    def draw_plot_ntnu(self):
        self.draw()

        # center of cut
        x_air_hole = 0
        y_air_hole = self.burden_floor + self.w4 / 2

        parameters = self.get_parameters_ntnu()
        burden_perimeter = parameters.get("burden_perimeter")
        spacing_perimeter = parameters.get("spacing_perimeter")
        burden_first_ring = parameters.get("burden_first_ring")
        spacing_first_ring = parameters.get("spacing_first_ring")
        burden_floor = parameters.get("burden_floor")
        spacing_floor = parameters.get("spacing_floor")
        burden_advance = parameters.get("burden_advance")
        spacing_advance = parameters.get("spacing_advance")

        # make required parameters
        self.advance_burden = burden_advance

        self.draw_cut_ntnu()
        # Crown holes
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_perimeter,
        )

        # Floor holes
        floor_hole_properties = dict(
            hole_type="Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        options = dict(roof_spacing=spacing_perimeter,
                       floor_spacing=spacing_floor,
                       wall_spacing=spacing_perimeter,
                       perimeter_hole_properties=perimeter_hole_properties,
                       floor_hole_properties=floor_hole_properties,
                       )
        self.draw_primitive_of_shape(options)
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        buffer_hole_properties = dict(
            hole_type="Buffer",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # At first, we need to check w/b and h/b
        # w = distance_from_cut_to_perimeter
        # h = distance from cut to main crown
        # (w/b, h/b) == 0 or 1 return and does not continue
        # (w/b, h/b) == 2: create middle of cut and wall
        # (w/b, h/b) > 2: continue
        x_avg_point_in_wall_crown = (self.main_coordinates['p2'][0] + self.main_coordinates['p3'][0]) / 2
        _w = x_avg_point_in_wall_crown - (self.w4 / 2)
        _h = (self.height + self.radius) - (self.w4 + burden_advance)
        horizontal_relative = round(_w / burden_advance)
        # print("w/b: ", horizontal_relative)
        vertical_relative = round(_h / burden_advance)
        # print("h/b: ", vertical_relative)
        parameters = {
            'horizontal_relative': horizontal_relative,
            'vertical_relative': vertical_relative,
            'x_air_hole': x_air_hole,
            'y_air_hole': y_air_hole,
            'cut_length': self.w4,
            'advance_burden': burden_advance,
            'advance_hole_properties': advance_hole_properties
        }
        if self.first_check_remaining_area(parameters) is False:
            self.final_draw()
            return
        else:
            # fill the inner crown
            # First ring
            # advance_hole_properties = dict(
            #     hole_type="Advance",
            #     hole_length=self.hole_length,
            #     hole_diameter=self.hole_diameter,
            #     explosive_type=self.explosive_type,
            # )
            #
            # buffer_hole_properties = dict(
            #     hole_type="Buffer",
            #     hole_length=self.hole_length,
            #     hole_diameter=self.hole_diameter,
            #     explosive_type=self.explosive_type,
            # )
            first_ring_radius = self.radius - burden_perimeter
            ring_height = self.height - burden_floor
            options = dict(
                perimeter_burden=burden_perimeter,
                floor_burden=burden_floor,
                advance_burden=burden_advance,
                spacing_first_ring=spacing_first_ring,
                buffer_hole_properties=buffer_hole_properties,
                x_air_hole=x_air_hole,
                y_air_hole=y_air_hole
            )
            self.draw_secondary_of_shape('ntnu', options)

            # Inner rings next to first ring
            next_to_first_ring_radius = first_ring_radius - burden_first_ring
            next_to_first_ring_R_arc = (2 * self.radius) - burden_perimeter - burden_first_ring
            next_to_first_ring_theta = math.asin(ring_height / next_to_first_ring_R_arc)

            next_to_first_ring_length = next_to_first_ring_radius * np.pi
            number_of_holes_next_to_first_ring = round(next_to_first_ring_length / spacing_advance) + 1
            thetas_in_next_to_first_ring = np.linspace(0, np.pi, number_of_holes_next_to_first_ring)
            for theta in thetas_in_next_to_first_ring:
                if (self.height / burden_advance) < 0.5:
                    if theta == 0. or theta == np.pi:
                        continue
                x_temp = next_to_first_ring_radius * np.cos(theta)
                y_temp = next_to_first_ring_radius * np.sin(theta) + self.height
                self.add_hole(x_temp, y_temp, advance_hole_properties)

            # draw left and right arcs in interior next to first ring
            next_to_first_ring_length_of_arc = next_to_first_ring_theta * next_to_first_ring_R_arc
            next_to_first_ring_number_of_holes_interior_wall_crown = (
                    round(next_to_first_ring_length_of_arc / spacing_advance) + 1
            )
            next_to_first_ring_thetas_in_arc = np.linspace(np.pi, (np.pi + next_to_first_ring_theta),
                                                           next_to_first_ring_number_of_holes_interior_wall_crown)
            for theta in next_to_first_ring_thetas_in_arc:
                if theta == np.pi:
                    continue
                x_left_temp = next_to_first_ring_R_arc * np.cos(theta) + self.main_coordinates['p2'][0]
                y_left_temp = next_to_first_ring_R_arc * np.sin(theta) + self.main_coordinates['p2'][1]
                # symmetry to the x-axis
                x_right_temp = -1 * x_left_temp
                y_right_temp = y_left_temp
                self.add_hole(x_left_temp, y_left_temp, advance_hole_properties)
                self.add_hole(x_right_temp, y_right_temp, advance_hole_properties)

            # Other advance is the same
            # fill the others is the same
            # x_avg_point_in_wall_crown = (self.main_coordinates['p2'][0] + self.main_coordinates['p3'][0]) / 2
            x_minus_x_center = math.sqrt(
                math.pow(((2 * self.radius) - burden_perimeter - burden_first_ring), 2) - math.pow(
                    (y_air_hole - self.height), 2))
            x_avg_point_in_wall_crown = x_minus_x_center + (-1 * self.radius)
            distance_between_cut_and_ring_in_horizontal = x_avg_point_in_wall_crown - \
                                                          (self.w4 / 2)

            distance_between_cut_and_ring_in_vertical = (self.height + self.radius) - \
                                                        (burden_perimeter + burden_first_ring + burden_floor
                                                         + self.w4)
            options = {
                'distance_between_cut_and_first_curve_in_horizontal': distance_between_cut_and_ring_in_horizontal,
                'distance_between_cut_and_first_curve_in_vertical': distance_between_cut_and_ring_in_vertical,
                'advance_burden': burden_advance,
                'perimeter_burden': (burden_perimeter + burden_first_ring),
                'x_air_hole': x_air_hole,
                'y_air_hole': y_air_hole,
                'cut_length': self.w4,
                'advance_spacing': spacing_advance,
                'difference_height': (self.height - burden_floor),
                'advance_hole_properties': advance_hole_properties
            }
            smallest_radius, R_last_arc = self.draw_rest_of_shape(options)
            # fill the white spaces
            options = {
                'advance_burden': burden_floor,
                'perimeter_burden': burden_perimeter,
                'cut_length': self.w4,
                'advance_spacing': spacing_advance,
                'smallest_radius': smallest_radius,
                'R_last_arc': R_last_arc,
                'advance_hole_properties': advance_hole_properties
            }
            self.fill_the_white_spaces(options)
            # y0 = burden_floor + self.w4
            # x0 = smallest_radius
            # delta_x = 2 * smallest_radius
            # number_of_col_could_have_in_white_space = round(delta_x / burden_advance) - 1
            # delta_y = (self.height + smallest_radius) - y0
            # number_of_row_could_have_in_white_space = round(delta_y / burden_advance)
            # offset_in_col = delta_y / (
            #     number_of_row_could_have_in_white_space if number_of_row_could_have_in_white_space != 0 else 1)
            # offset_in_row = delta_x / number_of_col_could_have_in_white_space
            #
            # for col in range(1, number_of_col_could_have_in_white_space):
            #     x_temp = x0 - (col * offset_in_row)
            #     for row in range(1, number_of_row_could_have_in_white_space):
            #         y_temp = y0 + (row * offset_in_col)
            #         self.add_hole(
            #             x_temp,
            #             y_temp,
            #             advance_hole_properties,
            #         )

            self.final_draw()

    def draw_cut_ntnu(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        x_air_hole = 0
        y_air_hole = self.burden_floor + self.w4 / 2

        self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_cut,
        )

        self.add_hole(x_air_hole - self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b1, cut_hole_properties)
        self.add_hole(x_air_hole + self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b1, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(x_air_hole - self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b3, cut_hole_properties)
        self.add_hole(x_air_hole + self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b3, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )

    def draw_plot_olafsson(self):
        self.draw()
        x_air_hole, y_air_hole = self.draw_cut_olafsson()

        # Perimeter
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        # Floor holes
        floor_hole_properties = dict(
            hole_type="Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        options = dict(roof_spacing=self.roof_spacing,
                       floor_spacing=self.floor_spacing,
                       wall_spacing=self.wall_spacing,
                       perimeter_hole_properties=perimeter_hole_properties,
                       floor_hole_properties=floor_hole_properties,
                       )
        self.draw_primitive_of_shape(options)
        # Fill the advance holes
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        # At first, we need to check w/b and h/b
        # w = distance_from_cut_to_perimeter
        # h = distance from cut to main crown
        # (w/b, h/b) == 0 or 1 return and does not continue
        # (w/b, h/b) == 2: create middle of cut and wall
        # (w/b, h/b) > 2: continue
        x_avg_point_in_wall_crown = (self.main_coordinates['p2'][0] + self.main_coordinates['p3'][0]) / 2
        _w = x_avg_point_in_wall_crown - (self.last_x / 2)
        _h = (self.height + self.radius) - (self.last_x + self.wall_burden)
        horizontal_relative = round(_w / self.wall_burden)
        # print("w/b: ", horizontal_relative)
        vertical_relative = round(_h / self.wall_burden)
        # print("h/b: ", vertical_relative)
        parameters = {
            'horizontal_relative': horizontal_relative,
            'vertical_relative': vertical_relative,
            'x_air_hole': x_air_hole,
            'y_air_hole': y_air_hole,
            'cut_length': self.last_x,
            'advance_burden': self.wall_burden,
            'advance_hole_properties': advance_hole_properties
        }
        if self.first_check_remaining_area(parameters) is False:
            self.final_draw()
            return
        else:
            # draw first crown next to perimeter
            options = dict(
                perimeter_burden=self.wall_burden,
                floor_burden=self.floor_burden,
                advance_burden=self.advance_burden,
                advance_spacing=self.advance_spacing,
                advance_hole_properties=advance_hole_properties,
                x_air_hole=x_air_hole,
                y_air_hole=y_air_hole
            )
            self.draw_secondary_of_shape('olafsson', options)

            # Fill others
            # check minimum distance
            # x_avg_point_in_wall_crown = (self.main_coordinates['p2'][0] + self.main_coordinates['p3'][0]) / 2
            x_minus_x_center = math.sqrt(
                math.pow(((2 * self.radius) - self.wall_burden), 2) - math.pow((y_air_hole - self.height), 2))
            x_avg_point_in_wall_crown = x_minus_x_center + (-1 * self.radius)
            # print("last_x: ", self.last_x)
            # print("x_avg_point_in_wall_crown: ", x_avg_point_in_wall_crown)
            from_cut_to_wall_distance = x_avg_point_in_wall_crown - (self.last_x + self.wall_burden)

            from_cut_to_crown_distance = (self.height + self.radius) - (self.roof_burden + (self.last_x
                                                                                            + self.floor_burden))
            options = {
                'distance_between_cut_and_first_curve_in_horizontal': from_cut_to_wall_distance,
                'distance_between_cut_and_first_curve_in_vertical': from_cut_to_crown_distance,
                'advance_burden': self.advance_burden,
                'perimeter_burden': self.wall_burden,
                'x_air_hole': x_air_hole,
                'y_air_hole': y_air_hole,
                'cut_length': self.last_x,
                'advance_spacing': self.advance_spacing,
                'difference_height': (self.height - self.floor_burden),
                'advance_hole_properties': advance_hole_properties
            }
            smallest_radius, R_last_arc = self.draw_rest_of_shape(options)
            options = {
                'advance_burden': self.floor_burden,
                'cut_length': self.last_x,
                'advance_spacing': self.advance_spacing,
                'smallest_radius': smallest_radius,
                'R_last_arc': R_last_arc,
                'advance_hole_properties': advance_hole_properties
            }
            self.fill_the_white_spaces(options)

            self.final_draw()

    def draw_cut_olafsson(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        required_parameters_length = len(self.required_parameters)
        if required_parameters_length == 4:
            (
                first_section,
                second_section,
                third_section,
                fourth_section,
            ) = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section
            b3, _ = third_section
            _, x4 = fourth_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x4 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(x_air_hole - b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b3, cut_hole_properties)
            self.add_hole(x_air_hole + b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b3, cut_hole_properties)
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
        elif required_parameters_length == 3:
            (
                first_section,
                second_section,
                third_section,
            ) = self.required_parameters
            _, x1 = first_section
            b2, _ = second_section
            _, x3 = third_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x3 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )

            self.add_hole(
                x_air_hole,
                y_air_hole - b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole,
                y_air_hole + b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )

        elif required_parameters_length == 2:
            first_section, second_section = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x2 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )

        elif required_parameters_length == 1:
            first_section = self.required_parameters
            _, x1 = first_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x1 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
        return x_air_hole, y_air_hole

    def draw_plot_ebm_angular(self):
        self.draw()
        self.draw_cut_ebm_angular()

        self.final_draw()

    def draw_cut_ebm_angular(self):
        self.x_coordinates_property = {}
        if (
                0
                < self.height
                - self.advance_burden
                - (self.cut_rows - 1) * self.cut_rows_vertical_distance
                < self.advance_burden
        ):
            self.operational_burden_advance = (
                                                      self.height
                                                      - (self.cut_rows - 1) * self.cut_rows_vertical_distance
                                              ) / 2
        else:
            self.operational_burden_advance = self.advance_burden

        self.outer_cut_hole_positive_x = self.triangle_half_length

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )

        # points_in_cut = []
        for i in range(self.cut_rows):
            # reset counter for each row (i)
            self.x_coordinates_property[i] = []
            counter = 0
            self.add_hole(
                -self.outer_cut_hole_positive_x,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = self.outer_cut_hole_positive_x
            y = self.operational_burden_advance + i * self.cut_rows_vertical_distance
            point_in_cut_negative = {"x": -x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_negative)

            self.add_hole(
                self.outer_cut_hole_positive_x,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_positive)

            counter += 1

            self.add_hole(
                -self.outer_cut_hole_positive_x
                - self.cut_develop_front_length,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x_dev_front_negative = -self.outer_cut_hole_positive_x - self.cut_develop_front_length
            point_in_cut_negative_dev_front = {"x": x_dev_front_negative, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_negative_dev_front)

            self.add_hole(
                self.outer_cut_hole_positive_x + self.cut_develop_front_length,
                self.operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x_dev_front_positive = self.outer_cut_hole_positive_x + self.cut_develop_front_length
            point_in_cut_positive_dev_front = {"x": x_dev_front_positive, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_positive_dev_front)

            counter += 1
            self.last_x_index_side = counter

        # small triangle
        if self.hole_length / self.cut_burden > 1:
            if self.method == "EBM Angular":
                small_triangle_length = (
                        2
                        * self.triangle_half_length
                        * (self.hole_length - self.cut_burden)
                        / self.hole_length
                )
                for i in range(self.cut_rows):
                    self.add_hole(
                        small_triangle_length / 2,
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    x = small_triangle_length / 2
                    y = self.operational_burden_advance + i * self.cut_rows_vertical_distance
                    length = math.fabs(x)
                    point_in_cut_positive = {"x": x, "index": 0, "type": "small-triangle",
                                             "length": length, "y": y}
                    self.x_coordinates_property[i].append(point_in_cut_positive)

                    self.add_hole(
                        -small_triangle_length / 2,
                        self.operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        cut_hole_properties,
                    )
                    point_in_cut_negative = {"x": -x, "index": 0, "type": "small-triangle",
                                             "length": length, "y": y}
                    self.x_coordinates_property[i].append(point_in_cut_negative)
            elif (
                    self.method == "Gustafsson"
                    or self.method == "Olafsson Angular"
                    or self.method == "Konya Angular"
            ):
                number_of_smaller_triangles = round(
                    self.hole_length / self.cut_burden
                )
                step = self.triangle_half_length / number_of_smaller_triangles
                for i in range(1, number_of_smaller_triangles):
                    for j in range(self.cut_rows):
                        x_positive = 0 - i * step
                        length = math.fabs(x_positive)
                        y = self.operational_burden_advance + j * self.cut_rows_vertical_distance
                        self.add_hole(
                            x_positive,
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        point_in_cut_positive = {"x": x_positive, "index": 0, "type": "small-triangle",
                                                 "length": length, "y": y}
                        self.x_coordinates_property[j].append(point_in_cut_positive)

                        x_negative = 0 + i * step
                        length = math.fabs(x_negative)
                        self.add_hole(
                            x_negative,
                            self.operational_burden_advance
                            + j * self.cut_rows_vertical_distance,
                            cut_hole_properties,
                        )
                        point_in_cut_negative = {"x": x_negative, "index": 0, "type": "small-triangle",
                                                 "length": length, "y": y}
                        self.x_coordinates_property[j].append(point_in_cut_negative)
