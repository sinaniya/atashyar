import math

from matplotlib import patches
import numpy as np
from mpl_toolkits.mplot3d import art3d
from sympy import symbols, solveset

from .shape import Shape


class ArchedRoof1(Shape):
    def __init__(self, parent=None, properties=None):
        super().__init__(parent, properties)

        self.last_x_index_side = None
        self.x_coordinates_property = None
        self.number_of_remaining_holes = None
        self.first_theta = np.arccos(self.width / (2 * self.radius))
        self.alpha = np.pi - 2 * self.first_theta

        self.H = (
            self.height - self.radius + self.radius * np.sin(self.first_theta)
        )

    def is_point_near_edges(self, point):
        return (
            self.is_point_near_floor(point)
            or self.is_point_near_right_wall(point)
            or self.is_point_near_left_wall(point)
            or self.is_point_near_curve(point)
        )

    def is_point_near_floor(self, point):
        x, y = point
        return (-self.width / 2 < x < self.width / 2) and (
            abs(y) < self.ERROR_TOLERANCE
        )

    def is_point_near_right_wall(self, point):
        x, y = point
        return (abs(x - self.width / 2) < self.ERROR_TOLERANCE) and (
            0 < y < self.height
        )

    def is_point_near_left_wall(self, point):
        x, y = point
        return (abs(x + self.width / 2) < self.ERROR_TOLERANCE) and (
            0 < y < self.height
        )

    def is_point_near_curve(self, point):
        x, y = point
        return (
            (self.radius - self.ERROR_TOLERANCE) ** 2
            < x ** 2 + (y - (self.height - self.radius)) ** 2
            < (self.radius + self.ERROR_TOLERANCE) ** 2
        )

    def snap_point_near_edges(self, point):
        x, y = point
        if self.is_point_near_floor(point):
            return (x, 0)
        elif self.is_point_near_right_wall(point):
            return (self.width / 2, y)
        elif self.is_point_near_left_wall(point):
            return (-self.width / 2, y)
        elif self.is_point_near_curve(point):
            this_radius = math.sqrt(
                x ** 2 + (y - (self.height - self.radius)) ** 2
            )
            d = self.radius - this_radius
            theta = math.atan2(y, x)
            return (x + d * math.cos(theta), y + d * math.sin(theta))

    def is_point_in_shape(self, point):
        return self.is_point_inside_rectangle_part(
            point
        ) or self.is_point_inside_crown_part(point)

    def is_point_inside_rectangle_part(self, point):
        x, y = point
        return (
            -self.width / 2 + self.ERROR_TOLERANCE
            < x
            < self.width / 2 - self.ERROR_TOLERANCE
        ) and (
            self.ERROR_TOLERANCE
            < y
            <= self.height
            - self.radius
            + self.radius * math.sin(math.acos(self.width / (2 * self.radius)))
        )

    def is_point_inside_crown_part(self, point):
        x, y = point
        return (
            y
            > self.height
            - self.radius
            + self.radius * math.sin(math.acos(self.width / (2 * self.radius)))
        ) and (
            x ** 2 + (y - self.height + self.radius) ** 2
            < (self.radius - self.ERROR_TOLERANCE) ** 2
        )

    def is_point_inside_cut(self, point):
        return True

    def draw(self):
        self.first_theta = np.arccos(self.width / (2 * self.radius))
        self.H = self.height + self.radius * (np.sin(self.first_theta) - 1)
        self.draw_line([-self.width / 2, self.width / 2], [0, 0], color="lime")
        self.append_rounded_point([-self.width / 2, self.width / 2], [0, 0])

        self.draw_line(
            [-self.width / 2, -self.width / 2], [0, self.H], color="lime"
        )
        self.append_rounded_point(
            [-self.width / 2, -self.width / 2], [0, self.H]
        )

        self.draw_line(
            [self.width / 2, self.width / 2], [0, self.H], color="lime"
        )
        self.append_rounded_point(
            [self.width / 2, self.width / 2], [0, self.H]
        )

        self.draw_patch()

    def draw_patch(self):
        if self.is_2d:
            patch = patches.Arc(
                (0, self.height - self.radius),
                2 * self.radius,
                2 * self.radius,
                theta1=math.degrees(self.first_theta),
                theta2=math.degrees(np.pi - self.first_theta),
                color="lime",
            )
            self.sending_patch = patches.Arc(
                (0, self.height - self.radius),
                2 * self.radius,
                2 * self.radius,
                theta1=math.degrees(self.first_theta),
                theta2=math.degrees(np.pi - self.first_theta),
                color="lime",
            )
            self.axes.add_patch(patch)

        else:
            # create piece of circle
            r = self.radius
            y0 = self.height - self.radius  # To have the tangent at y=0
            x0 = 0.0

            theta1_degree = math.degrees(self.first_theta)
            theta2_degree = math.degrees(np.pi - self.first_theta)
            theta1_radian = math.radians(theta1_degree)
            theta2_radian = math.radians(theta2_degree)
            theta = np.linspace(
                theta1_radian - np.pi / 2, theta2_radian - np.pi / 2, 201
            )

            z = np.zeros_like(theta)  # z=0
            y = r * np.cos(theta) + y0  # y - y0 = r*cos(theta)
            x = r * np.sin(theta) + x0  # x - x0 = r*sin(theta)

            self.append_rounded_point(x, y)
            self.axes.plot(x, y, z, color="lime")

    def get_area(self):
        theta = math.asin(self.width / (2 * self.radius))
        h = self.height - self.radius * (1 - math.cos(theta))
        area = h * self.width + (
            ((math.pi * self.radius ** 2 * 2 * theta) / (2 * math.pi))
            - (((h + self.radius - self.height) * self.width) / 2)
        )
        return area

    def get_perimeter(self):
        return 2 * self.H + self.radius * self.alpha

    def get_perimeter_average_length(self):
        return 2 * self.height + math.pi * (
            self.radius - self.perimeter_burden / 2
        )

    def check_space(self):
        return self.width / 2 < self.perimeter_burden + 0.4

    def draw_plot_ebm(self):

        self.draw()

        self.draw_cut_ebm()

        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_stemming,
        )

        # Crown holes
        number_of_holes_burden_curve = round(
            self.radius * self.alpha / (self.perimeter_spacing)
        )
        burden_curve_theta = np.linspace(
            self.first_theta,
            np.pi - self.first_theta,
            number_of_holes_burden_curve,
        )
        for t in burden_curve_theta:
            self.add_hole(
                self.radius * np.cos(t),
                self.height - self.radius + self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        # Walls holes
        number_of_wall_holes = round(self.H / self.perimeter_spacing)
        if number_of_wall_holes >= 3:
            y_wall_holes = np.linspace(0, self.H, number_of_wall_holes)[1:-1]
            for y in y_wall_holes:
                self.add_hole(-self.width / 2, y, perimeter_hole_properties)
                self.add_hole(self.width / 2, y, perimeter_hole_properties)

        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        # Floor holes
        number_of_floor_holes = round(self.width / self.advance_spacing)
        floor_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_floor_holes
        )
        for x in floor_holes:
            self.add_hole(x, 0, advance_hole_properties)

        # First ring
        ring_radius = self.radius - self.perimeter_burden
        x_right_wall = self.width / 2 - self.perimeter_burden
        y_right_wall = (
            self.height
            - self.radius
            + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
        )
        ring_theta = np.arccos(x_right_wall / ring_radius)
        number_of_holes_first_ring_crown = round(
            ring_radius * 2 * (np.pi / 2 - ring_theta) / self.advance_spacing
        )
        ring_alpha = np.linspace(
            ring_theta, np.pi - ring_theta, number_of_holes_first_ring_crown
        )
        for a in ring_alpha:
            x = ring_radius * np.cos(a)
            y = self.height - self.radius + ring_radius * np.sin(a)
            if x ** 2 + (
                y - self.advance_burden - self.cut_length / 2
            ) ** 2 < (0.75 ** 2):
                continue
            if y < 0.5 * self.advance_burden:
                continue
            self.add_hole(x, y, advance_hole_properties)

        number_of_ring_wall_holes = round(
            (y_right_wall) / self.advance_spacing
        )
        ring_right_wall = np.linspace(
            self.advance_burden, y_right_wall, number_of_ring_wall_holes
        )[:-1]
        for y in ring_right_wall:
            self.add_hole(x_right_wall, y, advance_hole_properties)
            self.add_hole(
                -self.width / 2 + self.perimeter_burden,
                y,
                advance_hole_properties,
            )

        # Inner rings
        number_of_crowns = round(
            (
                self.height
                - self.perimeter_burden
                - self.advance_burden
                - self.cut_length
            )
            / self.advance_burden
        )
        if number_of_crowns < 0:
            number_of_crowns = 0
        number_of_walls = round(
            (x_right_wall - self.cut_length / 2) / self.advance_burden
        )
        if number_of_walls < 0:
            number_of_walls = 0
        minimum_rings = min(number_of_crowns, number_of_walls)
        crown_operational_burden_advance = self.advance_burden
        wall_operational_burden_advance = self.advance_burden
        if minimum_rings != 0:
            crown_operational_burden_advance = (
                self.height
                - self.perimeter_burden
                - self.advance_burden
                - self.cut_length
            ) / number_of_crowns
            wall_operational_burden_advance = (
                x_right_wall - self.cut_length / 2
            ) / number_of_walls
            if minimum_rings >= 2:
                for _ in range(1, minimum_rings):
                    ring_radius -= crown_operational_burden_advance
                    x_right_wall -= wall_operational_burden_advance
                    x_left_wall = -x_right_wall
                    if ring_radius - x_right_wall <= 1:
                        crown_operational_burden_advance = (
                            wall_operational_burden_advance
                        )
                    y_right_wall = (
                        self.height
                        - self.radius
                        + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
                    )
                    this_theta = np.arccos(x_right_wall / ring_radius)
                    number_of_holes_this = round(
                        ring_radius
                        * 2
                        * (np.pi / 2 - this_theta)
                        / self.advance_spacing
                    )
                    this_alpha = np.linspace(
                        this_theta, np.pi - this_theta, number_of_holes_this
                    )

                    for a in this_alpha:
                        x = ring_radius * np.cos(a)
                        y = self.height - self.radius + ring_radius * np.sin(a)
                        if x ** 2 + (
                            y - self.advance_burden - self.cut_length / 2
                        ) ** 2 < (0.6 ** 2):
                            continue
                        if y < 0.5 * self.advance_burden:
                            continue
                        self.add_hole(x, y, advance_hole_properties)

                    number_of_this_wall_holes = round(
                        (y_right_wall) / self.advance_spacing
                    )
                    if number_of_this_wall_holes >= 2:
                        this_wall_holes_y = np.linspace(
                            self.advance_burden,
                            y_right_wall,
                            number_of_this_wall_holes,
                        )[:-1]
                        for y in this_wall_holes_y:
                            self.add_hole(
                                x_right_wall, y, advance_hole_properties
                            )
                            self.add_hole(
                                x_left_wall, y, advance_hole_properties
                            )

        elif minimum_rings == 1:
            this_theta = np.arccos(x_right_wall / ring_radius)
            number_of_holes_this = round(
                ring_radius
                * 2
                * (np.pi / 2 - this_theta)
                / self.advance_spacing
            )
            this_alpha = np.linspace(
                this_theta, np.pi - this_theta, number_of_holes_this
            )

            for a in this_alpha:
                x = ring_radius * np.cos(a)
                y = self.height - self.radius + ring_radius * np.sin(a)
                if x ** 2 + (
                    y - self.advance_burden - self.cut_length / 2
                ) ** 2 < (0.6 ** 2):
                    continue
                if y < 0.5 * self.advance_burden:
                    continue
                self.add_hole(x, y, advance_hole_properties)

            number_of_this_wall_holes = round(
                (y_right_wall) / self.advance_spacing
            )
            if number_of_this_wall_holes >= 2:
                x_left_wall = -x_right_wall
                this_wall_holes_y = np.linspace(
                    self.advance_burden,
                    y_right_wall,
                    number_of_this_wall_holes,
                )[:-1]
                for y in this_wall_holes_y:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(x_left_wall, y, advance_hole_properties)

        # White spaces
        if minimum_rings == number_of_crowns:
            rings_left = number_of_walls - minimum_rings
            for _ in range(1, rings_left + 1):
                x_right_wall -= wall_operational_burden_advance
                x_left_wall = -x_right_wall
                y_right_wall = (
                    self.height
                    - self.radius
                    + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
                )
                number_of_wall_holes = round(
                    (y_right_wall) / self.advance_spacing
                )
                ys = np.linspace(
                    self.advance_burden, y_right_wall, number_of_wall_holes
                )[:-1]
                for y in ys:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(x_left_wall, y, advance_hole_properties)
        else:
            white_space_width = 2 * x_right_wall
            x_columns = round(white_space_width / self.advance_spacing)
            if x_columns == 2:
                x_right_wall -= self.advance_spacing
                number_of_this_holes = math.ceil(
                    (
                        self.height
                        - self.radius
                        + ring_radius
                        - self.advance_burden
                        - self.cut_length
                    )
                    / self.advance_burden
                )
                ys = np.linspace(
                    self.advance_burden + self.cut_length,
                    self.height - self.radius + ring_radius,
                    number_of_this_holes,
                )[1:-1]
                for y in ys:
                    self.add_hole(0, y, advance_hole_properties)

            if x_columns == 3:
                x_right_wall -= self.advance_spacing
                number_of_this_holes = math.ceil(
                    (
                        self.height
                        - self.radius
                        + ring_radius
                        - self.advance_burden
                        - self.cut_length
                    )
                    / self.advance_burden
                )
                ys = np.linspace(
                    self.advance_burden + self.cut_length,
                    self.height - self.radius + ring_radius,
                    number_of_this_holes,
                )[1:-1]
                for y in ys:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(-x_right_wall, y, advance_hole_properties)

            if x_columns == 4:
                x_right_wall -= self.advance_spacing
                number_of_this_holes = math.ceil(
                    (y_right_wall - self.advance_burden - self.cut_length)
                    / self.advance_burden
                )
                ys = np.linspace(
                    self.advance_burden + self.cut_length,
                    y_right_wall,
                    number_of_this_holes,
                )[1:-1]
                for y in ys:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(-x_right_wall, y, advance_hole_properties)
                    self.add_hole(0, y, advance_hole_properties)

        self.final_draw()

    def draw_cut_ebm(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        self.add_hole(
            0, self.advance_burden + self.cut_length / 2, air_hole_properties
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )
        self.add_hole(
            0.15,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(
            0,
            self.advance_burden + self.cut_length / 2 + 0.18,
            cut_hole_properties,
        )
        self.add_hole(
            -0.22,
            self.advance_burden + self.cut_length / 2,
            cut_hole_properties,
        )
        self.add_hole(
            0,
            self.advance_burden + self.cut_length / 2 - 0.25,
            cut_hole_properties,
        )
        self.add_hole(
            self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            -self.cut_length / 2,
            self.advance_burden + self.cut_length,
            cut_hole_properties,
        )
        self.add_hole(
            self.cut_length / 2, self.advance_burden, cut_hole_properties
        )
        self.add_hole(
            -self.cut_length / 2, self.advance_burden, cut_hole_properties
        )

        patch = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.sending_patch_cut = patches.Rectangle(
            (-self.cut_length / 2, self.advance_burden),
            self.cut_length,
            self.cut_length,
            fill=False,
            edgecolor="blue",
        )
        self.axes.add_patch(patch)
        if not self.is_2d:
            art3d.pathpatch_2d_to_3d(patch, z=0, zdir="z")

    def draw_plot_ebm_angular(self):

        self.draw()

        # Floor holes
        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        number_of_floor_holes = round(self.width / self.floor_spacing) + 1
        floor_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_floor_holes
        )
        for x in floor_holes:
            self.add_hole(x, 0, advance_hole_properties)

        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.perimeter_stemming,
        )
        # Crown holes
        number_of_holes_burden_curve = round(
            self.radius * self.alpha / (self.roof_spacing)
        )
        burden_curve_theta = np.linspace(
            self.first_theta,
            np.pi - self.first_theta,
            number_of_holes_burden_curve,
        )
        for t in burden_curve_theta:
            self.add_hole(
                self.radius * np.cos(t),
                self.height - self.radius + self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        # Walls holes
        number_of_wall_holes = round(self.H / self.wall_spacing)
        y_wall_holes = np.linspace(0, self.H, number_of_wall_holes)[1:-1]
        for y in y_wall_holes:
            self.add_hole(-self.width / 2, y, perimeter_hole_properties)
            self.add_hole(self.width / 2, y, perimeter_hole_properties)

        # Cut
        self.x_coordinates_property = {}
        if (
            0
            < self.H
            - self.advance_burden
            - (self.cut_rows - 1) * self.cut_rows_vertical_distance
            < self.advance_burden
        ):
            operational_burden_advance = (
                self.H - (self.cut_rows - 1) * self.cut_rows_vertical_distance
            ) / 2
        else:
            operational_burden_advance = self.advance_burden

        x_left = -self.triangle_half_length
        x_right = self.triangle_half_length
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.cut_stemming,
        )
        for i in range(self.cut_rows):
            # reset counter for each row (i)
            self.x_coordinates_property[i] = []
            counter = 0

            self.add_hole(
                x_left,
                operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = x_left
            y = (
                operational_burden_advance
                + i * self.cut_rows_vertical_distance
            )
            point_in_cut_negative = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_negative)

            self.add_hole(
                x_right,
                operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = x_right
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_positive)

            # add to counter
            counter += 1

            self.add_hole(
                x_left - self.cut_develop_front_length,
                operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = x_left - self.cut_develop_front_length
            point_in_cut_negative = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_negative)

            self.add_hole(
                x_right + self.cut_develop_front_length,
                operational_burden_advance
                + i * self.cut_rows_vertical_distance,
                cut_hole_properties,
            )
            x = x_right + self.cut_develop_front_length
            point_in_cut_positive = {"x": x, "index": counter, "y": y}
            self.x_coordinates_property[i].append(point_in_cut_positive)

            counter += 1
            self.last_x_index_side = counter

        for i in range(self.cut_rows):
            if (
                operational_burden_advance
                + i * self.cut_rows_vertical_distance
                < self.H
            ):
                delta_x = (
                    self.width / 2
                    - self.x_length / 2
                    - self.cut_develop_length
                )
                self.number_of_remaining_holes = (
                    number_of_remaining_holes
                ) = round(delta_x / self.cut_wall_filler_length)
                operational_burden = (
                    x_left - self.cut_develop_front_length + self.width / 2
                ) / number_of_remaining_holes

                # reset counter for each row (i)
                counter = 0
                points_in_cut = []
                for j in range(1, number_of_remaining_holes):
                    self.add_hole(
                        x_left
                        - self.cut_develop_front_length
                        - j * operational_burden,
                        operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        advance_hole_properties,
                    )
                    x = (
                        x_left
                        - self.cut_develop_front_length
                        - j * operational_burden
                    )
                    y = (
                        operational_burden_advance
                        + i * self.cut_rows_vertical_distance
                    )
                    point_in_cut_negative = {
                        "x": x,
                        "index": self.last_x_index_side + counter,
                        "y": y,
                    }
                    points_in_cut.append(point_in_cut_negative)

                    self.add_hole(
                        x_right
                        + self.cut_develop_front_length
                        + j * operational_burden,
                        operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        advance_hole_properties,
                    )
                    x = (
                        x_right
                        + self.cut_develop_front_length
                        + j * operational_burden
                    )
                    point_in_cut_positive = {
                        "x": x,
                        "index": self.last_x_index_side + counter,
                        "y": y,
                    }
                    points_in_cut.append(point_in_cut_positive)

                    counter += 1
                self.x_coordinates_property[i].extend(points_in_cut)

            else:
                x = symbols("x")
                x1, x2 = sorted(
                    list(
                        solveset(
                            x ** 2
                            - self.radius ** 2
                            + (
                                operational_burden_advance
                                + i * self.cut_rows_vertical_distance
                                - self.height
                                + self.radius
                            )
                            ** 2,
                            x,
                        )
                    )
                )
                delta_x = -self.cut_develop_length - self.x_length / 2 - x1
                self.number_of_remaining_holes = (
                    number_of_remaining_holes
                ) = round(delta_x / self.cut_wall_filler_length)
                operational_burden = (
                    x_left - self.cut_develop_front_length - x1
                ) / number_of_remaining_holes

                # reset counter for each row (i)
                counter = 0
                points_in_cut = []
                for j in range(1, number_of_remaining_holes):
                    self.add_hole(
                        x_left
                        - self.cut_develop_front_length
                        - j * operational_burden,
                        operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        advance_hole_properties,
                    )
                    x = (
                        x_left
                        - self.cut_develop_front_length
                        - j * operational_burden
                    )
                    y = (
                        operational_burden_advance
                        + i * self.cut_rows_vertical_distance
                    )
                    point_in_cut_negative = {
                        "x": x,
                        "index": self.last_x_index_side + counter,
                        "y": y,
                    }
                    points_in_cut.append(point_in_cut_negative)

                    self.add_hole(
                        x_right
                        + self.cut_develop_front_length
                        + j * operational_burden,
                        operational_burden_advance
                        + i * self.cut_rows_vertical_distance,
                        advance_hole_properties,
                    )
                    x = (
                        x_right
                        + self.cut_develop_front_length
                        + j * operational_burden
                    )
                    point_in_cut_negative = {
                        "x": x,
                        "index": self.last_x_index_side + counter,
                        "y": y,
                    }
                    points_in_cut.append(point_in_cut_negative)

                self.x_coordinates_property[i].extend(points_in_cut)

        if self.hole_length / self.cut_develop_length > 1:
            small_triangle_length = (
                2
                * self.triangle_half_length
                * (self.hole_length - self.cut_develop_length)
                / self.hole_length
            )
            for i in range(self.cut_rows):
                x = small_triangle_length / 2
                y = (
                    operational_burden_advance
                    + i * self.cut_rows_vertical_distance
                )
                length = math.fabs(x)

                self.add_hole(
                    -small_triangle_length / 2,
                    operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                point_in_cut_negative = {
                    "x": -x,
                    "index": 0,
                    "type": "small-triangle",
                    "length": length,
                    "y": y,
                }
                self.x_coordinates_property[i].append(point_in_cut_negative)

                self.add_hole(
                    small_triangle_length / 2,
                    operational_burden_advance
                    + i * self.cut_rows_vertical_distance,
                    cut_hole_properties,
                )
                point_in_cut_positive = {
                    "x": x,
                    "index": 0,
                    "type": "small-triangle",
                    "length": length,
                    "y": y,
                }
                self.x_coordinates_property[i].append(point_in_cut_positive)

        # Burden ring
        cut_top = (
            operational_burden_advance
            + (self.cut_rows - 1) * self.cut_rows_vertical_distance
        )
        if cut_top >= self.H:
            ring_radius = self.radius - self.perimeter_burden
            x_this = math.sqrt(
                ring_radius ** 2 - (cut_top - self.height + self.radius) ** 2
            )

            phi = np.arctan((cut_top - self.height + self.radius) / x_this)
            ring_length = ring_radius * (np.pi - 2 * phi)
            number_of_ring_holes = (
                round(ring_length / self.advance_spacing) + 1
            )
            thetas = np.linspace(phi, np.pi - phi, number_of_ring_holes)[1:-1]
            for t in thetas:
                y = self.height - self.radius + ring_radius * np.sin(t)
                self.add_hole(
                    ring_radius * np.cos(t),
                    y,
                    advance_hole_properties,
                )

            # Inner rings
            remaining_height = self.height - self.perimeter_burden - cut_top
            if remaining_height > 0:
                number_of_remaining_rings = (
                    round(remaining_height / self.advance_burden) + 1
                )
                operational_burden = (
                    remaining_height / number_of_remaining_rings
                )

                for i in range(1, number_of_remaining_rings):
                    ring_radius = (
                        self.radius
                        - self.perimeter_burden
                        - i * operational_burden
                    )
                    x_this = math.sqrt(
                        ring_radius ** 2
                        - (cut_top - self.height + self.radius) ** 2
                    )

                    phi = np.arctan(
                        (cut_top - self.height + self.radius) / x_this
                    )
                    ring_length = ring_radius * (np.pi - 2 * phi)
                    number_of_ring_holes = (
                        round(ring_length / self.advance_spacing) + 1
                    )
                    thetas = np.linspace(
                        phi, np.pi - phi, number_of_ring_holes
                    )[1:-1]
                    for t in thetas:
                        y = self.height - self.radius + ring_radius * np.sin(t)
                        self.add_hole(
                            ring_radius * np.cos(t),
                            y,
                            advance_hole_properties,
                        )
        else:
            # Burden ring
            ring_radius = self.radius - self.perimeter_burden
            x_this = math.sqrt(
                ring_radius ** 2 - (self.H - self.height + self.radius) ** 2
            )

            phi = np.arctan((self.H - self.height + self.radius) / x_this)
            ring_length = ring_radius * (np.pi - 2 * phi)
            number_of_ring_holes = (
                round(ring_length / self.advance_spacing) + 1
            )
            thetas = np.linspace(phi, np.pi - phi, number_of_ring_holes)
            for t in thetas:
                y = self.height - self.radius + ring_radius * np.sin(t)
                self.add_hole(
                    ring_radius * np.cos(t),
                    y,
                    advance_hole_properties,
                )

            delta_y = self.H - cut_top
            number_of_perimeter_columns_holes = (
                round(delta_y / self.advance_spacing) + 1
            )
            ys = np.linspace(
                cut_top,
                self.H,
                number_of_perimeter_columns_holes,
            )[1:-1]
            for y in ys:
                self.add_hole(
                    self.width / 2 - self.wall_burden,
                    y,
                    advance_hole_properties,
                )
                self.add_hole(
                    -self.width / 2 + self.wall_burden,
                    y,
                    advance_hole_properties,
                )

            # Inner rings
            number_of_width_columns = round(
                (self.width - 2 * self.wall_burden) / self.advance_burden
            )
            xs = np.linspace(
                -self.width / 2 + self.wall_burden,
                self.width / 2 - self.wall_burden,
                number_of_width_columns,
            )[1:-1]
            ring_radius = self.radius - self.perimeter_burden
            for i in range(1, number_of_width_columns // 2):
                ring_radius -= self.advance_burden
                y_this = (
                    math.sqrt(ring_radius ** 2 - xs[i - 1] ** 2)
                    + self.height
                    - self.radius
                )
                number_of_column_holes = (
                    round((y_this - cut_top) / self.advance_spacing) + 1
                )
                ys = np.linspace(cut_top, y_this, number_of_column_holes)[1:-1]
                for y in ys:
                    self.add_hole(xs[i - 1], y, advance_hole_properties)
                    self.add_hole(-xs[i - 1], y, advance_hole_properties)

                phi = np.arctan(
                    (y_this - self.height + self.radius) / -xs[i - 1]
                )
                ring_length = ring_radius * (np.pi - 2 * phi)
                number_of_ring_holes = round(
                    ring_length / self.advance_spacing
                )
                thetas = np.linspace(phi, np.pi - phi, number_of_ring_holes)
                if len(thetas) == 1:
                    self.add_hole(
                        0,
                        self.height - self.radius + ring_radius,
                        advance_hole_properties,
                    )
                else:
                    for t in thetas:
                        self.add_hole(
                            ring_radius * np.cos(t),
                            self.height
                            - self.radius
                            + ring_radius * np.sin(t),
                            advance_hole_properties,
                        )
            if number_of_width_columns % 2:
                y_this = self.height - self.radius + ring_radius
                number_of_middle_column_holes = (
                    round((y_this - cut_top) / self.advance_burden) + 1
                )
                ys = np.linspace(
                    cut_top, y_this, number_of_middle_column_holes
                )[1:]
                for y in ys:
                    self.add_hole(0, y, advance_hole_properties)

        self.final_draw()

    def draw_plot_ntnu(self):

        self.draw()

        parameters = self.get_parameters_ntnu()

        burden_perimeter = parameters.get("burden_perimeter")
        spacing_perimeter = parameters.get("spacing_perimeter")
        burden_first_ring = parameters.get("burden_first_ring")
        spacing_first_ring = parameters.get("spacing_first_ring")
        burden_floor = parameters.get("burden_floor")
        spacing_floor = parameters.get("spacing_floor")
        burden_advance = parameters.get("burden_advance")
        spacing_advance = parameters.get("spacing_advance")

        self.draw_cut_ntnu()

        floor_hole_properties = dict(
            hole_type="Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # Floor holes
        number_of_floor_holes = round(self.width / spacing_floor)
        floor_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_floor_holes
        )
        for x in floor_holes:
            self.add_hole(x, 0, floor_hole_properties)

        # Crown holes
        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_perimeter,
        )

        number_of_holes_burden_curve = round(
            self.radius * self.alpha / (spacing_perimeter)
        )
        burden_curve_theta = np.linspace(
            self.first_theta,
            np.pi - self.first_theta,
            number_of_holes_burden_curve,
        )
        for t in burden_curve_theta:
            self.add_hole(
                self.radius * np.cos(t),
                self.height - self.radius + self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        # Walls holes
        self.H = self.height + self.radius * (np.sin(self.first_theta) - 1)
        number_of_wall_holes = round(self.H / spacing_perimeter)
        if number_of_wall_holes >= 3:
            y_wall_holes = np.linspace(0, self.H, number_of_wall_holes)[1:-1]
            for y in y_wall_holes:
                self.add_hole(-self.width / 2, y, perimeter_hole_properties)
                self.add_hole(self.width / 2, y, perimeter_hole_properties)

        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # First ring
        ring_radius = self.radius - burden_perimeter
        x_right_wall = self.width / 2 - burden_perimeter
        y_right_wall = (
            self.height
            - self.radius
            + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
        )
        ring_theta = np.arccos(x_right_wall / ring_radius)
        number_of_holes_first_ring_crown = round(
            ring_radius * 2 * (np.pi / 2 - ring_theta) / spacing_first_ring
        )
        ring_alpha = np.linspace(
            ring_theta, np.pi - ring_theta, number_of_holes_first_ring_crown
        )
        for a in ring_alpha:
            x = ring_radius * np.cos(a)
            y = self.height - self.radius + ring_radius * np.sin(a)
            if x ** 2 + (y - burden_floor - self.w4 / 2) ** 2 < (0.75 ** 2):
                continue
            if y < 0.5 * burden_advance:
                continue
            self.add_hole(x, y, advance_hole_properties)

        number_of_ring_wall_holes = round((y_right_wall) / spacing_first_ring)
        ring_right_wall = np.linspace(
            burden_floor, y_right_wall, number_of_ring_wall_holes
        )[:-1]
        for y in ring_right_wall:
            self.add_hole(x_right_wall, y, advance_hole_properties)
            self.add_hole(
                -self.width / 2 + burden_perimeter, y, advance_hole_properties
            )
        # ring near first ring
        ring_radius = self.radius - burden_perimeter - burden_first_ring
        x_right_wall = self.width / 2 - burden_perimeter - burden_first_ring
        y_right_wall = (
            self.height
            - self.radius
            + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
        )
        ring_theta = np.arccos(x_right_wall / ring_radius)
        number_of_holes_first_ring_crown = round(
            ring_radius * 2 * (np.pi / 2 - ring_theta) / spacing_first_ring
        )
        ring_alpha = np.linspace(
            ring_theta, np.pi - ring_theta, number_of_holes_first_ring_crown
        )
        for a in ring_alpha:
            x = ring_radius * np.cos(a)
            y = self.height - self.radius + ring_radius * np.sin(a)
            if x ** 2 + (y - burden_floor - self.w4 / 2) ** 2 < (0.75 ** 2):
                continue
            if y < 0.5 * burden_advance:
                continue
            self.add_hole(x, y, advance_hole_properties)

        number_of_ring_wall_holes = round((y_right_wall) / spacing_first_ring)
        ring_right_wall = np.linspace(
            burden_floor, y_right_wall, number_of_ring_wall_holes
        )[:-1]
        for y in ring_right_wall:
            self.add_hole(x_right_wall, y, advance_hole_properties)
            self.add_hole(-x_right_wall, y, advance_hole_properties)

        # remainings
        remaining_width = (
            self.width / 2 - burden_perimeter - burden_first_ring - self.w4 / 2
        )
        number_of_columns = round(remaining_width / burden_advance) + 1
        operational_burden_advance = remaining_width / number_of_columns
        for i in range(1, number_of_columns):
            x_right_wall = (
                self.width / 2
                - burden_perimeter
                - burden_first_ring
                - i * operational_burden_advance
            )
            ring_radius = (
                self.radius
                - burden_perimeter
                - burden_first_ring
                - i * operational_burden_advance
            )
            y = (
                self.height
                - self.radius
                + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
            )
            delta_y = y - burden_floor
            number_of_column_holes = round(delta_y / spacing_advance) + 1
            ys = np.linspace(burden_floor, y, number_of_column_holes)
            for y in ys:
                self.add_hole(x_right_wall, y, advance_hole_properties)
                self.add_hole(-x_right_wall, y, advance_hole_properties)

            ring_theta = np.arccos(x_right_wall / ring_radius)
            number_of_holes_crown = round(
                ring_radius * 2 * (np.pi / 2 - ring_theta) / spacing_advance
            )
            ring_alpha = np.linspace(
                ring_theta, np.pi - ring_theta, number_of_holes_crown
            )[1:-1]

            for a in ring_alpha:
                x = ring_radius * np.cos(a)
                y = self.height - self.radius + ring_radius * np.sin(a)
                self.add_hole(x, y, advance_hole_properties)

        remaining_width = 2 * x_right_wall
        number_of_columns = round(remaining_width / spacing_advance) + 1
        xs = np.linspace(-x_right_wall, x_right_wall, number_of_columns)[1:-1]
        for x in xs:
            y_upper = (
                self.height
                - self.radius
                + math.sqrt(ring_radius ** 2 - x ** 2)
            )
            number_of_column_holes = (
                round((y_upper - burden_floor - self.w4) / burden_advance) + 1
            )
            ys = np.linspace(
                burden_floor + self.w4, y_upper, number_of_column_holes
            )[1:-1]
            for y in ys:
                self.add_hole(x, y, advance_hole_properties)

        self.final_draw()

    def draw_cut_ntnu(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )
        x_air_hole = 0
        y_air_hole = self.burden_floor + self.w4 / 2

        self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
            stemming=self.t_cut,
        )

        self.add_hole(x_air_hole - self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b1, cut_hole_properties)
        self.add_hole(x_air_hole + self.b1, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b1, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole + self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w2 / 2,
            y_air_hole - self.w2 / 2,
            cut_hole_properties,
        )
        self.add_hole(x_air_hole - self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole + self.b3, cut_hole_properties)
        self.add_hole(x_air_hole + self.b3, y_air_hole, cut_hole_properties)
        self.add_hole(x_air_hole, y_air_hole - self.b3, cut_hole_properties)
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole - self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole + self.w4 / 2,
            cut_hole_properties,
        )
        self.add_hole(
            x_air_hole + self.w4 / 2,
            y_air_hole - self.w4 / 2,
            cut_hole_properties,
        )

    def draw_plot_olafsson(self):

        self.draw()

        self.draw_cut_olafsson()

        # Floor holes
        floor_hole_properties = dict(
            hole_type="Floor",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        number_of_floor_holes = round(self.width / self.floor_spacing)
        floor_holes = np.linspace(
            -self.width / 2, self.width / 2, number_of_floor_holes
        )
        for x in floor_holes:
            self.add_hole(x, 0, floor_hole_properties)

        perimeter_hole_properties = dict(
            hole_type="Perimeter",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # Crown holes
        number_of_holes_burden_curve = round(
            self.radius * self.alpha / (self.roof_spacing)
        )
        burden_curve_theta = np.linspace(
            self.first_theta,
            np.pi - self.first_theta,
            number_of_holes_burden_curve,
        )
        for t in burden_curve_theta:
            self.add_hole(
                self.radius * np.cos(t),
                self.height - self.radius + self.radius * np.sin(t),
                perimeter_hole_properties,
            )

        # Walls holes
        self.H = self.height + self.radius * (np.sin(self.first_theta) - 1)
        number_of_wall_holes = round(self.H / self.wall_spacing)
        if number_of_wall_holes >= 3:
            y_wall_holes = np.linspace(0, self.H, number_of_wall_holes)[1:-1]
            for y in y_wall_holes:
                self.add_hole(-self.width / 2, y, perimeter_hole_properties)
                self.add_hole(self.width / 2, y, perimeter_hole_properties)

        advance_hole_properties = dict(
            hole_type="Advance",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )

        # First ring
        ring_radius = self.radius - self.roof_burden
        x_right_wall = self.width / 2 - self.wall_burden
        y_right_wall = (
            self.height
            - self.radius
            + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
        )
        ring_theta = np.arccos(x_right_wall / ring_radius)
        number_of_holes_first_ring_crown = round(
            ring_radius * 2 * (np.pi / 2 - ring_theta) / self.advance_spacing
        )
        ring_alpha = np.linspace(
            ring_theta, np.pi - ring_theta, number_of_holes_first_ring_crown
        )
        for a in ring_alpha:
            x = ring_radius * np.cos(a)
            y = self.height - self.radius + ring_radius * np.sin(a)
            if x ** 2 + (y - self.floor_burden - self.last_x / 2) ** 2 < (
                0.75 ** 2
            ):
                continue
            if y < 0.5 * self.advance_burden:
                continue
            self.add_hole(x, y, advance_hole_properties)

        number_of_ring_wall_holes = round(
            (y_right_wall) / self.advance_spacing
        )
        ring_right_wall = np.linspace(
            self.floor_burden, y_right_wall, number_of_ring_wall_holes
        )[:-1]
        for y in ring_right_wall:
            self.add_hole(x_right_wall, y, advance_hole_properties)
            self.add_hole(
                -self.width / 2 + self.wall_burden, y, advance_hole_properties
            )

        # Inner rings
        number_of_crowns = round(
            (self.height - self.roof_burden - self.floor_burden - self.last_x)
            / self.advance_burden
        )
        if number_of_crowns < 0:
            number_of_crowns = 0
        number_of_walls = round((x_right_wall - 0.4) / self.advance_burden)
        if number_of_walls < 0:
            number_of_walls = 0
        minimum_rings = min(number_of_crowns, number_of_walls)
        crown_operational_burden_advance = self.advance_burden
        wall_operational_burden_advance = self.advance_burden
        if minimum_rings != 0:
            crown_operational_burden_advance = (
                self.height
                - self.roof_burden
                - self.floor_burden
                - self.last_x
            ) / number_of_crowns
            wall_operational_burden_advance = (
                x_right_wall - self.last_x / 2
            ) / number_of_walls
            if minimum_rings >= 2:
                for _ in range(1, minimum_rings):
                    ring_radius -= crown_operational_burden_advance
                    x_right_wall -= wall_operational_burden_advance
                    x_left_wall = -x_right_wall
                    if ring_radius - x_right_wall <= 1:
                        crown_operational_burden_advance = (
                            wall_operational_burden_advance
                        )
                    if abs(ring_radius) < abs(x_right_wall):
                        break
                    y_right_wall = (
                        self.height
                        - self.radius
                        + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
                    )
                    this_theta = np.arccos(x_right_wall / ring_radius)
                    number_of_holes_this = round(
                        ring_radius
                        * 2
                        * (np.pi / 2 - this_theta)
                        / self.advance_spacing
                    )
                    this_alpha = np.linspace(
                        this_theta, np.pi - this_theta, number_of_holes_this
                    )

                    for a in this_alpha:
                        x = ring_radius * np.cos(a)
                        y = self.height - self.radius + ring_radius * np.sin(a)
                        if x ** 2 + (
                            y - self.floor_burden - self.last_x / 2
                        ) ** 2 < (0.6 ** 2):
                            continue
                        if y < 0.5 * self.floor_burden:
                            continue
                        self.add_hole(x, y, advance_hole_properties)

                    number_of_this_wall_holes = round(
                        (y_right_wall) / self.advance_spacing
                    )
                    if number_of_this_wall_holes >= 2:
                        this_wall_holes_y = np.linspace(
                            self.floor_burden,
                            y_right_wall,
                            number_of_this_wall_holes,
                        )[:-1]
                        for y in this_wall_holes_y:
                            self.add_hole(
                                x_right_wall, y, advance_hole_properties
                            )
                            self.add_hole(
                                x_left_wall, y, advance_hole_properties
                            )

        elif minimum_rings == 1:
            this_theta = np.arccos(x_right_wall / ring_radius)
            number_of_holes_this = round(
                ring_radius
                * 2
                * (np.pi / 2 - this_theta)
                / self.advance_spacing
            )
            this_alpha = np.linspace(
                this_theta, np.pi - this_theta, number_of_holes_this
            )

            for a in this_alpha:
                x = ring_radius * np.cos(a)
                y = self.height - self.radius + ring_radius * np.sin(a)
                if x ** 2 + (y - self.floor_burden - self.last_x / 2) ** 2 < (
                    0.6 ** 2
                ):
                    continue
                if y < 0.5 * self.floor_burden:
                    continue
                self.add_hole(x, y, advance_hole_properties)

            number_of_this_wall_holes = round(
                (y_right_wall) / self.advance_spacing
            )
            if number_of_this_wall_holes >= 2:
                x_left_wall = -x_right_wall
                this_wall_holes_y = np.linspace(
                    self.floor_burden, y_right_wall, number_of_this_wall_holes
                )[:-1]
                for y in this_wall_holes_y:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(x_left_wall, y, advance_hole_properties)

        # White spaces
        if minimum_rings == number_of_crowns:
            rings_left = number_of_walls - minimum_rings
            for _ in range(1, rings_left + 1):
                x_right_wall -= wall_operational_burden_advance
                x_left_wall = -x_right_wall
                y_right_wall = (
                    self.height
                    - self.radius
                    + math.sqrt(ring_radius ** 2 - x_right_wall ** 2)
                )
                number_of_wall_holes = round(
                    (y_right_wall) / self.advance_spacing
                )
                ys = np.linspace(
                    self.floor_burden, y_right_wall, number_of_wall_holes
                )[:-1]
                for y in ys:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(x_left_wall, y, advance_hole_properties)

        else:
            white_space_width = 2 * x_right_wall
            x_columns = round(white_space_width / self.advance_spacing)
            if x_columns == 3:
                x_right_wall -= self.advance_spacing
                number_of_this_holes = math.ceil(
                    (
                        self.height
                        - self.radius
                        + ring_radius
                        - self.floor_burden
                        - self.last_x
                    )
                    / self.advance_burden
                )
                ys = np.linspace(
                    self.floor_burden + self.last_x,
                    self.height - self.radius + ring_radius,
                    number_of_this_holes,
                )[1:-1]
                for y in ys:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(-x_right_wall, y, advance_hole_properties)

            if x_columns == 4:
                x_right_wall -= self.advance_spacing
                number_of_this_holes = math.ceil(
                    (y_right_wall - self.floor_burden - self.last_x)
                    / self.advance_burden
                )
                ys = np.linspace(
                    self.floor_burden + self.last_x,
                    y_right_wall,
                    number_of_this_holes,
                )[1:-1]
                for y in ys:
                    self.add_hole(x_right_wall, y, advance_hole_properties)
                    self.add_hole(-x_right_wall, y, advance_hole_properties)
                    self.add_hole(0, y, advance_hole_properties)

        self.final_draw()

    def draw_cut_olafsson(self):
        # Air hole
        air_hole_properties = dict(
            hole_type="Uncharged",
            hole_length=self.hole_length,
            hole_diameter=self.air_hole_diameter,
        )

        # Cut holes
        cut_hole_properties = dict(
            hole_type="Cut",
            hole_length=self.hole_length,
            hole_diameter=self.hole_diameter,
            explosive_type=self.explosive_type,
        )
        required_parameters_length = len(self.required_parameters)
        if required_parameters_length == 4:
            (
                first_section,
                second_section,
                third_section,
                fourth_section,
            ) = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section
            b3, _ = third_section
            _, x4 = fourth_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x4 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(x_air_hole - b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b3, cut_hole_properties)
            self.add_hole(x_air_hole + b3, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b3, cut_hole_properties)
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole + x4 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x4 / 2,
                y_air_hole - x4 / 2,
                cut_hole_properties,
            )
        elif required_parameters_length == 3:
            (
                first_section,
                second_section,
                third_section,
            ) = self.required_parameters
            _, x1 = first_section
            b2, _ = second_section
            _, x3 = third_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x3 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )

            self.add_hole(
                x_air_hole,
                y_air_hole - b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole,
                y_air_hole + b2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - b2,
                y_air_hole,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole - x3 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x3 / 2, y_air_hole + x3 / 2, cut_hole_properties
            )

        elif required_parameters_length == 2:
            first_section, second_section = self.required_parameters
            b1, _ = first_section
            _, x2 = second_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x2 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(x_air_hole - b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole + b1, cut_hole_properties)
            self.add_hole(x_air_hole + b1, y_air_hole, cut_hole_properties)
            self.add_hole(x_air_hole, y_air_hole - b1, cut_hole_properties)
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole - x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole + x2 / 2,
                cut_hole_properties,
            )
            self.add_hole(
                x_air_hole + x2 / 2,
                y_air_hole - x2 / 2,
                cut_hole_properties,
            )

        elif required_parameters_length == 1:
            first_section = self.required_parameters
            _, x1 = first_section

            x_air_hole = 0
            y_air_hole = self.floor_burden + x1 / 2

            self.add_hole(x_air_hole, y_air_hole, air_hole_properties)

            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole - x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole - x1 / 2, cut_hole_properties
            )
            self.add_hole(
                x_air_hole + x1 / 2, y_air_hole + x1 / 2, cut_hole_properties
            )
