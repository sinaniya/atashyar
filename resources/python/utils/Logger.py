import logging

logging.basicConfig(
    filename="error.log",
    filemode="a",
    level=logging.ERROR,
    format="%(name)s - %(asctime)s - %(levelname)s - %(message)s",
    datefmt="%d-%b-%y %H:%M:%S",
)

logger = logging.getLogger("MainLogger")
