import re

import numpy as np


class Helper:
    def __init__(self):
        pass

    def create_annot_txt(self, attributes):
        final_text = ""
        for attr in attributes:
            # remove underscore from begging
            # capitalize attr
            # add an space at begging
            temp_attr = attr.replace("_", "")
            temp_attr_capitalize = temp_attr.capitalize()
            string_length = len(temp_attr_capitalize) + 1
            final_attr = temp_attr_capitalize.rjust(string_length)
            text = "{} = {}".format(
                "".join(str(final_attr)), "".join(str(attributes[attr]))
            )
            text += "\n"
            final_text += text
        return final_text

    def extract_float_from_str(self, string):
        numeric_const_pattern = r"""
             [-+]? # optional sign
             (?:
                 (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
                 |
                 (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
             )
             # followed by optional exponent part if desired
             (?: [Ee] [+-]? \d+ ) ?
             """
        rx = re.compile(numeric_const_pattern, re.VERBOSE)
        temp_str_list = rx.findall(string)
        return [float(i) for i in temp_str_list]

    def flatten(self, array):
        return np.array(array).flatten().tolist()

    def get_maximum_index_in_dict_into_array(self, array, key_index):
        max_value = 0
        for item in array:
            if item[key_index] >= max_value:
                max_value = item[key_index]
            else:
                continue
        return max_value
