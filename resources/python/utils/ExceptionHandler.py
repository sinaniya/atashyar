import sys
from traceback import format_exception

from PyQt5 import QtCore

from resources.python.utils.Logger import logger


class ExceptionHandler(QtCore.QObject):
    errorSignal = QtCore.pyqtSignal()

    def __init__(self):
        super(ExceptionHandler, self).__init__()
        self.err = None

    def handler(self, exctype, value, traceback):
        err = "".join(format_exception(exctype, value, traceback))
        self.err = err
        self.errorSignal.emit()
        sys._excepthook(exctype, value, traceback)

    @QtCore.pyqtSlot()
    def error_handler_slot(self):
        logger.error(self.err)
