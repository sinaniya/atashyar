from importlib import import_module
import inspect

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, QJsonValue
from matplotlib_backend_qtquick.backend_qtquick import (
    NavigationToolbar2QtQuick,
)
from matplotlib.backend_bases import MouseEvent, PickEvent
from matplotlib.collections import PathCollection
import matplotlib.gridspec as gridspec
import numpy as np
import xlsxwriter

from .shapes.circle import Circle
from .shapes.arched_roof_2 import ArchedRoof2
from .plot import Plot
from .utils.Helper import Helper


class Drawer(Plot):
    """This class will be used for qml(front-end side)"""

    xys_changed = pyqtSignal()
    """This is pyqt signal for getting changes of xy"""

    def __init__(self, parent=None):
        super().__init__(parent)

        self.gs = None
        """This is grid scrip having multiple plot in one canvas"""
        self.axes_angular_cut_page = None
        """This is grid scrip having multiple plot in one canvas"""
        self.hover_cid = None
        """This is cid to have enable/disable showing properties button"""
        self._shape = None
        """This is shape object to have draw """
        self.result = None
        """This is result of anything user selected"""
        self.annot = None
        """This is annotation object to have annotation plot"""
        self.black_list = []
        """This is a black list array"""

    def setup(self, canvas, is_2d=True, is_angular=False):
        """This is set up function to integrate matplotlib and pyqt
        initialize plot for creating
        Args:
            canvas(Canvas): canvas of pyqt
            is_2d(bool): indicates 2d selection or not
            is_angular(bool): indicates angular selection or not
        """
        self.canvas = self._shape.canvas = canvas
        self._shape.black_list = self.black_list
        self.figure = self.canvas.figure
        self.figure.clear()
        self.gs = None
        if is_2d:
            self.create_2d_axes(is_angular)
        else:
            self.create_3d_axes()
        self.canvas.mpl_connect("motion_notify_event", self.on_motion)
        self.setup_annotation()

        if isinstance(self._shape, ArchedRoof2):
            self._shape.canvas = self.canvas
            self._shape.axes = self.axes
            self._shape.black_list = self.black_list

        if self.get_caller_function_name() != "draw":
            self.toolbar = NavigationToolbar2QtQuick(canvas=self.canvas)

    def create_2d_axes(self, is_angular=False):
        """This method is helper and initialize 2d axes, For angular method creates 2 plots first is Front view,
        and second is Top view
        Args:
            is_angular(boolean): indicates angular is selected or not
        """
        if is_angular:
            self.figure.tight_layout()
            self.gs = gridspec.GridSpec(1, 2, width_ratios=[1, 1])
            self.axes = self._shape.axes = self.figure.add_subplot(self.gs[0])
            self.axes.title.set_text("Front View")
            self.axes.set_xlabel("X Axis")
            self.axes.set_ylabel("Y Axis")
            self.axes_angular_cut_page = (
                self._shape.axes_angular_cut_page
            ) = self.figure.add_subplot(self.gs[1])
            self.axes_angular_cut_page.title.set_text("Top View")
            self.axes_angular_cut_page.set_xlabel("X Axis")
            self.axes_angular_cut_page.set_ylabel("Z Axis")
            self.axes.set_aspect("equal", "box")
            self.axes.autoscale()
            self.axes.grid(True)
            self.axes_angular_cut_page.set_aspect("equal", "box")
            self.axes_angular_cut_page.autoscale()
            self.axes_angular_cut_page.grid(True)

        else:
            self.figure.tight_layout()
            self.axes = self._shape.axes = self.figure.add_subplot(111)
            self.axes.set_aspect("equal", "box")
            self.axes.autoscale()
            self.axes.set_xlabel("X Axis")
            self.axes.set_ylabel("Y Axis")
            self.axes.grid(True)

        # A known issue in matplotlib caused we use limiting manually
        # https://stackoverflow.com/questions/45757260/matplotlibs-autoscale-doesnt-seem-to-work-on-y-axis-for-small-values
        if self._properties["shape_name"] == "Arched Roof 1":
            first_theta = np.arccos(
                self._properties["width"] / (2 * self._properties["radius"])
            )
            H = self._properties["height"] + self._properties["radius"] * (
                np.sin(first_theta) - 1
            )
            width = (
                self._properties["width"]
                if ("width" in self._properties)
                else None
            )
            height = (
                self._properties["height"]
                if ("height" in self._properties)
                else None
            )
            # print(width, height)
            # N = width / height
            # if height > width:
            #     N = 1 / N
            #     # self.axes.set_aspect('datalim', 'box')
            #     self.axes.set_aspect(N, adjustable='box')
            # else:
            #     # self.axes.set_aspect('auto')
            #     self.axes.set_aspect(N, adjustable='box')

            self.axes.set_ylim(bottom=-1, top=H + self._properties['radius'] * 0.5)
        elif self._properties['shape_name'] == 'HorseShoe':
            radius = self._properties['radius']
            height = self._properties['height']
            xlimit = ((2 * radius) + radius/4) / 2
            ylimit = (radius + height) + 1/2
            self.axes.set_ylim(bottom=-0.5, top=ylimit)
            self.axes.set_xlim(left=-xlimit, right=xlimit)


    def create_3d_axes(self):
        """This method is helper and initialize 3d axes
        and second is Top view
        """
        self.figure.tight_layout()
        width = (
            self._properties["width"]
            if ("width" in self._properties)
            else None
        )
        height = (
            self._properties["height"]
            if ("height" in self._properties)
            else None
        )
        radius = (
            self._properties["radius"]
            if ("radius" in self._properties)
            else None
        )
        self.axes = self._shape.axes = self.figure.add_subplot(
            111, projection="3d"
        )
        # Set ratio based on shape and user's inputs(width, height)
        if self._properties["shape_name"] == "D-Shape":
            N = width / (height + width / 2)
            if height > width:
                N = 1 / N
                self.axes.set_box_aspect((1, N, 1))
            else:
                self.axes.set_box_aspect((N, 1, 1))
        elif self._properties["shape_name"] == "Rectangle":
            N = width / height
            if height > width:
                N = 1 / N
                self.axes.set_box_aspect((1, N, 1))
            else:
                self.axes.set_box_aspect((N, 1, 1))
        elif self._properties["shape_name"] == "Circle":
            N = radius / radius
            self.axes.set_box_aspect((N, N, N))
        elif self._properties["shape_name"] == "Arched Roof 1":
            N = width / height
            if height > width:
                N = 1 / N
                self.axes.set_box_aspect((1, N, 1))
            else:
                self.axes.set_box_aspect((N, 1, 1))
        elif self._properties["shape_name"] == "Arched Roof 2":
            N = width / height
            if height > width:
                N = 1 / N
                self.axes.set_box_aspect((1, N, 1))
            else:
                self.axes.set_box_aspect((N, 1, 1))
        self.axes.autoscale()
        self.axes.grid(True)
        self.setup_axes_view()

    def init_view_3d(self):
        self.axes.view_init(60, -70)

    def setup_axes_view(self):
        """This method is initialized view is 3d"""
        self.init_view_3d()
        self.axes.set_xlabel("X Axis")
        self.axes.set_ylabel("Y Axis")
        self.axes.set_zlabel("Z Axis")

    def setup_annotation(self):
        """This method is helper for cleaning main setup"""
        self.annot = self.axes.annotate(
            "",
            xy=(0, 0),
            xytext=(5, 5),
            xycoords="data",
            textcoords="offset points",
            bbox=dict(boxstyle="round", fc="w"),
            clip_on=False
            # arrowprops=dict(arrowstyle="->")
        )
        self.annot.set_visible(False)

    def is_angular_selection(self, properties):
        """This method is return boolean when angular is select by user
        Args:
             properties(object): holding props of user data
        Returns:
            bool: which means is angular selected or not"""
        if "angle" in properties and properties["angle"] != 0:
            return True
        return False

    def is_2d_selection(self, properties):
        """This method is return boolean when 2d radio button is select by user
        Args:
             properties(object): holding props of user data
        Returns:
            bool: which means is 2d selected or not"""
        if "plot_view" in properties and properties["plot_view"] == "2D":
            return True
        return False

    # ReImplement home function from inherited
    @pyqtSlot()
    def home(self, *args):
        """
        This method is coming back to first position of plot
        """
        if self.is_2d_selection(self._properties):
            pass
        else:
            self.init_view_3d()
        self.figure.canvas.draw_idle()
        self.toolbar.home(*args)

    @pyqtSlot(QObject, QJsonValue)
    def draw(self, canvas, properties):
        """
        This method is for creating shape in 2d in the first page
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        self.setup(canvas)
        self._shape.draw()
        if self.get_caller_function_name() == "draw":
            self.canvas.draw_idle()

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def ebm(
        self,
        canvas,
        properties,
    ):
        """This method is for drawing on ebm mode
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.ebm()
        return self.result

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def ebm_angular(self, canvas, properties):
        """This method is for drawing on ebm mode and having angle in cut
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self._make_shape_from_name(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.ebm_angular()
        return self.result

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def ntnu(self, canvas, properties):
        """This method is for drawing on NTNU mode
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.ntnu()
        return self.result

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def olafsson(self, canvas, properties):
        """This method is for drawing on olafsson mode
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.olafsson()
        return self.result

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def olafsson_angular(self, canvas, properties):
        """This method is for drawing on olafsson_angular mode
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.olafsson_angular()
        return self.result

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def konya(self, canvas, properties):
        """This method is for drawing on konya mode
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.konya()
        return self.result

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def konya_angular(self, canvas, properties):
        """This method is for drawing on konya_angular mode
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.konya_angular()
        return self.result

    @pyqtSlot(QObject, QJsonValue, result=QJsonValue)
    def gustafsson(self, canvas, properties):
        """This method is for drawing on gustafsson mode
        Args:
            canvas(Canvas): this is holding plot and integrating matplotlib with Qt widget
            properties(dict): containing user inputs
        """
        self._properties = properties.toVariant()
        self._make_shape_from_name(self._properties)
        is_angular = self.is_angular_selection(self._properties)
        is_2d = self.is_2d_selection(self._properties)
        self.setup(canvas, is_2d, is_angular)
        self.result = self._shape.gustafsson()
        return self.result

    def _get_module_name(self) -> str:
        return self._shape_name.lower().replace(" ", "_").replace("-", "_")

    def _get_class_name(self) -> str:
        return "".join(self._shape_name.replace("-", " ").split(" "))

    def _make_shape_from_name(self, properties):
        self._shape_name = properties.get("shape_name")
        module_name, class_name = (
            ".shapes." + self._get_module_name() + "." + self._get_class_name()
        ).rsplit(".", 1)
        self._shape = getattr(
            import_module(module_name, package=__package__), class_name
        )(parent=None, properties=properties)

    @pyqtSlot()
    def remove_button(self):
        self.cid = self.canvas.mpl_connect("pick_event", self.onPick)

    def onPick(self, event: PickEvent):
        scatter_to_contains_and_ind = {}
        # fill the map: scatter_to_contains_and_ind
        for s in self._shape.sc:
            cont, ind = s.contains(event)
            scatter_to_contains_and_ind[s] = [cont, ind]

        # extract the scatter which contains hover event
        s_contains = None
        for s in scatter_to_contains_and_ind:
            cont = scatter_to_contains_and_ind[s][0]
            if not cont:
                continue
            else:
                s_contains = s
                break

        ind = scatter_to_contains_and_ind[s_contains][1]
        hole = self.get_hole_by_xy(s_contains, ind)
        self.black_list.append(hole)
        self.xys_changed.emit()
        # if isinstance(event.artist, PathCollection):
        #     offsets = event.artist.get_offsets()
        #     xy = tuple(np.take(offsets, event.ind[0], 0))
        #     print(xy)
        #     hole = self.get_hole_by_xy(self._shape.sc, xy)
        #     print(hole)

    @pyqtSlot()
    def disable_remove_button(self):
        self.canvas.mpl_disconnect(self.cid)

    @pyqtSlot()
    def addPoint(self):
        self.cid = self.figure.canvas.mpl_connect(
            "button_press_event", self.onClick
        )

    def onClick(self, event: MouseEvent):
        x, y = event.xdata, event.ydata
        if self._shape.is_point_near_edges((x, y)):
            new_x, new_y = self._shape.snap_point_near_edges((x, y))
            if hasattr(
                self._shape.__class__, "is_point_near_floor"
            ) and callable(
                getattr(self._shape.__class__, "is_point_near_floor")
            ):
                if isinstance(self._shape, ArchedRoof2):
                    if self._shape.method == "NTNU":
                        self._shape.arched_roof_1.add_to_floor.append(
                            (new_x, new_y)
                        )
                    else:
                        self._shape.arched_roof_1.add_to_advance.append(
                            (new_x, new_y)
                        )
                else:
                    if self._shape.method == "NTNU":
                        self._shape.add_to_floor.append((new_x, new_y))
                    else:
                        self._shape.add_to_advance.append((new_x, new_y))
            else:

                if isinstance(self._shape, ArchedRoof2):
                    self._shape.arched_roof_1.add_to_perimeter.append(
                        (new_x, new_y)
                    )
                elif isinstance(self._shape, Circle):
                    self._shape.add_to_advance.append((new_x, new_y))
                else:
                    self._shape.add_to_perimeter.append((new_x, new_y))

        elif self._shape.is_point_in_shape((x, y)):
            if not self._shape.is_point_inside_cut((x, y)):
                if isinstance(self._shape, ArchedRoof2):
                    self._shape.arched_roof_1.add_to_advance.append((x, y))
                else:
                    self._shape.add_to_advance.append((x, y))
            else:
                if isinstance(self._shape, ArchedRoof2):
                    self._shape.arched_roof_1.add_to_cut.append((x, y))
                else:
                    self._shape.add_to_cut.append((x, y))

        self.xys_changed.emit(
            self.axes,
            self.rock_properties_string,
            self.explosive_properties_string,
            self.hole_properties_string,
        )
        self.figure.canvas.draw_idle()

    @pyqtSlot()
    def removeAdder(self):
        self.figure.canvas.mpl_disconnect(self.cid)

    @pyqtSlot(str)
    def export_excel(self, url):
        filename = (url + "/result.xlsx").replace("file:///", "")
        if filename:
            workbook = xlsxwriter.Workbook(filename=filename)
            worksheet = workbook.add_worksheet()

            row = 0
            col = 0

            self.export()
            worksheet.insert_image("A1", "result.png")

            worksheet.set_column(0, 0, 48)

            for key, value in self.result.items():
                format_key = " ".join(key.split("_")).capitalize()
                worksheet.write(row + 40, col, format_key)
                worksheet.write(row + 40, col + 1, value)
                row += 1

        workbook.close()

    @pyqtSlot()
    def set_view_to_side(self):
        """This method is for setting view to side for seeing depth of hole"""
        if self.is_2d_selection(self._properties):
            return
        else:
            self.axes.view_init(0, 0)
            self.figure.canvas.draw_idle()

    @pyqtSlot()
    def show_property(self):
        """This method is for showing properties of hole when mouse is hovered on"""
        self.hover_cid = self.figure.canvas.mpl_connect(
            "motion_notify_event", self.hover
        )

    @pyqtSlot(result=QJsonValue)
    def get_result(self):
        """This method is for getting results from calling in qml"""
        # print(self.result)
        return self.result

    @pyqtSlot()
    def hide_property(self):
        """This method is for not showing properties of hole when mouse is hovered on"""
        vis = self.annot.get_visible()
        self.figure.canvas.mpl_disconnect(self.hover_cid)
        if vis:
            self.annot.set_visible(False)
            self.figure.canvas.draw_idle()

    def get_caller_function_name(self) -> str:
        return inspect.stack()[1].function

    def get_hole_by_xy(self, s, ind):
        """This method is for getting hole using xy coordinates
        Args:
            ind(tuple<int,int>): positions of any hole
            s(matplotlib.collections): this is an object which return by scatter method
        Returns:
            Hole: return found hole"""
        # In 3d mode has a bug and position of mouse is incorrect
        pos = s.get_offsets()[ind["ind"][0]]
        x_of_mouse, y_of_mouse = pos
        holes = self._shape.holes
        right_hole = None
        tolerance = 0.01
        is_x_ok = False
        is_y_ok = False
        for hole in holes:
            x, y = hole.coordinates
            if x + tolerance >= x_of_mouse >= x - tolerance:
                is_x_ok = True
            if y + tolerance >= y_of_mouse >= y - tolerance:
                is_y_ok = True
            if is_x_ok and is_y_ok:
                right_hole = hole
                break
        return right_hole

    def update_annot(self, hole, s, ind):
        """This method is for helper to update annotation
        Args:
             hole(Hole): Hole object
             ind(tuple<int,int>): positions of any hole
             s(matplotlib.collections): this is an object which return by scatter method

        Returns:
        """
        if hole is not None:
            attrs = hole.__dict__
            helper = Helper()
            annotation_text = helper.create_annot_txt(attrs)
            # set x,y
            pos = s.get_offsets()[ind["ind"][0]]
            x, y = pos
            self.annot.xy = pos
            coord_split = helper.extract_float_from_str(self.get_coordinates())
            mouse_X = coord_split[0]
            mouse_Y = coord_split[1]

            # check annotation is not overflow
            _, upper_x_bound = self.axes.get_xbound()
            _, upper_y_bound = self.axes.get_ybound()
            if mouse_Y >= upper_y_bound * 0.9:
                y = y - 120
                self.annot.set_position((x, y))

            elif upper_y_bound * 0.7 < mouse_Y < upper_y_bound * 0.9:
                y = y - 100
                self.annot.set_position((x, y))

            elif upper_y_bound * 0.6 < mouse_Y < upper_y_bound * 0.7:
                y = y - 90
                self.annot.set_position((x, y))

            elif upper_y_bound * 0.55 < mouse_Y < upper_y_bound * 0.0:
                y = y - 80
                self.annot.set_position((x, y))

            elif upper_y_bound * 0.05 < mouse_Y <= upper_y_bound * 0.07:
                y = y + 5
                self.annot.set_position((x, y))
            elif mouse_Y < upper_y_bound * 0.05:
                y = y + 2
                self.annot.set_position((x, y))

            if mouse_X >= upper_x_bound * 0.9:
                x = x - 135
                self.annot.set_position((x, y))
            elif upper_x_bound * 0.8 < mouse_X < upper_x_bound * 0.9:
                x = x - 100
                self.annot.set_position((x, y))
            elif upper_x_bound * 0.7 < mouse_X < upper_x_bound * 0.8:
                x = x - 75
                self.annot.set_position((x, y))
            elif upper_x_bound * 0.6 < mouse_X < upper_x_bound * 0.7:
                x = x - 55
                self.annot.set_position((x, y))
            elif mouse_X < -1 * (upper_x_bound * 0.9):
                x = x + 10
                self.annot.set_position((x, y))

            self.annot.set_text(annotation_text)
            # create style
            self.annot.get_bbox_patch().set_facecolor("white")
            # self.annot.get_bbox_patch().set_alpha(1)

    def hover(self, event):
        """This method is for initializing hovering mouse event to show hole properties when mouse is hovered on
        Args:
             event: event of user
        Returns:
        """
        is_visible_annotation = self.annot.get_visible()
        scatter_to_contains_and_ind = {}
        if event.inaxes == self.axes:
            # fill the map: scatter_to_contains_and_ind
            for s in self._shape.sc:
                cont, ind = s.contains(event)
                scatter_to_contains_and_ind[s] = [cont, ind]

            # extract the scatter which contains hover event
            s_contains = None
            for s in scatter_to_contains_and_ind:
                cont = scatter_to_contains_and_ind[s][0]
                if not cont:
                    continue
                else:
                    s_contains = s
                    break

            # check hover on hole, if not annotation must be hidden
            if s_contains is None:
                if is_visible_annotation:
                    self.annot.set_visible(False)
                    self.figure.canvas.draw_idle()
                return
            ind = scatter_to_contains_and_ind[s_contains][1]
            # prevent infinite loop
            if is_visible_annotation:
                return
            else:
                hole = self.get_hole_by_xy(s_contains, ind)
                self.update_annot(hole, s_contains, ind)
                self.annot.set_visible(True)
                self.figure.canvas.draw_idle()

