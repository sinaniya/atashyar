from PyQt5.QtCore import pyqtProperty, QObject, pyqtSignal, pyqtSlot
import matplotlib
from mpldxf import FigureCanvasDxf

matplotlib.backend_bases.register_backend("dxf", FigureCanvasDxf)


class Plot(QObject):

    coordinates_changed = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.figure = None
        self.toolbar = None

        self._coordinates = ""
        self.cid = None
        self.axes = None

    def get_coordinates(self):
        return self._coordinates

    def set_coordinates(self, coordinates):
        self._coordinates = coordinates
        self.coordinates_changed.emit(self._coordinates)

    coordinates = pyqtProperty(
        str, get_coordinates, set_coordinates, notify=coordinates_changed
    )

    @pyqtSlot()
    def pan(self, *args):
        self.toolbar.pan(*args)

    @pyqtSlot()
    def zoom(self, *args):
        self.toolbar.zoom(*args)

    @pyqtSlot()
    def home(self, *args):
        self.toolbar.home(*args)

    @pyqtSlot()
    def export(self, *args):
        self.figure.savefig("result.png")
        self.figure.savefig("result.dxf")

    def on_motion(self, event):
        if event.inaxes == self.axes:
            self.coordinates = f"X: {event.xdata:.2f}, Y: {event.ydata:.2f}"
