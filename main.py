import sys

from PyQt5.QtCore import QCoreApplication, Qt, QUrl
from PyQt5.QtGui import QGuiApplication, QIcon
from PyQt5.QtQml import QQmlApplicationEngine, qmlRegisterType
from matplotlib_backend_qtquick.backend_qtquickagg import (
    FigureCanvasQtQuickAgg,
)

from resources.python.drawer import Drawer
from resources.python.db_utils.database_manager import DatabaseManager
from resources.python.utils.ExceptionHandler import ExceptionHandler
from resources.python.utils.Logger import logger

exceptionHandler = ExceptionHandler()
sys._excepthook = sys.excepthook
sys.excepthook = exceptionHandler.handler

if __name__ == "__main__":
    try:
        # app_file_log = open('app.log', 'a')
        # sys.stderr = app_file_log
        # sys.stdout = app_file_log
        sys.argv += ["--style", "material"]
        QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)
        app = QGuiApplication(sys.argv)
        app.setWindowIcon(QIcon("resources/icons/logo.png"))
        engine = QQmlApplicationEngine()
        context = engine.rootContext()

        database_manager = DatabaseManager()
        database_manager.connect_to_database()
        rocks_db = database_manager.rocks_db
        explosives_db = database_manager.explosives_db
        context.setContextProperty("rocksDB", rocks_db)
        context.setContextProperty("explosivesDB", explosives_db)

        drawer = Drawer()
        context.setContextProperty("drawer", drawer)
        qmlRegisterType(
            FigureCanvasQtQuickAgg, "Backend", 1, 0, "FigureCanvas"
        )
        engine.load(QUrl.fromLocalFile("main.qml"))
        if not engine.rootObjects():
            # app_file_log.close()
            sys.exit(-1)
        exceptionHandler.errorSignal.connect(
            exceptionHandler.error_handler_slot
        )
        sys.exit(app.exec_())
    except Exception as e:
        logger.exception(e)
        # app_file_log.close()

