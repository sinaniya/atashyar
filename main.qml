import QtQuick 2.15
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.15

import "resources/qml/components"

Window {
    id: root

    property real dp : Screen.pixelDensity * 10 * 2.54 / 160
    property alias addNewDialog: addNewDialog

    Material.theme: Material.Dark
    Material.accent: "#ffa500"

    visible: true
    visibility: Window.Maximized
    minimumHeight: Screen.desktopAvailableHeight
    minimumWidth: Screen.desktopAvailableWidth
    flags: Qt.Window | Qt.FramelessWindowHint

    title: qsTr("Atashyar")

    Rectangle {
        anchors.fill: parent
        color: "#1d1d2b"

        Rectangle {
            id: menuContainer
            height: parent.height
            width: 128*dp
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 32*dp
            color: "#1d1d2b"

            CustomMenuButton {
                id: menuButton
            }

            CustomMenu {
                id: mainMenu
                x: menuButton.x - 16*dp
                y: menuButton.y + 112*dp
            }

            Image {
                source: "resources/icons/logo.png"
                sourceSize.width: 128*dp
                sourceSize.height: 128*dp
                anchors.bottom: companyName.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: 16*dp
            }

            Text {
                id: companyName
                text: qsTr("Atashyar")
                color: "white"
                font.bold: true
                font.pixelSize: 20*dp
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: 64*dp
            }
        }

        Rectangle {
            height: parent.height
            width: parent.width - menuContainer.width - 64*dp
            anchors.left: menuContainer.right
            anchors.leftMargin: 32*dp
            anchors.top: parent.top
            color: "#33334c"

            Loader {
                id: stackLayoutLoader
                anchors.fill: parent
                source: "resources/qml/components/CustomStackLayout.qml"
            }

            PageIndicator {
                id: pageIndicator
                count: 3
                currentIndex: if (stackLayoutLoader.item) {stackLayoutLoader.item.currentIndex} else {0}
                anchors.top: stackLayoutLoader.top
                anchors.topMargin: 16*dp
                anchors.horizontalCenter: parent.horizontalCenter

                delegate: Rectangle {
                    id: delegateBG
                    implicitWidth: 8*dp
                    implicitHeight: 8*dp

                    radius: width / 2
                    color: "#ffa500"

                    opacity: model.index === pageIndicator.currentIndex ? 0.95: 0.45

                    Behavior on opacity {
                        OpacityAnimator {
                            duration: 100
                        }
                    }
                }
            }
        }

        Image {
            id: closeButton
            source: "resources/icons/close.svg"
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 8*dp

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    root.close()
                }
            }
        }

        ColorOverlay {
            anchors.fill: closeButton
            color: "orange"
            source: closeButton
        }

        Image {
            id: minimizeButton
            source: "resources/icons/minimize.svg"
            anchors.top: parent.top
            anchors.right: closeButton.left
            anchors.margins: 8*dp

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    root.showMinimized()
                }
            }
        }

        ColorOverlay {
            anchors.fill: minimizeButton
            source: minimizeButton
            color: "orange"
        }
    }

    AddNewDialog {
        id: addNewDialog
    }
}
